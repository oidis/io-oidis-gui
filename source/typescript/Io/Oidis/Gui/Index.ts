/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpRequestParserTest } from "@io-oidis-commons/Io/Oidis/Commons/RuntimeTests/HttpRequestParserTest.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { AsyncBaseHttpController } from "./HttpProcessor/Resolvers/AsyncBaseHttpController.js";
import { RuntimeTestRunner } from "./HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IndexPage } from "./Pages/IndexPage.js";
import { BaseViewer } from "./Primitives/BaseViewer.js";
import { ErrorPagesTest } from "./RuntimeTests/ErrorPagesTest.js";
import { ElementEventsManagerTest } from "./RuntimeTests/Events/ElementEventsManagerTest.js";
import { EventsManagerTest } from "./RuntimeTests/Events/EventsManagerTest.js";
import { GuiObjectManagerTest } from "./RuntimeTests/GuiObjectManagerTest.js";
import { ImageFiltersTest } from "./RuntimeTests/ImageProcessor/ImageFiltersTest.js";
import { ImageTransformTest } from "./RuntimeTests/ImageProcessor/ImageTransformTest.js";
import { PerformanceTest } from "./RuntimeTests/PerformanceTest.js";
import { AbstractGuiObjectTest } from "./RuntimeTests/Primitives/AbstractGuiObjectTest.js";
import { AsyncBasePanelViewerTest } from "./RuntimeTests/Primitives/AsyncBasePanelViewerTest.js";
import { BaseGuiObjectTest } from "./RuntimeTests/Primitives/BaseGuiObjectTest.js";
import { BasePanelTest } from "./RuntimeTests/Primitives/BasePanelTest.js";
import { BasePanelViewerTest } from "./RuntimeTests/Primitives/BasePanelViewerTest.js";
import { FormsObjectTest } from "./RuntimeTests/Primitives/FormsObjectTest.js";
import { GuiCommonsTest } from "./RuntimeTests/Primitives/GuiCommonsTest.js";
import { GuiElementTest } from "./RuntimeTests/Primitives/GuiElementTest.js";
import { PrimitivesTest } from "./RuntimeTests/Primitives/PrimitivesTest.js";
import { SerializationTest } from "./RuntimeTests/SerializationTest.js";
import { ElementManagerTest } from "./RuntimeTests/Utils/ElementManagerTest.js";
import { ScreenPositionTest } from "./RuntimeTests/Utils/ScreenPositionTest.js";
import { StaticPageContentManagerTest } from "./RuntimeTests/Utils/StaticPageContentManagerTest.js";
import { TextSelectionManagerTest } from "./RuntimeTests/Utils/TextSelectionManagerTest.js";
import { WindowManagerTest } from "./RuntimeTests/Utils/WindowManagerTest.js";

/**
 * Index request resolver class provides handling of web index page.
 */
export class Index extends AsyncBaseHttpController {

    constructor() {
        super();
        this.setInstanceOwner(new IndexPage());
    }

    protected async onSuccess($instance : IndexPage) : Promise<void> {
        $instance.headerTitle.Content("Oidis Framework base GUI Library");
        $instance.headerInfo.Content("Common and abstract classes or interfaces connected with creation and handling of GUI.");

        let content : string = "";
        /* dev:start */
        content += `
            <h3>Runtime tests</h3>

            <H4>Page content</H4>
            <a href="${WindowManagerTest.CreateLink()}">Window Manager</a>
            <a href="${StaticPageContentManagerTest.CreateLink()}">Static Page Content Manager</a>
            <a href="${EventsManagerTest.CreateLink()}">Global Events Factory</a>
            <a href="${ErrorPagesTest.CreateLink()}">Error Pages</a>

            <H4>GUI elements</H4>
            <a href="${GuiObjectManagerTest.CreateLink()}">GUI Object Manager</a>
            <a href="${ElementEventsManagerTest.CreateLink()}">Element Events Manager</a>
            <a href="${ElementManagerTest.CreateLink()}">Element Manager</a>
            <a href="${PrimitivesTest.CreateLink()}">GUI Primitives</a>
            <a href="${SerializationTest.CreateLink()}">GUI Primitives Serialization</a>
            <a href="${ScreenPositionTest.CallbackLink()}">Screen position test</a>
            <a href="${PerformanceTest.CallbackLink()}">Performance test</a>

            <H4>GUI utils</H4>
            <a href="${RuntimeTestRunner.CreateLink(HttpRequestParserTest)}">Http Request Parser Test</a>
            <a href="#${this.createLink("/web/PersistenceManagerTest")}">Persistence Manager Test</a>

            <H4>Draw GUI resolvers</H4>
            <a href="${BasePanelViewerTest.CallbackLink()}">Base Panel Viewer Test</a>
            <a href="#${this.createLink("/web/" + StringUtils.Replace(this.getClassName(), ".", "/"))}">Missing Viewer Test</a>
            <a href="${BaseViewer.CallbackLink()}">Viewer Bad Implementation Test</a>
            <a href="${AsyncBasePanelViewerTest.CallbackLink()}">Asynchronous Loader Test</a>
            <a href="${TextSelectionManagerTest.CallbackLink()}">TextSelectionManager test</a>

            <H4>Io.Oidis.Gui.ImageProcessor</H4>
            <a href="${ImageTransformTest.CallbackLink()}">ImageTransform test</a>
            <a href="${ImageFiltersTest.CallbackLink()}">ImageFilters test</a>

            <H4>Io.Oidis.Gui.Primitives</H4>
            <a href="${BaseGuiObjectTest.CallbackLink()}">BaseGuiObject test</a>
            <a href="${FormsObjectTest.CallbackLink()}">FormsObject test</a>
            <a href="${BasePanelTest.CallbackLink()}">BasePanel test</a>
            <a href="${AbstractGuiObjectTest.CallbackLink()}">AbstractGuiObject test</a>
            <a href="${GuiElementTest.CallbackLink()}">GuiElement test</a>
            <a href="${GuiCommonsTest.CallbackLink()}">GuiCommons test</a>
`;
        /* dev:end */

        $instance.pageIndex.Content(content);
        $instance.footerInfo.Content("" +
            "version: " + this.getEnvironmentArgs().getProjectVersion() +
            ", build: " + this.getEnvironmentArgs().getBuildTime() +
            ", " + this.getEnvironmentArgs().getProjectConfig().target.copyright);
    }
}
