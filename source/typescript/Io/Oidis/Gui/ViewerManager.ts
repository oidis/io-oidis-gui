/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GeneralEventOwner } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { TimeoutManager } from "@io-oidis-commons/Io/Oidis/Commons/Events/TimeoutManager.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { FatalError } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/FatalError.js";
import { ResolverFatalException } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/ResolverFatalException.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "./Enums/Events/EventType.js";
import { PanelContentType } from "./Enums/PanelContentType.js";
import { PersistenceType } from "./Enums/PersistenceType.js";
import { ViewerManagerEventArgs } from "./Events/Args/ViewerManagerEventArgs.js";
import { EventsManager } from "./Events/EventsManager.js";
import { GuiObjectManager } from "./GuiObjectManager.js";
import { IBaseViewer } from "./Interfaces/Primitives/IBaseViewer.js";
import { IGuiCommons } from "./Interfaces/Primitives/IGuiCommons.js";
import { BasePanel } from "./Primitives/BasePanel.js";
import { BasePanelViewer } from "./Primitives/BasePanelViewer.js";
import { BaseViewer } from "./Primitives/BaseViewer.js";
import { BaseViewerArgs } from "./Primitives/BaseViewerArgs.js";
import { ElementManager } from "./Utils/ElementManager.js";
import { ViewerCacheManager } from "./ViewerCacheManager.js";

/**
 * ViewerManager class provides resolving and preparation of GUI objects and panels views.
 */
export class ViewerManager extends BaseObject {
    private static loadedViewers : ArrayList<BaseViewer>;
    private readonly viewerName : string;
    private viewerInstance : BaseViewer;
    private readonly childElement : IGuiCommons;
    private viewerArgs : BaseViewerArgs;
    private reloadCache : boolean;
    private testMode : boolean;
    private includeChildren : boolean;
    private asyncCallback : boolean;
    private cacheFilePath : string;
    private id : string;
    private cache : ViewerCacheManager;

    /**
     * @param {string} $managerId Specify viewer manager id, which should be validated.
     * @returns {boolean} Returns true, if viewer's manager has been already successfully processed, otherwise false.
     */
    public static Exists($managerId : string) : boolean {
        return ObjectValidator.IsSet(ViewerManager.loadedViewers) && ViewerManager.loadedViewers.KeyExists($managerId);
    }

    /**
     * @returns {ArrayList<BaseViewer>} Returns list of successfully processed viewers.
     */
    public static getLoadedViewers() : ArrayList<BaseViewer> {
        if (!ObjectValidator.IsSet(ViewerManager.loadedViewers)) {
            ViewerManager.loadedViewers = new ArrayList<BaseViewer>();
        }
        return ViewerManager.loadedViewers;
    }

    /**
     * @param {string} $viewerName Viewer name, which should be processed.
     * @param {IGuiCommons} [$childElement] Specify required child, which should be looked up in viewer persistence.
     * @param {string} [$viewerArgs] Specify viewer args, which should be passed to desired viewer instance.
     */
    constructor($viewerName : string, $childElement? : IGuiCommons, $viewerArgs? : BaseViewerArgs) {
        super();

        this.viewerName = $viewerName;
        this.viewerInstance = null;
        this.childElement = $childElement;
        this.reloadCache = false;
        this.testMode = false;
        this.includeChildren = true;
        this.asyncCallback = false;
        this.viewerArgs = $viewerArgs;
        this.cacheFilePath = "";
    }

    /**
     * @param {boolean} [$value] Set cache reload status.
     * @returns {boolean} Returns true, if viewer cache can be reloaded, otherwise false.
     */
    public ReloadCacheEnabled($value? : boolean) : boolean {
        this.reloadCache = Property.Boolean(this.reloadCache, $value);
        return this.reloadCache;
    }

    /**
     * Clear cache associated with managed viewer instance
     * @param {boolean} [$force=true] Clear also gui commons and values persistence
     * @returns {void}
     */
    public ClearCache($force : boolean = true) : void {
        this.cache.Clear();
        if ($force) {
            PersistenceFactory.getPersistence(PersistenceType.GUI_COMMONS);
            PersistenceFactory.getPersistence(PersistenceType.AUTOFILL);
            PersistenceFactory.getPersistence(PersistenceType.ERROR_FLAGS);
            PersistenceFactory.getPersistence(PersistenceType.FORM_VALUES);
        }
        PersistenceFactory.DestroyAll();
        if (ViewerManager.loadedViewers.KeyExists(this.getId())) {
            ViewerManager.loadedViewers.RemoveAt(ViewerManager.loadedViewers.getKeys().indexOf(this.getId()));
        }
    }

    /**
     * @param {boolean} [$value] Set test mode status.
     * @returns {boolean} Returns true, if viewer should be prepared in test mode, otherwise false.
     */
    public TestModeEnabled($value? : boolean) : boolean {
        this.testMode = Property.Boolean(this.testMode, $value);
        return this.testMode;
    }

    /**
     * @param {boolean} [$value] Set child visibility status.
     * @returns {boolean} Returns true, if child viewers should be prepared and visible in output, otherwise false.
     */
    public IncludeChildren($value? : boolean) : boolean {
        this.includeChildren = Property.Boolean(this.includeChildren, $value);
        return this.includeChildren;
    }

    /**
     * @param {boolean} [$value] Set async callback flag.
     * @returns {boolean} Returns true, if viewer will be prepared as asynchronous callback, otherwise false.
     */
    public IsAsyncCallback($value ? : boolean) : boolean {
        this.asyncCallback = Property.Boolean(this.asyncCallback, $value);
        return this.asyncCallback;
    }

    /**
     * @param {string} $value Specify file path from, which should be cache loaded.
     * @returns {void}
     */
    public setCacheFilePath($value : string) : void {
        this.cacheFilePath = Property.String(this.cacheFilePath, $value);
    }

    /**
     * @returns {void}
     */
    public Process() : void {
        if (!ObjectValidator.IsSet(ViewerManager.loadedViewers)) {
            ViewerManager.loadedViewers = new ArrayList<BaseViewer>();
        }
        const events : EventsManager = <EventsManager>EventsManager.getInstanceSingleton();
        const childListIds : ArrayList<string> = new ArrayList<string>();
        const childIdPath : ArrayList<string> = new ArrayList<string>();

        const fireManagerIsComplete : any = ($viewer : BaseViewer, $eventArgs : ViewerManagerEventArgs) : void => {
            if (childListIds.IsEmpty()) {
                this.viewerInstance = $viewer;
                $eventArgs.Result($viewer);
                if (!$eventArgs.AsyncCallback()) {
                    if (!ViewerManager.loadedViewers.KeyExists(this.getId())) {
                        ViewerManager.loadedViewers.Add($viewer, this.getId());
                    }
                }
                events.setEvent(GeneralEventOwner.BODY, EventType.ON_LOAD, () : void => {
                    const elements : ArrayList<IGuiCommons> = GuiObjectManager.getInstanceSingleton().getAll();
                    elements.foreach(($element : IGuiCommons) : void => {
                        if (!$element.IsCached() && !$element.IsLoaded() &&
                            ObjectValidator.IsEmptyOrNull($element.Parent()) &&
                            ElementManager.Exists($element.Id())) {
                            if (ObjectValidator.IsEmptyOrNull($element.InstanceOwner())) {
                                $element.InstanceOwner(this.viewerInstance);
                            }
                            if ($element.Visible()) {
                                if ((<any>$element).hasAsynchronousDraw()) {
                                    $element.Visible(true);
                                } else {
                                    events.FireEvent($element, EventType.ON_START);
                                    if (ElementManager.IsVisible($element.Id())) {
                                        events.FireEvent($element, EventType.BEFORE_LOAD, false);
                                    }
                                }
                            }
                        }
                    });
                });
                events.FireEvent(ViewerManager.ClassName(), EventType.ON_COMPLETE, $eventArgs);
            }
        };

        const prepareViewerInstance : any = ($eventArgs : ViewerManagerEventArgs,
                                             $manager : GuiObjectManager,
                                             $reflection : Reflection) : void => {
            if (!ObjectValidator.IsEmptyOrNull($eventArgs.Result())) {
                const loaded : boolean = ViewerManager.loadedViewers.KeyExists(this.getId());
                let viewer : BaseViewer = <BaseViewer>$eventArgs.Result();
                viewer.TestModeEnabled($eventArgs.TestMode());
                let panelViewer : BasePanelViewer;
                if ($reflection.IsMemberOf(viewer, BasePanelViewer)) {
                    panelViewer = <BasePanelViewer>viewer;
                    const registerChildId : any = ($instance : BasePanel) : void => {
                        $instance.getChildPanelList().foreach(
                            ($childViewer : BasePanelViewer) : void => {
                                childListIds.Add($childViewer.getInstance().Id());
                                registerChildId($childViewer.getInstance());
                            });
                    };
                    if (!loaded) {
                        registerChildId(panelViewer.getInstance());
                    }

                    if (!$eventArgs.IncludeChildren()) {
                        panelViewer.getInstance().getChildPanelList().foreach(
                            ($childViewer : BasePanelViewer) : void => {
                                $childViewer.getInstance().ContentType(PanelContentType.HIDDEN);
                            });
                    } else if (!ObjectValidator.IsEmptyOrNull($eventArgs.ChildElement())) {
                        if ($manager.Exists($eventArgs.ChildElement())) {
                            let childList : ArrayList<any> = panelViewer.getInstance().getChildPanelList();
                            let childIndex : number;
                            for (childIndex = childIdPath.Length() - 2; childIndex >= 0; childIndex--) {
                                const childId : string = childIdPath.getItem(childIndex);
                                if (childList.KeyExists(childId)) {
                                    panelViewer = childList.getItem(childId);
                                    childList = panelViewer.getInstance().getChildPanelList();
                                }
                            }
                        }
                    }

                    if ($eventArgs.AsyncCallback()) {
                        panelViewer.getInstance().ContentType(PanelContentType.WITHOUT_ELEMENT_WRAPPER);
                    } else {
                        panelViewer.getInstance().ContentType(PanelContentType.WITH_ELEMENT_WRAPPER);
                    }
                    viewer = <BaseViewer>panelViewer;
                }
                viewer.ViewerArgs($eventArgs.ViewerArgs());

                if ((!viewer.IsCached() && !$eventArgs.AsyncCallback() && (!loaded || $eventArgs.TestMode())) ||
                    (viewer.IsCached() && !$eventArgs.AsyncCallback() &&
                        !loaded && ObjectValidator.IsEmptyOrNull($eventArgs.ViewerArgs()))) {
                    viewer.PrepareImplementation();
                }

                if (!ObjectValidator.IsEmptyOrNull($eventArgs.ChildElement())) {
                    const instance : any = viewer.getInstance();
                    if (!ObjectValidator.IsEmptyOrNull(instance) &&
                        ObjectValidator.IsFunction(instance.PanelViewer)) {
                        panelViewer = instance.PanelViewer();
                        if (!ObjectValidator.IsEmptyOrNull(panelViewer)) {
                            if ($eventArgs.AsyncCallback()) {
                                panelViewer.getInstance().ContentType(PanelContentType.WITHOUT_ELEMENT_WRAPPER);
                            }
                            panelViewer.PrepareImplementation();
                            viewer = panelViewer;
                            if (viewer.getInstance() !== $eventArgs.ChildElement()) {
                                viewer = null;
                            }
                        }
                    }
                    fireManagerIsComplete(viewer, $eventArgs, $manager);
                } else if ($reflection.IsMemberOf(viewer, BasePanelViewer) && !loaded) {
                    const asyncChildLoader : TimeoutManager = new TimeoutManager();
                    const prepareChildPanels : any = ($viewer : BasePanelViewer) : void => {
                        $viewer.getInstance().getChildPanelList().foreach(
                            ($childViewer : BasePanelViewer) : void => {
                                if (!ObjectValidator.IsEmptyOrNull($childViewer)) {
                                    asyncChildLoader.Add(() : void => {
                                        if (!$viewer.IsCached()) {
                                            events.FireEvent(ViewerManager.ClassName(), EventType.ON_CHANGE, false);
                                            $childViewer.PrepareImplementation();
                                        }

                                        childListIds.RemoveAt(childListIds.IndexOf($childViewer.getInstance().Id()));
                                        if (childListIds.IsEmpty()) {
                                            fireManagerIsComplete(viewer, $eventArgs, $manager);
                                        }
                                    });
                                    prepareChildPanels($childViewer);
                                }
                            });
                    };
                    if (!(<BasePanelViewer>viewer).getInstance().getChildPanelList().IsEmpty()) {
                        prepareChildPanels(<BasePanelViewer>viewer);
                        asyncChildLoader.Execute();
                    } else {
                        fireManagerIsComplete(viewer, $eventArgs, $manager);
                    }
                } else if ($reflection.IsMemberOf(viewer, BaseViewer) && !loaded && viewer.IsCached()) {
                    if (!ObjectValidator.IsEmptyOrNull($eventArgs.ViewerArgs())) {
                        viewer.PrepareImplementation();
                    }
                    fireManagerIsComplete(viewer, $eventArgs, $manager);
                } else {
                    fireManagerIsComplete(viewer, $eventArgs, $manager);
                }
            } else {
                events.FireEvent(ViewerManager.ClassName(), EventType.ON_COMPLETE, $eventArgs);
            }
        };

        const reflection : Reflection = Reflection.getInstance();
        const eventArgs : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        eventArgs.ViewerClass(reflection.getClass(this.viewerName));
        if (this.testMode && !this.asyncCallback &&
            ObjectValidator.IsEmptyOrNull(this.viewerArgs) &&
            !ObjectValidator.IsEmptyOrNull(eventArgs.ViewerClass())) {
            this.viewerArgs = eventArgs.ViewerClass().getTestViewerArgs();
        }
        eventArgs.ViewerArgs(this.viewerArgs);
        eventArgs.ChildElement(this.childElement);
        eventArgs.TestMode(this.testMode);
        eventArgs.AsyncCallback(this.asyncCallback);
        eventArgs.ReloadCache(this.reloadCache);

        events.FireEvent(ViewerManager.ClassName(), EventType.ON_START, eventArgs, false);

        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        if (!ObjectValidator.IsEmptyOrNull(eventArgs.ViewerClass()) &&
            ObjectValidator.IsSet(eventArgs.ViewerClass().CallbackLink) &&
            ObjectValidator.IsSet(eventArgs.ViewerClass().getTestViewerArgs)) {
            if (eventArgs.AsyncCallback() && !ObjectValidator.IsEmptyOrNull(eventArgs.ChildElement())) {
                if (manager.Exists(eventArgs.ChildElement())) {
                    childIdPath.Add(eventArgs.ChildElement().Id());
                    const collectChildIds : any = ($parent : IGuiCommons) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($parent)) {
                            childIdPath.Add($parent.Id());
                            collectChildIds($parent.Parent());
                        }
                    };
                    collectChildIds(eventArgs.ChildElement().Parent());
                    const parentId : string = childIdPath.getLast();
                    ViewerManager.loadedViewers.foreach(($viewer : BaseViewer, $key? : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($viewer.getInstance())) {
                            if ($viewer.getInstance().Id() === parentId) {
                                this.id = $key;
                            }
                        }
                    });
                }
            }
            this.cache = new ViewerCacheManager(this.getId(), this.cacheFilePath);
            if (!eventArgs.AsyncCallback() && eventArgs.ReloadCache()) {
                this.ClearCache();
            }
            this.cache.Load(() : void => {
                if (!eventArgs.AsyncCallback()) {
                    if (!ViewerManager.loadedViewers.KeyExists(this.getId())) {
                        eventArgs.Result(this.cache);
                        events.FireEvent(ViewerManager.ClassName(), EventType.ON_COMPLETE, eventArgs);
                        this.cache.Process(($viewer : BaseViewer) : void => {
                            eventArgs.Result($viewer);
                            events.setEvent(ViewerManager.ClassName(), EventType.ON_COMPLETE,
                                ($eventArgs : ViewerManagerEventArgs) : void => {
                                    try {
                                        if (!$eventArgs.AsyncCallback() &&
                                            !ObjectValidator.IsEmptyOrNull($eventArgs.Result()) &&
                                            (<BaseViewer>$eventArgs.Result()).IsMemberOf(BaseViewer) &&
                                            (<BaseViewer>$eventArgs.Result()).IsCached()) {
                                            const element : IGuiCommons = (<BaseViewer>$eventArgs.Result()).getInstance();
                                            if (!ObjectValidator.IsEmptyOrNull(element) &&
                                                !element.IsPrepared() &&
                                                element.IsLoaded()) {
                                                (<any>element).innerCode();
                                                if (element.Visible() && ElementManager.IsVisible(element.Id())) {
                                                    (<any>element).completed = true;
                                                    events.FireEvent(element, EventType.ON_COMPLETE);
                                                }
                                            }
                                        }
                                    } catch (ex) {
                                        try {
                                            ExceptionsManager.Throw(ViewerManager.ClassName(), ex);
                                        } catch (ex) {
                                            ExceptionsManager.Throw(ResolverFatalException.ClassName(),
                                                new ResolverFatalException(ViewerManager.ClassName() + " OnComplete fatal error."));
                                        }
                                    }
                                });
                            try {
                                prepareViewerInstance(eventArgs, manager, reflection);
                            } catch (ex) {
                                try {
                                    ExceptionsManager.Throw(ViewerManager.ClassName(), ex);
                                } catch (ex) {
                                    ExceptionsManager.Throw(FatalError.ClassName(),
                                        new FatalError(ViewerManager.ClassName() + " Viewer preparation fatal error."));
                                }
                            }
                        });
                    } else {
                        fireManagerIsComplete(ViewerManager.loadedViewers.getItem(this.getId()), eventArgs, manager);
                    }
                } else {
                    eventArgs.Result(ViewerManager.loadedViewers.getItem(this.getId()));
                    prepareViewerInstance(eventArgs, manager, reflection);
                }
            }, () : void => {
                let childLookUp : string = "";
                if (!ObjectValidator.IsEmptyOrNull(eventArgs.ChildElement())) {
                    childLookUp = " and lookup child \"" + eventArgs.ChildElement().Id() + "\"";
                }
                if (ViewerManager.loadedViewers.KeyExists(this.getId())) {
                    LogIt.Debug("Get viewer \"" + this.viewerName + "\"" + childLookUp);
                    eventArgs.Result(ViewerManager.loadedViewers.getItem(this.getId()));
                    prepareViewerInstance(eventArgs, manager, reflection);
                } else {
                    LogIt.Debug("Create viewer \"" + this.viewerName + "\"" + childLookUp);
                    const viewerClass : any = eventArgs.ViewerClass();
                    const viewer : BaseViewer = new viewerClass(eventArgs.ViewerArgs());
                    if (reflection.Implements(viewer, IBaseViewer)) {
                        (<any>viewer).asyncLoad(($viewer : BaseViewer) : void => {
                            eventArgs.Result($viewer);
                            prepareViewerInstance(eventArgs, manager, reflection);
                        });
                    } else {
                        eventArgs.Result(null);
                        events.FireEvent(ViewerManager.ClassName(), EventType.ON_COMPLETE, eventArgs);
                    }
                }
            });
        } else {
            events.FireEvent(ViewerManager.ClassName(), EventType.ON_COMPLETE, eventArgs);
        }
    }

    /**
     * @returns {string} Returns manager's hash based on desired settings.
     */
    public getId() : string {
        if (!ObjectValidator.IsSet(this.id)) {
            this.id = this.viewerName;
            if (!ObjectValidator.IsEmptyOrNull(this.cacheFilePath)) {
                this.id = this.cacheFilePath;
            }
            if (this.testMode) {
                this.id += "T";
            }
            this.id = StringUtils.getSha1(this.id);
        }
        return this.id;
    }

    /**
     * Create cache from viewer handled by the manager.
     * @param {Callback} [$handler] Handler, which should be called after successfully created cache.
     * @returns {void}
     */
    public getCache($handler? : ($eventsArgs : ViewerManagerEventArgs) => void) : void {
        this.cache.setViewerInstance(this.viewerInstance);
        this.cache.Create(() : void => {
            const eventsArgs : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            eventsArgs.ViewerClass(Reflection.getInstance().getClass(this.viewerName));
            eventsArgs.ViewerArgs(this.viewerArgs);
            eventsArgs.ChildElement(this.childElement);
            eventsArgs.TestMode(this.testMode);
            eventsArgs.AsyncCallback(this.asyncCallback);
            eventsArgs.ReloadCache(this.reloadCache);
            if (ObjectValidator.IsSet($handler)) {
                $handler(eventsArgs);
            }
        });
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        return "Viewer id: " + this.getId() + StringUtils.NewLine($htmlTag) +
            "Viewer instance: " + this.viewerInstance.ToString() + StringUtils.NewLine($htmlTag) +
            this.cache.ToString($prefix, $htmlTag);
    }

    /**
     * Provides raw cache data in format specified by the cache storage.
     * It can also provides saving of the cache for ability to read it's raw data.
     * @param {Callback} $handler Handler, which should be called when the cache data are ready.
     * @returns {void}
     */
    public getCacheRawData($handler? : ($data : string) => void) : void {
        this.cache.getRawData($handler);
    }
}
