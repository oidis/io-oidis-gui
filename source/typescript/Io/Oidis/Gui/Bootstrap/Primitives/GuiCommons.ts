/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EnvironmentArgs } from "@io-oidis-commons/Io/Oidis/Commons/EnvironmentArgs.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "../../Enums/Events/EventType.js";
import { ElementEventsManager } from "../../Events/ElementEventsManager.js";
import { EventsManager } from "../../Events/EventsManager.js";
import { HttpManager } from "../../HttpProcessor/HttpManager.js";
import { Loader } from "../../Loader.js";
import { ElementManager } from "../../Utils/ElementManager.js";
import { bootstrap } from "../../Utils/EnvironmentHelper.js";

export class GuiCommons extends BaseObject {
    private readonly id : string;
    private instance : HTMLElement;
    private readonly children : any;
    private parent : GuiCommons;
    private events : ElementEventsManager;
    private loaded : boolean;
    private registered : boolean;
    private completed : boolean;
    private isWrapper : boolean;
    private visible : boolean;
    private enabled : boolean;
    private active : boolean;
    private data : any;
    private readonly environment : EnvironmentArgs;
    private readonly httpManager : HttpManager;
    private lazyLoad : boolean;

    public static getRenderTarget() : HTMLElement {
        const element : HTMLElement = ElementManager.getElement("PageContentHolder", true);
        if (!ObjectValidator.IsEmptyOrNull(element)) {
            this.getRenderTarget = () : HTMLElement => {
                return element;
            };
        }
        return element;
    }

    constructor() {
        super();
        this.id = this.getUID();
        this.instance = null;
        this.children = {};
        this.parent = null;
        this.loaded = false;
        this.registered = false;
        this.completed = false;
        this.isWrapper = false;
        this.visible = true;
        this.enabled = true;
        this.active = false;
        this.data = null;
        this.environment = Loader.getInstance().getEnvironmentArgs();
        this.httpManager = Loader.getInstance().getHttpManager();
        this.lazyLoad = false;
    }

    public Id() : string {
        return this.id;
    }

    public WithLazyLoad($value? : boolean) : boolean {
        return this.lazyLoad = Property.Boolean(this.lazyLoad, $value);
    }

    public Content($value : string) : void {
        ElementManager.setInnerHtml(this.instance, $value);
    }

    public Clear() : void {
        this.Content("");
        if (this.completed) {
            for (const item of Object.keys(this.children)) {
                delete this[item];
                delete this.children[item];
            }
        }
    }

    public InstanceOwner($value? : any) : any {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.instance = $value;
            if (ObjectValidator.IsString(this.instance)) {
                const element : HTMLElement = ElementManager.getElement(this.instance);
                if (!ObjectValidator.IsEmptyOrNull(element)) {
                    this.instance = element;
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(this.instance) && !ObjectValidator.IsEmptyOrNull(this.instance.dataset)) {
                delete this.instance.dataset.oidisBind;
            }
        }
        return this.instance;
    }

    public Parent($value? : GuiCommons) : GuiCommons {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.parent = $value;
        }
        return this.parent;
    }

    public Data($value? : any) : any {
        if (ObjectValidator.IsSet($value)) {
            this.data = $value;
        }
        return this.data;
    }

    public toString() : string {
        return this.Id();
    }

    public getEvents() : ElementEventsManager {
        this.events = new ElementEventsManager(<any>this);
        if (this.completed) {
            this.registered = true;
            this.events.Subscribe(this.InstanceOwner());
        }
        this.getEvents = () : ElementEventsManager => {
            return this.events;
        };
        return this.events;
    }

    public Register() : void {
        for (const name in this.children) {
            if (this.children.hasOwnProperty(name)) {
                const component : GuiCommons = <GuiCommons>this[this.children[name]];
                if (ObjectValidator.IsObject(component)) {
                    component.Register();
                } else {
                    LogIt.Warning("{0} was declared but instance is missing. Did you miss it in constructor?",
                        this.getClassName() + "." + this.children[name]);
                }
            }
        }
        if (ObjectValidator.IsString(this.instance)) {
            this.InstanceOwner(ElementManager.getElement(this.instance));
            this.syncInputDOM(this.instance);
        }
        if (!ObjectValidator.IsEmptyOrNull(this.events) && !this.registered) {
            this.registered = true;
            this.events.Subscribe(this.InstanceOwner());
        }
        if (this.loaded && !this.completed) {
            this.completed = true;
            EventsManager.getInstanceSingleton().FireEvent(<any>this, EventType.ON_COMPLETE);
        }
    }

    public Enabled($value? : boolean) : boolean {
        this.enabled = Property.Boolean(this.enabled, $value);
        if (ObjectValidator.IsSet($value)) {
            this.setAttribute(!this.enabled, "disabled");
        }
        return this.enabled;
    }

    public Active($value? : boolean) : boolean {
        this.active = Property.Boolean(this.active, $value);
        if (ObjectValidator.IsSet($value)) {
            this.setAttribute(this.active, "active");
        }
        return this.active;
    }

    public Visible($value? : boolean) : boolean {
        this.visible = Property.Boolean(this.visible, $value);
        if (ObjectValidator.IsSet($value)) {
            this.setAttribute(!this.visible, "invisible");
            this.setAttribute(!this.visible, "d-none");
        }
        return this.visible;
    }

    public Render() : void {
        let element : HTMLElement = this.getDOM();
        if (!ObjectValidator.IsEmptyOrNull(element.innerHTML) && !this.isWrapper) {
            let parent : HTMLElement;
            if (ObjectValidator.IsEmptyOrNull(this.Parent())) {
                parent = GuiCommons.getRenderTarget();
            } else if (this.IsMemberOf("Io.Oidis.Gui.Bootstrap.Primitives.BasePanel")) {
                this.Clear();
                parent = this.InstanceOwner();
            } else {
                if (ObjectValidator.IsEmptyOrNull(this.Parent().InstanceOwner())) {
                    this.Parent().InstanceOwner(this.Parent().getDOM());
                }
                parent = this.Parent().InstanceOwner();
            }
            if (!ObjectValidator.IsEmptyOrNull(parent)) {
                if ((<any>element).isTemplate) {
                    while (<any>element.firstChild) {
                        parent.appendChild(<any>element.firstChild);
                    }
                    element = parent;
                    if (!ObjectValidator.IsEmptyOrNull(element)) {
                        this.getDOM = () : HTMLElement => {
                            return element;
                        };
                    }
                } else {
                    parent.appendChild(element);
                }
            } else {
                LogIt.Warning("Parent element has not been found for: {0}", this.getClassName());
            }
        }

        for (const name in this.children) {
            if (this.children.hasOwnProperty(name)) {
                const component : GuiCommons = <GuiCommons>this[this.children[name]];
                component.Parent(this);
                if (ObjectValidator.IsObject(component) && !component.WithLazyLoad()) {
                    component.Render();
                }
            }
        }
        this.syncInputDOM(this.instance);
        this.InstanceOwner(this.instance);
        if (ObjectValidator.IsObject(this.instance) && !this.loaded) {
            this.loaded = true;
            EventsManager.getInstanceSingleton().FireEvent(<any>this, EventType.ON_LOAD);
        }
        this.Register();
    }

    public AddChild($element : GuiCommons) : void {
        const isPanel : boolean = Reflection.getInstance().IsMemberOf($element, "Io.Oidis.Gui.Bootstrap.Primitives.BasePanel");
        this.children[$element.Id()] = $element.Id();
        this[$element.Id()] = $element;
        if (!isPanel) {
            $element.Parent(this);
        } else {
            $element.parent = null;
        }
        const dom : HTMLElement = !ObjectValidator.IsObject(this.InstanceOwner()) ? this.getDOM() : this.InstanceOwner();
        let owner : HTMLElement = dom;
        if ((<any>owner).isTemplate) {
            if (owner.tagName.toUpperCase() === "TABLE" && owner.childElementCount === 1) {
                owner = <any>owner.firstChild;
            }
        }
        const childDom : HTMLElement = $element.getDOM();
        if (isPanel) {
            const container : HTMLElement = document.createElement("div");
            if (!$element.Visible()) {
                container.classList.add("d-none");
            }
            owner.appendChild(container);
            $element.InstanceOwner(container);
            $element.Parent(this);
            $element.WithLazyLoad(false);
            $element.Render();
        } else {
            if (!ObjectValidator.IsEmptyOrNull(childDom.innerHTML)) {
                if ((<any>childDom).isTemplate) {
                    let firstChild : any = null;
                    while (<any>childDom.firstChild) {
                        if (firstChild === null) {
                            firstChild = childDom.firstChild;
                        }
                        owner.appendChild(<any>childDom.firstChild);
                    }
                    if (firstChild !== null && ObjectValidator.IsEmptyOrNull($element.InstanceOwner())) {
                        this.syncInputDOM(firstChild);
                        $element.InstanceOwner(firstChild);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(dom)) {
                        $element.getDOM = () : HTMLElement => {
                            return dom;
                        };
                    }
                } else {
                    owner.appendChild(childDom);
                }
            }
            if (ObjectValidator.IsObject($element.InstanceOwner()) && !$element.loaded) {
                $element.loaded = true;
                EventsManager.getInstanceSingleton().FireEvent(<any>$element, EventType.ON_LOAD);
            }
            this.Register();
        }
    }

    public SwapChild($old : GuiCommons, $new : GuiCommons) : void {
        if ($old.Parent() === this) {
            this.children[$new.Id()] = this.children[$old.Id()];
            delete this.children[$old.Id()];
            this[this.children[$new.Id()]] = $new;
            $new.Parent(this);
            const dom : HTMLElement = $old.InstanceOwner();
            const childDom : HTMLElement = $new.getDOM();
            let swapped : boolean = false;
            let owner : any = null;
            try {
                if ((<any>childDom).isTemplate) {
                    owner = childDom.firstChild;
                    while (<any>childDom.lastChild) {
                        dom.parentNode.insertBefore(childDom.lastChild, dom);
                    }
                } else {
                    owner = childDom;
                    dom.parentNode.insertBefore(childDom, dom);
                }
                swapped = true;
            } catch (ex) {
                LogIt.Error("SwapChild failed", ex);
            }
            if (swapped) {
                dom.parentNode.removeChild(dom);
                if (owner !== null && ObjectValidator.IsEmptyOrNull($new.InstanceOwner())) {
                    this.syncInputDOM(owner);
                    $new.InstanceOwner(owner);
                }
                if (ObjectValidator.IsObject($new.InstanceOwner()) && !$new.loaded) {
                    $new.loaded = true;
                    EventsManager.getInstanceSingleton().FireEvent(<any>$new, EventType.ON_LOAD);
                }
                this.Register();
            }
        }
    }

    public getChildList() : GuiCommons[] {
        return Object.keys(this.children).map(($item) => this[this.children[$item]]);
    }

    public AssignLabel($value : GuiCommons) : void {
        if (!this.registerLabel($value.InstanceOwner())) {
            LogIt.Warning("Failed to assign Label for " + this.Id() + " element: target must be valid HTML <label> element");
        }
    }

    protected innerCode() : string {
        return "";
    }

    protected innerHtml() : string {
        return "";
    }

    protected setAttribute($value : boolean, $keyword : string) : void {
        if (ObjectValidator.IsObject(this.instance)) {
            try {
                if ($value) {
                    this.instance.classList.add($keyword);
                } else {
                    this.instance.classList.remove($keyword);
                }
            } catch (ex) {
                LogIt.Warning("ClassList modification failure for " + this.Id() + ". Fallback to ElementManager.");
                const currentClass : string = this.instance.className;
                if (!ObjectValidator.IsEmptyOrNull(currentClass) && currentClass !== $keyword) {
                    $keyword = " " + $keyword;
                }
                if ($value) {
                    if (!StringUtils.Contains(currentClass, $keyword)) {
                        ElementManager.setClassName(this.instance, currentClass + $keyword);
                    }
                } else {
                    ElementManager.setClassName(this.instance, StringUtils.Remove(currentClass, $keyword));
                }
            }
        }
    }

    protected getContent() : any {
        const code : string = this.innerCode();
        const html : string = this.innerHtml();
        const content : any = {
            code,
            html,
            toString: () : string => {
                return code + html;
            }
        };
        this.getContent = () : any => {
            return content;
        };
        return content;
    }

    protected getDOMTemplateType() : string {
        const content : string = this.getContent().html.trim();
        if (StringUtils.StartsWith(content, "<tr")) {
            return "tbody";
        } else if (StringUtils.StartsWith(content, "<tbody")) {
            return "table";
        }
        return "span";
    }

    protected getDOM() : HTMLElement {
        const dom : HTMLElement = document.createElement(this.getDOMTemplateType());
        (<any>dom).isTemplate = true;
        const content : string = this.getContent().toString();
        if (!ObjectValidator.IsEmptyOrNull(content)) {
            if (this.IsMemberOf("Io.Oidis.Gui.Bootstrap.Primitives.BasePanel") && ObjectValidator.IsEmptyOrNull(this.Parent())) {
                dom.insertAdjacentHTML("beforeend", `
                <section data-oidis-bind="${this}" class="${(StringUtils.Remove(this.getNamespaceName(), "."))}">
                <section class="${(StringUtils.Remove(this.getClassNameWithoutNamespace(), "."))}">
                    ${content}
                </section>
                </section>`);
            } else {
                dom.insertAdjacentHTML("beforeend", content);
            }

            const reflection : Reflection = Reflection.getInstance();
            this.getProperties().forEach(($name : string) : void => {
                if (reflection.IsMemberOf(this[$name], GuiCommons) && $name !== "parent") {
                    this.children[this[$name].Id()] = $name;
                }
            });
            const elements : HTMLElement[] = <any>dom.querySelectorAll("[data-oidis-bind]");
            for (const element of elements) {
                const id : string = element.dataset.oidisBind;
                if (id === "undefined") {
                    LogIt.Warning(this.getClassName() + " contains binding for uninitialized components.");
                } else if (id === this.Id()) {
                    this.InstanceOwner(element);
                } else if (!ObjectValidator.IsEmptyOrNull(this.children)) {
                    const name : string = this.children[id];
                    if (!ObjectValidator.IsEmptyOrNull(name)) {
                        const component : GuiCommons = this[name];
                        if (!ObjectValidator.IsEmptyOrNull(component)) {
                            let generate : boolean = ObjectValidator.IsEmptyOrNull(element.innerHTML);
                            if (element instanceof HTMLInputElement) {
                                generate = false;
                            }
                            if (!ObjectValidator.IsEmptyOrNull(component.Parent()) && component.Parent().isWrapper || this.isWrapper) {
                                generate = false;
                            }
                            if (generate) {
                                const componentDom : HTMLElement = component.getDOM();
                                if (!ObjectValidator.IsEmptyOrNull(componentDom.innerHTML)) {
                                    if ((<any>componentDom).isTemplate) {
                                        while (<any>componentDom.firstChild) {
                                            element.appendChild(<any>componentDom.firstChild);
                                        }
                                    }
                                }
                            } else {
                                component.isWrapper = true;
                            }
                            component.syncInputDOM(element);
                            component.InstanceOwner(element);
                            component.Parent(this);
                        }
                    }
                }
            }

            (<any>dom.querySelectorAll("[data-bs-toggle=\"tooltip\"]")).forEach(($element : HTMLElement) : void => {
                new bootstrap.Tooltip($element);
            });
        }

        if (!ObjectValidator.IsEmptyOrNull(dom)) {
            this.getDOM = () : HTMLElement => {
                return dom;
            };
        }
        return dom;
    }

    protected getInputDOM() : HTMLInputElement {
        const dom : HTMLInputElement = document.createElement("input");
        this.getInputDOM = () : HTMLInputElement => {
            return dom;
        };
        return dom;
    }

    protected registerLabel($target : Element) : boolean {
        if (!ObjectValidator.IsEmptyOrNull($target) && $target.nodeName.toLowerCase() === "label") {
            this.getInputDOM().id = this.getClassNameWithoutNamespace() + "_" + this.Id();
            $target.setAttribute("for", this.getInputDOM().id);
            return true;
        }
        return false;
    }

    protected getHttpManager() : HttpManager {
        return this.httpManager;
    }

    protected getEnvironmentArgs() : EnvironmentArgs {
        return this.environment;
    }

    private syncInputDOM($target : any) : void {
        if (!ObjectValidator.IsEmptyOrNull($target) && ObjectValidator.IsSet($target.value)) {
            if ($target.value !== this.getInputDOM().value) {
                $target.value = this.getInputDOM().value;
            }
            if (this.getInputDOM().nodeName.toLowerCase() === "input") {
                this.registerLabel(this.getInputDOM().nextElementSibling);
            }
            this.getInputDOM = () : HTMLInputElement => {
                return $target;
            };
        }
    }
}

export class AbstractGuiObject extends GuiCommons {
    private readonly text : string;

    constructor($text : string) {
        super();
        this.text = $text;
    }

    protected innerHtml() : string {
        return `<button data-oidis-bind="${this}" class="btn btn-primary d-block" type="button" style="margin-bottom: 10px;">${this.text}</button>`;
    }
}
