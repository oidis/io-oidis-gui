/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseViewerLayerType } from "../../Enums/BaseViewerLayerType.js";
import { CommonGuiViewer } from "../../Primitives/BaseViewer.js";
import { BaseViewerArgs } from "../../Primitives/BaseViewerArgs.js";
import { ElementManager } from "../../Utils/ElementManager.js";
import { bootstrap } from "../../Utils/EnvironmentHelper.js";
import { AbstractGuiObject, GuiCommons } from "./GuiCommons.js";

export class BaseViewer extends CommonGuiViewer {
    private argsHash : number;

    public static getTestRenderTarget() : HTMLElement {
        const element : HTMLElement = ElementManager.getElement("TestContentSideBar");
        if (!ObjectValidator.IsEmptyOrNull(element)) {
            this.getTestRenderTarget = () : HTMLElement => {
                return element;
            };
        }
        return element;
    }

    constructor($args? : BaseViewerArgs) {
        super($args);
        this.argsHash = null;
    }

    public ViewerArgs($args? : BaseViewerArgs) : BaseViewerArgs {
        if (!ObjectValidator.IsEmptyOrNull($args) && !ObjectValidator.IsString($args)) {
            super.ViewerArgs($args);
            const instance : GuiCommons = this.getInstance();
            if (!ObjectValidator.IsEmptyOrNull(instance)) {
                const newHash : number = $args.getHash();
                if (this.argsHash !== newHash) {
                    this.argsHash = newHash;
                    this.PrepareImplementation();
                }
            }
        }
        return super.ViewerArgs($args);
    }

    public PrepareImplementation() : void {
        const args : BaseViewerArgs = super.ViewerArgs();
        const instance : GuiCommons = this.getInstance();
        const testButtons : ArrayList<AbstractGuiObject> = this.getTestButtons();
        if (!ObjectValidator.IsEmptyOrNull(instance)) {
            if (!ObjectValidator.IsEmptyOrNull(args)) {
                if (!args.Visible()) {
                    instance.Visible(false);
                }
                this.argsHandler(instance, args);
            }

            this.setLayer(BaseViewerLayerType.DEFAULT);
            this.beforeLoad(instance, args);
            instance.getEvents().setOnComplete(() : void => {
                this.afterLoad(instance, args);
            });
        }

        if (this.TestModeEnabled()) {
            testButtons.Clear();
            if (!ObjectValidator.IsEmptyOrNull(this.getTestSubscriber())) {
                const testProcessor : any = new (this.getTestSubscriber())();
                testProcessor.setOwner(this);
                this.testContent(testProcessor.Process());
            }
            let testImplementationAppend : string = <string>this.testImplementation(instance, args);
            if (!ObjectValidator.IsSet(testImplementationAppend)) {
                testImplementationAppend = "";
            }
            this.testContent(this.testContent() + testImplementationAppend);
            testButtons.foreach(($button : any) : void => {
                this.testContent(this.testContent() + $button.getContent().toString());
            });
        } else {
            this.testContent("");
            this.normalImplementation(instance, args);
        }

        if (!this.TestModeEnabled() && ObjectValidator.IsEmptyOrNull(instance) ||
            this.TestModeEnabled() &&
            ObjectValidator.IsEmptyOrNull(instance) && ObjectValidator.IsEmptyOrNull(this.testContent())) {
            ExceptionsManager.Throw(this.getClassName(),
                "Class '" + this.getClassName() + "' does not contains valid implementation for current view mode.");
        }
    }

    public Show() : string {
        let output : string = "";

        if (!ObjectValidator.IsEmptyOrNull(this.testContent())) {
            output += this.testContent();

            setTimeout(() : void => {
                this.getTestButtons().foreach(($button : GuiCommons) : void => {
                    $button.Register();
                });
            });
        }

        return output;
    }

    /**
     * @returns {GuiCommons} Returns GUI object instance held by this viewer.
     */
    public getInstance() : GuiCommons {
        return super.getInstance();
    }

    protected setInstance($value : GuiCommons) : void {
        super.setInstance($value);
    }

    protected beforeLoad($instance? : GuiCommons, $args? : BaseViewerArgs) : void {
        super.beforeLoad($instance, $args);
    }

    protected afterLoad($instance? : GuiCommons, $args? : BaseViewerArgs) : void {
        super.afterLoad($instance, $args);
    }

    protected layerHandler($layer : BaseViewerLayerType, $instance? : GuiCommons) : void {
        super.layerHandler($layer, $instance);
    }

    protected argsHandler($instance? : GuiCommons, $args? : BaseViewerArgs) : void {
        super.argsHandler($instance, $args);
    }

    protected normalImplementation($instance? : GuiCommons, $args? : BaseViewerArgs) : void {
        super.normalImplementation($instance, $args);
    }

    protected testImplementation($instance? : GuiCommons, $args? : BaseViewerArgs) : string | void {
        return super.testImplementation($instance, $args);
    }

    protected addTestButton($text : string, $onClick : () => void, $hideSideBar : boolean = true) : void {
        const button : AbstractGuiObject = new AbstractGuiObject($text);
        button.getEvents().setOnClick($onClick);
        if ($hideSideBar) {
            button.getEvents().setOnClick(() : void => {
                bootstrap.Offcanvas.getInstance(BaseViewer.getTestRenderTarget()).hide();
            });
        }
        this.getTestButtons().Add(button);
    }

    protected getTestButtons() : ArrayList<AbstractGuiObject> {
        return super.getTestButtons();
    }

    protected excludeCacheData() : string[] {
        return super.excludeCacheData().concat(["argsHash"]);
    }
}
