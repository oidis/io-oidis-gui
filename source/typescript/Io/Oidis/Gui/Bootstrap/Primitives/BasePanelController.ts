/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EnvironmentArgs } from "@io-oidis-commons/Io/Oidis/Commons/EnvironmentArgs.js";
import { HttpRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EventsManager } from "../../Events/EventsManager.js";
import { HttpManager } from "../../HttpProcessor/HttpManager.js";
import { Loader } from "../../Loader.js";
import { BasePanel } from "./BasePanel.js";

export class BasePanelController<Panel = BasePanel> extends BaseObject {
    private readonly panelInstance : Panel;
    private readonly environment : EnvironmentArgs;
    private readonly httpManager : HttpManager;
    private readonly eventsManager : EventsManager;
    private processed : boolean;
    private parent : any;
    private requestArgs : HttpRequestEventArgs;

    constructor($instance : Panel) {
        super();
        this.panelInstance = $instance;
        this.environment = Loader.getInstance().getEnvironmentArgs();
        this.httpManager = Loader.getInstance().getHttpManager();
        this.eventsManager = Loader.getInstance().getHttpResolver().getEvents();
        this.processed = false;
        this.parent = null;
        this.requestArgs = null;
    }

    public async Process($force? : boolean) : Promise<void> {
        if (!this.processed || $force) {
            this.processed = true;
            try {
                await this.resolver(this.panelInstance);
            } catch (ex) {
                LogIt.Error(ex);
            }
        } else {
            LogIt.Warning("Processing of {0} skipped. Try to use Process with $force=true.", this.getClassName());
        }
    }

    public async FetchData(...$args : any[]) : Promise<void> {
        // this method should be used for speed optimization
    }

    public setParent($instance : any) : void {
        this.parent = $instance;
    }

    public RequestArgs($value? : HttpRequestEventArgs) : HttpRequestEventArgs {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.requestArgs = $value;
        }
        return this.requestArgs;
    }

    public async AccessValidator() : Promise<boolean> {
        return true;
    }

    protected async beforeLoad($instance? : Panel) : Promise<void> {
        // override this method for ability to access initiated panel instance before it is shown
    }

    protected getInstanceOwner() : Panel {
        return this.panelInstance;
    }

    protected getParent() : any {
        return this.parent;
    }

    protected async resolver($instance? : Panel) : Promise<void> {
        // override this method for ability to execute implementation in correct order
    }

    protected getHttpManager() : HttpManager {
        return this.httpManager;
    }

    protected getEnvironmentArgs() : EnvironmentArgs {
        return this.environment;
    }

    protected getEventsManager() : EventsManager {
        return this.eventsManager;
    }
}
