/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { ElementManager } from "../../Utils/ElementManager.js";
import { GuiCommons } from "./GuiCommons.js";

export class BasePanel extends GuiCommons {
    public Visible($value? : boolean) : boolean {
        const visible : boolean = super.Visible($value);
        if (ObjectValidator.IsSet($value)) {
            if (visible) {
                ElementManager.Show(this.InstanceOwner());
            } else {
                ElementManager.Hide(this.InstanceOwner());
            }
        }
        return visible;
    }
}
