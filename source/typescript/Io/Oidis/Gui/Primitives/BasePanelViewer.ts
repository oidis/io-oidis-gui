/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { TimeoutManager } from "@io-oidis-commons/Io/Oidis/Commons/Events/TimeoutManager.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { PanelContentType } from "../Enums/PanelContentType.js";
import { EventsManager } from "../Events/EventsManager.js";
import { IEventsManager } from "../Interfaces/IEventsManager.js";
import { IBasePanel } from "../Interfaces/Primitives/IBasePanel.js";
import { IBasePanelViewer } from "../Interfaces/Primitives/IBasePanelViewer.js";
import { IBaseViewer } from "../Interfaces/Primitives/IBaseViewer.js";
import { BasePanelViewerArgs } from "./BasePanelViewerArgs.js";
import { BaseViewer } from "./BaseViewer.js";
import { BaseViewerArgs } from "./BaseViewerArgs.js";

/**
 * BasePanelViewer should be used as abstract class for extending to the panel viewers and
 * it is providing base methods connected with preparation of panel content.
 */

export class BasePanelViewer extends BaseViewer implements IBasePanelViewer {
    private static asyncRequestsList : ArrayList<string>;

    /**
     * @param {BasePanelViewerArgs} [$args] Set initialization arguments object.
     */
    constructor($args? : BasePanelViewerArgs) {
        super($args);
    }

    /**
     * @returns {BasePanel} Returns Panel instance held by this viewer.
     */
    public getInstance() : IBasePanel {
        return <IBasePanel>super.getInstance();
    }

    /**
     * @param {BasePanelViewerArgs} [$args] Set panel viewer arguments.
     * @returns {BasePanelViewerArgs} Returns panel viewer arguments.
     */
    public ViewerArgs($args? : BasePanelViewerArgs) : BasePanelViewerArgs {
        return <BasePanelViewerArgs>super.ViewerArgs(<BaseViewerArgs>$args);
    }

    /**
     * Provides preparation of panel's instance, which is held by this viewer.
     * @returns {void}
     */
    public PrepareImplementation() : void {
        super.PrepareImplementation();
        const instance : IBasePanel = <IBasePanel>this.getInstance();

        if (!ObjectValidator.IsEmptyOrNull(instance) &&
            Reflection.getInstance().Implements(instance, IBasePanel)) {

            const events : IEventsManager = EventsManager.getInstanceSingleton();
            instance.getChildPanelList().foreach(($childViewer : BasePanelViewer) : void => {
                if (!ObjectValidator.IsEmptyOrNull($childViewer)) {
                    $childViewer.InstanceOwner(<IBaseViewer>this);
                }
            });

            if (!ObjectValidator.IsEmptyOrNull(this.ViewerArgs()) && this.ViewerArgs().AsyncEnabled()) {
                if (instance.ContentType() === PanelContentType.WITH_ELEMENT_WRAPPER ||
                    instance.ContentType() === PanelContentType.ASYNC_LOADER && this.IsCached()) {
                    instance.ContentType(PanelContentType.ASYNC_LOADER);

                    let viewerName : string = this.getClassName();
                    let parentViewer : BaseViewer = <BaseViewer>this.InstanceOwner();
                    while (!ObjectValidator.IsEmptyOrNull(parentViewer)) {
                        viewerName = parentViewer.getClassName();
                        parentViewer = <BaseViewer>parentViewer.InstanceOwner();
                    }

                    viewerName = StringUtils.Replace(viewerName, ".", "/");
                    if (!ObjectValidator.IsSet(BasePanelViewer.asyncRequestsList)) {
                        BasePanelViewer.asyncRequestsList = new ArrayList<string>();
                    }

                    events.setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE,
                        ($eventArgs : AsyncRequestEventArgs) : void => {
                            if ($eventArgs.POST().KeyExists(HttpRequestConstants.ELEMENT_INSTANCE)) {
                                BasePanelViewer.asyncRequestsList.RemoveAt(
                                    BasePanelViewer.asyncRequestsList.IndexOf(
                                        $eventArgs.POST().getItem(HttpRequestConstants.ELEMENT_INSTANCE)));
                                if (!BasePanelViewer.asyncRequestsList.IsEmpty()) {
                                    const eventArgs : EventArgs = new EventArgs();
                                    eventArgs.Owner(BasePanelViewer.asyncRequestsList.getLast());
                                    events.FireEvent(BasePanelViewer.ClassName(), EventType.ON_ASYNC_REQUEST, eventArgs);
                                }
                            }
                        });

                    events.setEvent(BasePanelViewer.ClassName(), EventType.ON_ASYNC_REQUEST, ($eventArgs : EventArgs) : void => {
                        const element : IBasePanel = <IBasePanel>$eventArgs.Owner();
                        const postData : ArrayList<any> = new ArrayList<any>();
                        postData.Add(element, HttpRequestConstants.ELEMENT_INSTANCE);
                        postData.Add(
                            StringUtils.Contains(this.getHttpManager().getRequest().getScriptPath(),
                                HttpRequestConstants.HIDE_CHILDREN), HttpRequestConstants.HIDE_CHILDREN);
                        this.getHttpManager().ReloadTo("/async/" + viewerName, postData, true);
                    });

                    events.setEvent(instance, EventType.ON_ASYNC_REQUEST, ($eventArgs : EventArgs) : void => {
                        BasePanelViewer.asyncRequestsList.Add($eventArgs.Owner());
                        if (BasePanelViewer.asyncRequestsList.Length() === 1) {
                            events.FireEvent(BasePanelViewer.ClassName(), EventType.ON_ASYNC_REQUEST, $eventArgs);
                        }
                    });
                }
            }
        }
    }

    /**
     * @param {string} [$EOL] Specify end of line which should be used for rendering
     * @returns {string} Returns rendered content
     */
    public Show($EOL? : string) : string {
        if (!ObjectValidator.IsEmptyOrNull(this.getInstance()) &&
            this.getInstance().ContentType() === PanelContentType.WITHOUT_ELEMENT_WRAPPER) {
            return this.getInstance().Draw($EOL);
        }
        return super.Show($EOL);
    }

    protected asyncLoad($handler : ($viewer : BasePanelViewer) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.getInstance())) {
            const asyncLoader : TimeoutManager = new TimeoutManager();
            (<any>this.getInstance()).asyncChildListLoad($handler, asyncLoader);
            asyncLoader.Execute();
        } else {
            $handler(this);
        }
    }
}
