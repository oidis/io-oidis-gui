/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { SyntaxConstants } from "@io-oidis-commons/Io/Oidis/Commons/Enums/SyntaxConstants.js";
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { TimeoutManager } from "@io-oidis-commons/Io/Oidis/Commons/Events/TimeoutManager.js";
import { IArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IArrayList.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Alignment } from "../Enums/Alignment.js";
import { EventType } from "../Enums/Events/EventType.js";
import { FitToParent } from "../Enums/FitToParent.js";
import { GeneralCssNames } from "../Enums/GeneralCssNames.js";
import { GuiCommonsArgType } from "../Enums/GuiCommonsArgType.js";
import { GuiOptionType } from "../Enums/GuiOptionType.js";
import { PositionType } from "../Enums/PositionType.js";
import { ElementEventsManager } from "../Events/ElementEventsManager.js";
import { EventsManager } from "../Events/EventsManager.js";
import { GuiObjectManager } from "../GuiObjectManager.js";
import { GuiOptionsManager } from "../GuiOptionsManager.js";
import { HttpManager } from "../HttpProcessor/HttpManager.js";
import { IGuiCommonsEvents } from "../Interfaces/Events/IGuiCommonsEvents.js";
import { IEventsHandler } from "../Interfaces/IEventsHandler.js";
import { IEventsManager } from "../Interfaces/IEventsManager.js";
import { IBaseViewer } from "../Interfaces/Primitives/IBaseViewer.js";
import { IGuiCommons } from "../Interfaces/Primitives/IGuiCommons.js";
import { IGuiCommonsArg } from "../Interfaces/Primitives/IGuiCommonsArg.js";
import { IGuiElement } from "../Interfaces/Primitives/IGuiElement.js";
import { IResponsiveElement } from "../Interfaces/Primitives/IResponsiveElement.js";
import { Loader } from "../Loader.js";
import { ElementOffset } from "../Structures/ElementOffset.js";
import { PropagableNumber } from "../Structures/PropagableNumber.js";
import { Size } from "../Structures/Size.js";
import { ElementManager } from "../Utils/ElementManager.js";
import { GuiElement } from "./GuiElement.js";

/**
 * GuiCommons should be used as abstract class for extending to any of GUI object
 * and it is providing base methods connected with GUI elements.
 */
export abstract class GuiCommons extends BaseObject implements IGuiCommons {
    private static maxAsyncConcurrentCount : number = 50;
    private static guiElementClass : any = GuiElement;

    protected outputEndOfLine : string;
    private options : GuiOptionsManager;
    private availableOptionsList : ArrayList<GuiOptionType>;
    private events : ElementEventsManager;
    private readonly guiId : string;
    private guiPath : string;
    private parent : GuiCommons;
    private owner : IBaseViewer;
    private visible : boolean;
    private enabled : boolean;
    private interfaceClassName : string;
    private styleClassName : string;
    private containerClassName : string;
    private loaded : boolean;
    private asyncDrawEnabled : boolean;
    private contentLoaded : boolean;
    private childElements : ArrayList<IGuiCommons>;
    private waitFor : ArrayList<IGuiCommons>;
    private cached : boolean;
    private prepared : boolean;
    private completed : boolean;
    private innerHtmlMap : IGuiElement;
    private alignment : Alignment;
    private fitToParent : FitToParent;
    private widthOfColumn : PropagableNumber;
    private wrappingElement : IGuiElement;
    private wrappingContainer : IGuiElement;

    /**
     * @param {any} [$dataObject] Data object suitable for data reconstruction.
     * @returns {any} Returns class instance in case of, that data object is suitable for object reconstruction,
     * otherwise null. This method is mainly focused to unserialization purposes, but it can also returns class singleton.
     */
    public static getInstance($dataObject? : any) : any {
        const output : any = Reflection.getInstanceOf(this.ClassName(), $dataObject);
        if (!ObjectValidator.IsEmptyOrNull(output)) {
            output.setInstanceAttributes();
        }
        return output;
    }

    /**
     * @param {IGuiElement} $value Specify GuiElement class, which should be used for element rendering.
     * @returns {IGuiElement} Returns GuiElement class, which is used for element rendering.
     */
    public static GuiElementClass($value? : any) : any {
        if (ObjectValidator.IsSet($value) && Reflection.getInstance().ClassHasInterface($value, IGuiElement)) {
            GuiCommons.guiElementClass = $value;
        }
        return GuiCommons.guiElementClass;
    }

    /**
     * @param {GuiCommons} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static Show($element : GuiCommons) : void {
        ElementManager.Show($element.Id());
        const eventArgs : EventArgs = new EventArgs();
        eventArgs.Owner($element);
        if ($element.IsLoaded()) {
            EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_SHOW, eventArgs, false);
            EventsManager.getInstanceSingleton().FireEvent(GuiCommons.ClassName(), EventType.ON_SHOW, eventArgs, false);
        } else {
            const parent : IGuiCommons = $element.Parent();
            if (!ObjectValidator.IsEmptyOrNull(parent) && parent.IsCompleted()) {
                EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_SHOW, eventArgs, false);
                EventsManager.getInstanceSingleton().FireEvent(GuiCommons.ClassName(), EventType.ON_SHOW, eventArgs, false);
            }
        }
    }

    /**
     * @param {GuiCommons} $element Specify element, which should be handled.
     * @returns {void}
     */
    public static Hide($element : GuiCommons) : void {
        ElementManager.Hide($element.Id());
        const eventArgs : EventArgs = new EventArgs();
        eventArgs.Owner($element);
        if ($element.IsLoaded()) {
            EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_HIDE, eventArgs);
            EventsManager.getInstanceSingleton().FireEvent(GuiCommons.ClassName(), EventType.ON_HIDE, eventArgs);
        } else {
            const parent : IGuiCommons = $element.Parent();
            if (!ObjectValidator.IsEmptyOrNull(parent) && parent.IsCompleted()) {
                EventsManager.getInstanceSingleton().FireEvent($element, EventType.ON_HIDE, eventArgs);
                EventsManager.getInstanceSingleton().FireEvent(GuiCommons.ClassName(), EventType.ON_HIDE, eventArgs);
            }
        }
    }

    /**
     * @param {GuiCommons} $element Specify element, which will animate.
     * @returns {void}
     */
    public static CreateAnimationContext($element : GuiCommons) : void {
        ElementManager.setCssProperty($element, "transform", "rotate(0deg)");
    }

    protected static asyncLoadChildren($loaderItems : ArrayList<() => void>) : void {
        if (GuiCommons.maxAsyncConcurrentCount > 1) {
            const loaderItemsLength : number = $loaderItems.Length();
            const loadManager : TimeoutManager = new TimeoutManager();
            let loaderIndex : number;
            for (loaderIndex = 0;
                 loaderIndex < Math.ceil(loaderItemsLength / GuiCommons.maxAsyncConcurrentCount);
                 loaderIndex++) {
                loadManager.Add(($loaderIndex? : number) : void => {
                    let index : number;
                    for (index = 0; index < GuiCommons.maxAsyncConcurrentCount; index++) {
                        const itemIndex : number = $loaderIndex * GuiCommons.maxAsyncConcurrentCount + index;
                        if (itemIndex < loaderItemsLength) {
                            $loaderItems.getItem(itemIndex)();
                        } else {
                            break;
                        }
                    }
                });
            }
            loadManager.Execute();
        } else {
            EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                $loaderItems.foreach(($item : () => void) : void => {
                    $item();
                });
            });
        }
    }

    /**
     * @param {string} [$id] Force to set element's id, instead of generated one.
     */
    constructor($id? : string) {
        super();
        if (ObjectValidator.IsEmptyOrNull($id)) {
            const manager : GuiObjectManager = this.getGuiManager();
            do {
                this.guiId =
                    this.getClassNameWithoutNamespace() +
                    (new Date().getTime()).toString() +
                    Math.floor(Math.random() * 1000).toString();
            } while (manager.Exists(this));
        } else {
            this.guiId = $id;
        }
        this.parent = null;
        this.owner = null;

        this.prepared = false;
        this.completed = false;
        this.loaded = false;
        this.innerHtmlMap = null;
        this.getGuiManager().Add(this);
    }

    /**
     * @returns {IGuiCommonsEvents} Returns events connected with the element.
     */
    public getEvents() : IGuiCommonsEvents {
        let initialized : boolean = false;
        if (ObjectValidator.IsEmptyOrNull(this.events)) {
            let eventsManagerClass : any = this.getEventsManagerClass();
            if (ObjectValidator.IsEmptyOrNull(eventsManagerClass)) {
                eventsManagerClass = ElementEventsManager;
            }
            this.events = new eventsManagerClass(this);
            initialized = true;
        } else if (ObjectValidator.IsEmptyOrNull(this.events.getOwner())) {
            (<any>this.events).owner = this;
            initialized = true;
        }
        if (initialized) {
            this.getEvents = () : IGuiCommonsEvents => {
                return <IGuiCommonsEvents>this.events;
            };
        }
        return <IGuiCommonsEvents>this.events;
    }

    /**
     * @returns {GuiOptionsManager} Returns manager of element's output and behaviour options.
     */
    public getGuiOptions() : GuiOptionsManager {
        if (ObjectValidator.IsEmptyOrNull(this.options)) {
            this.options = new GuiOptionsManager(this.availableGuiOptions());
        }
        return this.options;
    }

    /**
     * @returns {IGuiElement} Returns class with IGuiElement interface, which is used for element rendering.
     */
    public getGuiElementClass() : any {
        return GuiCommons.guiElementClass;
    }

    /**
     * @param {boolean} [$value] Set visibility of element content at the screen.
     * @returns {boolean} Returns true, if content will be visible, otherwise false.
     */
    public Visible($value? : boolean) : boolean {
        if (ObjectValidator.IsSet($value)) {
            this.visible = Property.Boolean(this.visible, $value);
            if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
                if (ElementManager.Exists(this.Id())) {
                    const thisClass : any = Reflection.getInstance().getClass(this.getClassName());
                    if (this.visible) {
                        const events : IEventsManager = EventsManager.getInstanceSingleton();
                        if (!this.isContentLoaded() || !this.IsLoaded()) {
                            events.FireAsynchronousMethod(() : void => {
                                if (this.visible) {
                                    if (!this.isContentLoaded()) {
                                        if (ObjectValidator.IsEmptyOrNull(this.outputEndOfLine)) {
                                            this.outputEndOfLine = StringUtils.NewLine(false);
                                        }
                                        const EOL : string = this.outputEndOfLine;
                                        ElementManager.setInnerHtml(this.Id(), this.guiContent().Draw(EOL + "        ") + EOL + "   ");
                                        events.FireEvent(this, EventType.ON_START);
                                    }
                                    thisClass.Show(this);
                                    if (!this.IsLoaded() && ElementManager.IsVisible(this.Id())) {
                                        if (this.IsCached() && !this.IsPrepared()) {
                                            this.innerCode();
                                        }
                                        events.FireEvent(this, EventType.ON_LOAD);
                                    }
                                }
                            }, false);
                        } else {
                            thisClass.Show(this);
                            if (this.IsCached() && (this.IsPrepared() || !ObjectValidator.IsEmptyOrNull(this.Parent()))) {
                                if (!this.IsPrepared()) {
                                    this.innerCode();
                                }
                                if (!this.IsCompleted()) {
                                    let fireCompleted : boolean = true;
                                    const validateVisibility : any = ($parent : GuiCommons) : void => {
                                        if (!ObjectValidator.IsEmptyOrNull($parent)) {
                                            if (!$parent.Visible() || !ElementManager.IsVisible($parent.Id())) {
                                                fireCompleted = false;
                                            } else {
                                                validateVisibility($parent.Parent());
                                            }
                                        }
                                    };
                                    validateVisibility(this.Parent());
                                    if (fireCompleted) {
                                        this.completed = true;
                                        events.FireEvent(this, EventType.ON_COMPLETE);
                                    }
                                }
                            }
                        }
                    } else {
                        thisClass.Hide(this);
                    }
                }
            }
        }
        if (!ObjectValidator.IsSet(this.visible)) {
            this.visible = true;
        }
        return this.visible;
    }

    /**
     * @param {boolean} [$value] Switch type of element mode between enabled and disabled.
     * @returns {boolean} Returns true, if element is in enabled mode, otherwise false.
     */
    public Enabled($value? : boolean) : boolean {
        if (!ObjectValidator.IsSet(this.enabled)) {
            this.enabled = true;
        }
        this.enabled = Property.Boolean(this.enabled, $value);
        if (!this.enabled) {
            if (!this.IsLoaded()) {
                this.getGuiOptions().Add(GuiOptionType.DISABLE);
            } else if (!this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                Echo.Printf("Disabled HTML mod can be missing at run time for element " + this.Id() + ". " +
                    "Did you add GuiOptionType?");
            }
        }
        if (ObjectValidator.IsSet($value) && ElementManager.IsVisible(this.Id())) {
            if (!this.enabled && this.getGuiOptions().Contains(GuiOptionType.DISABLE)) {
                ElementManager.setClassName(this.Id() + "_Status", GeneralCssNames.DISABLE);
            } else {
                ElementManager.setClassName(this.Id() + "_Status", "");
            }
        }

        return this.enabled;
    }

    /**
     * @param {string} [$value] Set type of css class name connected with the object instance.
     * @returns {string} Returns css class name connected with the object instance.
     */
    public StyleClassName($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            if (this.styleClassNameSetterValidator($value)) {
                this.styleClassName = StringUtils.Remove($value, ".");
                if (this.IsLoaded()) {
                    ElementManager.setClassName(this.Id() + "_GuiWrapper", this.styleClassName);
                }
            }
        } else if (!ObjectValidator.IsSet(this.styleClassName)) {
            this.styleClassName = "";
        }

        return this.styleClassName;
    }

    /**
     * @returns {string} Returns element id suitable to correct element identification in page context.
     */
    public Id() : string {
        return this.guiId;
    }

    /**
     * @param {GuiCommons} [$value] Set instance of the element owner.
     * @returns {GuiCommons} Returns instance of the element owner, if the element has owner, otherwise null.
     */
    public Parent($value? : GuiCommons) : GuiCommons {
        if (ObjectValidator.IsSet($value)) {
            this.parent = $value;
            if (!ObjectValidator.IsSet(this.owner)) {
                this.owner = <IBaseViewer>this.parent.InstanceOwner();
            }
        }
        return this.parent;
    }

    /**
     * @param {IBaseViewer} [$value] Set viewer instance of the GUI object instance owner.
     * @returns {IBaseViewer} Returns viewer instance of the GUI object instance owner, if the instance has owner, otherwise null.
     */
    public InstanceOwner($value? : IBaseViewer) : IBaseViewer {
        if (ObjectValidator.IsSet($value)) {
            this.owner = $value!;
        }
        if (ObjectValidator.IsEmptyOrNull(this.owner)) {
            if (!ObjectValidator.IsEmptyOrNull(this.Parent())) {
                this.owner = <IBaseViewer>this.Parent().InstanceOwner();
            } else {
                this.owner = null;
            }
        }
        return this.owner;
    }

    /**
     * @param {string} [$value] Set path to instance of the GUI object, which can be used for its usage identification.
     * @returns {string} Returns path to instance of the GUI object in current usage.
     */
    public InstancePath($value? : string) : string {
        if (ObjectValidator.IsEmptyOrNull(this.guiPath)) {
            this.guiPath = this.getClassName();
            if (!ObjectValidator.IsEmptyOrNull(this.InstanceOwner())) {
                let property : any;
                for (property in this.owner) {
                    if (this.owner[property] === this) {
                        this.guiPath += "." + property;
                        break;
                    }
                }
            }
        }
        return this.guiPath = Property.String(this.guiPath, $value);
    }

    /**
     * @returns {IArrayList<IGuiCommons>} Returns list of child elements belonging to this instance.
     */
    public getChildElements() : IArrayList<IGuiCommons> {
        if (!ObjectValidator.IsSet(this.childElements)) {
            this.childElements = new ArrayList<IGuiCommons>();
        }
        return this.childElements;
    }

    /**
     * @returns {ElementOffset} Returns element's position relative to screen top left corner.
     */
    public getScreenPosition() : ElementOffset {
        return ElementManager.getScreenPosition(this.guiContentId());
    }

    /**
     * Specify rendering position of the element or override position defined by CSS
     * @param {number} $top Specify element's top position
     * @param {number} $left Specify element's left position
     * @param {PositionType} [$type] Specify element's position type
     * @returns {void}
     */
    public setPosition($top : number, $left : number, $type? : PositionType) : void {
        const id : string = this.Id() + "_GuiWrapper";
        const setPosition : IEventsHandler = () : void => {
            if (PositionType.Contains($type)) {
                ElementManager.setCssProperty(id, "position", $type.toString());
            } else {
                const positionType : string = ElementManager.getCssValue(id, "position");
                if (ObjectValidator.IsEmptyOrNull(positionType) || positionType === "static") {
                    ElementManager.setCssProperty(id, "position", "relative");
                }
            }
            ElementManager.setCssProperty(id, "top", $top);
            ElementManager.setCssProperty(id, "left", $left);
        };
        if (this.IsLoaded()) {
            setPosition();
        } else {
            this.getEvents().setOnComplete(setPosition);
        }
    }

    /**
     * @returns {Size} Returns current element's width and height based on available information.
     */
    public getSize() : Size {
        if (this.IsLoaded()) {
            return new Size(this.guiContentId());
        }
        return new Size();
    }

    /**
     * Can be overriden in extending classes for FitToParent capability.
     * @param {number} [$value] Specify element's width value.
     * @returns {number} Returns element's width value if reachable otherwise undefined.
     */
    public Width($value? : number) : number {
        return undefined;
    }

    /**
     * Can be overridden in extending classes for FitToParent capability.
     * @param {number} [$value] Specify element's height value.
     * @returns {number} Returns element's height value if reachable otherwise undefined.
     */
    public Height($value? : number) : number {
        return undefined;
    }

    /**
     * @param {FitToParent} [$value] Specify how should this element be stretched inside its parent.
     * @returns {IGuiCommons} Returns reference to the current GuiCommons instance.
     */
    public FitToParent($value : FitToParent) : IGuiCommons {
        this.fitToParent = $value;
        return this;
    }

    /**
     * @returns {FitToParent} Returns set FitToParent.
     */
    public getFitToParent() : FitToParent {
        return this.fitToParent;
    }

    /**
     * @returns {boolean} Returns true, if element has been loaded from cache, otherwise false.
     */
    public IsCached() : boolean {
        if (!ObjectValidator.IsSet(this.cached)) {
            this.cached = !ObjectValidator.IsEmptyOrNull(this.InstanceOwner()) && this.InstanceOwner().IsCached() && this.IsLoaded();
        }
        return this.cached;
    }

    /**
     * @returns {boolean} Returns true, if element has been prepared by it's owner, otherwise false.
     */
    public IsPrepared() : boolean {
        if (!ObjectValidator.IsSet(this.prepared)) {
            this.prepared = false;
        }
        return this.prepared;
    }

    /**
     * @returns {boolean} Returns true, if element has been loaded, otherwise false.
     */
    public IsLoaded() : boolean {
        if (!ObjectValidator.IsSet(this.loaded)) {
            this.loaded = false;
        }
        return this.loaded;
    }

    /**
     * @returns {boolean} Returns true, if element's ON_COMPLETE event has been fired, otherwise false.
     */
    public IsCompleted() : boolean {
        if (!ObjectValidator.IsSet(this.completed)) {
            this.completed = false;
        }
        return this.completed;
    }

    /**
     * Disable asynchronous rendering of the element to the screen.
     * @returns {void}
     */
    public DisableAsynchronousDraw() : void {
        this.asyncDrawEnabled = false;
    }

    /**
     * @param {string} [$EOL] Specify end of line format for generated html output.
     * @returns {string} Returns element's content suitable for renderings at the screen.
     */
    public Draw($EOL? : string) : string {
        if (ObjectValidator.IsEmptyOrNull($EOL)) {
            $EOL = StringUtils.NewLine(false);
        }
        this.outputEndOfLine = $EOL;

        const id : string = this.Id();
        let parentId : string = "not registered";
        if (!ObjectValidator.IsEmptyOrNull(this.Parent())) {
            parentId = this.Parent().Id();
        }
        let content : string | IGuiElement = "";
        if (!this.hasAsynchronousDraw()) {
            content = this.guiContent();
        }

        if (ObjectValidator.IsEmptyOrNull(this.InstanceOwner()) ||
            !ObjectValidator.IsEmptyOrNull(this.InstanceOwner()) &&
            ObjectValidator.IsSet(this.InstanceOwner().TestModeEnabled) && this.InstanceOwner().TestModeEnabled()) {
            this.getEvents().FireAsynchronousMethod(() : void => {
                LogIt.Debug("Draw of GUI object placeholder " + id + " with parent " + parentId);
            }, false);
        }

        const mainWrapper : IGuiElement = this.addElement().StyleClassName(this.getCssInterfaceName());
        const wrappingElement : IGuiElement = this.getWrappingElement();
        if (ObjectValidator.IsSet(wrappingElement)) {
            if (wrappingElement.getGuiTypeTag() === GeneralCssNames.ROW) {
                mainWrapper.Id(this.guiId + "_GuiCol").GuiTypeTag("GuiCol");
            }
            mainWrapper.setWrappingElement(wrappingElement);
            this.setWrappingElement(mainWrapper);
        }
        return mainWrapper
            .Add(this.addElement(this.guiId + "_GuiWrapper")
                .GuiTypeTag("GuiWrapper")
                .StyleClassName(this.StyleClassName())
                .Add(this.addElement(this.guiId)
                    .StyleClassName(this.getCssContainerName())
                    .Visible(!ObjectValidator.IsEmptyOrNull(content) && this.Visible())
                    .Add(content)
                )
            )
            .Draw($EOL);
    }

    /**
     * @returns {IGuiCommonsArg[]} Returns array of element's attributes.
     */
    public getArgs() : IGuiCommonsArg[] {
        const size : Size = this.getSize();
        const position : ElementOffset = this.getScreenPosition();

        return [
            {
                name : "Id",
                type : GuiCommonsArgType.TEXT,
                value: this.Id()
            },
            {
                name : "StyleClassName",
                type : GuiCommonsArgType.TEXT,
                value: this.StyleClassName()
            },
            {
                name : "Enabled",
                type : GuiCommonsArgType.BOOLEAN,
                value: this.Enabled()
            },
            {
                name : "Visible",
                type : GuiCommonsArgType.BOOLEAN,
                value: this.Visible()
            },
            {
                name : "Width",
                type : GuiCommonsArgType.NUMBER,
                value: size.Width()
            },
            {
                name : "Height",
                type : GuiCommonsArgType.NUMBER,
                value: size.Height()
            },
            {
                name : "Top",
                type : GuiCommonsArgType.NUMBER,
                value: position.Top()
            },
            {
                name : "Left",
                type : GuiCommonsArgType.NUMBER,
                value: position.Left()
            }
        ];
    }

    /**
     * @param {IGuiCommonsArg} $value Specify argument value, which should be passed to element.
     * @param {boolean} [$force=false] Specify, if value should be set without fire of events connected with argument change.
     * @returns {void}
     */
    public setArg($value : IGuiCommonsArg, $force : boolean = false) : void {
        switch ($value.name) {
        case "StyleClassName":
            this.StyleClassName(<string>$value.value);
            break;
        case "Visible":
            this.Visible(<boolean>$value.value);
            break;
        case "Enabled":
            this.Enabled(<boolean>$value.value);
            break;
        case "Width":
            ElementManager.setCssProperty(this.guiContentId(), "width", <number>$value.value);
            break;
        case "Height":
            ElementManager.setCssProperty(this.guiContentId(), "height", <number>$value.value);
            break;
        case "Top":
            ElementManager.setCssProperty(this.guiContentId(), "top", <number>$value.value);
            break;
        case "Left":
            ElementManager.setCssProperty(this.guiContentId(), "left", <number>$value.value);
            break;
        default:
            break;
        }
    }

    /**
     * @returns {IGuiElement} Returns reference to element's inner HTML structure object.
     */
    public getInnerHtmlMap() : IGuiElement {
        if (ObjectValidator.IsEmptyOrNull(this.innerHtmlMap)) {
            this.innerHtmlMap = this.innerHtml();
        }
        return this.innerHtmlMap;
    }

    /**
     * @returns {IGuiElement} Returns reference to element wrapping this instance inside generated HTML.
     */
    public getWrappingElement() : IGuiElement {
        return this.wrappingElement;
    }

    /**
     * @param {IGuiCommonsArg} $value Specify element wrapping this instance inside generated HTML.
     */
    public setWrappingElement($value : IGuiElement) : void {
        this.wrappingElement = $value;
    }

    /**
     * @returns {IGuiElement} Returns reference to element wrapping this instance before conversion to HTML.
     */
    public getWrappingContainer() : IGuiElement {
        return this.wrappingContainer;
    }

    /**
     * @param {IGuiCommonsArg} $value Specify element wrapping this instance before conversion to HTML.
     */
    public setWrappingContainer($value : IGuiElement) : void {
        this.wrappingContainer = $value;
    }

    /**
     * @returns {boolean} Returns true if element should prevent scroll otherwise false.
     */
    public IsPreventingScroll() : boolean {
        return false;
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        return this.getClassName() + " (" + this.Id() + ")";
    }

    public toString() : string {
        return this.ToString();
    }

    /**
     * @returns {ElementEventsManager} This method should return class with
     * interface Io.Oidis.Gui.Interfaces.Events.IGuiCommonsEvents
     */
    protected getEventsManagerClass() : any {
        return ElementEventsManager;
    }

    protected addElement($id? : string) : IGuiElement {
        return new GuiCommons.guiElementClass().Id($id);
    }

    protected addRow($id? : string) : IResponsiveElement {
        if (ObjectValidator.IsEmptyOrNull($id)) {
            $id = this.getRandomGuiId() + "_" + GeneralCssNames.ROW;
        }
        return new GuiCommons.guiElementClass().Id($id).GuiTypeTag(GeneralCssNames.ROW);
    }

    protected addColumn($id? : string) : IResponsiveElement {
        if (ObjectValidator.IsEmptyOrNull($id)) {
            $id = this.getRandomGuiId() + "_" + GeneralCssNames.COLUMN;
        }
        return new GuiCommons.guiElementClass().Id($id).GuiTypeTag(GeneralCssNames.COLUMN);
    }

    /**
     * Specify attributes of the instance after unserialization.
     */
    protected setInstanceAttributes() : void {
        this.parent = null;
        this.owner = null;

        this.prepared = false;
        this.completed = false;
        this.loaded = false;
        this.innerHtmlMap = null;
        this.getGuiManager().Add(this);
    }

    protected beforeCacheCreation($preparationResultHandler : ($id : string, $value : any) => void) : void {
        // override this method for ability to prepare gui element for cache creation
    }

    protected afterCacheCreation($id : string, $value : any) : void {
        // override this method for ability to restore gui element after cache creation
    }

    protected availableGuiOptions() : ArrayList<GuiOptionType> {
        if (!ObjectValidator.IsSet(this.availableOptionsList)) {
            this.availableOptionsList = new ArrayList<GuiOptionType>();
        }
        this.availableOptionsList.Add(GuiOptionType.DISABLE);
        return this.availableOptionsList;
    }

    protected styleClassNameSetterValidator($value : string) : boolean {
        return true;
    }

    protected cssInterfaceName() : string {
        return this.getNamespaceName();
    }

    protected cssContainerName() : string {
        return this.getClassNameWithoutNamespace();
    }

    protected getGuiTypeTag() : string {
        return this.getCssContainerName();
    }

    /**
     * @returns {IGuiElement} String Used for definition of Code, which should be included as part of GUI object
     */
    protected innerCode() : IGuiElement {
        if (!ObjectValidator.IsSet(this.waitFor)) {
            this.waitFor = new ArrayList<IGuiCommons>();
        }

        const events : IEventsManager = EventsManager.getInstanceSingleton();

        if (this.getChildElements().IsEmpty()) {
            let parameter : any;
            for (parameter in this) {
                if (parameter !== "parent" && parameter !== "owner" &&
                    typeof this[parameter] !== SyntaxConstants.FUNCTION) {
                    const propertyInstance : GuiCommons = this[parameter];
                    if (!ObjectValidator.IsEmptyOrNull(propertyInstance) &&
                        ObjectValidator.IsSet(propertyInstance.Parent)) {
                        propertyInstance.Parent(this);
                        propertyInstance.InstancePath(this.InstancePath() + "." + parameter);
                        this.getChildElements().Add(propertyInstance, propertyInstance.Id());
                    }
                }
            }
        }

        if (!this.IsLoaded()) {
            this.cached = false;

            this.getEvents().setOnStart(
                ($eventArgs : EventArgs) : void => {
                    const element : GuiCommons = <GuiCommons>$eventArgs.Owner();
                    if (ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) ||
                        !ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) &&
                        ObjectValidator.IsSet(element.InstanceOwner().TestModeEnabled) && element.InstanceOwner().TestModeEnabled()) {
                        LogIt.Debug("Start " + element.Id());
                    }
                    element.getChildElements().foreach(($child : IGuiCommons) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($child)) {
                            if ($child.Visible() && !$child.IsLoaded() && !element.waitFor.Contains($child)) {
                                element.waitFor.Add($child);
                            }
                            $child.getEvents().setOnComplete(
                                ($eventArgs : EventArgs) : void => {
                                    const child : GuiCommons = <GuiCommons>$eventArgs.Owner();
                                    const parent : GuiCommons = child.Parent();
                                    if (!ObjectValidator.IsEmptyOrNull(parent)) {
                                        if (!parent.waitFor.IsEmpty()) {
                                            parent.waitFor.RemoveAt(parent.waitFor.IndexOf(child));
                                        }
                                        if (parent.waitFor.IsEmpty() && !parent.IsCompleted()) {
                                            parent.completed = true;
                                            events.FireEvent(parent, EventType.ON_COMPLETE);
                                        }
                                    }
                                });
                        }
                    });
                });

            this.getEvents().setOnLoad(
                ($eventArgs : EventArgs) : void => {
                    const element : GuiCommons = <GuiCommons>$eventArgs.Owner();
                    if (ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) ||
                        !ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) &&
                        ObjectValidator.IsSet(element.InstanceOwner().TestModeEnabled) && element.InstanceOwner().TestModeEnabled()) {
                        LogIt.Debug("Loaded " + element.Id());
                    }
                    element.cached = false;
                    element.loaded = true;
                    if (element.Visible()) {
                        if (ObjectValidator.IsSet(element.waitFor) && !element.waitFor.IsEmpty()) {
                            const loaderItems : ArrayList<() => void> = new ArrayList<() => void>();
                            element.waitFor.foreach(($child : IGuiCommons) : void => {
                                loaderItems.Add(() : void => {
                                    if (!$child.IsLoaded() && $child.Visible()) {
                                        $child.Visible(true);
                                    }
                                });
                            });

                            GuiCommons.asyncLoadChildren(loaderItems);
                        } else if (!element.IsCompleted()) {
                            element.completed = true;
                            events.FireEvent(element, EventType.ON_COMPLETE);
                        }
                    }
                });

            this.getEvents().setBeforeLoad(
                ($eventArgs : EventArgs) : void => {
                    const element : GuiCommons = <GuiCommons>$eventArgs.Owner();
                    if (ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) ||
                        !ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) &&
                        ObjectValidator.IsSet(element.InstanceOwner().TestModeEnabled) && element.InstanceOwner().TestModeEnabled()) {
                        LogIt.Debug("Prepare for load " + $eventArgs.Owner().Id());
                    }
                    events.FireEvent(element, EventType.ON_LOAD);
                });
        } else {
            this.cached = true;
        }

        this.getEvents().setOnComplete(
            ($eventArgs : EventArgs) : void => {
                const element : GuiCommons = <GuiCommons>$eventArgs.Owner();
                if (element.Visible()) {
                    if (ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) ||
                        !ObjectValidator.IsEmptyOrNull(element.InstanceOwner()) &&
                        ObjectValidator.IsSet(element.InstanceOwner().TestModeEnabled) && element.InstanceOwner().TestModeEnabled()) {
                        LogIt.Debug("Completed " + element.Id());
                    }
                    const parentIsCached : boolean = element.IsCached();
                    element.getChildElements().foreach(($child : GuiCommons) : void => {
                        if (parentIsCached && $child.IsLoaded()) {
                            $child.cached = true;
                        }
                        if (!$child.IsCompleted()) {
                            let fireCompleted : boolean = false;
                            if ($child.Visible() && ElementManager.IsVisible($child.Id())) {
                                fireCompleted = true;
                            }
                            if (fireCompleted) {
                                const validateVisibility : any = ($parent : GuiCommons) : void => {
                                    if (!ObjectValidator.IsEmptyOrNull($parent)) {
                                        if (!$parent.Visible() || !ElementManager.IsVisible($parent.Id())) {
                                            fireCompleted = false;
                                        } else {
                                            validateVisibility($parent.Parent());
                                        }
                                    }
                                };
                                validateVisibility($child.Parent());
                                if (fireCompleted) {
                                    if (!$child.IsPrepared()) {
                                        $child.innerCode();
                                    }
                                    $child.completed = true;
                                    events.FireEvent($child, EventType.ON_COMPLETE);
                                }
                            }
                        }
                    });

                    element.completed = true;
                    element.getEvents().Subscribe();
                }
            });

        this.prepared = true;

        return this.addElement();
    }

    /**
     * @returns {IGuiElement} String Used for definition of HTML with final settings
     */
    protected innerHtml() : IGuiElement {
        return this.addElement();
    }

    protected getHttpManager() : HttpManager {
        return Loader.getInstance().getHttpResolver().getManager();
    }

    protected getEventsManager() : EventsManager {
        return Loader.getInstance().getHttpResolver().getEvents();
    }

    protected getGuiManager() : GuiObjectManager {
        return GuiObjectManager.getInstanceSingleton();
    }

    protected guiContent() : IGuiElement {
        this.contentLoaded = true;
        let innerCode : string | IGuiElement = "";
        if (!this.IsPrepared()) {
            innerCode = this.innerCode();
        }
        this.innerHtmlMap = this.innerHtml();
        return this.addElement().Add(innerCode).Add(this.innerHtmlMap);
    }

    protected guiContentId() : string {
        return this.Id();
    }

    protected unhide($process : ($id : string, $element : HTMLElement) => boolean) : void {
        const clone : HTMLElement = <HTMLElement>ElementManager.getElement(this.Id())
            .parentElement.parentElement.cloneNode(true);
        ElementManager.setOpacity(clone, 0);
        document.body.appendChild(clone);
        const elements : any = clone.getElementsByTagName("div");
        let index : number;
        for (index = 0; index < elements.length; index++) {
            if (!$process((<HTMLElement>elements[index]).id, <HTMLElement>elements[index])) {
                break;
            }
        }
        document.body.removeChild(clone);
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        exclude.push(
            "options", "availableOptionsList",
            "parent", "owner", "guiPath",
            "visible", "enabled", "prepared", "completed",
            "interfaceClassName", "styleClassName", "containerClassName",
            "loaded", "asyncDrawEnabled", "contentLoaded",
            "waitFor",
            "outputEndOfLine",
            "innerHtmlMap"
        );
        if (this.getEvents().getAll().IsEmpty()) {
            exclude.push("events");
        }
        return exclude;
    }

    protected excludeCacheData() : string[] {
        const exclude : string[] = [
            "options", "availableOptionsList",
            "events",
            "childElements", "waitFor",
            "cached", "prepared", "completed",
            "parent", "owner", "guiPath",
            "interfaceClassName", "styleClassName", "containerClassName",
            "innerHtmlMap"
        ];
        if (this.visible === true) {
            exclude.push("visible");
        }
        if (this.enabled === true) {
            exclude.push("enabled");
        }
        if (this.asyncDrawEnabled === true) {
            exclude.push("asyncDrawEnabled");
        }
        if (this.contentLoaded === false) {
            exclude.push("contentLoaded");
        } else if (this.contentLoaded === true && !this.IsMemberOf("Io.Oidis.Gui.Primitives.BasePanel")) {
            exclude.push("outputEndOfLine");
        }
        if (this.loaded === false) {
            exclude.push("loaded");
        }

        return exclude;
    }

    private isContentLoaded() : boolean {
        if (!ObjectValidator.IsSet(this.contentLoaded)) {
            this.contentLoaded = false;
        }
        return this.contentLoaded;
    }

    private hasAsynchronousDraw() : boolean {
        if (!ObjectValidator.IsSet(this.asyncDrawEnabled)) {
            this.asyncDrawEnabled = true;
        }
        return this.asyncDrawEnabled;
    }

    private getCssInterfaceName() : string {
        if (!ObjectValidator.IsSet(this.interfaceClassName)) {
            this.interfaceClassName = StringUtils.Remove(this.cssInterfaceName(), ".");
        }
        return this.interfaceClassName;
    }

    private getCssContainerName() : string {
        if (!ObjectValidator.IsSet(this.containerClassName)) {
            this.containerClassName = StringUtils.Remove(this.cssContainerName(), ".");
        }
        return this.containerClassName;
    }

    private getRandomGuiId() : string {
        return (new Date().getTime()).toString() +
            Math.floor(Math.random() * 100000).toString();
    }
}
