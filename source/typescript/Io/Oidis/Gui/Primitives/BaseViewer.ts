/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseViewerLayerType } from "../Enums/BaseViewerLayerType.js";
import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { EventsManager } from "../Events/EventsManager.js";
import { GuiObjectManager } from "../GuiObjectManager.js";
import { HttpManager } from "../HttpProcessor/HttpManager.js";
import { IBaseGuiObject } from "../Interfaces/Primitives/IBaseGuiObject.js";
import { IBaseViewer } from "../Interfaces/Primitives/IBaseViewer.js";
import { Loader } from "../Loader.js";
import { AbstractGuiObject } from "./AbstractGuiObject.js";
import { BaseViewerArgs } from "./BaseViewerArgs.js";

export abstract class CommonGuiViewer extends BaseObject {
    private args : BaseViewerArgs;
    private instance : any;
    private owner : IBaseViewer;
    private testAppend : string;
    private testModeEnabled : boolean;
    private testButtons : ArrayList<AbstractGuiObject>;
    private layer : BaseViewerLayerType;
    private testSubscriber : any;

    /**
     * @param {boolean} [$testMode] Set, if created link should be in test mode.
     * @returns {string} Returns link, which can be used for resolver registration or invoke by link element.
     */
    public static CallbackLink($testMode : boolean = false) : string {
        const prefix : string = Loader.getInstance().getHttpManager()
            .getRequest().getUrlArgs().KeyExists(HttpRequestConstants.DESIGNER) ? "design" : "web";
        let link : string = Loader.getInstance().getHttpManager()
            .CreateLink("/" + prefix + "/" + StringUtils.Replace(this.ClassName(), ".", "/"));
        link = StringUtils.Replace(link, "/#/", "#/");
        if (!StringUtils.StartsWith(link, "#")) {
            link = "#" + link;
        }
        if ($testMode) {
            link += "/" + HttpRequestConstants.TEST_MODE;
        }
        return link;
    }

    protected static getTestViewerArgs() : BaseViewerArgs {
        return null;
    }

    /**
     * @param {BaseViewerArgs} [$args] Set initialization arguments object.
     */
    protected constructor($args? : BaseViewerArgs) {
        super();
        this.instance = null;
        this.testButtons = new ArrayList<AbstractGuiObject>();
        this.testAppend = "";
        this.owner = null;

        this.ViewerArgs($args);
        this.TestModeEnabled(false);
    }

    /**
     * @param {IBaseViewer} [$value] Set owner of the viewer instance.
     * @returns {IBaseViewer} Returns owner instance of the current instance, if the instance has owner, otherwise null.
     */
    public InstanceOwner($value? : IBaseViewer) : IBaseViewer {
        if (ObjectValidator.IsSet($value)) {
            this.owner = <BaseViewer>$value;
        }
        return this.owner;
    }

    /**
     * @param {boolean} [$value] Specify, if viewer's object instance should be processed in test mode.
     * @returns {boolean} Returns true, if viewer's object instance will be processed in test mode, otherwise false.
     */
    public TestModeEnabled($value? : boolean) : boolean {
        return this.testModeEnabled = Property.Boolean(this.testModeEnabled, $value);
    }

    /**
     * @param {BaseViewerArgs} [$args] Set viewer arguments.
     * @returns {BaseViewerArgs} Returns viewer arguments.
     */
    public ViewerArgs($args? : BaseViewerArgs) : BaseViewerArgs {
        if (ObjectValidator.IsSet($args)) {
            this.args = $args;
        }
        return this.args;
    }

    public getInstance() : any {
        return this.instance;
    }

    /**
     * @returns {string} Returns class name with namespace for structure of supported layers.
     */
    public getLayersType() : string {
        return BaseViewerLayerType.ClassName();
    }

    /**
     * @param {BaseViewerLayerType} $type Specify type of layer, which should be aplied on current view.
     * @returns {void}
     */
    public setLayer($type : BaseViewerLayerType) : void {
        if (!ObjectValidator.IsEmptyOrNull(this.instance) && !ObjectValidator.IsEmptyOrNull($type) && this.layer !== $type) {
            this.layerHandler($type, this.instance);
            this.layer = $type;
        }
    }

    /**
     * Provides preparation of GUI object instance, which is held by this viewer.
     * @returns {void}
     */
    public PrepareImplementation() : void {
        if (!ObjectValidator.IsEmptyOrNull(this.instance)) {
            this.instance.InstanceOwner(this);
            const instanceWithViewer : any = this.instance;
            if (ObjectValidator.IsFunction(instanceWithViewer.PanelViewer) &&
                !ObjectValidator.IsEmptyOrNull(instanceWithViewer.PanelViewer())) {
                instanceWithViewer.PanelViewer().InstanceOwner(this);
            }

            if (!ObjectValidator.IsEmptyOrNull(this.args)) {
                if (!this.args.Visible()) {
                    this.instance.Visible(false);
                }
                this.argsHandler(this.instance, this.args);
            }

            if (!this.instance.IsCompleted()) {
                this.setLayer(BaseViewerLayerType.DEFAULT);
                this.beforeLoad(this.instance, this.args);
                this.instance.getEvents().setOnComplete(() : void => {
                    this.afterLoad(this.instance, this.args);
                });
            }
        }

        if (this.testModeEnabled) {
            if (!ObjectValidator.IsEmptyOrNull(this.testSubscriber)) {
                const testProcessor : any = new this.testSubscriber();
                testProcessor.setOwner(this);
                this.testAppend = testProcessor.Process();
                if (!ObjectValidator.IsSet(this.testAppend)) {
                    this.testAppend = "";
                }
            }
            let testImplementationAppend : string = <string>this.testImplementation(this.instance, this.args);
            if (!ObjectValidator.IsSet(testImplementationAppend)) {
                testImplementationAppend = "";
            }
            this.testAppend += testImplementationAppend;
            this.testButtons.foreach(($button : AbstractGuiObject) : void => {
                this.testAppend += $button.Draw();
            });
            if (!ObjectValidator.IsEmptyOrNull(this.testAppend)) {
                this.testAppend = "<div style=\"clear: both; height: 50px;\"></div>" + this.testAppend;
            }
            this.testAppend += "<style>body{overflow: auto; margin: inherit;}</style>";
        } else {
            this.testAppend = "";
            this.normalImplementation(this.instance, this.args);
        }

        if (!this.testModeEnabled && ObjectValidator.IsEmptyOrNull(this.instance) ||
            this.testModeEnabled &&
            ObjectValidator.IsEmptyOrNull(this.instance) && ObjectValidator.IsEmptyOrNull(this.testAppend)) {
            ExceptionsManager.Throw(this.getClassName(),
                "Class '" + this.getClassName() + "' does not contains valid implementation for current view mode.");
        }
    }

    /**
     * @param {string} [$EOL] Specify element, which should be handled.
     * @returns {string} Returns content of the viewer in string format, suitable for further rendering to the screen.
     */
    public Show($EOL? : string) : string {
        if (ObjectValidator.IsEmptyOrNull($EOL)) {
            $EOL = StringUtils.NewLine(false);
        }
        let output : string = "";

        if (!ObjectValidator.IsEmptyOrNull(this.instance)) {
            output += this.instance.Draw($EOL);
        }

        if (!ObjectValidator.IsEmptyOrNull(this.testAppend)) {
            output += this.testAppend;
        }

        return output;
    }

    /**
     * @returns {object} Returns data suitable for object serialization.
     */
    public SerializationData() : object {
        return {instance: this.instance};
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let id : string = "instance NOT DEFINED";
        if (!ObjectValidator.IsEmptyOrNull(this.getInstance())) {
            id = this.getInstance().Id();
        }
        return this.getClassName() + " (" + id + ")";
    }

    public toString() : string {
        return this.ToString();
    }

    protected setInstance($value : any) : void {
        this.instance = $value;
        if (!ObjectValidator.IsEmptyOrNull(this.instance)) {
            this.instance.InstanceOwner(this);
        }
    }

    protected beforeLoad($instance? : any, $args? : BaseViewerArgs) : void {
        // override this method for ability to prepare instance before it is loaded
    }

    protected afterLoad($instance? : any, $args? : BaseViewerArgs) : void {
        // override this method for ability to setup instance after it is loaded
    }

    protected layerHandler($layer : BaseViewerLayerType, $instance? : any) : void {
        // override this method for ability to setup instance according to selected layer
    }

    protected argsHandler($instance? : any, $args? : BaseViewerArgs) : void {
        // override this method for ability distribute viewer args into the instance
    }

    protected normalImplementation($instance? : any, $args? : BaseViewerArgs) : void {
        // override this method for ability to setup default instance behaviour
    }

    protected testImplementation($instance? : any, $args? : BaseViewerArgs) : string | void {
        this.normalImplementation($instance, $args);
        return "";
    }

    protected abstract addTestButton($text : string, $onClick : () => void) : void;

    protected setTestSubscriber($className : IClassName | string) : void {
        this.testSubscriber = $className;
        if (ObjectValidator.IsString($className)) {
            this.testSubscriber = Reflection.getInstance().getClass(<string>$className);
        }
    }

    protected getHttpManager() : HttpManager {
        return Loader.getInstance().getHttpResolver().getManager();
    }

    protected getEventsManager() : EventsManager {
        return Loader.getInstance().getHttpResolver().getEvents();
    }

    protected getGuiManager() : GuiObjectManager {
        return GuiObjectManager.getInstanceSingleton();
    }

    protected getTestButtons() : ArrayList<any> {
        return this.testButtons;
    }

    protected getTestSubscriber() : any {
        return this.testSubscriber;
    }

    protected testContent($value? : string) : string {
        return this.testAppend = Property.String(this.testAppend, $value);
    }

    protected excludeCacheData() : string[] {
        return ["testAppend", "testButtons", "isPrinted", "args", "layer"];
    }
}

/**
 * BaseViewer should be used as abstract class for extending to the GUI viewers and
 * it is providing base methods connected with preparation of GUI object.
 */
export class BaseViewer extends CommonGuiViewer implements IBaseViewer {
    private argsHash : number;
    private isPrinted : boolean;
    private isCached : boolean;

    constructor($args? : BaseViewerArgs) {
        super($args);
        this.argsHash = null;
        this.isPrinted = false;
        this.isCached = false;
    }

    public ViewerArgs($args? : BaseViewerArgs) : BaseViewerArgs {
        if (!ObjectValidator.IsEmptyOrNull($args) && !ObjectValidator.IsString($args)) {
            super.ViewerArgs($args);
            const instance : IBaseGuiObject = this.getInstance();
            if (!ObjectValidator.IsEmptyOrNull(instance) &&
                (instance.IsLoaded() || this.IsCached() || !instance.IsLoaded() && !instance.Visible())) {
                const newHash : number = $args.getHash();
                if (this.argsHash !== newHash) {
                    this.argsHash = newHash;
                    this.PrepareImplementation();
                }
            }
        }
        return super.ViewerArgs($args);
    }

    /**
     * @returns {IBaseGuiObject} Returns GUI object instance held by this viewer.
     */
    public getInstance() : IBaseGuiObject {
        return super.getInstance();
    }

    /**
     * @param {boolean} [$value] Set viewer cache status.
     * @returns {boolean} Returns true, if viewer has been loaded from cache, otherwise false.
     */
    public IsCached($value? : boolean) : boolean {
        return this.isCached = Property.Boolean(this.isCached, $value);
    }

    /**
     * @returns {boolean} Returns true, if viewer's instance HTML output has been printed, otherwise false.
     */
    public IsPrinted() : boolean {
        if (!ObjectValidator.IsSet(this.isPrinted)) {
            this.isPrinted = false;
        }
        return this.isPrinted;
    }

    public Show($EOL? : string) : string {
        const output : string = super.Show($EOL);
        this.isPrinted = true;
        return output;
    }

    public SerializationData() : object {
        return JsonUtils.Extend(super.SerializationData(), {isCached: this.isCached});
    }

    protected setInstance($value : IBaseGuiObject) : void {
        super.setInstance($value);
    }

    protected asyncLoad($handler : ($viewer : BaseViewer) => void) : void {
        $handler(this);
    }

    protected beforeLoad($instance? : IBaseGuiObject, $args? : BaseViewerArgs) : void {
        super.beforeLoad($instance, $args);
    }

    protected afterLoad($instance? : IBaseGuiObject, $args? : BaseViewerArgs) : void {
        super.afterLoad($instance, $args);
    }

    protected layerHandler($layer : BaseViewerLayerType, $instance? : IBaseGuiObject) : void {
        super.layerHandler($layer, $instance);
    }

    protected argsHandler($instance? : IBaseGuiObject, $args? : BaseViewerArgs) : void {
        super.argsHandler($instance, $args);
    }

    protected normalImplementation($instance? : IBaseGuiObject, $args? : BaseViewerArgs) : void {
        super.normalImplementation($instance, $args);
    }

    protected testImplementation($instance? : IBaseGuiObject, $args? : BaseViewerArgs) : string | void {
        return super.testImplementation($instance, $args);
    }

    protected addTestButton($text : string, $onClick : () => void, $buttonClassName? : any) : void {
        if (ObjectValidator.IsEmptyOrNull($buttonClassName) || !ObjectValidator.IsSet($buttonClassName.ClassName)) {
            $buttonClassName = AbstractGuiObject;
        }
        let button : AbstractGuiObject;
        try {
            button = new $buttonClassName($text);
        } catch (ex) {
            button = new AbstractGuiObject($text);
        }
        button.getEvents().setOnClick($onClick);
        this.getTestButtons().Add(button);
    }

    protected getTestButtons() : ArrayList<AbstractGuiObject> {
        return super.getTestButtons();
    }

    protected excludeCacheData() : string[] {
        return super.excludeCacheData().concat(["isCached", "isPrinted", "argsHash"]);
    }
}
