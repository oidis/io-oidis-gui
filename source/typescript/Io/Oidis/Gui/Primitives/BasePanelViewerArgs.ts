/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { IBasePanelViewerArgs } from "../Interfaces/Primitives/IBasePanelViewerArgs.js";
import { BaseViewerArgs } from "./BaseViewerArgs.js";

/**
 * BasePanelViewerArgs is abstract structure handling data connected with BaseViewer.
 */
export class BasePanelViewerArgs extends BaseViewerArgs implements IBasePanelViewerArgs {

    private asyncEnabled : boolean = false;

    /**
     * @param {boolean} [$value] Specify, if panel content should be loaded asynchronously.
     * @returns {boolean} Returns true, if panel content can be loaded asynchronously, otherwise false.
     */
    public AsyncEnabled($value? : boolean) : boolean {
        this.asyncEnabled = Property.Boolean(this.asyncEnabled, $value);
        return this.asyncEnabled;
    }
}
