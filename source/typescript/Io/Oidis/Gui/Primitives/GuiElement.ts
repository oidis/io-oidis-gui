/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IArrayList.js";
import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Alignment } from "../Enums/Alignment.js";
import { FitToParent } from "../Enums/FitToParent.js";
import { GeneralCssNames } from "../Enums/GeneralCssNames.js";
import { UnitType } from "../Enums/UnitType.js";
import { VisibilityStrategy } from "../Enums/VisibilityStrategy.js";
import { IBaseViewer } from "../Interfaces/Primitives/IBaseViewer.js";
import { IGuiCommons } from "../Interfaces/Primitives/IGuiCommons.js";
import { IGuiElement } from "../Interfaces/Primitives/IGuiElement.js";
import { IResponsiveElement } from "../Interfaces/Primitives/IResponsiveElement.js";
import { PropagableNumber } from "../Structures/PropagableNumber.js";

/**
 * GuiElement class provides structure for handling of element rendering.
 */
export class GuiElement extends BaseObject implements IGuiElement, IResponsiveElement {
    protected id : string;
    protected styleClassName : string;
    protected visible : boolean;
    protected guiType : string;
    protected width : number;
    protected height : number;
    private readonly items : any[];
    private itemIndex : number;
    private attributes : ArrayList<string>;
    private alignment : Alignment | (() => Alignment);
    private fitToParent : FitToParent | (() => FitToParent);
    private visibilityStrategy : VisibilityStrategy | (() => VisibilityStrategy);
    private heightOfRow : PropagableNumber | (() => PropagableNumber);
    private widthOfColumn : PropagableNumber | (() => PropagableNumber);
    private wrappingElement : IGuiElement;

    constructor() {
        super();
        this.id = null;
        this.styleClassName = null;
        this.visible = null;
        this.guiType = null;
        this.width = null;
        this.height = null;
        this.items = [];
        this.itemIndex = 0;
        this.attributes = null;
    }

    /**
     * @param {string} $value Specify element's id.
     * @returns {IGuiElement} Returns reference to current GuiElement instance.
     */
    public Id($value : string) : IGuiElement {
        this.id = Property.String(this.id, $value);
        return this;
    }

    /**
     * @returns {string} Returns current element's id
     */
    public getId() : string {
        return this.id;
    }

    /**
     * @param {string} $value Specify element's guiTag value.
     * @returns {IGuiElement} Returns reference to current GuiElement instance.
     */
    public GuiTypeTag($value : string) : IGuiElement {
        this.guiType = Property.String(this.guiType, $value);
        return this;
    }

    /**
     * @param {string|BaseEnum} $value Specify element's CSS className value.
     * @returns {IGuiElement} Returns reference to current GuiElement instance.
     */
    public StyleClassName($value : string | BaseEnum) : IGuiElement {
        this.styleClassName = Property.String(this.styleClassName, "" + $value);
        return this;
    }

    /**
     * @param {boolean} $value Specify element's visibility.
     * @returns {IGuiElement} Returns reference to current GuiElement instance.
     */
    public Visible($value : boolean) : IGuiElement {
        this.visible = Property.Boolean(this.visible, $value);
        return this;
    }

    /**
     * @returns {boolean} Returns currently set visible status.
     */
    public getVisible() : boolean {
        return this.visible;
    }

    /**
     * @param {number} $value Specify element's width value in pixel format.
     * @returns {IGuiElement} Returns reference to current GuiElement instance.
     */
    public Width($value : number) : IGuiElement {
        this.width = Property.PositiveInteger(this.width, $value);
        return this;
    }

    /**
     * @returns {number} Returns currently set width.
     */
    public getWidth() : number {
        return this.width;
    }

    /**
     * @param {number} $value Specify element's height value in pixel format.
     * @returns {IGuiElement} Returns reference to current IGuiElement instance.
     */
    public Height($value : number) : IGuiElement {
        this.height = Property.PositiveInteger(this.height, $value);
        return this;
    }

    /**
     * @returns {number} Returns currently set height.
     */
    public getHeight() : number {
        return this.height;
    }

    /**
     * @param {Alignment | () => Alignment} [$value] Specify how should this element be aligned - can be set to propagate to it's
     * children.
     * @returns {IResponsiveElement} Returns reference to the current IResponsiveElement instance.
     */
    public Alignment($value : Alignment | (() => Alignment)) : IResponsiveElement {
        this.alignment = $value;
        return this;
    }

    /**
     * @returns {Alignment} Returns set Alignment.
     */
    public getAlignment() : Alignment {
        if (ObjectValidator.IsFunction(this.alignment)) {
            return (<any>this.alignment)();
        }
        return this.alignment;
    }

    /**
     * @param {FitToParent | () => FitToParent} [$value] Specify how should all GuiCommons children be stretched inside their columns
     * @returns {IResponsiveElement} Returns reference to the current IResponsiveElement instance.
     */
    public FitToParent($value : FitToParent | (() => FitToParent)) : IResponsiveElement {
        this.fitToParent = $value;
        return this;
    }

    /**
     * @returns {FitToParent} Returns set FitToParent.
     */
    public getFitToParent() : FitToParent {
        if (ObjectValidator.IsFunction(this.fitToParent)) {
            return (<any>this.fitToParent)();
        }
        return this.fitToParent;
    }

    /**
     * @param {VisibilityStrategy | () => VisibilityStrategy} [$value] Specify how should visibility of container be handled
     * @returns {IResponsiveElement} Returns reference to the current IResponsiveElement instance.
     */
    public VisibilityStrategy($value : VisibilityStrategy | (() => VisibilityStrategy)) : IResponsiveElement {
        this.visibilityStrategy = $value;
        return this;
    }

    /**
     * @returns {VisibilityStrategy} Returns set VisibilityStrategy value.
     */
    public getVisibilityStrategy() : VisibilityStrategy {
        if (ObjectValidator.IsFunction(this.visibilityStrategy)) {
            return (<any>this.visibilityStrategy)();
        }
        return this.visibilityStrategy;
    }

    /**
     * @param {string} [$value] Specify column width in either pixels or percentage : '10px', '10%' etc.
     * @param {boolean} [$isPropagated] Specify whether or not the value should be propagated to all child columns.
     * @returns {IResponsiveElement} Returns reference to the current IResponsiveElement instance.
     */
    public WidthOfColumn($value : string, $isPropagated? : boolean) : IResponsiveElement;

    /**
     * @param {string} [$value] Specify column width value.
     * @param {UnitType} [$unitType] Specify unit type of the value.
     * @param {boolean} [$isPropagated] Specify whether or not the value should be propagated to all child columns.
     * @returns {IResponsiveElement} Returns reference to the current IResponsiveElement instance.
     */
    public WidthOfColumn($value : number, $unitType : UnitType, $isPropagated? : boolean) : IResponsiveElement;

    /**
     * @param {() => PropagableNumber} [$value] Specify column width propagable value. Value is parsed from callback when getter
     * method is called / during gui resize computation.
     * @returns {IResponsiveElement} Returns reference to the current IResponsiveElement instance.
     */
    public WidthOfColumn($value : () => PropagableNumber) : IResponsiveElement;

    public WidthOfColumn($value : string | number | (() => PropagableNumber),
                         par1? : boolean | UnitType, par2? : boolean) : IResponsiveElement {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (ObjectValidator.IsFunction($value)) {
                this.widthOfColumn = <any>$value;
            } else if (ObjectValidator.IsString($value)) {
                this.widthOfColumn = new PropagableNumber(<any>$value, Boolean(par1));
            } else if (ObjectValidator.IsSet(par1)) {
                this.widthOfColumn = new PropagableNumber({number: Number($value), unitType: <UnitType>par1}, Boolean(par2));
            }
        } else {
            if (ObjectValidator.IsBoolean(par1)) {
                this.widthOfColumn = new PropagableNumber({number: -1, unitType: null}, Boolean(par1));
            } else if (ObjectValidator.IsSet(par2)) {
                this.widthOfColumn = new PropagableNumber({number: -1, unitType: null}, Boolean(par2));
            } else {
                this.widthOfColumn = new PropagableNumber({number: -1, unitType: null}, false);
            }
        }
        return this;
    }

    /**
     * @param {string} [$value] Specify row height in either pixels or percentage : '10px', '10%' etc.
     * @param {boolean} [$isPropagated] Specify whether or not the value should be propagated to all child rows.
     * @returns {IResponsiveElement} Returns reference to the current IResponsiveElement instance.
     */
    public HeightOfRow($value : string, $isPropagated? : boolean) : IResponsiveElement;

    /**
     * @param {string} [$value] Specify row height value.
     * @param {UnitType} [$unitType] Specify unit type of the value.
     * @param {boolean} [$isPropagated] Specify whether or not the value should be propagated to all child rows.
     * @returns {IResponsiveElement} Returns reference to the current IResponsiveElement instance.
     */
    public HeightOfRow($value : number, $unitType : UnitType, $isPropagated? : boolean) : IResponsiveElement;

    /**
     * @param {() => PropagableNumber} [$value] Specify row height propagable value. Value is parsed from callback when getter method is
     * called / during gui resize computation.
     * @returns {IResponsiveElement} Returns reference to the current IResponsiveElement instance.
     */
    public HeightOfRow($value : () => PropagableNumber) : IResponsiveElement;

    public HeightOfRow($value : string | number | (() => PropagableNumber),
                       par1? : boolean | UnitType, par2? : boolean) : IResponsiveElement {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (ObjectValidator.IsFunction($value)) {
                this.heightOfRow = <any>$value;
            } else if (ObjectValidator.IsString($value)) {
                this.heightOfRow = new PropagableNumber(<any>$value, Boolean(par1));
            } else if (ObjectValidator.IsSet(par1)) {
                this.heightOfRow = new PropagableNumber({number: Number($value), unitType: <UnitType>par1}, Boolean(par2));
            }
        } else {
            if (ObjectValidator.IsBoolean(par1)) {
                this.heightOfRow = new PropagableNumber({number: -1, unitType: null}, Boolean(par1));
            } else if (ObjectValidator.IsSet(par2)) {
                this.heightOfRow = new PropagableNumber({number: -1, unitType: null}, Boolean(par2));
            } else {
                this.heightOfRow = new PropagableNumber({number: -1, unitType: null}, false);
            }
        }
        return this;
    }

    /**
     * @returns {PropagableNumber} Returns set width of column
     */
    public getWidthOfColumn() : PropagableNumber {
        if (ObjectValidator.IsFunction(this.widthOfColumn)) {
            return (<any>this.widthOfColumn)();
        }
        return <PropagableNumber>this.widthOfColumn;
    }

    /**
     * @returns {PropagableNumber} Returns set height of row
     */
    public getHeightOfRow() : PropagableNumber {
        if (ObjectValidator.IsFunction(this.heightOfRow)) {
            return (<any>this.heightOfRow)();
        }
        return <PropagableNumber>this.heightOfRow;
    }

    /**
     * @param {string} $key Specify element's CSS attribute name.
     * @param {string} $value Specify element's CSS attribute value.
     * @returns {IGuiElement} Returns reference to current GuiElement instance.
     */
    public setAttribute($key : string, $value : string) : IGuiElement {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (this.attributes === null) {
                this.attributes = new ArrayList<string>();
            }
            if (ObjectValidator.IsEmptyOrNull($key)) {
                this.attributes.Add($value);
            } else {
                this.attributes.Add($value, $key);
            }
        }
        return this;
    }

    /**
     * @returns {IGuiElement} Returns current element's GuiType
     */
    public getGuiTypeTag() : string {
        return this.guiType;
    }

    /**
     * @param {string|IGuiElement|IResponsiveElement|IGuiCommons|IBaseViewer|HTMLElement} $value Specify element's content.
     * @returns {IGuiElement} Returns reference to current GuiElement instance.
     */
    public Add($value : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement) : IGuiElement {
        if (ObjectValidator.IsEmptyOrNull($value)) {
            return this;
        }
        const isNative : boolean = (ObjectValidator.IsString($value) || $value instanceof HTMLElement);
        let element : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement = $value;
        if (!isNative) {
            const object : IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer =
                <IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>$value;
            if (object.Implements(IGuiCommons)) {
                const commons : IGuiCommons = <IGuiCommons>object;
                if (this.guiType === GeneralCssNames.COLUMN) {
                    const rowElement : IGuiElement = new GuiElement().GuiTypeTag(GeneralCssNames.ROW);
                    rowElement.Id(commons.Id() + "_" + GeneralCssNames.ROW);
                    rowElement.Add($value);
                    element = rowElement;
                    commons.setWrappingElement(rowElement);
                    rowElement.setWrappingElement(this);
                } else {
                    commons.setWrappingElement(this);
                }
                commons.setWrappingContainer(this);
            } else if (object.Implements(IGuiElement) && object.Implements(IResponsiveElement)) {
                const guiElement : IGuiElement = <IGuiElement>object;
                if (this.guiType === GeneralCssNames.COLUMN && guiElement.getGuiTypeTag() === GeneralCssNames.COLUMN) {
                    const rowElement : IGuiElement = new GuiElement().GuiTypeTag(GeneralCssNames.ROW);
                    rowElement.Id(this.getUID() + "_" + GeneralCssNames.ROW);
                    rowElement.Add($value);
                    guiElement.setWrappingElement(rowElement);
                    element = rowElement;
                } else if (this.guiType === GeneralCssNames.ROW && guiElement.getGuiTypeTag() === GeneralCssNames.ROW) {
                    const columnElement : IGuiElement = new GuiElement().GuiTypeTag(GeneralCssNames.COLUMN);
                    columnElement.Id(this.getUID() + "_" + GeneralCssNames.COLUMN);
                    columnElement.Add($value);
                    guiElement.setWrappingElement(columnElement);
                    element = columnElement;
                } else {
                    guiElement.setWrappingElement(this);
                }
            }
        } else if (this.guiType === GeneralCssNames.COLUMN || this.guiType === GeneralCssNames.ROW) {
            const guiWrapper : IGuiElement = new GuiElement().GuiTypeTag("GuiWrapper");
            guiWrapper.addChildElement($value);
            const columnElement : IGuiElement = new GuiElement().GuiTypeTag(GeneralCssNames.COLUMN);
            columnElement.Id(this.getUID() + "_" + GeneralCssNames.COLUMN);
            columnElement.addChildElement(guiWrapper);

            if (this.guiType === GeneralCssNames.COLUMN) {
                const rowElement : IGuiElement = new GuiElement().GuiTypeTag(GeneralCssNames.ROW);
                rowElement.Id(this.getUID() + "_" + GeneralCssNames.ROW);
                rowElement.addChildElement(columnElement);
                columnElement.setWrappingElement(rowElement);
                element = rowElement;
            } else {
                columnElement.setWrappingElement(this);
                element = columnElement;
            }
        }
        this.items[this.itemIndex++] = element;
        return this;
    }

    /**
     * @param {string|IGuiElement|IResponsiveElement|IGuiCommons|IBaseViewer|HTMLElement} $value Specify element to
     * be directly inserted as a child inside this instance.
     */
    public addChildElement($value : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement) : void {
        this.items[this.itemIndex++] = $value;
    }

    /**
     * @returns {IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>} Returns reference to IArrayList
     * containing only non-native gui elements.
     */
    public getGuiChildElements() : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer> {
        const size : number = this.getChildElements().Length();
        let index : number = 0;
        const guiChildren : IArrayList<IGuiElement | IGuiCommons> = new ArrayList<IGuiElement | IGuiCommons>();

        for (index; index < size; index++) {
            const childElement : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement =
                this.getChildElement(index);
            const isNative : boolean = (ObjectValidator.IsString(childElement) || childElement instanceof HTMLElement);
            if (!isNative) {
                const object : IBaseObject = <IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>childElement;
                if (object.Implements(IGuiElement) || object.Implements(IGuiCommons)) {
                    guiChildren.Add(<IGuiElement | IGuiCommons> childElement);
                }
            }
        }
        return guiChildren;
    }

    /**
     * @returns {IArrayList<string | IGuiElement | IGuiCommons | IBaseViewer | HTMLElement>} Returns list of child elements
     * belonging to this instance.
     */
    public getChildElements() : IArrayList<string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement> {
        return ArrayList.ToArrayList(this.items);
    }

    /**
     * @param {number} $index Specify index of desired child. Index starts with 0.
     * @returns {string|IGuiElement|IGuiCommons|IBaseViewer|HTMLElement} Returns element's child, if desired index exists,
     * otherwise null.
     */
    public getChildElement($index : number) : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement {
        if ($index >= 0 && $index < this.itemIndex) {
            return this.items[$index];
        }
        return null;
    }

    /**
     * @returns {ArrayList<string>} Returns element's CSS attributes.
     */
    public getAttributes() : ArrayList<string> {
        if (this.attributes === null) {
            this.attributes = new ArrayList<string>();
        }
        return this.attributes;
    }

    /**
     * @param {string} $EOL Specify end of line value, which should be used for rendering.
     * @returns {string} Returns element's content suitable for element rendering or storage.
     */
    public Draw($EOL : string) : string {
        let id : string = "";
        if (this.id !== null && this.id !== "") {
            id = " id=\"" + this.id + "\"";
        }

        let guiType : string = "";
        if (this.guiType !== null && this.guiType !== "") {
            guiType = " guiType=\"" + this.guiType + "\"";
        }

        let styleClass : string = "";
        if (this.styleClassName !== null && this.styleClassName !== "") {
            styleClass = " class=\"" + this.styleClassName + "\"";
        }

        let style : string = "";
        if (this.visible !== null) {
            if (this.visible) {
                this.setAttribute("display", "block");
            } else {
                this.setAttribute("display", "none");
            }
        }
        if (this.width !== null) {
            this.setAttribute("width", this.width === 0 ? "0" : this.width + "px");
        }
        if (this.height !== null) {
            this.setAttribute("height", this.height === 0 ? "0" : this.height + "px");
        }
        if (!ObjectValidator.IsEmptyOrNull(this.attributes)) {
            this.attributes.SortByKeyUp();
            this.attributes.foreach(($value : string, $key : string) : void => {
                if (style !== "") {
                    style += " ";
                }
                if (!ObjectValidator.IsEmptyOrNull($key) && ObjectValidator.IsString($key)) {
                    style += StringUtils.Remove($key, ":") + ": " + $value + ";";
                } else {
                    if (!StringUtils.EndsWith($value, ";")) {
                        $value += ";";
                    }
                    style += $value;
                }
            });
        }
        if (style !== "") {
            style = " style=\"" + StringUtils.Replace(style, ";;", ";") + "\"";
        }

        const attributes : string = id + guiType + styleClass + style;

        let content : string = "";
        const contentEOL : string = attributes !== "" ? $EOL + "   " : $EOL;
        let index : number;
        for (index = 0; index < this.itemIndex; index++) {
            if (!ObjectValidator.IsEmptyOrNull(this.items[index])) {
                if (ObjectValidator.IsString(this.items[index])) {
                    content += this.itemToString(this.items[index], content !== "" ? $EOL : "", index);
                } else if (ObjectValidator.IsSet(this.items[index].outerHTML)) {
                    content += this.itemToString(this.items[index], $EOL, index);
                } else if (ObjectValidator.IsSet(this.items[index].Draw) || ObjectValidator.IsSet(this.items[index].Show)) {
                    content += this.itemToString(this.items[index], contentEOL, index);
                }
            }
        }

        if (attributes !== "") {
            if (StringUtils.EndsWith(content, "</div>")) {
                content += $EOL;
            }
            return $EOL + "<div" + attributes + ">" + content + "</div>";
        }
        return content;
    }

    /**
     * @param {string} $EOL Specify end of line value, which should be used for rendering.
     * @returns {HTMLElement} Returns HTMLElement element suitable for manipulation with DOM.
     */
    public ToDOMElement($EOL : string) : HTMLElement {
        const element : HTMLElement = document.createElement("div");
        element.innerHTML = this.Draw($EOL);
        return <HTMLElement>element.childNodes[1];
    }

    /**
     * @returns {IGuiElement} Returns reference to element wrapping this instance.
     */
    public getWrappingElement() : IGuiElement {
        return this.wrappingElement;
    }

    /**
     * @param {IGuiCommonsArg} $value Specify wrapping element.
     */
    public setWrappingElement($value : IGuiElement) : void {
        this.wrappingElement = $value;
    }

    public toString() : string {
        return this.Draw(StringUtils.NewLine(false));
    }

    protected itemToString($item : string | IGuiElement | IGuiCommons | IBaseViewer | HTMLElement, $EOL : string,
                           $key? : number) : string {
        if (!ObjectValidator.IsEmptyOrNull($item)) {
            if (ObjectValidator.IsString($item)) {
                return $EOL + $item;
            } else if (ObjectValidator.IsSet((<HTMLElement>$item).outerHTML)) {
                return $EOL + (<HTMLElement>$item).outerHTML;
            } else if (ObjectValidator.IsSet((<IGuiCommons>$item).Draw)) {
                return (<IGuiCommons>$item).Draw($EOL);
            } else if (ObjectValidator.IsSet((<IBaseViewer>$item).Show)) {
                return (<IBaseViewer>$item).Show($EOL);
            }
        }
        return "";
    }
}
