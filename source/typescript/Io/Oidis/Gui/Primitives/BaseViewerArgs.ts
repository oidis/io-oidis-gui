/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseArgs } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseArgs.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { IBaseViewerArgs } from "../Interfaces/Primitives/IBaseViewerArgs.js";

/**
 * BaseViewerArgs is abstract structure handling data connected with BaseViewer.
 */
export class BaseViewerArgs extends BaseArgs implements IBaseViewerArgs {

    private visible : boolean = true;

    /**
     * @param {boolean} [$value] Specify, if viewer's content should be visible at the screen.
     * @returns {boolean} Returns true, if viewer's content will be visible at the screen, otherwise false.
     */
    public Visible($value? : boolean) : boolean {
        this.visible = Property.Boolean(this.visible, $value);
        return this.visible;
    }
}
