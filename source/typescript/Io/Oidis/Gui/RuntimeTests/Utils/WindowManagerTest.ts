/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "../../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { Size } from "../../Structures/Size.js";
import { WindowManager } from "../../Utils/WindowManager.js";

export class WindowManagerTest extends RuntimeTestRunner {

    public testGetSize() : void {
        const size : Size = WindowManager.getSize();
        Echo.Println("window width: " + size.Width() + " px");
        Echo.Println("window height: " + size.Height() + " px");
    }

    public testViewHTMLCode() : void {
        this.addButton("View HTML code", () : any => {
            const code : string = WindowManager.ViewHTMLCode();
            Echo.Println(code);
        });
    }

    public testGetMousePosition() : void {
        Echo.Println("<div style=\"position: relative; top: 500px; left: 1200px; border: 1px solid red; width: 200px;\" " +
            "id=\"GetPositionButton\">Get mouse position</div>");
        this.getEventsManager().FireAsynchronousMethod(() : void => {
            const element : HTMLElement = document.getElementById("GetPositionButton");
            element.onclick = ($eventArgs : MouseEvent) : any => {
                Echo.Printf(WindowManager.getMouseX($eventArgs));
                Echo.Printf(WindowManager.getMouseY($eventArgs));
            };
        });
    }
}
/* dev:end */
