/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "../../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { ElementManager } from "../../Utils/ElementManager.js";
import { TextSelectionManager } from "../../Utils/TextSelectionManager.js";

export class TextSelectionManagerTest extends RuntimeTestRunner {

    public testSelectAll() : void {
        const element : HTMLElement = document.createElement("div");
        const elementinput : HTMLInputElement = document.createElement("input");
        TextSelectionManager.SelectAll(element);

        const inputelement : HTMLInputElement = document.createElement("input");
        TextSelectionManager.SelectAll(inputelement);
        TextSelectionManager.SelectAll("dialog");
        TextSelectionManager.SelectAll(elementinput);
        TextSelectionManager.Clear(elementinput);
        this.assertEquals(TextSelectionManager.Clear(), true);
    }

    public testgetSelectedText() : void {
        Echo.Println("<label><input id=\"testId\" type=\"text\">Multiple</label><br>");
        this.assertEquals(TextSelectionManager.getSelectedText(<HTMLInputElement>ElementManager.getElement("testId")), "");
        TextSelectionManager.SelectAll(<HTMLInputElement>ElementManager.getElement("testId"));
        this.assertEquals(TextSelectionManager.getSelectedText(<HTMLInputElement>ElementManager.getElement("testId")), "Multiple");
        TextSelectionManager.SelectAll(<HTMLInputElement>ElementManager.getElement("testId"));
    }

    public testsetCurrentPosition() : void {
        const elementinput : HTMLInputElement = document.createElement("input");
        Echo.Println("<label><input id=\"testId\" type=\"text\">Multiple</label><br>");
        this.assertEquals(TextSelectionManager.setCurrentPosition(<HTMLInputElement>ElementManager.getElement("testId", true), 5), "");
        TextSelectionManager.SelectAll(<HTMLInputElement>ElementManager.getElement("testId"));
        this.assertEquals(TextSelectionManager.getCurrentPosition(<HTMLInputElement>ElementManager.getElement("testId", true)), 5);
    }

    public testgetCurrentPosition() : void {
        const elementinput : HTMLInputElement = document.createElement("input");
        Echo.Println("<label><input id=\"testId\" type=\"text\">Multiple</label><br>");
        TextSelectionManager.setCurrentPosition(<HTMLInputElement>ElementManager.getElement("testId"), 4);
        TextSelectionManager.SelectAll(<HTMLInputElement>ElementManager.getElement("testId"));
        this.assertEquals(TextSelectionManager.getCurrentPosition(<HTMLInputElement>ElementManager.getElement("testId")), 4);
    }

    public testPasteTextAtCurrentPosition() : void {
        const elementinput2 : HTMLInputElement = document.createElement("input");
        Echo.Println("<label><input id=\"testId\" type=\"text\">Multiple</label><br>");
        TextSelectionManager.setCurrentPosition(<HTMLInputElement>ElementManager.getElement("testId"), 4);
        TextSelectionManager.PasteTextAtCurrentPosition("new");
        this.assertEquals(TextSelectionManager.getSelectedText(), "Multnewiple");
    }

    public testClear() : void {
        const elementinput : HTMLInputElement = document.createElement("input");
        TextSelectionManager.Clear(elementinput);
        this.assertEquals(TextSelectionManager.Clear(), true, "");
    }
}
/* dev:end */
