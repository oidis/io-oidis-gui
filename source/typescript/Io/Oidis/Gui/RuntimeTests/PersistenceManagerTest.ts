/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { BaseHttpResolver } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { FormsObject } from "../Primitives/FormsObject.js";

export class PersistenceManagerTest extends BaseHttpResolver {
    protected resolver() : void {
        this.getEventsManager().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_SUCCESS,
            () : void => {
                Echo.Println("<b>Collected data:</b>");
                Echo.Printf(FormsObject.CollectValues());
            });
        this.getHttpManager().ReloadTo("/PersistenceManager", null, true);
    }
}
/* dev:end */
