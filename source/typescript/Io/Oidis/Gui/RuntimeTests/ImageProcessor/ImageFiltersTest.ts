/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { JsonpFileReader } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/Handlers/JsonpFileReader.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { GrayscaleType } from "../../Enums/GrayscaleType.js";
import { RuntimeTestRunner } from "../../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { ImageFilters } from "../../ImageProcessor/ImageFilters.js";
import { ImageTransform } from "../../ImageProcessor/ImageTransform.js";
import { GuiElement } from "../../Primitives/GuiElement.js";
import { ElementManager } from "../../Utils/ElementManager.js";

export class ImageFiltersTest extends RuntimeTestRunner {
    private imageIndex : number = 0;

    public __IgnoretestGrayscale() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Grayscale(pixels);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestGrayscale.jsonp");

        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels2 : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels2 = ImageFilters.Grayscale(pixels2, GrayscaleType.AVERAGE);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels2, 0, 0);
            return $input;
        }, "ImageFiltersTestGrayscaleSecond.jsonp");
    }

    public __IgnoretestThreshold() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Threshold(pixels, 100);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestThreshold.jsonp");

        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Threshold(pixels, 110);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestThresholdSecond.jsonp");

        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Threshold(pixels, 200);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestThresholdThird.jsonp");
    }

    public __IgnoretestInvert() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Invert(pixels);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestInvert.jsonp");
    }

    public __IgnoretestBrightness() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Brightness(pixels, -200);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestBrightness.jsonp");

        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Brightness(pixels, 100);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestBrightnessSeconds.jsonp");
    }

    public __IgnoretestContrast() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Contrast(pixels, 50);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestContrast.jsonp");

        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Contrast(pixels, -50);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestContrastSecond.jsonp");
    }

    public __IgnoretestHorizontalFlip() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.HorizontalFlip(pixels);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestHorizontalFlip.jsonp");

    }

    public __IgnoretestVerticalFlip() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.VerticalFlip(pixels);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestVerticalFlip.jsonp");
    }

    public __IgnoretestConvolve() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Convolve(pixels, [200]);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestConvolve.jsonp");
    }

    public __IgnoretestGaussianBlur() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.GaussianBlur(pixels, 30);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestGaussian.jsonp");
    }

    public __IgnoretestLaplace() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Laplace(pixels);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestLaplace.jsonp");
    }

    public __IgnoretestSobel() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Sobel(pixels);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestSobel.jsonp");
    }

    public testSharp() : void {
        this.assertImage(($input : HTMLCanvasElement) : HTMLCanvasElement => {
            const sourceContext : CanvasRenderingContext2D = $input.getContext("2d");
            let pixels : ImageData = sourceContext.getImageData(0, 0, $input.width, $input.height);

            pixels = ImageFilters.Sharp(pixels);

            sourceContext.clearRect(0, 0, $input.width, $input.height);
            sourceContext.putImageData(pixels, 0, 0);
            return $input;
        }, "ImageFiltersTestSharp.jsonp");
    }

    private assertImage($actual : ($input : HTMLCanvasElement) => HTMLCanvasElement, $expected : string) : void {
        const input : HTMLImageElement = document.createElement("img");
        const output : HTMLCanvasElement = document.createElement("canvas");
        const imageId : string = "testImage_" + this.imageIndex;

        input.onload = () : void => {
            let data : HTMLCanvasElement = ImageTransform.ToCanvas(input);
            data = $actual(data);
            output.width = data.width;
            output.height = data.height;
            output.getContext("2d").drawImage(data, 0, 0);
            ElementManager.getElement(imageId).parentNode.appendChild(output);

            JsonpFileReader.Load("test/resource/data/Io/Oidis/Gui/RuntimeTests/ImageProcessor/" + $expected,
                ($data : string) : void => {
                    this.assertEquals(ImageTransform.getStream(output, true), $data);
                });
        };

        input.src = "test/resource/graphics/Io/Oidis/Gui/RuntimeTests/ImageProcessor/ChessBoard.png";

        const data : GuiElement = new GuiElement();
        data.Id(imageId);
        data.Add(<HTMLElement>input);
        Echo.Println(data.Draw(""));
        this.imageIndex++;
    }
}
/* dev:end */
