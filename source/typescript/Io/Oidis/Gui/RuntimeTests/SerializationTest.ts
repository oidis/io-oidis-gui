/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectEncoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectEncoder.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "../Enums/Events/EventType.js";
import { RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { AbstractGuiObject } from "../Primitives/AbstractGuiObject.js";
import { BaseGuiObject } from "../Primitives/BaseGuiObject.js";
import { BasePanel } from "../Primitives/BasePanel.js";
import { FormsObject } from "../Primitives/FormsObject.js";
import { GuiCommons } from "../Primitives/GuiCommons.js";
import { ElementManager } from "../Utils/ElementManager.js";
import { BasePanelViewerTest } from "./Primitives/BasePanelViewerTest.js";

class MockGuiCommons extends GuiCommons {
}

class MockBaseGuiObject extends BaseGuiObject {
}

class MockFormsObject extends FormsObject {
}

export class SerializationTest extends RuntimeTestRunner {

    public testGuiCommons() : void {
        const serialized : string = ObjectEncoder.Serialize(new MockGuiCommons());
        Echo.Println(StringUtils.HardWrap(serialized, 150));
        Echo.PrintCode(ObjectDecoder.Unserialize(serialized).Draw());
    }

    public testBaseGuiObject() : void {
        const serialized : string = ObjectEncoder.Serialize(new MockBaseGuiObject());
        Echo.Println(StringUtils.HardWrap(serialized, 150));
        Echo.PrintCode(ObjectDecoder.Unserialize(serialized).Draw());
    }

    public testAbstractGuiObject() : void {
        const object : AbstractGuiObject = new AbstractGuiObject();
        object.getEvents().setEvent(EventType.ON_CLICK, () : void => {
            Echo.Println("deserialized onlick");
        });
        const serialized : string = ObjectEncoder.Serialize(object);
        Echo.Println(StringUtils.HardWrap(serialized, 150));
        const deserialized : AbstractGuiObject = ObjectDecoder.Unserialize(serialized);
        deserialized.getEvents().setEvent(EventType.ON_MOUSE_OVER, ($args : EventArgs) : void => {
            ElementManager.setCssProperty($args.Owner(), "border", "1px solid green");
        });
        deserialized.getEvents().setEvent(EventType.ON_CLICK, () : void => {
            Echo.Println("not desirialized onlick");
        });
        deserialized.getEvents().setEvent(EventType.ON_MOUSE_OUT, ($args : EventArgs) : void => {
            ElementManager.setCssProperty($args.Owner(), "border", "0px solid red");
        });

        Echo.PrintCode(deserialized.Draw());
        Echo.Println(deserialized.Draw());
    }

    public testFormsObject() : void {
        const serialized : string = ObjectEncoder.Serialize(new MockFormsObject());
        Echo.Println(StringUtils.HardWrap(serialized, 150));
        Echo.PrintCode(ObjectDecoder.Unserialize(serialized).Draw());
    }

    public testBasePanel() : void {
        const serialized : string = ObjectEncoder.Serialize(new BasePanel());
        Echo.Println(StringUtils.HardWrap(serialized, 150));
        Echo.PrintCode(ObjectDecoder.Unserialize(serialized).Draw());
    }

    public testBasePanelViewer() : void {
        const serialized : string = ObjectEncoder.Serialize(new BasePanelViewerTest());
        Echo.Println(StringUtils.HardWrap(serialized, 150));
        Echo.PrintCode(ObjectDecoder.Unserialize(serialized).Show());
    }

    public testCrcCheck() : void {
        const testArray : ArrayList<any> = new ArrayList<any>();
        testArray.Add(null, "crc");
        testArray.Add(new ArrayList<any>(), "variables");
        testArray.getItem("variables").Add("string");
        testArray.getItem("variables").Add(true);
        testArray.getItem("variables").Add(123);
        testArray.getItem("variables").Add(new MockBaseGuiObject());
        testArray.getItem("variables").Add(($arg : string) : string => {
            return $arg;
        });
        testArray.Add(StringUtils.getCrc(ObjectEncoder.Serialize(testArray.getItem("variables"))), "crc");

        Echo.Println("<i>Serialized object</i>");
        let serialized : string = ObjectEncoder.Serialize(testArray);
        Echo.Println(StringUtils.HardWrap(serialized, 150) + StringUtils.NewLine());

        Echo.Println("<i>After deserialization</i>");
        const deserialized : ArrayList<any> = ObjectDecoder.Unserialize(serialized);
        Echo.Printf(deserialized);
        serialized = ObjectEncoder.Serialize(deserialized.getItem("variables"));

        Echo.Println("<i>Serialized variables after deserialization</i>");
        Echo.Println(StringUtils.NewLine() + StringUtils.HardWrap(serialized, 150));
        this.assertEquals(deserialized.getItem("crc"), StringUtils.getCrc(serialized));
    }

    public testAsyncSerialization() : void {
        const testArray : ArrayList<any> = new ArrayList<any>();
        testArray.Add(null, "crc");
        testArray.Add(new ArrayList<any>(), "variables");
        testArray.getItem("variables").Add("string");
        testArray.getItem("variables").Add(true);
        testArray.getItem("variables").Add(123);
        testArray.getItem("variables").Add(new MockBaseGuiObject());
        testArray.getItem("variables").Add(($arg : string) : string => {
            return $arg;
        });
        testArray.Add(StringUtils.getCrc(ObjectEncoder.Serialize(testArray.getItem("variables"))), "crc");

        let testStart : number = new Date().getTime();
        ObjectEncoder.Serialize(testArray, ($data : string) : void => {
            Echo.Printf(ObjectDecoder.Unserialize($data));
            Echo.Printf("process time: " + (new Date().getTime() - testStart) / 1000);

            testStart = new Date().getTime();
            ObjectDecoder.Unserialize($data, ($output : any) : void => {
                Echo.Printf("process time: " + (new Date().getTime() - testStart) / 1000);
                Echo.Printf($output);
            });
        });
    }
}
/* dev:end */
