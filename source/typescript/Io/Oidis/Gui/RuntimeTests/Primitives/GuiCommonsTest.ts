/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { GuiOptionType } from "../../Enums/GuiOptionType.js";
import { RuntimeTestRunner } from "../../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { BaseViewer } from "../../Primitives/BaseViewer.js";
import { BaseViewerArgs } from "../../Primitives/BaseViewerArgs.js";
import { GuiCommons } from "../../Primitives/GuiCommons.js";
import { Size } from "../../Structures/Size.js";
import { ElementManager } from "../../Utils/ElementManager.js";

class MockGuiCommons extends GuiCommons {
}

export class GuiCommonsTest extends RuntimeTestRunner {

    public __IgnoretestGuiElementClass() : void {
        const element : GuiCommons = new MockGuiCommons("testId2");
        Echo.Println("<div id=\"testId2\" class=\"testClass\">test element</div>");
        GuiCommons.GuiElementClass("testClass");
        this.assertEquals(GuiCommons.GuiElementClass(), "testClass");
    }

    public testEchoElement() : void {
        const element : GuiCommons = new MockGuiCommons();
        Echo.PrintCode(element.Draw());
    }

    public testConstructor() : void {
        const element : GuiCommons = new MockGuiCommons();
        Echo.Println("<div class=\"IoOidisGuiPrimitives\"> " +
            "<div id=\"GuiCommons1488787840472650_GuiWrapper\" guiType=\"GuiWrapper\"> " +
            "<div id=\"GuiCommons1488787840472650\" class=\"GuiCommons\" style=\"display: none;\"></div> </div> </div>");
        this.assertEquals(element.Enabled(true), true);
        Echo.Println("<div class=\"IoOidisGuiPrimitives\"> " +
            "<div id=\"GuiCommons1488787840472650_GuiWrapper\" guiType=\"GuiWrapper\"> " +
            "<div id=\"GuiCommons1488787840472650\" class=\"GuiCommons\" style=\"display: none;\"></div> </div> </div>");
    }

    public testgetGuiElementClass() : void {
        const element : GuiCommons = new MockGuiCommons();
        Echo.Println("<div class=\"IoOidisGuiPrimitives\"> " +
            "<div id=\"GuiCommons1488789078814758_GuiWrapper\" guiType=\"GuiWrapper\"> " +
            "<div id=\"GuiCommons1488789078814758\" class=\"GuiCommons\" style=\"display: none;\"></div> </div> </div>");
        this.assertEquals(ElementManager.getClassName(element), null);
    }

    public testVisible() : void {
        const element : GuiCommons = new MockGuiCommons("testId1");
        element.Visible(true);
        this.assertEquals(element.Visible(), true);
        Echo.PrintCode(element.Draw());
        const element2 : GuiCommons = new MockGuiCommons("testId5");
        element2.Visible(false);
        this.assertEquals(element2.Visible(), false);
        Echo.PrintCode(element2.Draw());
    }

    public __IgnoretestEnabled() : void {
        const element : GuiCommons = new MockGuiCommons("testId7");
        Echo.Println("<div id=\"testId7\" class=\"testClass\">test element</div>");
        element.Enabled(true);
        this.assertEquals(ElementManager.IsEnabled("testId7"), true);
    }

    public __IgnoretestgetGuiOptions() : void {
        const element : GuiCommons = new MockGuiCommons("testId2");
        element.DisableAsynchronousDraw();
        element.getGuiOptions().Add(GuiOptionType.ALL);
        this.assertEquals(element.getGuiOptions().Contains(GuiOptionType.ALL), true);
        this.assertEquals(element.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">" +
            "\r\n   <div id=\"testId2_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n       <div id=\"testId2\" class=\"GuiCommons\" style=\"display: none;\"></div>" +
            "\r\n   </div>" +
            "\r\n</div>");
        Echo.PrintCode(element.Draw());
        const element2 : GuiCommons = new MockGuiCommons("testId2");
        element2.getGuiOptions().Add(GuiOptionType.TOOL_TIP);
        this.assertEquals(element2.getGuiOptions(), GuiOptionType.TOOL_TIP);
        this.assertEquals(element2.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">" +
            "\r\n   <div id=\"testId2_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n       <div id=\"testId2\" class=\"GuiCommons\" style=\"display: block;\"></div>" +
            "\r\n   </div>" +
            "\r\n</div>");
        Echo.PrintCode(element.Draw());
    }

    public testParent() : void {
        const elementparent : GuiCommons = new MockGuiCommons();
        const element : GuiCommons = new MockGuiCommons();
        this.assertEquals(element.Parent(elementparent), elementparent);
    }

    public testInstanceOwner() : void {
        const baseviewerargs : BaseViewerArgs = new BaseViewerArgs();
        const baseviewer : BaseViewer = new BaseViewer(baseviewerargs);
        const element : GuiCommons = new MockGuiCommons();
        this.assertEquals(element.InstanceOwner(baseviewer), baseviewer);
    }

    public testInstancePath() : void {
        const element : GuiCommons = new MockGuiCommons("testId3");
        Echo.Println("<div id=\"testId8\" class=\"testClass\">test element</div>");
        this.assertEquals(element.InstancePath("rootElement/ChildElement/SubChildElement/testId3"),
            "rootElement/ChildElement/SubChildElement/testId3");
    }

    public testgetChildElements() : void {
        const element : GuiCommons = new MockGuiCommons("testId9");
        const element2 : GuiCommons = new MockGuiCommons("testId10");
        const element3 : GuiCommons = new MockGuiCommons("testId11");
        element.getChildElements().Add(element2);
        element.getChildElements().Add(element3);
        const child : ArrayList<GuiCommons> = new ArrayList<GuiCommons>();
        child.Add(element2);
        child.Add(element3);
        this.assertEquals(element.getChildElements().Equal(child), true);
    }

    public testgetSize() : void {
        const element : GuiCommons = new MockGuiCommons("testId12");
        const origin : Size = element.getSize();
        const golden : Size = new Size();
        this.assertEquals(origin.Width(), golden.Width());
        this.assertEquals(origin.Height(), golden.Height());
    }

    public testsetArg() : void {
        const element : GuiCommons = new MockGuiCommons("testId15");
        element.Id();
        element.StyleClassName("ToolTip");
        element.Enabled(true);
        element.Visible(true);
        Echo.PrintCode(element.Draw());

        this.assertEquals(
            ArrayList.ToArrayList(element.getArgs()).Equal(ArrayList.ToArrayList([
                {name: "Id", type: "Text", value: "testId15"},
                {name: "StyleClassName", type: "Text", value: "ToolTip"},
                {name: "Enabled", type: "Bool", value: true},
                {name: "Visible", type: "Bool", value: true},
                {name: "Width", type: "Number", value: 0},
                {name: "Height", type: "Number", value: 0},
                {name: "Top", type: "Number", value: 0},
                {name: "Left", type: "Number", value: 0}
            ])), true);
    }

    public testDraw() : void {
        const element : GuiCommons = new MockGuiCommons("testId14");
        Echo.Printf(JSON.stringify(element.Draw()));
        this.assertEquals(element.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">" +
            "\r\n   <div id=\"testId14_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"testId14\" class=\"GuiCommons\" style=\"display: none;\"></div>" +
            "\r\n   </div>" +
            "\r\n</div>");
    }
}

/* dev:end */
