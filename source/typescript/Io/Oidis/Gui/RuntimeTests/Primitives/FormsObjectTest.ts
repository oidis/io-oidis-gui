/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "../../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { FormsObject } from "../../Primitives/FormsObject.js";

class MockFormsObject extends FormsObject {
}

export class FormsObjectTest extends RuntimeTestRunner {

    public testgetEvents() : void {
        const formsobject : FormsObject = new MockFormsObject();
        const handler : any = () : void => {
            // test event handler
        };
        formsobject.getEvents().setEvent("test", handler);
        this.assertEquals(formsobject.getEvents().Exists("test"), true);
    }

    public testNotification() : void {
        const formsobject : FormsObject = new MockFormsObject();
        this.assertEquals(formsobject.Notification(), (<any>FormsObject).notification);
    }

    public testEnabled() : void {
        const formsobject : FormsObject = new MockFormsObject();
        this.assertEquals(formsobject.Enabled(), true, "validate enabled");
        const formsobject2 : FormsObject = new MockFormsObject();
        this.assertEquals(formsobject2.Enabled(false), false);
    }

    public testError() : void {
        const formsobject : FormsObject = new MockFormsObject();
        this.assertEquals(formsobject.Error(true), true);
        const formsobject2 : FormsObject = new MockFormsObject();
        this.assertEquals(formsobject2.Error(false), false);
    }

    public testTabIndex() : void {
        const formsobject : FormsObject = new MockFormsObject();
        this.assertEquals(formsobject.TabIndex(5), 5);
    }

    public testgetSelectorEvents() : void {
        const formsobject : FormsObject = new MockFormsObject();
        Echo.PrintCode(JSON.stringify(formsobject));
        const handler : any = () : void => {
            // test event handler
        };
        formsobject.getEvents().setEvent("test", handler);
        this.assertEquals(formsobject.getEvents().Exists("test"), true);
    }
}
/* dev:end */
