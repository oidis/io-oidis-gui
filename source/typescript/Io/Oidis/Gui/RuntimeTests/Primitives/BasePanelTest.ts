/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { PanelContentType } from "../../Enums/PanelContentType.js";
import { RuntimeTestRunner } from "../../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { BasePanel } from "../../Primitives/BasePanel.js";
import { BasePanelViewerArgs } from "../../Primitives/BasePanelViewerArgs.js";

export class BasePanelTest extends RuntimeTestRunner {

    public testgetEvents() : void {
        const basepanel : BasePanel = new BasePanel();
        const handler : any = () : void => {
            // test event handler
        };
        basepanel.getEvents().setEvent("test", handler);
        this.assertEquals(basepanel.getEvents().Exists("test"), true);
    }

    public testContentType() : void {
        const basepanel : BasePanel = new BasePanel();
        Echo.PrintCode(JSON.stringify(basepanel));
        this.assertEquals(basepanel.ContentType(PanelContentType.WITH_ELEMENT_WRAPPER),
            PanelContentType.WITH_ELEMENT_WRAPPER);
        Echo.Println("<i>Panel with out element wrapper</i>");
    }

    public __IgnoretestAddChild() : void {
        const basepanel : BasePanel = new BasePanel();
        const basepanel2 : BasePanel = new BasePanel();
        const basepanel3 : BasePanel = new BasePanel();
        basepanel.AddChild(basepanel2, "test");
        basepanel.AddChild(basepanel3, "test2");
        this.assertEquals(basepanel.getChildElements(), new ArrayList());
    }

    public testValue() : void {
        const basepanel : BasePanel = new BasePanel();
        const viewerargs : BasePanelViewerArgs = new BasePanelViewerArgs();
        this.assertEquals(basepanel.Value("viewerargs"), null);
    }

    public testScrollable() : void {
        const basepanel : BasePanel = new BasePanel();
        this.assertEquals(basepanel.Scrollable(true), true);
    }

    public testWidth() : void {
        const basepanel : BasePanel = new BasePanel();
        this.assertEquals(basepanel.Width(8), 8);
        Echo.PrintCode(JSON.stringify(basepanel.Draw()));
    }

    public testHeight() : void {
        const basepanel : BasePanel = new BasePanel();
        basepanel.Height(16);
        this.assertEquals(basepanel.Height(), 16);
    }

    public testGetScrollTop() : void {
        const basepanel : BasePanel = new BasePanel();
        this.assertEquals(basepanel.getScrollTop(), -1);
    }

    public testGetScrollLeft() : void {
        const basepanel : BasePanel = new BasePanel();
        this.assertEquals(basepanel.getScrollLeft(), -1);
    }

    public testDraw() : void {
        const basepanel : BasePanel = new BasePanel("basePanelId");
        basepanel.DisableAsynchronousDraw();
        Echo.Printf(JSON.stringify(basepanel.Draw()));
        this.assertEquals(basepanel.Draw(),
            "\r\n<div id=\"basePanelId_PanelEnvelop\" class=\"Panel\" style=\"display: block;\">" +
            "\r\n    <div class=\"IoOidisGuiPrimitives\">" +
            "\r\n       <div id=\"basePanelId_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n          <div id=\"basePanelId\" class=\"BasePanel\" style=\"display: block;\">" +
            "\r\n             <div guiType=\"Panel\">" +
            "\r\n                <div id=\"basePanelId_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">" +
            "\r\n                   <div id=\"basePanelId_PanelContent\" class=\"Content\">" +
            "\r\n                      <div style=\"clear: both;\"></div>" +
            "\r\n                   </div>" +
            "\r\n                </div>" +
            "\r\n             </div>" +
            "\r\n          </div>" +
            "\r\n       </div>" +
            "\r\n    </div>" +
            "\r\n</div>");
    }
}
/* dev:end */
