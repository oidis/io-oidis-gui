/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { GuiCommonsArgType } from "../../Enums/GuiCommonsArgType.js";
import { RuntimeTestRunner } from "../../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IGuiCommonsArg } from "../../Interfaces/Primitives/IGuiCommonsArg.js";
import { BaseGuiObject } from "../../Primitives/BaseGuiObject.js";
import { BasePanel } from "../../Primitives/BasePanel.js";

class MockBaseGuiObject extends BaseGuiObject {
}

export class BaseGuiObjectTest extends RuntimeTestRunner {

    public testTitle() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject();
        this.assertEquals(basegui.Title(), (<any>BaseGuiObject).title);
    }

    public testChanged() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject();
        this.assertEquals(basegui.Changed(), false);
    }

    public testValue() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject();
        this.assertEquals(basegui.Value(), null);
    }

    public __IgnoretestsetArgs() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject();
        basegui.Id();
        basegui.StyleClassName("ToolTip");
        basegui.Enabled(true);
        basegui.Title();
        this.assertEquals(basegui.getArgs(),
            {
                completed     : false,
                enabled       : true,
                guiId         : "BaseGuiObject1488986926575443",
                innerHtmlMap  : null,
                loaded        : false,
                owner         : null,
                parent        : null,
                prepared      : false,
                styleClassName: "ToolTip"
            });
    }

    public testsetArgs20() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject();
        basegui.Title();
        const basepanel : BasePanel = new BasePanel();
        basegui.Value(basepanel);
        basegui.setArg(<IGuiCommonsArg>{
            name : "Title",
            type : GuiCommonsArgType.TEXT,
            value: true
        }, true);

        basegui.setArg(<IGuiCommonsArg>{
            name : "Value",
            type : GuiCommonsArgType.TEXT,
            value: false
        }, false);
    }
}
/* dev:end */
