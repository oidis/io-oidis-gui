/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BasePanel } from "../../Primitives/BasePanel.js";
import { BasePanelViewer } from "../../Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "../../Primitives/BasePanelViewerArgs.js";

export class BasePanelViewerTest extends BasePanelViewer {

    constructor($args? : BasePanelViewerArgs) {
        super($args);
        this.setInstance(new BasePanel());
    }

    public getInstance() : BasePanel {
        return <BasePanel>super.getInstance();
    }

    protected normalImplementation() : void {
        const args : BasePanelViewerArgs = this.ViewerArgs();
        if (!ObjectValidator.IsEmptyOrNull(args) && !ObjectValidator.IsEmptyOrNull(this.getInstance())) {
            // handle viewer args
        }
    }

    protected testImplementation() : string {
        const args : BasePanelViewerArgs = new BasePanelViewerArgs();
        args.Visible(true);
        args.AsyncEnabled(false);
        this.ViewerArgs(args);
        this.normalImplementation();

        return "test append";
    }
}
/* dev:end */
