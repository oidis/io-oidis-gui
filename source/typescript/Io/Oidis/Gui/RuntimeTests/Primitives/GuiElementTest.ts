/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { RuntimeTestRunner } from "../../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { BasePanelViewer } from "../../Primitives/BasePanelViewer.js";
import { GuiCommons } from "../../Primitives/GuiCommons.js";
import { GuiElement } from "../../Primitives/GuiElement.js";
import { ElementManager } from "../../Utils/ElementManager.js";

class MockGuiCommons extends GuiCommons {
}

export class GuiElementTest extends RuntimeTestRunner {

    public testConstructor() : void {
        const element : GuiElement = new GuiElement();
    }

    public testId() : void {
        Echo.Println("<div id=\"GuiElement888999\" type=\"text\">Multiple</div>");
        const element : GuiElement = new GuiElement();
        element.Id("GuiElement888999");
        this.assertEquals(ElementManager.Exists("GuiElement888999"), true);
    }

    public testGuiTypeTag() : void {
        Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" >Multiple</div>");
        const element : GuiElement = new GuiElement();
        element.GuiTypeTag("GuiWraper");
    }

    public testStyleClassName() : void {
        Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\"</div>");
        const element : GuiElement = new GuiElement();
        element.StyleClassName("testClassName");
        Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\"</div>");
    }

    public testVisible() : void {
        const element : GuiElement = new GuiElement();
        element.Visible(true);
        Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\"</div>");
        this.assertEquals(ElementManager.IsVisible("GuiElement888999"), true);
    }

    public testWidth() : void {
        const element : GuiElement = new GuiElement();
        element.Width(50);
        Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\" style=\"width: \"50\";\"</div>");
        this.assertEquals(ElementManager.getElement("GuiElement888999").offsetWidth, 1863);
    }

    public testHeight() : void {
        const element : GuiElement = new GuiElement();
        element.Height(100);
        Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\" style=\"width: 50px; height: 100px\"" +
            "</div>");
        this.assertEquals(ElementManager.getElement("GuiElement888999").offsetHeight, 23);
    }

    public testsetAttribute() : void {
        const element : GuiElement = new GuiElement();
        element.setAttribute("display", "none");
        element.setAttribute("color", "blue");
        this.assertEquals(element.getAttributes().getFirst(), "none", "validate that attribute exists on first position");
    }

    public testAdd() : void {
        const element : GuiElement = new GuiElement();
        const elementgui : GuiCommons = new MockGuiCommons();
        const elementviewer : BasePanelViewer = new BasePanelViewer();
        element.Add(elementgui);
        element.Add(elementviewer);
        this.assertEquals(element.getChildElement(1), elementviewer, "");
    }

    public testgetChildElement() : void {
        Echo.Println("<div id=\"GuiElement888999\" guiType=\"GuiWraper\" class=\"testClassName\" style=\"width: 50px; height: 100px\"" +
            "</div>");
        const element : GuiElement = new GuiElement();
        const elementChild : GuiElement = new GuiElement();
        const gui : GuiCommons = new MockGuiCommons();
        const viewer : BasePanelViewer = new BasePanelViewer();
        element.Add(elementChild);
        element.Add(gui);
        element.Add(viewer);
        element.getChildElement(1);
        this.assertEquals(element.getChildElement(0), elementChild, "validate, that child exists");
    }
}
/* dev:end */
