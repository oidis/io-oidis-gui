/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { BasePanelViewerArgs } from "../../Primitives/BasePanelViewerArgs.js";
import { BasePanelViewerTest } from "./BasePanelViewerTest.js";

export class AsyncBasePanelViewerTest extends BasePanelViewerTest {

    protected normalImplementation() : void {
        if (!this.getInstance().IsLoaded()) {
            const args : BasePanelViewerArgs = new BasePanelViewerArgs();
            args.Visible(true);
            args.AsyncEnabled(true);
            this.ViewerArgs(args);
        }
        super.normalImplementation();
    }

    protected testImplementation() : string {
        this.normalImplementation();
        return "async test append";
    }
}
/* dev:end */
