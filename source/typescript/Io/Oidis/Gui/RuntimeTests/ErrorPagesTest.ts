/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { FatalError } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/FatalError.js";
import { RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";

export class ErrorPagesTest extends RuntimeTestRunner {

    public testExceptionsManager() : void {
        this.addButton("native exception", () : void => {
            throw new Error("native error");
        });
        this.addButton("custom exception", () : void => {
            ExceptionsManager.Throw(ErrorPagesTest.ClassName(), "my error is here");
        });
        this.addButton("fatal error", () : void => {
            ExceptionsManager.Throw(ErrorPagesTest.ClassName(), new FatalError("my fatal error is here"));
        });
    }

    public testHttpManager() : void {
        this.addButton("http 301", () : void => {
            this.getHttpManager().Return301Moved("/testFile/location");
        });
        this.addButton("http 403", () : void => {
            this.getHttpManager().Return403Forbidden();
        });
        this.addButton("http 404", () : void => {
            this.getHttpManager().Return404NotFound();
        });
    }
}
/* dev:end */
