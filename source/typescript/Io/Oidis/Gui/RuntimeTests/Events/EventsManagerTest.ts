/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "../../Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../Enums/Events/GeneralEventOwner.js";
import { KeyEventArgs } from "../../Events/Args/KeyEventArgs.js";
import { MoveEventArgs } from "../../Events/Args/MoveEventArgs.js";
import { RuntimeTestRunner } from "../../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { KeyEventHandler } from "../../Utils/KeyEventHandler.js";

export class EventsManagerTest extends RuntimeTestRunner {

    public IntegralTest() : void {
        this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.ON_LOAD,
            () : void => {
                Echo.Println("Window has been loaded");
            });
        this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_LOAD,
            () : void => {
                Echo.Println("Body has been loaded");
            });

        let undloadFired : boolean = false;
        this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.BEFORE_REFRESH,
            ($args : EventArgs) : void => {
                if (!undloadFired) {
                    undloadFired = true;
                    $args.PreventDefault();
                    Echo.Println("Window is undloading" + StringUtils.NewLine());
                }
            });

        this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST,
            () : void => {
                Echo.Println("Request has been changed");
            });

        this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.ON_RESIZE,
            () : void => {
                Echo.Println("Window has been resized");
            });

        this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_CLICK,
            () : void => {
                Echo.Println("Body has been clicked");
            });

        this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_DOWN,
            ($args : KeyEventArgs) : void => {
                Echo.Println("Body key down - " + $args.getKeyCode() + " (" + $args.NativeEventArgs().key + ")");
            });
        this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_PRESS,
            ($args : KeyEventArgs) : void => {
                Echo.Println("Body key press - " + $args.getKeyCode() + " (" + $args.NativeEventArgs().key + ")");

                Echo.Println("Is navigation key: ");
                Echo.Printf(KeyEventHandler.IsNavigate($args.NativeEventArgs()));

                Echo.Println("Is integer or navigate: ");
                Echo.Printf(KeyEventHandler.IsInteger($args.NativeEventArgs()));
            });
        this.getEventsManager().setEvent(GeneralEventOwner.BODY, EventType.ON_KEY_UP,
            ($args : KeyEventArgs) : void => {
                Echo.Println("Body key up - " + $args.getKeyCode() + " (" + $args.NativeEventArgs().key + ")");
            });

        this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
            ($args : MoveEventArgs) : void => {
                Echo.Println("distance X: " + $args.getDistanceX());
                Echo.Println("distance Y: " + $args.getDistanceY());
            });
    }
}
/* dev:end */
