/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { EventType } from "../../Enums/Events/EventType.js";
import { ElementEventsManager } from "../../Events/ElementEventsManager.js";
import { RuntimeTestRunner } from "../../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { ElementManager } from "../../Utils/ElementManager.js";
import { WindowManager } from "../../Utils/WindowManager.js";

export class ElementEventsManagerTest extends RuntimeTestRunner {

    public testSubscribe() : void {
        LogIt.Debug(this.getEventsManager().toString());
        Echo.Println("<div id=\"testId0\">test click element</div>");
        Echo.Println("<div id=\"testId1\">test hover element</div>");
        Echo.Println("<div id=\"testId3\">test exception element</div>");

        const manager1 : ElementEventsManager = new ElementEventsManager("testId0");
        manager1.setEvent(EventType.ON_CLICK, () : void => {
            Echo.Println("Element has been clicked");
        });
        manager1.Subscribe();

        const manager2 : ElementEventsManager = new ElementEventsManager();
        manager2.setEvent(EventType.ON_MOUSE_OVER, () : void => {
            ElementManager.setCssProperty("testId1", "border", "1px solid red");
        });
        manager2.setEvent(EventType.ON_MOUSE_OUT, () : void => {
            ElementManager.setCssProperty("testId1", "border", "0px solid red");
        });
        manager2.Subscribe("testId1");
        manager2.Subscribe("testId#");

        const manager3 : ElementEventsManager = new ElementEventsManager("testId3");
        manager3.setEvent(EventType.ON_CLICK, () : void => {
            ExceptionsManager.Throw(ElementEventsManagerTest.ClassName(), "my error is here");
        });
        manager3.Subscribe();

        const manager4 : ElementEventsManager = new ElementEventsManager();
        manager4.setEvent(EventType.ON_DOUBLE_CLICK, () : void => {
            Echo.Println("Window has been double clicked");
            Echo.Println(WindowManager.ViewHTMLCode());
        });
        manager4.Subscribe();
        LogIt.Debug(this.getEventsManager().toString());
    }
}
/* dev:end */
