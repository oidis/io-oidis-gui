/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/* dev:start */
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { DirectionType } from "../Enums/DirectionType.js";
import { RuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { AbstractGuiObject } from "../Primitives/AbstractGuiObject.js";
import { ElementManager } from "../Utils/ElementManager.js";

export class PerformanceTest extends RuntimeTestRunner {

    public __IgnoreThreadTest() : void {
        for (let index : number = 0; index < 500; index++) {
            const id : string = "testId" + index;
            Echo.Println("<div id=\"" + id + "\" style=\"background-color: red;\">test element</div>");
            ElementManager.setOpacity(id, 0);
            ElementManager.ChangeOpacity(id, DirectionType.UP, 10);
        }
    }

    public ObjectTest() : void {
        const registered : string[] = [];
        for (let index : number = 0; index < 5000; index++) {
            const element : AbstractGuiObject = new AbstractGuiObject();
            if (registered.indexOf(element.Id()) === -1) {
                registered.push(element.Id());
            } else {
                Echo.Printf("Duplicity found!!! at index: {0}", index);
                break;
            }
        }
    }
}
/* dev:end */
