/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionCode } from "@io-oidis-commons/Io/Oidis/Commons/Enums/ExceptionCode.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { ErrorPageException } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/ErrorPageException.js";
import { Exception } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { BaseErrorPage } from "./BaseErrorPage.js";

export class ExceptionErrorPage extends BaseErrorPage {
    private fatalErrorExists : boolean;
    private exceptionsList : ArrayList<Exception>;

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        if (ObjectValidator.IsEmptyOrNull(this.fatalErrorExists)) {
            if ($GET.KeyExists(HttpRequestConstants.EXCEPTION_TYPE)) {
                this.fatalErrorExists =
                    StringUtils.ToInteger($GET.getItem(HttpRequestConstants.EXCEPTION_TYPE)) === ExceptionCode.FATAL_ERROR;
            } else {
                this.fatalErrorExists = false;
            }
        }
        if (ObjectValidator.IsEmptyOrNull(this.exceptionsList)) {
            if ($POST.KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
                this.exceptionsList = $POST.getItem(HttpRequestConstants.EXCEPTIONS_LIST);
            } else {
                this.exceptionsList = new ArrayList<Exception>();
            }
        }
    }

    protected getExceptionsList() : ArrayList<Exception> {
        return this.exceptionsList;
    }

    protected isFatalError() : boolean {
        return this.fatalErrorExists;
    }

    protected getPageTitle() : string {
        if (this.isFatalError()) {
            return "Oidis - Fatal Error";
        } else {
            return "Oidis - Exception";
        }
    }

    protected getMessageHeader() : string {
        if (this.isFatalError()) {
            return "FATAL Error!";
        } else {
            return "Oops, something went wrong...";
        }
    }

    protected getMessageBody() : string {
        LogIt.Debug("executed GUI error page");
        let errorsOutput : string = "";
        const EOL : string = StringUtils.NewLine(false);

        this.getExceptionsList().foreach(($exception : Exception) : void => {
            errorsOutput += "thrown by: ";
            errorsOutput += "<b>" + $exception.Owner() + "</b>: ";
            errorsOutput += StringUtils.NewLine();
            errorsOutput += $exception.ToString() + StringUtils.NewLine();
            LogIt.Error("thrown by: " + $exception.Owner() + ", message: " + $exception.ToString("", false));
        });
        const echo : string = this.getEchoOutput();
        if (this.getEnvironmentArgs().HtmlOutputAllowed()) {
            LogIt.Debug("echo before exception: " + StringUtils.NewLine(false) +
                StringUtils.StripTags(StringUtils.Replace(echo, StringUtils.NewLine(), StringUtils.NewLine(false))));
        } else {
            ExceptionsManager.Clear();
        }

        return errorsOutput + EOL +
            "       <div class=\"Echo\">" + EOL +
            "           <span onclick=\"" +
            "document.getElementById('exceptionEcho').style.display=" +
            "document.getElementById('exceptionEcho').style.display===" +
            "'block'?'none':'block';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
            "Echo output before exception" +
            "           </span>" + EOL +
            "           <div id=\"exceptionEcho\" class=\"Text\" style=\"border: 0 solid black; display: none;\">" + EOL +
            echo + EOL +
            "           </div>" + StringUtils.NewLine() +
            "           <a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; " +
            "text-decoration: none;\" " +
            "href=\"#" + this.createLink("/about/Cache") + "\">Cache info</a>" + EOL +
            "       </div>";
    }

    protected resolver() : void {
        try {
            super.resolver();
        } catch (ex) {
            try {
                ExceptionsManager.Throw(this.getClassName(), ex);
            } catch (ex) {
                // register error page self-error and continue with processing of general exception manager
            }
            try {
                ExceptionsManager.Throw(this.getClassName(), new ErrorPageException(this.getClassName() + " self error."));
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }
    }
}
