/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseErrorPage as Parent } from "@io-oidis-commons/Io/Oidis/Commons/ErrorPages/BaseErrorPage.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { StaticPageContentManager } from "../Utils/StaticPageContentManager.js";

export abstract class BaseErrorPage extends Parent {

    protected getFaviconSource() : string {
        return "resource/graphics/icon.ico";
        /// TODO: replace error icons by icons with Oidis branding
        // return "resource/graphics/Io/Oidis/Gui/ErrorIcon.ico";
    }

    protected getPageTitle() : string {
        return "Oidis - Error";
    }

    protected getMessageHeader() : string {
        return "Error";
    }

    protected getMessageBody() : string {
        return "Something has went wrong";
    }

    protected getPageBody() : string {
        const EOL : string = StringUtils.NewLine(false);
        let output : string = "";
        /// TODO: use card from BT and some updated design
        output +=
            "<div class=\"Exception\">" + EOL +
            "   <h1>" + this.getMessageHeader() + "</h1>" + EOL +
            "   <div class=\"Message\">" + EOL +
            this.getMessageBody() + EOL +
            "   </div>" + EOL +
            "</div>";

        return output;
    }

    protected resolver() : void {
        const title : string = this.getPageTitle();
        const body : string = this.getPageBody();

        try {
            ExceptionsManager.ThrowExit();
        } catch (ex) {
            // stop all background execution, but continue in execution of current resolver
        }
        Echo.ClearAll();
        StaticPageContentManager.Clear();
        if (!ObjectValidator.IsEmptyOrNull(this.getFaviconSource())) {
            StaticPageContentManager.FaviconSource(this.getFaviconSource());
        }
        StaticPageContentManager.Title(title);
        StaticPageContentManager.BodyAppend(body);
        StaticPageContentManager.Draw();
    }
}
