/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseErrorPage } from "./BaseErrorPage.js";

export class Http403ForbiddenPage extends BaseErrorPage {

    // protected getFaviconSource() : string {
    //     return "resource/graphics/Io/Oidis/Gui/ForbiddenIcon.ico";
    // }

    protected getPageTitle() : string {
        return "Oidis - HTTP 403";
    }

    protected getMessageHeader() : string {
        return "HTTP status 403";
    }

    protected getMessageBody() : string {
        return "Access denied.";
    }
}
