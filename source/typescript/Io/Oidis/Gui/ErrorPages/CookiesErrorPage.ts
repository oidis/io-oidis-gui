/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseErrorPage } from "./BaseErrorPage.js";

export class CookiesErrorPage extends BaseErrorPage {

    protected getPageTitle() : string {
        return "Oidis - Cookies Disabled";
    }

    protected getMessageHeader() : string {
        return "Cookies are disabled";
    }

    protected getMessageBody() : string {
        return "This library requires enabled Cookies in the browser for ability to " +
            "store persistence data. See link below for more information: " + StringUtils.NewLine() +
            "<a href=\"http://www.wikihow.com/Enable-Cookies-in-Your-Internet-Web-Browser\" " +
            "target=\"_blank\">How to enable Cookies?</a>";
    }
}
