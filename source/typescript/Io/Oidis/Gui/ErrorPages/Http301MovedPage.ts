/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { BaseErrorPage } from "./BaseErrorPage.js";

export class Http301MovedPage extends BaseErrorPage {
    private link : string;

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        this.link = "";
        if ($POST.KeyExists(HttpRequestConstants.HTTP301_LINK)) {
            this.link = $POST.getItem(HttpRequestConstants.HTTP301_LINK);
        }
    }

    // protected getFaviconSource() : string {
    //     return "resource/graphics/Io/Oidis/Gui/MovedIcon.ico";
    // }

    protected getPageTitle() : string {
        return "Oidis - HTTP 301";
    }

    protected getMessageHeader() : string {
        return "HTTP status 301";
    }

    protected getMessageBody() : string {

        return "File has been moved. New address is:" + StringUtils.NewLine() +
            "<a href=\"#" + this.link + "\">" + this.getRequest().getHostUrl() + "#" + this.link + "</a>";
    }
}
