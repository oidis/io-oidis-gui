/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpRequestConstants } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpRequestConstants.js";
import { HttpManager as Parent } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpManager.js";
import { BrowserErrorPage } from "../ErrorPages/BrowserErrorPage.js";
import { CookiesErrorPage } from "../ErrorPages/CookiesErrorPage.js";
import { ExceptionErrorPage } from "../ErrorPages/ExceptionErrorPage.js";
import { Http301MovedPage } from "../ErrorPages/Http301MovedPage.js";
import { Http403ForbiddenPage } from "../ErrorPages/Http403ForbiddenPage.js";
import { Http404NotFoundPage } from "../ErrorPages/Http404NotFoundPage.js";
import { PersistenceManager } from "./Resolvers/PersistenceManager.js";

/**
 * HttpManager class provides handling of current http request
 */
export class HttpManager extends Parent {
    protected registerGeneralResolvers() : void {
        super.registerGeneralResolvers();
        this.OverrideResolver("/ServerError/Cookies", CookiesErrorPage);
        this.OverrideResolver("/ServerError/Browser", BrowserErrorPage);
        this.OverrideResolver("/ServerError/Http/Moved", Http301MovedPage);
        this.OverrideResolver("/ServerError/Http/Forbidden", Http403ForbiddenPage);
        this.OverrideResolver("/ServerError/Http/NotFound", Http404NotFoundPage);
        this.OverrideResolver("/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}", ExceptionErrorPage);
        this.RegisterResolver("/PersistenceManager", PersistenceManager);
    }
}
