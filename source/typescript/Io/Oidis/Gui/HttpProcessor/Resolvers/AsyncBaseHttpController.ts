/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { HttpRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { HttpRequestParser } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpRequestParser.js";
import { BaseHttpResolver } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { isBrowser } from "@io-oidis-commons/Io/Oidis/Commons/Utils/EnvironmentHelper.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanel } from "../../Bootstrap/Primitives/BasePanel.js";
import { BasePanelController } from "../../Bootstrap/Primitives/BasePanelController.js";
import { BaseViewer } from "../../Bootstrap/Primitives/BaseViewer.js";
import { GuiCommons } from "../../Bootstrap/Primitives/GuiCommons.js";
import { GeneralEventOwner } from "../../Enums/Events/GeneralEventOwner.js";
import { Loader } from "../../Loader.js";
import { ElementManager } from "../../Utils/ElementManager.js";
import { StaticPageContentManager } from "../../Utils/StaticPageContentManager.js";

export class AsyncBaseHttpController extends BaseHttpResolver {
    private static rendered : AsyncBaseHttpController;
    private pageInstance : BasePanel;
    private pageTitle : string;
    private pageCache : any;
    private faviconSource : string;
    private language : LanguageType;
    private processing : boolean;
    private parallelProcessing : boolean;
    private loaderProcess : number;

    public static getInstance<TController>() : TController {
        const singleton : string = "__Singleton." + this.ClassName();
        if (this.hasOwnProperty(singleton)) {
            return this[singleton];
        }
        return this[singleton] = <any>(new this());
    }

    protected constructor() {
        super();

        this.pageInstance = null;
        this.pageTitle = this.getEnvironmentArgs().getProjectName() + " v" + this.getEnvironmentArgs().getProjectVersion();
        this.faviconSource = "resource/graphics/icon.ico";
        this.language = LanguageType.EN;

        this.processing = false;
        this.parallelProcessing = false;
        this.loaderProcess = null;
    }

    public InstanceOwner() : BasePanel {
        return this.pageInstance;
    }

    public PreparePageContent() : void {
        try {
            this.setPageTitle(StringUtils.Format(this.pageTitle, this.getEnvironmentArgs().getProjectVersion()));
        } catch (ex) {
            LogIt.Error(ex);
        }
        this.setLanguage(this.language);
        this.setPageIconSource(this.faviconSource);
    }

    public ParallelProcessing($value? : boolean) : boolean {
        return this.parallelProcessing = Property.Boolean(this.parallelProcessing, $value);
    }

    public RequestArgs($value? : HttpRequestEventArgs) : HttpRequestEventArgs {
        const value : HttpRequestEventArgs = super.RequestArgs($value);
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            const reflection : Reflection = Reflection.getInstance();
            const properties : string[] = this.getProperties();
            for (const name of properties) {
                if (this.hasOwnProperty(name) && reflection.IsMemberOf(this[name], BasePanelController)) {
                    this[name].RequestArgs(value);
                }
            }
        }
        return value;
    }

    public Process() : void {
        if (!isBrowser) {
            throw new Error("Unsupported backend call of frontend controller");
        }
        if (!this.processing && !this.parallelProcessing || this.parallelProcessing) {
            if (this.parallelProcessing && this.processing) {
                LogIt.Warning("Executed parallel processing of {0}", this.getClassName());
            }
            try {
                const renderTarget : any = GuiCommons.getRenderTarget();
                if (!this.browserValidator()) {
                    LogIt.Error("Unsupported browser. User Agent: " + this.getRequest().getUserAgent());
                    this.getHttpManager().ReloadTo("/ServerError/Browser", null, true);
                    this.stop();
                } else if (this.isRendered(renderTarget)) {
                    if (!ObjectValidator.IsEmptyOrNull(renderTarget) && renderTarget.isViewerRunner) {
                        delete renderTarget.isViewerRunner;
                        if (!ObjectValidator.IsEmptyOrNull(BaseViewer.getTestRenderTarget())) {
                            const content : HTMLElement = ElementManager.getElement("Content", true);
                            content.appendChild(renderTarget);
                            content.removeChild(content.firstElementChild);
                            BaseViewer.getTestRenderTarget = () : HTMLElement => {
                                return null;
                            };
                        }
                    }
                    AsyncBaseHttpController.rendered = this;

                    (new Promise<boolean>(($resolve : ($status : boolean) => void, $reject : ($error : Error) => void) => {
                        try {
                            let resolverStatus : boolean = false;
                            this.canResolve(($status : boolean) : void => {
                                resolverStatus = $status;
                            }).then(() : void => {
                                $resolve(resolverStatus);
                            }).catch(($ex : Error) => {
                                $reject($ex);
                            });
                        } catch (ex) {
                            $reject(ex);
                        }
                    })).then(($status : boolean) : void => {
                        if ($status) {
                            this.resolver();
                        }
                    }).catch((ex) : void => {
                        this.onError(ex);
                    });
                } else {
                    this.PreparePageContent();
                    // eslint-disable-next-line prefer-spread
                    this.onSuccess.apply(this, this.getProcessArgs()).catch(async ($error : Error) : Promise<void> => {
                        await this.onError($error);
                    });
                }
            } catch (ex) {
                this.onError(ex);
            }
        } else {
            LogIt.Warning("Skipped parallel processing of {0}", this.getClassName());
        }
    }

    protected setInstanceOwner($value : BasePanel) : void {
        if (!ObjectValidator.IsEmptyOrNull($value) && $value.IsMemberOf(BasePanel)) {
            this.pageInstance = $value;
        }
    }

    protected setPageTitle($value : string) : void {
        this.pageTitle = Property.String(this.pageTitle, $value);
        StaticPageContentManager.Title(this.pageTitle);
    }

    protected setPageIconSource($value : string) : void {
        this.faviconSource = Property.String(this.faviconSource, $value);
        StaticPageContentManager.FaviconSource(this.faviconSource);
    }

    protected setLanguage($value : LanguageType) : void {
        this.language = Property.EnumType(this.language, $value, LanguageType);
        StaticPageContentManager.Language(this.language);
    }

    protected getLanguage() : LanguageType {
        return this.language;
    }

    protected loadResources() : void {
        this.PreparePageContent();
        this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_LOAD);
    }

    protected isRendered($target : any) : boolean {
        return AsyncBaseHttpController.rendered !== this ||
            ObjectValidator.IsEmptyOrNull($target) ||
            !ObjectValidator.IsEmptyOrNull($target) && $target.isViewerRunner;
    }

    protected async canResolve($resolve : ($status : boolean) => void) : Promise<void> {
        $resolve(true);
    }

    protected getProcessArgs() : any[] {
        return [this.InstanceOwner()];
    }

    protected resolver() : void {
        this.processing = true;
        this.getEventsManager().setEvent(this.getClassName(), EventType.ON_LOAD, async () : Promise<void> => {
            try {
                const renderUI : boolean = ObjectValidator.IsEmptyOrNull(this.pageCache);
                const instance : BasePanel = this.InstanceOwner();
                let holder : HTMLElement = GuiCommons.getRenderTarget();
                if (ObjectValidator.IsEmptyOrNull(holder)) {
                    const findHolder : any = async ($index : number = 0) : Promise<void> => {
                        await new Promise<void>(($resolve) : void => {
                            setTimeout($resolve, 250);
                        });
                        holder = GuiCommons.getRenderTarget();
                        if (ObjectValidator.IsEmptyOrNull(holder) && $index < 5) {
                            findHolder($index + 1);
                        }
                    };
                    await findHolder();
                }
                if (!ObjectValidator.IsEmptyOrNull(holder)) {
                    if (renderUI) {
                        (<any>StaticPageContentManager).init();
                        ElementManager.setInnerHtml(holder, "");
                        this.loaderProcess = this.getEventsManager().FireAsynchronousMethod(() : void => {
                            this.showLoader(true);
                        }, 200);
                    }
                    if (renderUI) {
                        await this.beforeLoad.apply(this, this.getProcessArgs()); // eslint-disable-line prefer-spread
                        await instance.Render();
                        if (!ObjectValidator.IsEmptyOrNull(holder.innerHTML)) {
                            (<any>StaticPageContentManager).isPrinted = true;
                            this.pageCache = holder.firstElementChild;
                        }
                        await this.afterLoad.apply(this, this.getProcessArgs()); // eslint-disable-line prefer-spread
                        const reflection : Reflection = Reflection.getInstance();
                        const properties : string[] = this.getProperties();
                        for await (const name of properties) {
                            if (this.hasOwnProperty(name) && reflection.IsMemberOf(this[name], BasePanelController)) {
                                this[name].setParent(this);
                                this[name].RequestArgs(this.RequestArgs());
                                await this[name].Process();
                            }
                        }

                        this.getEventsManager().FireEvent(GeneralEventOwner.BODY, EventType.ON_LOAD);

                        this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_SUCCESS);
                        this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_COMPLETE);
                    } else {
                        holder.removeChild(holder.firstElementChild);
                        holder.appendChild(this.pageCache);
                    }

                    this.PreparePageContent();
                    await this.onSuccess.apply(this, this.getProcessArgs()); // eslint-disable-line prefer-spread

                    if (renderUI) {
                        instance.Register();
                        if (!ObjectValidator.IsEmptyOrNull(this.loaderProcess)) {
                            clearTimeout(this.loaderProcess);
                        }
                        this.showLoader(false);
                    }
                } else {
                    window.location.reload();
                }
            } catch (ex) {
                this.getEventsManager().FireEvent(this.getClassName(), EventType.ON_ERROR);
                await this.onError(ex);
            }
            this.processing = false;
        });
        this.loadResources();
    }

    protected async beforeLoad($instance? : BasePanel, ...$args : any []) : Promise<void> {
        // override this method for ability to prepare instance before it is loaded
    }

    protected async afterLoad($instance? : BasePanel, ...$args : any []) : Promise<void> {
        // override this method for ability to setup instance after it is loaded
    }

    protected async onSuccess($instance? : BasePanel, ...$args : any []) : Promise<void> {
        // override this method for ability to handle controller state after it has been successfully loaded
    }

    protected async onError($error : Error) : Promise<void> {
        LogIt.Error($error);
    }

    protected showLoader($value : boolean) : void {
        if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
            if ($value) {
                ElementManager.Hide(GuiCommons.getRenderTarget());
                ElementManager.Show(this.getLoaderElement());
            } else {
                ElementManager.Hide(this.getLoaderElement());
                ElementManager.Show(GuiCommons.getRenderTarget());
            }
        }
    }

    protected getRefererUrl() : string {
        let referer : string = null;
        if (this.RequestArgs().POST().KeyExists("Referer")) {
            const request : HttpRequestParser = this.RequestArgs().POST().getItem("Referer");
            if (this.getHttpManager().getRequest().getUrl() !== request.getUrl()) {
                referer = request.getUrl();
            }
        }
        return referer;
    }

    private getLoaderElement() : HTMLElement {
        const element : HTMLElement = ElementManager.getElement("PageLoader");
        if (!ObjectValidator.IsEmptyOrNull(element)) {
            this.getLoaderElement = () : HTMLElement => {
                return element;
            };
        }
        return element;
    }
}
