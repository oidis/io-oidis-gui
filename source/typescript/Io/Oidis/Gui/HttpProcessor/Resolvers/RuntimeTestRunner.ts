/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { GeneralEventOwner } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";
import { RuntimeTestRunner as Parent } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { HttpRequestConstants } from "../../Enums/HttpRequestConstants.js";
import { GuiObjectManager } from "../../GuiObjectManager.js";
import { IEventsManager } from "../../Interfaces/IEventsManager.js";
import { IGuiCommons } from "../../Interfaces/Primitives/IGuiCommons.js";
import { Loader } from "../../Loader.js";
import { BaseViewer } from "../../Primitives/BaseViewer.js";
import { ElementManager } from "../../Utils/ElementManager.js";
import { StaticPageContentManager } from "../../Utils/StaticPageContentManager.js";
import { WindowManager } from "../../Utils/WindowManager.js";

/**
 * RuntimeTestRunner class provides handling of unit tests at runtime.
 */
export abstract class RuntimeTestRunner extends Parent {

    /**
     * @param {IClassName} [$className] Specify class name from, which should be created the runtime unit test link.
     * @returns {string} Returns link in string format, which can be used for resolver registration or as runtime callback.
     */
    public static CreateLink($className? : IClassName) : string {
        if (!ObjectValidator.IsSet($className)) {
            $className = this; // eslint-disable-line @typescript-eslint/no-this-alias
        }
        if (ObjectValidator.IsSet((<any>$className).ClassName)) {
            return "#" + Loader.getInstance().getHttpManager().CreateLink(
                "/" + HttpRequestConstants.RUNTIME_TEST + "/" + StringUtils.Replace((<any>$className).ClassName(), ".", "/"));
        }
        return Loader.getInstance().getHttpManager().CreateLink("");
    }

    /**
     * @returns {string} Returns link, which can be used for resolver registration or invoke by link element.
     */
    public static CallbackLink() : string {
        return this.CreateLink();
    }

    /**
     * Execute resolver implementation.
     * @returns {void}
     */
    public Process() : void {
        Echo.ClearAll();
        this.resolver();
    }

    protected resolver() : void {
        StaticPageContentManager.Clear();
        StaticPageContentManager.Title("Oidis - Unit Test");
        StaticPageContentManager.Draw();
        this.getEvents().setOnSuiteStart(() : void => {
            ElementManager.setClassName(document.body, "RuntimeTest");
        });
        const autoScrollHandler : any = () : void => {
            WindowManager.ScrollToBottom();
        };
        this.getEvents().setOnTestCaseEnd(autoScrollHandler);
        this.getEvents().setOnSuiteEnd(autoScrollHandler);

        let testName : string = this.getRequest().getScriptPath();
        let executeParent : boolean = true;
        if (StringUtils.Contains(testName, HttpRequestConstants.RUNTIME_TEST)) {
            testName = StringUtils.Substring(testName,
                StringUtils.IndexOf(testName, HttpRequestConstants.RUNTIME_TEST + "/") + 12, StringUtils.Length(testName));
            testName = StringUtils.Replace(testName, "/", ".");
            if (testName !== this.getClassName()) {
                const reflect : Reflection = Reflection.getInstance();
                const testClass : any = reflect.getClass(testName);
                if (!ObjectValidator.IsEmptyOrNull(testClass)) {
                    const testObject : Parent = new testClass();
                    if (reflect.IsMemberOf(testObject, Parent)) {
                        testObject.Process();
                        const testViewer : BaseViewer = new BaseViewer();
                        const events : IEventsManager = this.getEventsManager();
                        events.setEvent(GeneralEventOwner.BODY, EventType.ON_LOAD, () : void => {
                            const elements : ArrayList<IGuiCommons> = GuiObjectManager.getInstanceSingleton().getAll();
                            elements.foreach(($element : IGuiCommons) : void => {
                                if (!$element.IsCached() && !$element.IsLoaded() &&
                                    ObjectValidator.IsEmptyOrNull($element.Parent()) &&
                                    ElementManager.Exists($element.Id())) {
                                    if (ObjectValidator.IsEmptyOrNull($element.InstanceOwner())) {
                                        $element.InstanceOwner(testViewer);
                                    }
                                    if ($element.Visible()) {
                                        if ((<any>$element).hasAsynchronousDraw()) {
                                            $element.Visible(true);
                                        } else {
                                            events.FireEvent($element, EventType.ON_START);
                                            if (ElementManager.IsVisible($element.Id())) {
                                                events.FireEvent($element, EventType.BEFORE_LOAD, false);
                                            }
                                        }
                                    }
                                }
                            });
                        });
                        events.FireEvent(GeneralEventOwner.BODY, EventType.ON_LOAD);
                        executeParent = false;
                    }
                }
            }
        }
        if (executeParent) {
            super.resolver();
        }
    }
}
