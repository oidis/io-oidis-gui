/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { AsyncHttpResolver } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/AsyncHttpResolver.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "../../Enums/Events/EventType.js";
import { HttpRequestConstants } from "../../Enums/HttpRequestConstants.js";
import { ViewerManagerEventArgs } from "../../Events/Args/ViewerManagerEventArgs.js";
import { IGuiCommons } from "../../Interfaces/Primitives/IGuiCommons.js";
import { BaseViewer } from "../../Primitives/BaseViewer.js";
import { ViewerManager } from "../../ViewerManager.js";

/**
 * AsyncDrawGuiObject class provides handling of requests for rendering of GUI objects.
 */
export class AsyncDrawGuiObject extends AsyncHttpResolver {

    private objectName : string;
    private testModeEnabled : boolean;
    private includeChildrenEnabled : boolean;
    private childElement : IGuiCommons;

    /**
     * Set or get resolver class name or callback.
     * @param {string} [$value] Object class name in string format, which should be used by the resolver.
     * @returns {string} Returns class name of requested object for rendering.
     */
    public ObjectClassName($value? : string) : string {
        if (ObjectValidator.IsEmptyOrNull($value)) {
            if (!ObjectValidator.IsSet(this.objectName)) {
                this.objectName = this.RequestArgs().Url();
                this.objectName = StringUtils.Remove(this.objectName, "/" + HttpRequestConstants.TEST_MODE);
                this.objectName = StringUtils.Remove(this.objectName, "/" + HttpRequestConstants.FORCE_REFRESH);
                this.objectName = StringUtils.Substring(this.objectName,
                    StringUtils.IndexOf(this.objectName, "async/") + 6, StringUtils.Length(this.objectName));
                this.objectName = StringUtils.Replace(this.objectName, "/", ".");
            }
        } else {
            this.objectName = Property.String(this.objectName, $value);
        }

        return this.objectName;
    }

    protected testMode() : boolean {
        if (!ObjectValidator.IsSet(this.testModeEnabled)) {
            this.testModeEnabled = StringUtils.Contains(this.getRequest().getScriptPath(), HttpRequestConstants.TEST_MODE);
        }

        return this.testModeEnabled;
    }

    protected includeChildren() : boolean {
        if (!ObjectValidator.IsSet(this.includeChildrenEnabled)) {
            if (this.RequestArgs().POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN)) {
                this.includeChildrenEnabled = !this.RequestArgs().POST().getItem(HttpRequestConstants.HIDE_CHILDREN);
            } else {
                this.includeChildrenEnabled = true;
            }
        }

        return this.includeChildrenEnabled;
    }

    protected getChildElement() : IGuiCommons {
        if (!ObjectValidator.IsSet(this.childElement)) {
            if (this.RequestArgs().POST().KeyExists(HttpRequestConstants.ELEMENT_INSTANCE)) {
                this.childElement = this.RequestArgs().POST().getItem(HttpRequestConstants.ELEMENT_INSTANCE);
            } else {
                this.childElement = null;
            }
        }

        return this.childElement;
    }

    protected loadGuiObject() : ViewerManager {
        const viewerManager : ViewerManager = new ViewerManager(this.ObjectClassName(), this.getChildElement());
        viewerManager.TestModeEnabled(this.testMode());
        viewerManager.IncludeChildren(this.includeChildren());
        viewerManager.IsAsyncCallback(true);
        return viewerManager;
    }

    protected resolver() : void {
        this.getEventsManager().setEvent(ViewerManager.ClassName(), EventType.ON_COMPLETE,
            ($eventArgs : ViewerManagerEventArgs) : void => {
                if ($eventArgs.AsyncCallback()) {
                    const view : BaseViewer = <BaseViewer>$eventArgs.Result();
                    if (!ObjectValidator.IsEmptyOrNull(view)) {
                        this.RequestArgs().Result(view.Show(this.getEnvelopEOL()));
                        this.RequestArgs().POST().Add($eventArgs.ChildElement(), HttpRequestConstants.ELEMENT_INSTANCE);
                        this.RequestArgs().POST().Add(!$eventArgs.IncludeChildren(), HttpRequestConstants.HIDE_CHILDREN);
                        this.success();
                    } else {
                        this.RequestArgs().Result(
                            "<h1>Viewer \"" + this.ObjectClassName() + "\" instance has not been found!</h1>");
                        this.error();
                    }
                }
            });
        this.loadGuiObject().Process();
    }

    private getEnvelopEOL() : string {
        if (this.RequestArgs().POST().KeyExists(HttpRequestConstants.ELEMENT_INSTANCE)) {
            return (<any>this.getChildElement()).outputEndOfLine;
        }
        return "";
    }
}
