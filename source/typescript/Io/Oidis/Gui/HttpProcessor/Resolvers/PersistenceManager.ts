/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { AsyncHttpResolver } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/AsyncHttpResolver.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { PersistenceType } from "../../Enums/PersistenceType.js";
import { FormsObject } from "../../Primitives/FormsObject.js";
import { FormValue } from "../../Structures/FormValue.js";

/**
 * PersistenceManager class provides automated store of UI state based on user interaction
 */
export class PersistenceManager extends AsyncHttpResolver {

    protected resolver() : void {
        const data : ArrayList<FormValue> = FormsObject.CollectValues();
        const addValues : ArrayList<string> = new ArrayList<string>();
        const removeValues : ArrayList<string> = new ArrayList<string>();
        const addErrors : ArrayList<boolean> = new ArrayList<boolean>();
        const removeErrors : ArrayList<string> = new ArrayList<string>();
        data.foreach(($formElement : FormValue) : void => {
            if (!ObjectValidator.IsEmptyOrNull($formElement.Value())) {
                addValues.Add($formElement.Value(), $formElement.Id());
            } else {
                removeValues.Add($formElement.Id());
            }
            if ($formElement.ErrorFlag()) {
                addErrors.Add($formElement.ErrorFlag(), $formElement.Id());
            } else {
                removeErrors.Add($formElement.Id());
            }
        });
        PersistenceFactory.getPersistence(PersistenceType.FORM_VALUES).Variable(addValues);
        PersistenceFactory.getPersistence(PersistenceType.FORM_VALUES).Destroy(removeValues);
        PersistenceFactory.getPersistence(PersistenceType.ERROR_FLAGS).Variable(addErrors);
        PersistenceFactory.getPersistence(PersistenceType.ERROR_FLAGS).Destroy(removeErrors);
        this.success();
    }
}
