/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpResolver as Parent, IHttpResolverContext } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { EventsManager } from "../Events/EventsManager.js";
import { GuiObjectManager } from "../GuiObjectManager.js";
import { HttpManager } from "./HttpManager.js";

export class HttpResolver extends Parent {
    private readonly guiRegister : GuiObjectManager;

    constructor($baseUrl? : string) {
        super($baseUrl);

        const guiObjectManager : any = this.getGuiObjectManagerClass();
        this.guiRegister = new guiObjectManager();
    }

    public getManager() : HttpManager {
        return <HttpManager>super.getManager();
    }

    public getEvents() : EventsManager {
        return <EventsManager>super.getEvents();
    }

    /**
     * @returns {GuiObjectManager} Returns GuiObjectManager instance used for management of GUI used in resolved requests.
     */
    public getGuiRegister() : GuiObjectManager {
        return this.guiRegister;
    }

    protected async getStartupResolvers($context : IHttpResolverContext) : Promise<any> {
        if ($context.isBrowser) {
            if (!$context.isProd) {
                await this.registerResolver(async () => (await import("../Index.js")).Index,
                    "/", "/index", "/web/");
                await this.registerResolver(async () => (await import("../Designer/DesignerRunner.js")).DesignerRunner,
                    "/design/**/*", "design/");
                await this.registerResolver(async () => (await import("./Resolvers/RuntimeTestRunner.js")).RuntimeTestRunner,
                    "/" + HttpRequestConstants.RUNTIME_TEST + "/**/*");
                await this.registerResolver(async () => (await import("../RuntimeTests/PersistenceManagerTest.js")).PersistenceManagerTest,
                    "/web/PersistenceManagerTest");
            }

            await this.registerResolver(async () => (await import("./Resolvers/AsyncDrawGuiObject.js")).AsyncDrawGuiObject,
                "/async/**/*");
            return this.registerResolver(async () => (await import("./Resolvers/DrawGuiObject.js")).DrawGuiObject,
                "/web/**/*");
        }
        return {};
    }

    protected getHttpManagerClass() : any {
        return HttpManager;
    }

    protected getEventsManagerClass() : any {
        return EventsManager;
    }

    protected getGuiObjectManagerClass() : any {
        return GuiObjectManager;
    }
}
