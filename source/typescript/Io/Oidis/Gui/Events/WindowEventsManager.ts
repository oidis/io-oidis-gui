/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "../Enums/Events/EventType.js";
import { GeneralEventOwner } from "../Enums/Events/GeneralEventOwner.js";
import { IAsyncRequestEventsHandler } from "../Interfaces/Events/IAsyncRequestEventsHandler.js";
import { IErrorEventsHandler } from "../Interfaces/Events/IErrorEventsHandler.js";
import { IHttpRequestEventsHandler } from "../Interfaces/Events/IHttpRequestEventsHandler.js";
import { IKeyEventsHandler } from "../Interfaces/Events/IKeyEventsHandler.js";
import { IMessageEventsHandler } from "../Interfaces/Events/IMessageEventsHandler.js";
import { IMouseEventsHandler } from "../Interfaces/Events/IMouseEventsHandler.js";
import { IMoveEventsHandler } from "../Interfaces/Events/IMoveEventsHandler.js";
import { IResizeEventsHandler } from "../Interfaces/Events/IResizeEventsHandler.js";
import { IScrollEventsHandler } from "../Interfaces/Events/IScrollEventsHandler.js";
import { IEventsHandler } from "../Interfaces/IEventsHandler.js";
import { EventsManager } from "./EventsManager.js";

/**
 * WindowEventsManager class provides handling of native events subscribed to window or body elements.
 */
export class WindowEventsManager extends BaseObject {
    private events : EventsManager;

    constructor() {
        super();

        this.events = <EventsManager>EventsManager.getInstanceSingleton();
    }

    /**
     * @param {string} $type Validate this type of event.
     * @returns {boolean} Returns true, if event type has been registered, otherwise false.
     */
    public Exists($type : string) : boolean {
        return this.events.Exists(this.getOwner($type), $type);
    }

    /**
     * @returns {ArrayList<ArrayList<IEventsHandler>>} Returns list of all registered events.
     */
    public getAll() : ArrayList<ArrayList<IEventsHandler>> {
        return this.events.getAll().getItem(GeneralEventOwner.BODY);
    }

    /**
     * @param {string} $type Subscribe handler to this type of event.
     * @param {IEventsHandler} $handler Function suitable for handling of the event.
     * @returns {void}
     */
    public setEvent($type : string, $handler : IEventsHandler) : void {
        this.events.setEvent(this.getOwner($type), $type, $handler);
    }
   
    public FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean) : number;

    public FireAsynchronousMethod($handler : () => void, $waitForMilliseconds? : number) : number;

    /**
     * @param {Function} $handler Function suitable for handling of the event.
     * @param {boolean} [$clearBefore=true] Clean up currently running thread with same handler.
     * @param {number} [$waitForMilliseconds] Specify wait time before execution of the handler in milliseconds.
     * @returns {number} Returns thread number allocated for asynchronous execution.
     */
    public FireAsynchronousMethod($handler : () => void, $clearBefore? : any, $waitForMilliseconds? : number) : number {
        return this.events.FireAsynchronousMethod($handler, $clearBefore, $waitForMilliseconds);
    }

    /**
     * @param {string} $type Event type of subscribed handler.
     * @param {IEventsHandler} $handler Handler, which should be removed.
     * @returns {void}
     */
    public RemoveHandler($type : string, $handler : IEventsHandler) : void {
        this.events.RemoveHandler(this.getOwner($type), $type, $handler);
    }

    /**
     * Clear all events, if event type has not been specified.
     * Clear subset of events subscribed to event type, if type has been specified.
     * @param {string} [$type] Specify desired type of event.
     * @returns {void}
     */
    public Clear($type? : string) : void {
        this.events.Clear(this.getOwner($type), $type);
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let output : string = "Registered events list:";
        if ($htmlTag) {
            output = "<b>" + output + "</b>";
        }
        output = $prefix + output + StringUtils.NewLine($htmlTag);
        this.getAll().foreach(($value : ArrayList<any>, $type? : string) : void => {
            output += $prefix + StringUtils.Tab(1, $htmlTag) + "[\"" + $type + "\"]" +
                " hooked handlers count: " + $value.Length() + StringUtils.NewLine($htmlTag);
        });
        return output;
    }

    public toString() : string {
        return this.ToString();
    }

    /**
     * @param {IErrorEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnError($handler : IErrorEventsHandler) : void {
        this.setEvent(EventType.ON_ERROR, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setBeforeRefresh($handler : IEventsHandler) : void {
        this.setEvent(EventType.BEFORE_REFRESH, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnLoad($handler : IEventsHandler) : void {
        this.setEvent(EventType.ON_LOAD, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnBlur($handler : IEventsHandler) : void {
        this.setEvent(EventType.ON_BLUR, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnFocus($handler : IEventsHandler) : void {
        this.setEvent(EventType.ON_FOCUS, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnClick($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_CLICK, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnRightClick($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_RIGHT_CLICK, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnDoubleClick($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_DOUBLE_CLICK, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMouseOver($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_MOUSE_OVER, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMouseOut($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_MOUSE_OUT, $handler);
    }

    /**
     * @param {IMoveEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMove($handler : IMoveEventsHandler) : void {
        this.events.setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMouseDown($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_MOUSE_DOWN, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMouseUp($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_MOUSE_UP, $handler);
    }

    /**
     * @param {IKeyEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnKeyDown($handler : IKeyEventsHandler) : void {
        this.setEvent(EventType.ON_KEY_DOWN, $handler);
    }

    /**
     * @param {IKeyEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnKeyUp($handler : IKeyEventsHandler) : void {
        this.setEvent(EventType.ON_KEY_UP, $handler);
    }

    /**
     * @param {IKeyEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnKeyPress($handler : IKeyEventsHandler) : void {
        this.setEvent(EventType.ON_KEY_PRESS, $handler);
    }

    /**
     * @param {IResizeEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnResize($handler : IResizeEventsHandler) : void {
        this.setEvent(EventType.ON_RESIZE, $handler);
    }

    /**
     * @param {IScrollEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnScroll($handler : IScrollEventsHandler) : void {
        this.setEvent(EventType.ON_SCROLL, $handler);
    }

    /**
     * @param {IHttpRequestEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnRequest($handler : IHttpRequestEventsHandler) : void {
        this.setEvent(EventType.ON_HTTP_REQUEST, $handler);
    }

    /**
     * @param {IAsyncRequestEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnAsyncRequest($handler : IAsyncRequestEventsHandler) : void {
        this.setEvent(EventType.ON_ASYNC_REQUEST, $handler);
    }

    /**
     * @param {IMessageEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMessage($handler : IMessageEventsHandler) : void {
        this.setEvent(EventType.ON_MESSAGE, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnDragOver($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_DRAG_OVER, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnDrop($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_DROP, $handler);
    }

    private getOwner($type : string) : string {
        switch ($type) {
        case EventType.ON_MOUSE_MOVE:
            return GeneralEventOwner.MOUSE_MOVE;
        case EventType.ON_HTTP_REQUEST:
        case EventType.ON_ASYNC_REQUEST:
            return GeneralEventOwner.WINDOW;
        default:
            return GeneralEventOwner.BODY;
        }
    }
}
