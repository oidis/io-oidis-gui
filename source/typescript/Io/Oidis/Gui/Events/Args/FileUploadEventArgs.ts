/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ProgressEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ProgressEventArgs.js";
import { FileHandler } from "@io-oidis-commons/Io/Oidis/Commons/IOApi/Handlers/FileHandler.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";

/**
 * FileUploadEventArgs class provides args connected with file upload.
 */
export class FileUploadEventArgs extends ProgressEventArgs {
    private id : string;
    private index : number;
    private name : string;
    private handler : FileHandler;

    constructor() {
        super();
        this.id = "";
        this.index = -1;
        this.name = "";
        this.handler = null;
    }

    /**
     * @param {string} [$value] Set file id value.
     * @returns {string} Returns file id value.
     */
    public Id($value? : string) : string {
        return this.id = Property.String(this.id, $value);
    }

    /**
     * @param {number} [$value] Set file chunk index value.
     * @returns {number} Returns file chunk index value, if file chunk has been specified otherwise -1.
     */
    public Index($value? : number) : number {
        return this.index = Property.PositiveInteger(this.index, $value);
    }

    /**
     * @param {string} [$value] Set file name value.
     * @returns {string} Returns file name value.
     */
    public Name($value? : string) : string {
        return this.name = Property.String(this.name, $value);
    }

    /**
     * @param {FileHandler} [$value] Set file handler instance.
     * @returns {FileHandler} Returns file handler instance, if handler has been specified, otherwise null.
     */
    public File($value? : FileHandler) : FileHandler {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.handler = $value;
        }
        return this.handler;
    }
}
