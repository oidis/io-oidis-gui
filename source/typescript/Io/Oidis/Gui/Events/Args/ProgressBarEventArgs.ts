/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ProgressEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ProgressEventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { DirectionType } from "../../Enums/DirectionType.js";

/**
 * ProgressBarEventArgs class provides args connected with progress bar processing.
 */
export class ProgressBarEventArgs extends ProgressEventArgs {
    private percentageValue : number;
    private directionType : DirectionType;

    constructor() {
        super();
        this.percentageValue = 0;
        this.directionType = DirectionType.UP;
    }

    /**
     * @param {DirectionType} [$type] Specify progress change direction.
     * @returns {DirectionType} Returns type of progress change direction.
     */
    public DirectionType($type? : DirectionType) : DirectionType {
        if (ObjectValidator.IsSet($type)) {
            this.directionType = $type;
        }
        return this.directionType;
    }

    /**
     * @param {number} [$value] Set current value in percentage range <0;100>.
     * @returns {number} Returns number value of the current value in percentage range <0;100>.
     */
    public Percentage($value? : number) : number {
        return this.percentageValue = Property.PositiveInteger(this.percentageValue, $value, 0, 100);
    }
}
