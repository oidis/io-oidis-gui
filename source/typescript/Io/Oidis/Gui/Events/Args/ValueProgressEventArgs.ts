/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ProgressEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ProgressEventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { DirectionType } from "../../Enums/DirectionType.js";
import { ProgressType } from "../../Enums/ProgressType.js";
import { UnitType } from "../../Enums/UnitType.js";

/**
 * ValueProgressEventArgs class provides args connected with change of some value.
 */
export class ValueProgressEventArgs extends ProgressEventArgs {
    private ownerId : string;
    private directionType : DirectionType;
    private startEventType : string;
    private changeEventType : string;
    private completeEventType : string;
    private step : number;
    private progressType : ProgressType;
    private timeStamp : number;
    private floatValue : number;
    private stepUnitType : UnitType;

    /**
     * @param {string} $id Set value progress args owner id.
     */
    constructor($id : string) {
        super();
        this.OwnerId($id);
        this.directionType = DirectionType.UP;
        this.startEventType = $id + "_" + EventType.ON_START;
        this.changeEventType = $id + "_" + EventType.ON_CHANGE;
        this.completeEventType = $id + "_" + EventType.ON_COMPLETE;
        this.progressType = ProgressType.LINEAR;
        this.step = 1;
        this.timeStamp = 0;
        this.floatValue = super.CurrentValue();
        this.stepUnitType = UnitType.PX;
    }

    /**
     * @param {string} [$value] Set args owner id.
     * @returns {string} Returns owner id of current args instance.
     */
    public OwnerId($value? : string) : string {
        return this.ownerId = Property.String(this.ownerId, $value);
    }

    /**
     * @param {DirectionType} [$value] Set type of direction in, which should be value changed.
     * Allowed is UP or DOWN direction.
     * @returns {DirectionType} Returns direction type of value change.
     */
    public DirectionType($value? : DirectionType) : DirectionType {
        if (!ObjectValidator.IsEmptyOrNull($value) &&
            ($value === DirectionType.DOWN || $value === DirectionType.UP)) {
            this.directionType = $value;
        }
        return this.directionType;
    }

    /**
     * @param {string} [$value] Set type of event, which should be fired at start time of value change.
     * @returns {string} Returns event type, which will be fired at start time of value change.
     */
    public StartEventType($value? : string) : string {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.startEventType = $value;
        }
        return this.startEventType;
    }

    /**
     * @param {string} [$value] Set type of event, which should be fired at time of value change.
     * @returns {string} Returns event type, which will be fired at time of value change.
     */
    public ChangeEventType($value? : string) : string {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.changeEventType = $value;
        }
        return this.changeEventType;
    }

    /**
     * @param {string} [$value] Set type of event, which should be fired, when value change is finished.
     * @returns {string} Returns event type, which will be fired, when value change is finished.
     */
    public CompleteEventType($value? : string) : string {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.completeEventType = $value;
        }
        return this.completeEventType;
    }

    /**
     * @param {number} [$value] Set average progress rate per time step (e.g. per 10ms)
     * @returns {number} Returns average progress rate per time step (e.g. per 10ms)
     */
    public Step($value? : number) : number {
        return this.step = ObjectValidator.IsSet($value) ? $value : this.step;
    }

    /**
     * @param {number} [$value] Set average progress rate per time step (e.g. per 10ms)
     * @returns {number} Returns average progress rate per time step (e.g. per 10ms)
     */
    public StepUnitType($value? : UnitType) : UnitType {
        return this.stepUnitType = ObjectValidator.IsSet($value) ? $value : this.stepUnitType;
    }

    /**
     * @param {ProgressType} [$value] Set type of algorithm, which should be used for change of value.
     * @returns {ProgressType} Returns type of algorithm, which is used for change of value.
     */
    public ProgressType($value? : ProgressType) : ProgressType {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.progressType = $value;
        }
        return this.progressType;
    }

    /**
     * @param {number} [$value] Set timestamp.
     * @returns {number} Returns timestamp.
     */
    public TimeStamp($value? : number) : number {
        return this.timeStamp = ObjectValidator.IsSet($value) ? $value : this.timeStamp;
    }

    /**
     * @param {number} [$value] Set current value of change.
     * @returns {number} Returns number value of current change.
     */
    public CurrentValue($value? : number) : number {
        const value : number = super.CurrentValue($value);
        if (ObjectValidator.IsSet($value)) {
            this.FloatValue(value);
        }
        return value;
    }

    /**
     * @param {number} [$value] Set float current value of change.
     * @returns {number} Returns number value of float current change.
     */
    public FloatValue($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            super.CurrentValue($value);
            this.floatValue = $value;
        }
        return this.floatValue;
    }
}
