/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { DirectionType } from "../../Enums/DirectionType.js";
import { OrientationType } from "../../Enums/OrientationType.js";

/**
 * ScrollEventArgs class provides args connected with usage of scrollbars.
 */
export class ScrollEventArgs extends EventArgs {
    private orientationType : OrientationType;
    private directionType : DirectionType;
    private position : number;

    /**
     * @param {OrientationType} [$value] Set scrolling orientation.
     * @returns {OrientationType} Returns scrollbar orientation.
     */
    public OrientationType($value? : OrientationType) : OrientationType {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.orientationType = $value;
        }
        return this.orientationType;
    }

    /**
     * @param {DirectionType} [$value] Set scrolling direction.
     * @returns {DirectionType} Returns scrollbar direction.
     */
    public DirectionType($value? : DirectionType) : DirectionType {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if ($value === DirectionType.RIGHT) {
                $value = DirectionType.DOWN;
            } else if ($value === DirectionType.LEFT) {
                $value = DirectionType.UP;
            }
            this.directionType = $value;
        }
        return this.directionType;
    }

    /**
     * @param {number} [$value] Set current scrollbar position. Position should be in range <0;100>.
     * @returns {number} Returns current scrollbar position.
     */
    public Position($value? : number) : number {
        if (ObjectValidator.IsSet($value)) {
            if ($value > 0 && $value < 1) {
                $value = $value * 100;
            }
            if ($value < 0) {
                $value = 0;
            }
            if ($value > 100) {
                $value = 100;
            }
        }
        return this.position = Property.PositiveInteger(this.position, $value, 0, 100);
    }
}
