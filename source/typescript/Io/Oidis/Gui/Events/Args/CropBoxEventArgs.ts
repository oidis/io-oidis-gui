/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { ResizeEventArgs } from "./ResizeEventArgs.js";

/**
 * CropBoxEventArgs class provides args connected with resizing of crop box.
 */
export class CropBoxEventArgs extends ResizeEventArgs {

    private offsetLeft : number;
    private offsetTop : number;

    /**
     * @param {number} [$value] Set left offset of top left crop corner.
     * @returns {number} Returns left offset of top left crop corner.
     */
    public OffsetLeft($value? : number) : number {
        return this.offsetLeft = Property.Integer(this.offsetLeft, $value);
    }

    /**
     * @param {number} [$value] Set top offset of top left crop corner.
     * @returns {number} Returns top offset of top left crop corner.
     */
    public OffsetTop($value? : number) : number {
        return this.offsetTop = Property.Integer(this.offsetTop, $value);
    }
}
