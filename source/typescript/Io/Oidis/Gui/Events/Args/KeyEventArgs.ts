/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";

/**
 * KeyEventArgs class provides args connected with keyboard events.
 */
export class KeyEventArgs extends EventArgs {
    /**
     * @param {KeyboardEvent} [$eventArgs] Specify native key event args provided by native key handler
     */
    constructor($eventArgs? : KeyboardEvent) {
        super();
        this.NativeEventArgs($eventArgs);
    }

    /**
     * @param {KeyboardEvent} [$value] If specified, set event args of native event.
     * @returns {KeyboardEvent} Returns native event args provided by native event handler.
     */
    public NativeEventArgs($value? : KeyboardEvent) : KeyboardEvent {
        return <KeyboardEvent>super.NativeEventArgs($value);
    }

    /**
     * @returns {number} Returns number representation of pressed key, if native event has been provided, otherwise null.
     */
    public getKeyCode() : number {
        const nativeEvent : KeyboardEvent = this.NativeEventArgs();
        if (ObjectValidator.IsSet(nativeEvent)) {
            return ObjectValidator.IsSet(nativeEvent.keyCode) && nativeEvent.keyCode !== 0 ?
                nativeEvent.keyCode : nativeEvent.charCode;
        }
        return null;
    }

    /**
     * @returns {string} Returns string representation of pressed key, if native event has been provided, otherwise null.
     */
    public getKeyChar() : string {
        const code : number = this.getKeyCode();
        if (!ObjectValidator.IsEmptyOrNull(code)) {
            const specialCharTable : object = {
                "#96" : "0", "#97": "1", "#98": "2", "#99": "3", "#100": "4", "#101": "5", "#102": "6",
                "#103": "7", "#104": "8", "#105": "9", "#106": "*", "#107": "+", "#109": "-", "#110": ".",
                "#111": "/", "#173": "-", "#186": ";", "#187": "=", "#188": ",", "#190": ".", "#191": "/",
                "#192": "`", "#219": "[", "#220": "\\", "#221": "]", "#222": "'"
            };

            if (specialCharTable.hasOwnProperty("#" + code)) {
                return specialCharTable["#" + code];
            }
            if (this.NativeEventArgs().shiftKey) {
                return Convert.UnicodeToString(code);
            }
            return StringUtils.ToLowerCase(Convert.UnicodeToString(code));
        }
        return null;
    }
}
