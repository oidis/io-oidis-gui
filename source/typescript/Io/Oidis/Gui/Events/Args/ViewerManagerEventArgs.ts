/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { IGuiCommons } from "../../Interfaces/Primitives/IGuiCommons.js";
import { BaseViewer } from "../../Primitives/BaseViewer.js";
import { BaseViewerArgs } from "../../Primitives/BaseViewerArgs.js";
import { ViewerCacheManager } from "../../ViewerCacheManager.js";

/**
 * ViewerManagerEventArgs class provides args connected with processing of particular viewer.
 */
export class ViewerManagerEventArgs extends EventArgs {
    private viewerClass : any;
    private viewerArgs : BaseViewerArgs;
    private child : IGuiCommons;
    private reloadCache : boolean;
    private testMode : boolean;
    private includeChildren : boolean;
    private asyncCallback : boolean;
    private result : any;

    constructor() {
        super();
        this.viewerClass = null;
        this.child = null;
        this.reloadCache = false;
        this.testMode = false;
        this.includeChildren = true;
        this.asyncCallback = false;
        this.result = null;
        this.viewerArgs = null;
    }

    /**
     * @param {any} [$value] Specify viewer class name, which is processed by ViewerManager.
     * @returns {any} Returns viewer class name currently processed by ViewerManager.
     */
    public ViewerClass($value? : any) : any {
        if (ObjectValidator.IsSet($value)) {
            this.viewerClass = $value;
        }
        return this.viewerClass;
    }

    /**
     * @param {BaseViewerArgs} [$value] Specify viewer args, which are processed by ViewerManager.
     * @returns {BaseViewerArgs} Return arguments of Viewer.
     */
    public ViewerArgs($value? : BaseViewerArgs) : BaseViewerArgs {
        if (ObjectValidator.IsSet($value)) {
            this.viewerArgs = $value;
        }
        return this.viewerArgs;
    }

    /**
     * @param {IGuiCommons} [$value] Specify, if child element should be rendered
     * @returns {IGuiCommons} Return true, if child element should be rendered otherwise false
     */
    public ChildElement($value? : IGuiCommons) : IGuiCommons {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.child = $value;
        }
        return this.child;
    }

    /**
     * @param {boolean} [$value] Specify, if cache should be reloaded
     * @returns {boolean} Return true, if cache should be reloaded otherwise false
     */
    public ReloadCache($value? : boolean) : boolean {
        return this.reloadCache = Property.Boolean(this.reloadCache, $value);
    }

    /**
     * @param {boolean} [$value] Define if test mode should be enabled or not
     * @returns {boolean} Returns test mode state.
     */
    public TestMode($value? : boolean) : boolean {
        return this.testMode = Property.Boolean(this.testMode, $value);
    }

    /**
     * @param {boolean} [$value] Define if panel should be rendered with children or with placeholders only
     * @returns {boolean} Returns if children should be included in rendered panel.
     */
    public IncludeChildren($value? : boolean) : boolean {
        return this.includeChildren = Property.Boolean(this.includeChildren, $value);
    }

    /**
     * @param {boolean} [$value] Define if specific callback should be invoked asynchronously after rendering
     * @returns {boolean} Returns true if async callback will be used otherwise false
     */
    public AsyncCallback($value? : boolean) : boolean {
        return this.asyncCallback = Property.Boolean(this.asyncCallback, $value);
    }

    /**
     * @param {BaseViewer|ViewerCacheManager} [$value] Specify result of processing.
     * @returns {BaseViewer|ViewerCacheManager} Returns viewer instance or viewer's cache manager based on viewer manager settings,
     * if viewer name or cache has been found, otherwise null.
     */
    public Result($value? : BaseViewer | ViewerCacheManager) : BaseViewer | ViewerCacheManager {
        if (ObjectValidator.IsSet($value)) {
            this.result = $value;
        }
        return this.result;
    }
}
