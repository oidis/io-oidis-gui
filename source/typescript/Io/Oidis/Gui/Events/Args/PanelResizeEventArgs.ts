/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { ResizeEventArgs } from "./ResizeEventArgs.js";

/**
 * PanelResizeEventArgs class provides args connected with resizing of panel element.
 */
export class PanelResizeEventArgs extends ResizeEventArgs {
    private scrollable : boolean;

    /**
     * @param {boolean} [$value] Specify, if panel can be scrolled.
     * @returns {boolean} Returns true, if it is able to use scrollbars in panel, otherwise false.
     */
    public Scrollable($value? : boolean) : boolean {
        this.scrollable = Property.Boolean(this.scrollable, $value);
        return this.scrollable;
    }
}
