/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";

/**
 * TouchEventArgs class provides args connected with touch events.
 */
export class TouchEventArgs extends EventArgs {
    /**
     * @param {TouchEvent} [$eventArgs] Specify native touch event args provided by native touch handler
     */
    constructor($eventArgs? : TouchEvent) {
        super();
        this.NativeEventArgs($eventArgs);
    }

    public NativeEventArgs($value? : TouchEvent) : TouchEvent {
        return <TouchEvent>super.NativeEventArgs($value);
    }
}
