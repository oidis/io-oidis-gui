/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";

export class MediaEventArgs extends EventArgs {
    private message : string;

    constructor() {
        super();
        this.message = "";
    }

    public EventMessage($value? : string) : string {
        return this.message = Property.String(this.message, $value);
    }
}
