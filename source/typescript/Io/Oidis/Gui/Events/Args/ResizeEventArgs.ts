/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";

/**
 * ResizeEventArgs class provides args connected with resizing of element.
 */
export class ResizeEventArgs extends EventArgs {
    private width : number;
    private height : number;
    private availableWidth : number;
    private availableHeight : number;
    private scrollBarWidth : number;
    private scrollBarHeight : number;
    private scrollBarChanged : boolean;

    /**
     * @param {number} [$value] Set element's width value.
     * @returns {number} Returns element's width.
     */
    public Width($value? : number) : number {
        return this.width = Property.PositiveInteger(this.width, $value);
    }

    /**
     * @param {number} [$value] Set element's height value.
     * @returns {number} Returns element's height.
     */
    public Height($value? : number) : number {
        return this.height = Property.PositiveInteger(this.height, $value);
    }

    /**
     * @param {number} [$value] Set element's width, which is available, if is visible vertical scrollbar.
     * @returns {number} Returns element's width, which is available, if is visible vertical scrollbar.
     */
    public AvailableWidth($value? : number) : number {
        return this.availableWidth = Property.PositiveInteger(this.availableWidth, $value);
    }

    /**
     * @param {number} [$value] Set element's height, which is available, if is visible horizontal scrollbar.
     * @returns {number} Returns element's height, which is available, if is visible horizontal scrollbar.
     */
    public AvailableHeight($value? : number) : number {
        return this.availableHeight = Property.PositiveInteger(this.availableHeight, $value);
    }

    /**
     * @param {number} [$value] Specify, scrollbar width.
     * @returns {number} Returns element's scrollbar width.
     */
    public ScrollBarWidth($value? : number) : number {
        return this.scrollBarWidth = Property.PositiveInteger(this.scrollBarWidth, $value);
    }

    /**
     * @param {number} [$value] Specify, scrollbar height.
     * @returns {number} Returns element's scrollbar height.
     */
    public ScrollBarHeight($value? : number) : number {
        return this.scrollBarHeight = Property.PositiveInteger(this.scrollBarHeight, $value);
    }
}
