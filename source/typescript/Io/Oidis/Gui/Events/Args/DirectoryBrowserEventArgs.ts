/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { IDirectoryBrowser } from "../../Interfaces/UserControls/IDirectoryBrowser.js";

/**
 * DirectoryBrowserEventArgs class provides args connected with usage of DirectoryBrowser.
 */
export class DirectoryBrowserEventArgs extends EventArgs {
    private value : string;

    /**
     * @param {IDirectoryBrowser} [$value] Specified object, which owns current args
     * @returns {IDirectoryBrowser} Returns event args owner object.
     */
    public Owner($value? : IDirectoryBrowser) : IDirectoryBrowser {
        return super.Owner($value);
    }

    /**
     * @param {string} [$value] Set current selected item value.
     * @returns {string} Returns value of currently selected item.
     */
    public Value($value? : string) : string {
        return this.value = Property.String(this.value, $value);
    }
}
