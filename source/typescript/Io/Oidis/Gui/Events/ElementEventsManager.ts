/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { IllegalArgumentException } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/IllegalArgumentException.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { AudioEventType } from "../Enums/Events/AudioEventType.js";
import { BasePanelHolderEventType } from "../Enums/Events/BasePanelHolderEventType.js";
import { DialogEventType } from "../Enums/Events/DialogEventType.js";
import { DirectoryBrowserEventType } from "../Enums/Events/DirectoryBrowserEventType.js";
import { EventType } from "../Enums/Events/EventType.js";
import { ScrollBarEventType } from "../Enums/Events/ScrollBarEventType.js";
import { VideoEventType } from "../Enums/Events/VideoEventType.js";
import { GuiObjectManager } from "../GuiObjectManager.js";
import { IAudioEvents } from "../Interfaces/Events/IAudioEvents.js";
import { IBasePanelEvents } from "../Interfaces/Events/IBasePanelEvents.js";
import { IBasePanelHolderEvents } from "../Interfaces/Events/IBasePanelHolderEvents.js";
import { IButtonEvents } from "../Interfaces/Events/IButtonEvents.js";
import { ICropBoxEvents } from "../Interfaces/Events/ICropBoxEvents.js";
import { IDialogEvents } from "../Interfaces/Events/IDialogEvents.js";
import { IDirectoryBrowserEvents } from "../Interfaces/Events/IDirectoryBrowserEvents.js";
import { IDirectoryBrowserEventsHandler } from "../Interfaces/Events/IDirectoryBrowserEventsHandler.js";
import { IDragBarEvents } from "../Interfaces/Events/IDragBarEvents.js";
import { IDragBarEventsHandler } from "../Interfaces/Events/IDragBarEventsHandler.js";
import { IDropDownListEvents } from "../Interfaces/Events/IDropDownListEvents.js";
import { IFormsObjectEvents } from "../Interfaces/Events/IFormsObjectEvents.js";
import { IGuiCommonsEvents } from "../Interfaces/Events/IGuiCommonsEvents.js";
import { ILabelEvents } from "../Interfaces/Events/ILabelEvents.js";
import { IMediaEventsHandler } from "../Interfaces/Events/IMediaEventsHandler.js";
import { IMouseEventsHandler } from "../Interfaces/Events/IMouseEventsHandler.js";
import { IProgressBarEvents } from "../Interfaces/Events/IProgressBarEvents.js";
import { IResizeBarEvents } from "../Interfaces/Events/IResizeBarEvents.js";
import { IResizeBarEventsHandler } from "../Interfaces/Events/IResizeBarEventsHandler.js";
import { IResizeEventsHandler } from "../Interfaces/Events/IResizeEventsHandler.js";
import { IScrollBarEvents } from "../Interfaces/Events/IScrollBarEvents.js";
import { IScrollEventsHandler } from "../Interfaces/Events/IScrollEventsHandler.js";
import { ISelectBoxEvents } from "../Interfaces/Events/ISelectBoxEvents.js";
import { ITextAreaEvents } from "../Interfaces/Events/ITextAreaEvents.js";
import { ITouchEventsHandler } from "../Interfaces/Events/ITouchEventsHandler.js";
import { IVideoEvents } from "../Interfaces/Events/IVideoEvents.js";
import { IEventsHandler } from "../Interfaces/IEventsHandler.js";
import { IGuiCommons } from "../Interfaces/Primitives/IGuiCommons.js";
import { Loader } from "../Loader.js";
import { ElementManager } from "../Utils/ElementManager.js";
import { MouseEventArgs } from "./Args/MouseEventArgs.js";
import { TouchEventArgs } from "./Args/TouchEventArgs.js";
import { EventsManager } from "./EventsManager.js";

/**
 * ElementEventsManager class provides handling of native events subscribed to HTML elements.
 */
export class ElementEventsManager extends BaseObject implements IGuiCommonsEvents, IFormsObjectEvents, IBasePanelEvents,
                                                                IBasePanelHolderEvents, IButtonEvents, ICropBoxEvents, IDialogEvents,
                                                                IDirectoryBrowserEvents, IDragBarEvents, IDropDownListEvents,
                                                                ILabelEvents, IProgressBarEvents, IResizeBarEvents, IScrollBarEvents,
                                                                ISelectBoxEvents, ITextAreaEvents, IAudioEvents, IVideoEvents {
    protected globalEvents : EventsManager;
    protected localEvents : EventsManager;
    protected owner : any;
    private readonly eventsList : ArrayList<ArrayList<IEventsHandler>>;
    private subscriber : string;
    private isSubscribed : boolean;

    /**
     * @param {string|IGuiCommons} [$owner] Set events owner id.
     * @param {string} [$subscriber] Set events subscriber id.
     */
    constructor($owner? : string | IGuiCommons, $subscriber? : string) {
        super();

        this.eventsList = new ArrayList<ArrayList<IEventsHandler>>();
        this.globalEvents = <EventsManager>EventsManager.getInstanceSingleton();
        this.localEvents = new EventsManager();
        if (ObjectValidator.IsSet($owner)) {
            this.owner = $owner;
        }
        this.subscriber = null;
        this.Subscriber($subscriber);
        this.isSubscribed = false;
    }

    /**
     * @returns {string} Returns element id of the events owner, if owner has been set, otherwise null.
     */
    public getOwner() : string {
        if (ObjectValidator.IsEmptyOrNull(this.owner)) {
            return null;
        } else if (ObjectValidator.IsSet(this.owner.Id)) {
            return this.owner.Id();
        } else {
            return this.owner;
        }
    }

    /**
     * @param {string} [$value] Set element id of the events subscriber.
     * @returns {string} Returns element id of the events subscriber, if subscriber has been set, otherwise null.
     */
    public Subscriber($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            this.subscriber = Property.NullString($value);
        }
        return this.subscriber;
    }

    /**
     * @param {string} $type Validate this type of event.
     * @returns {boolean} Returns true, if event type has been registered, otherwise false.
     */
    public Exists($type : string) : boolean {
        return this.eventsList.KeyExists($type);
    }

    /**
     * @returns {ArrayList<ArrayList<IEventsHandler>>} Returns list of all registered events.
     */
    public getAll() : ArrayList<ArrayList<IEventsHandler>> {
        return this.eventsList;
    }

    /**
     * @param {string} $type Subscribe handler to this type of event.
     * @param {IEventsHandler} $handler Function suitable for handling of the event.
     * @returns {void}
     */
    public setEvent($type : string, $handler : IEventsHandler) : void {
        if (!ObjectValidator.IsEmptyOrNull($type)) {
            if (!this.Exists($type)) {
                this.eventsList.Add(new ArrayList<IEventsHandler>(), $type);
            }
            this.eventsList.getItem($type).Add($handler, StringUtils.getSha1($handler.toString()));
        } else {
            ExceptionsManager.Throw(this.getClassName(), new IllegalArgumentException("Event type can not be null."));
        }
    }

    public FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean) : number;

    public FireAsynchronousMethod($handler : () => void, $waitForMilliseconds? : number) : number;

    public FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean, $waitForMilliseconds? : number) : number;

    /**
     * @param {Function} $handler Function suitable for handling of the event.
     * @param {boolean} [$clearBefore=true] Clean up currently running thread with same handler.
     * @param {number} [$waitForMilliseconds] Specify wait time before execution of the handler in milliseconds.
     * @returns {number} Returns thread number allocated for asynchronous execution.
     */
    public FireAsynchronousMethod($handler : () => void, $clearBefore? : any, $waitForMilliseconds? : number) : number {
        return this.localEvents.FireAsynchronousMethod($handler, $clearBefore, $waitForMilliseconds);
    }

    /**
     * @param {string} $type Event type of subscribed handler.
     * @param {IEventsHandler} $handler Handler, which should be removed.
     * @returns {void}
     */
    public RemoveHandler($type : string, $handler : IEventsHandler) : void {
        if (this.Exists($type)) {
            const handlers : ArrayList<IEventsHandler> = this.eventsList.getItem($type);
            const handlerName : string = StringUtils.getSha1($handler.toString());
            if (handlers.KeyExists(handlerName)) {
                handlers.RemoveAt(handlers.getKeys().indexOf(handlerName));
            }
        }
    }

    /**
     * Clear all events, if event type has not been specified.
     * Clear subset of events subscribed to event type, if type has been specified.
     * @param {string} [$type] Specify desired type of event.
     * @returns {void}
     */
    public Clear($type? : string) : void {
        if (!this.isSubscribed) {
            this.eventsList.foreach(($handlers : ArrayList<IEventsHandler>, $key : string) : void => {
                if (!ObjectValidator.IsSet($type) || $type === $key) {
                    $handlers.Clear();
                }
            });
        } else {
            if (ObjectValidator.IsSet($type)) {
                this.eventsList.RemoveAt(this.eventsList.getKeys().indexOf($type));
            } else {
                this.eventsList.Clear();
            }
        }
    }

    public Subscribe($force? : boolean) : void;

    /**
     * Hook all registered events to manager owner or to specified target id.
     * As default target is used window, if is not set, either owner nor target id.
     * @param {string} [$targetId] Specify target element id to, which should be handlers subscribed.
     * @param {boolean} [$force=false] Specify if handlers should be subscribed only to non-cached DOM element.
     * @returns {void}
     */
    public Subscribe($targetId? : string, $force? : boolean) : void;

    public Subscribe($targetId? : string | boolean, $force : boolean = false) : void {
        if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
            setTimeout(() : void => {
                try {
                    if (ObjectValidator.IsBoolean($targetId)) {
                        $force = <boolean>$targetId;
                        $targetId = null;
                    }
                    const target : HTMLElement = this.getTarget(<string>$targetId, $force);
                    if (!ObjectValidator.IsEmptyOrNull(target)) {
                        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                        const reflection : Reflection = Reflection.getInstance();
                        this.eventsList.foreach(($handlers : ArrayList<IEventsHandler>, $key? : string) : void => {
                            if (ObjectValidator.IsSet(target[$key])) {
                                target[$key] = ($args? : Event) : any => {
                                    try {
                                        const handlerArgs : EventArgs = new EventArgs();
                                        handlerArgs.Owner(this.owner);
                                        if (ObjectValidator.IsSet($args)) {
                                            handlerArgs.NativeEventArgs($args);
                                        } else {
                                            handlerArgs.NativeEventArgs(event);
                                        }
                                        handlerArgs.Type($key);
                                        $handlers.foreach(($handler : IEventsHandler) : void => {
                                            $handler(handlerArgs, manager, reflection);
                                        });
                                    } catch (ex) {
                                        ExceptionsManager.HandleException(ex);
                                    }
                                };
                                this.isSubscribed = true;
                            }
                        });
                    }
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            });
        }
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let output : string = "Registered events list:";
        if ($htmlTag) {
            output = "<b>" + output + "</b>";
        }
        output = $prefix + output + StringUtils.NewLine($htmlTag);
        this.eventsList.foreach(($value : ArrayList<any>, $type? : string) : void => {
            output += $prefix + StringUtils.Tab(1, $htmlTag) + "[\"" + $type + "\"]" +
                " hooked handlers count: " + $value.Length() + StringUtils.NewLine($htmlTag);
        });
        return output;
    }

    public toString() : string {
        return this.ToString();
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnStart($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_START, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setBeforeLoad($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.BEFORE_LOAD, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnLoad($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_LOAD, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnComplete($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_COMPLETE, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnShow($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_SHOW, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnHide($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_HIDE, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnFocus($handler : IEventsHandler) : void {
        if (this.targetIsInputElement()) {
            this.setEvent(EventType.ON_FOCUS, $handler);
        } else {
            this.globalEvents.setEvent(this.owner, EventType.ON_FOCUS, $handler);
        }
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnBlur($handler : IEventsHandler) : void {
        if (this.targetIsInputElement()) {
            this.setEvent(EventType.ON_BLUR, $handler);
        } else {
            this.globalEvents.setEvent(this.owner, EventType.ON_BLUR, $handler);
        }
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMouseOver($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_MOUSE_OVER, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMouseOut($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_MOUSE_OUT, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMouseMove($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_TOUCH_MOVE, this.getTouchHandler($handler));
        this.setEvent(EventType.ON_MOUSE_MOVE, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMouseDown($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_TOUCH_START, this.getTouchHandler($handler));
        this.setEvent(EventType.ON_MOUSE_DOWN, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMouseUp($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_TOUCH_END, this.getTouchHandler($handler));
        this.setEvent(EventType.ON_MOUSE_UP, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnClick($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_CLICK, $handler);
    }

    /**
     * @param {IMouseEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnDoubleClick($handler : IMouseEventsHandler) : void {
        this.setEvent(EventType.ON_DOUBLE_CLICK, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnChange($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_CHANGE, $handler);
    }

    /**
     * @param {IResizeEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setBeforeResize($handler : IResizeEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.BEFORE_RESIZE, $handler);
    }

    /**
     * @param {IResizeEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnResize($handler : IResizeEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_RESIZE, $handler);
    }

    /**
     * @param {IResizeBarEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnResizeStart($handler : IResizeBarEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_RESIZE_START, $handler);
    }

    /**
     * @param {IResizeBarEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnResizeChange($handler : IResizeBarEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_RESIZE_CHANGE, $handler);
    }

    /**
     * @param {IResizeBarEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnResizeComplete($handler : IResizeBarEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_RESIZE_COMPLETE, $handler);
    }

    /**
     * @param {IDragBarEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnDragStart($handler : IDragBarEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_DRAG_START, $handler);
    }

    /**
     * @param {IDragBarEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnDrag($handler : IDragBarEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_DRAG, $handler);
    }

    /**
     * @param {IDragBarEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnDragChange($handler : IDragBarEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_DRAG_CHANGE, $handler);
    }

    /**
     * @param {IDragBarEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnDragComplete($handler : IDragBarEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_DRAG_COMPLETE, $handler);
    }

    /**
     * @param {IScrollEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnScroll($handler : IScrollEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_SCROLL, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnOpen($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, DialogEventType.ON_OPEN, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnClose($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, DialogEventType.ON_CLOSE, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setBeforeOpen($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, BasePanelHolderEventType.BEFORE_OPEN, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setBeforeClose($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, BasePanelHolderEventType.BEFORE_CLOSE, $handler);
    }

    /**
     * @param {IDirectoryBrowserEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnPathRequest($handler : IDirectoryBrowserEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, DirectoryBrowserEventType.ON_PATH_REQUEST, $handler);
    }

    /**
     * @param {IDirectoryBrowserEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnDirectoryRequest($handler : IDirectoryBrowserEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, DirectoryBrowserEventType.ON_DIRECTORY_REQUEST, $handler);
    }

    /**
     * @param {IDirectoryBrowserEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnCreateDirectoryRequest($handler : IDirectoryBrowserEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, DirectoryBrowserEventType.ON_CREATE_DIRECTORY_REQUEST, $handler);
    }

    /**
     * @param {IDirectoryBrowserEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnRenameRequest($handler : IDirectoryBrowserEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, DirectoryBrowserEventType.ON_RENAME_REQUEST, $handler);
    }

    /**
     * @param {IDirectoryBrowserEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnDeleteRequest($handler : IDirectoryBrowserEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, DirectoryBrowserEventType.ON_DELETE_REQUEST, $handler);
    }

    /**
     * @param {IScrollEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnArrow($handler : IScrollEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, ScrollBarEventType.ON_ARROW, $handler);
    }

    /**
     * @param {IScrollEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnButton($handler : IScrollEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, ScrollBarEventType.ON_BUTTON, $handler);
    }

    /**
     * @param {IScrollEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnTracker($handler : IScrollEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, ScrollBarEventType.ON_TRACKER, $handler);
    }

    /**
     * @param {IEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnSelect($handler : IEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, EventType.ON_SELECT, $handler);
    }

    /**
     * @param {IMediaEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnFullscreenEnd($handler : IMediaEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, VideoEventType.ON_FULLSCREEN_END, $handler);
    }

    /**
     * @param {IMediaEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnFullscreenStart($handler : IMediaEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, VideoEventType.ON_FULLSCREEN_START, $handler);
    }

    /**
     * @param {IMediaEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnMute($handler : IMediaEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, AudioEventType.ON_MUTE, $handler);
    }

    /**
     * @param {IMediaEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnPause($handler : IMediaEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, AudioEventType.ON_PAUSE, $handler);
    }

    /**
     * @param {IMediaEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnPlay($handler : IMediaEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, AudioEventType.ON_PLAY, $handler);
    }

    /**
     * @param {IMediaEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnStop($handler : IMediaEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, AudioEventType.ON_STOP, $handler);
    }

    /**
     * @param {IMediaEventsHandler} $handler Specify event callback for current event type.
     * @returns {void}
     */
    public setOnUnmute($handler : IMediaEventsHandler) : void {
        this.globalEvents.setEvent(this.owner, AudioEventType.ON_UNMUTE, $handler);
    }

    protected excludeSerializationData() : string[] {
        const exclude : string[] = super.excludeSerializationData();
        if (!ObjectValidator.IsString(this.owner)) {
            exclude.push("owner");
        }
        return exclude;
    }

    protected targetIsInputElement() : boolean {
        const target : string = !ObjectValidator.IsEmptyOrNull(this.subscriber) ? this.subscriber : this.owner;
        if (ObjectValidator.IsString(target)) {
            if (ElementManager.Exists(target)) {
                return StringUtils.StartsWith((<HTMLInputElement>ElementManager.getElement(target)).outerHTML, "<input");
            }
            return !this.isSubscribed;
        }
        return false;
    }

    private getTouchHandler($handler : IMouseEventsHandler) : ITouchEventsHandler {
        return ($eventArgs : TouchEventArgs, $manager : GuiObjectManager, $reflection? : Reflection) : void => {
            const touches : TouchList = $eventArgs.NativeEventArgs().touches;
            let index : number;
            const touchesLength : number = touches.length;
            for (index = 0; index < touchesLength; index++) {
                const mouseEventArgs : MouseEventArgs = new MouseEventArgs(<any>touches[index]);
                mouseEventArgs.Owner($eventArgs.Owner());
                $handler(mouseEventArgs, $manager, $reflection);
            }
        };
    }

    private getTarget($targetId? : string, $force? : boolean) : any {
        let target : any;
        const owner : string = this.getOwner();
        if (!ObjectValidator.IsEmptyOrNull($targetId)) {
            target = ElementManager.getElement($targetId, $force);
        } else if (!ObjectValidator.IsEmptyOrNull(this.subscriber)) {
            target = ElementManager.getElement(this.subscriber, $force);
        } else if (!ObjectValidator.IsEmptyOrNull(owner)) {
            target = ElementManager.getElement(owner, $force);
        } else {
            target = document;
        }
        return target;
    }
}
