/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Exception } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { IEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventArgs.js";
import { IEventsManager } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventsManager.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IEventsHandler } from "../Interfaces/IEventsHandler.js";

/**
 * AnimationThreadPool class provides animation scheduling.
 */
export class AnimationThreadPool extends BaseObject {
    private static threadId : number;

    /**
     * @param {string} $owner Specify thread owner.
     * @param {string} $type Specify type of thread.
     * @returns {boolean} Returns true, if specified thread is running, otherwise false.
     */
    public static IsRunning($owner : string, $type : string) : boolean {
        return AnimationThreadPool.getEvents().Exists($owner, $type);
    }

    /**
     * @param {string} $owner Specify thread owner.
     * @param {string} $type Specify type of thread.
     * @param {IEventsHandler} [$handler] Function suitable for handling of the thread.
     * @param {IEventArgs} [$args] Specify initial thread args.
     * @returns {void}
     */
    public static AddThread($owner : string, $type : string, $handler? : IEventsHandler, $args? : IEventArgs) : void {
        AnimationThreadPool.getEvents().setEvent($owner, $type, $handler, $args);
    }

    /**
     * @param {string} $owner Specify thread owner.
     * @param {string} $type Specify type of thread.
     * @param {IEventArgs} $args Specify current thread args.
     * @returns {void}
     */
    public static setThreadArgs($owner : string, $type : string, $args : IEventArgs) : void {
        AnimationThreadPool.getEvents().setEventArgs($owner, $type, $args);
    }

    /**
     * @param {string} $owner Specify thread owner.
     * @param {string} $type Specify type of thread, which should be removed.
     * @returns {void}
     */
    public static RemoveThread($owner : string, $type : string) : void {
        AnimationThreadPool.getEvents().Clear($owner, $type);
    }

    /**
     * Fire execution of all registered threads.
     * @returns {void}
     */
    public static Execute() : void {
        if (ObjectValidator.IsEmptyOrNull(AnimationThreadPool.threadId)) {
            AnimationThreadPool.startLoop();
        }
    }

    /**
     * Stop execution of all registered threads.
     * @returns {void}
     */
    public static Clear() : void {
        if (this.IsSupported()) {
            AnimationThreadPool.Clear = () : void => {
                window.cancelAnimationFrame(AnimationThreadPool.threadId);
                AnimationThreadPool.threadId = null;
                AnimationThreadPool.getEvents().Clear();
            };
            AnimationThreadPool.Clear();
        } else {
            // eslint-disable-next-line @typescript-eslint/only-throw-error
            throw new Exception(AnimationThreadPool.ClassName() + " is not supported in this execution environment.");
        }
    }

    /**
     * @returns {boolean} Returns true, if current environment is supported, otherwise false.
     */
    public static IsSupported() : boolean {
        return ObjectValidator.IsSet(window.requestAnimationFrame);
    }

    private static startLoop() : void {
        if (this.IsSupported()) {
            const events : IEventsManager = AnimationThreadPool.getEvents();
            AnimationThreadPool.startLoop = () : void => {
                if (!events.getAll().IsEmpty()) {
                    AnimationThreadPool.threadId = window.requestAnimationFrame(() : void => {
                        const owners : any[] = events.getAll().getKeys();
                        let ownerIndex : number;
                        let typeIndex : number;
                        for (ownerIndex = 0; ownerIndex < owners.length; ownerIndex++) {
                            const types : any[] = events.getAll().getItem(owners[ownerIndex]).getKeys();
                            for (typeIndex = 0; typeIndex < types.length; typeIndex++) {
                                events.FireEvent(owners[ownerIndex], types[typeIndex], false);
                            }
                        }
                        AnimationThreadPool.startLoop();
                    });
                } else {
                    AnimationThreadPool.threadId = null;
                }
            };
            return AnimationThreadPool.startLoop();
        } else {
            // eslint-disable-next-line @typescript-eslint/only-throw-error
            throw new Exception(AnimationThreadPool.ClassName() + " is not supported in this execution environment.");
        }
    }

    private static getEvents() : IEventsManager {
        const instance : IEventsManager = new globalThis.Io.Oidis.Gui.Events.EventsManager();
        this.getEvents = () : IEventsManager => {
            return instance;
        };
        return instance;
    }
}
