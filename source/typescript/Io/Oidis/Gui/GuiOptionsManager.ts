/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { GuiOptionType } from "./Enums/GuiOptionType.js";

/**
 * GuiOptionsManager class provides register of all allowed GUI objects options.
 */
export class GuiOptionsManager extends BaseObject {
    private optionsList : ArrayList<GuiOptionType>;
    private readonly availableOptionsList : ArrayList<GuiOptionType>;

    constructor($availableList : ArrayList<GuiOptionType>) {
        super();
        this.optionsList = new ArrayList<GuiOptionType>();
        this.availableOptionsList = $availableList;
    }

    /**
     * @param {GuiOptionType} $guiOptionType Option type, which should be allowed at current GUI object.
     * @returns {void}
     */
    public Add($guiOptionType : GuiOptionType) : void {
        if (!ObjectValidator.IsEmptyOrNull($guiOptionType)) {
            if (this.availableOptionsList.Length() === 0) {
                ExceptionsManager.Throw(this.getClassName(),
                    "No available gui option has been set to current GUI object.");
            }

            if (!this.optionsList.Contains($guiOptionType)) {
                if ($guiOptionType === GuiOptionType.ALL) {
                    this.optionsList = this.availableOptionsList;
                } else {
                    if (this.availableOptionsList.Contains($guiOptionType)) {
                        this.optionsList.Add($guiOptionType);
                    } else {
                        ExceptionsManager.Throw(this.getClassName(),
                            "Option '" + GuiOptionType[$guiOptionType] + "' is not allowed on this object.");
                    }
                }
            }
        }
    }

    /**
     * @param {...GuiOptionType} $guiOptionType Open-ended options type, which should be validated.
     * @returns {boolean} Returns true, if GUI object has allowed desired option type, otherwise false.
     */
    public Contains(...$guiOptionType : GuiOptionType[]) : boolean {
        let index : number;
        for (index = 0; index < $guiOptionType.length; index++) {
            if (this.optionsList.Contains($guiOptionType[index])) {
                return true;
            }
        }
        return false;
    }
}
