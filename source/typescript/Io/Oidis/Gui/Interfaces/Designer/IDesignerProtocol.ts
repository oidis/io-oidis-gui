/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { DesignerTaskType } from "../../Enums/DesignerTaskType.js";
import { GuiCommonsArgType } from "../../Enums/GuiCommonsArgType.js";
import { IGuiCommonsArg } from "../Primitives/IGuiCommonsArg.js";
import { IDesignerElementMap } from "./IDesignerElementMap.js";

export interface IDesignerItemProtocol {
    id : string;
    top : number;
    left : number;
    width : number;
    height : number;
    zoom : number;
    draggable : boolean;
    resizeable : boolean;
}

export interface IDesignerItemArgProtocol {
    id : string;
    name : string;
    type : GuiCommonsArgType;
    value : string | number | boolean;
}

export interface IDesignerItemArgsProtocol {
    id : string;
    value : IGuiCommonsArg[];
}

export interface IDesignerEventProtocol {
    type : string;
    args : string;
}

export interface IDesignerProjectMapItems {
    names : string[];
}

export interface IDesignerProjectMapViewers extends IDesignerProjectMapItems {
    links : string[];
}

export interface IDesignerProjectMapGuiObjects {
    components : IDesignerProjectMapItems;
    userControls : IDesignerProjectMapItems;
    panels : IDesignerProjectMapItems;
}

export interface IDesignerProjectMapProtocol {
    host : string;
    blankViewer : string;
    viewers : IDesignerProjectMapViewers;
    runtimeTests : IDesignerProjectMapViewers;
    guiObjects : IDesignerProjectMapGuiObjects;
}

export interface IDesignerInstanceMapItem extends IDesignerElementMap {
    top : number;
    left : number;
    width : number;
    height : number;
}

export interface IDesignerProtocol {
    task : DesignerTaskType;
    data : any;
}

// generated-code-start
/* eslint-disable */
export const IDesignerItemProtocol = globalThis.RegisterInterface(["id", "top", "left", "width", "height", "zoom", "draggable", "resizeable"]);
export const IDesignerItemArgProtocol = globalThis.RegisterInterface(["id", "name", "type", "value"]);
export const IDesignerItemArgsProtocol = globalThis.RegisterInterface(["id", "value"]);
export const IDesignerEventProtocol = globalThis.RegisterInterface(["type", "args"]);
export const IDesignerProjectMapItems = globalThis.RegisterInterface(["names"]);
export const IDesignerProjectMapViewers = globalThis.RegisterInterface(["links"], <any>IDesignerProjectMapItems);
export const IDesignerProjectMapGuiObjects = globalThis.RegisterInterface(["components", "userControls", "panels"]);
export const IDesignerProjectMapProtocol = globalThis.RegisterInterface(["host", "blankViewer", "viewers", "runtimeTests", "guiObjects"]);
export const IDesignerInstanceMapItem = globalThis.RegisterInterface(["top", "left", "width", "height"], <any>IDesignerElementMap);
export const IDesignerProtocol = globalThis.RegisterInterface(["task", "data"]);
/* eslint-enable */
// generated-code-end
