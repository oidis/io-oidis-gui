/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { GuiOptionType } from "../../Enums/GuiOptionType.js";

export interface IBaseGuiGroupObjectArgs extends IBaseObject {
    Visible($value? : boolean) : boolean;

    ForceSetValue($value? : boolean) : boolean;

    Value($value? : string | number | boolean) : string | number | boolean;

    Enabled($value? : boolean) : boolean;

    Error($value? : boolean) : boolean;

    AddGuiOption($value : GuiOptionType) : void;

    getGuiOptionsList() : ArrayList<GuiOptionType>;

    TitleText($value? : string) : string;

    Width($value? : number) : number;

    Resize($value? : boolean) : boolean;
}

// generated-code-start
/* eslint-disable */
export const IBaseGuiGroupObjectArgs = globalThis.RegisterInterface(["Visible", "ForceSetValue", "Value", "Enabled", "Error", "AddGuiOption", "getGuiOptionsList", "TitleText", "Width", "Resize"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
