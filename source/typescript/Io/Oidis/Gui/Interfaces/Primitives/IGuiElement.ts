/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IArrayList.js";
import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { IBaseViewer } from "./IBaseViewer.js";
import { IGuiCommons } from "./IGuiCommons.js";
import { IResponsiveElement } from "./IResponsiveElement.js";

export interface IGuiElement extends IBaseObject {
    Id($value : string) : IGuiElement;

    getId() : string;

    GuiTypeTag($value : string) : IGuiElement;

    StyleClassName($value : string | BaseEnum) : IGuiElement;

    Visible($value : boolean) : IGuiElement;

    getVisible() : boolean;

    Width($value : number) : IGuiElement;

    getWidth() : number;

    Height($value : number) : IGuiElement;

    getHeight() : number;

    setAttribute($key : string, $value : string) : IGuiElement;

    Add($value : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement) : IGuiElement;

    addChildElement($value : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement) : void;

    getChildElements() : IArrayList<string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement>;

    getChildElement($index : number) : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement;

    getGuiChildElements() : IArrayList<IGuiCommons | IGuiElement | IResponsiveElement | IBaseViewer>;

    getGuiTypeTag() : string;

    getAttributes() : ArrayList<string>;

    Draw($EOL : string) : string;

    ToDOMElement($EOL : string) : HTMLElement;

    getWrappingElement() : IGuiElement;

    setWrappingElement($value : IGuiElement) : void;
}

// generated-code-start
/* eslint-disable */
export const IGuiElement = globalThis.RegisterInterface(["Id", "getId", "GuiTypeTag", "StyleClassName", "Visible", "getVisible", "Width", "getWidth", "Height", "getHeight", "setAttribute", "Add", "addChildElement", "getChildElements", "getChildElement", "getGuiChildElements", "getGuiTypeTag", "getAttributes", "Draw", "ToDOMElement", "getWrappingElement", "setWrappingElement"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
