/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBasePanelViewerArgs } from "./IBasePanelViewerArgs.js";
import { IBaseViewer } from "./IBaseViewer.js";

export interface IBasePanelViewer extends IBaseViewer {
    ViewerArgs($args? : IBasePanelViewerArgs) : IBasePanelViewerArgs;
}

// generated-code-start
export const IBasePanelViewer = globalThis.RegisterInterface(["ViewerArgs"], <any>IBaseViewer);
// generated-code-end
