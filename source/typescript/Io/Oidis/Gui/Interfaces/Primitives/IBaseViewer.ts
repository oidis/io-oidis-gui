/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { BaseViewerLayerType } from "../../Enums/BaseViewerLayerType.js";
import { IBaseGuiObject } from "./IBaseGuiObject.js";
import { IBaseViewerArgs } from "./IBaseViewerArgs.js";

export interface IBaseViewer extends IBaseObject {
    InstanceOwner($value? : IBaseViewer) : IBaseViewer;

    IsCached($value? : boolean) : boolean;

    IsPrinted() : boolean;

    TestModeEnabled($value? : boolean) : boolean;

    ViewerArgs($args? : IBaseViewerArgs) : IBaseViewerArgs;

    getInstance() : IBaseGuiObject;

    setLayer($type : BaseViewerLayerType) : void;

    getLayersType() : string;

    PrepareImplementation() : void;

    Show($EOL? : string) : string;
}

// generated-code-start
/* eslint-disable */
export const IBaseViewer = globalThis.RegisterInterface(["InstanceOwner", "IsCached", "IsPrinted", "TestModeEnabled", "ViewerArgs", "getInstance", "setLayer", "getLayersType", "PrepareImplementation", "Show"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
