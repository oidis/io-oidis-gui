/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseGuiGroupObjectArgs } from "./IBaseGuiGroupObjectArgs.js";
import { IFormsObject } from "./IFormsObject.js";

export interface IBaseGuiGroupObject extends IFormsObject {
    Configuration($value? : IBaseGuiGroupObjectArgs) : IBaseGuiGroupObjectArgs;

    getWidth() : number;
}

// generated-code-start
export const IBaseGuiGroupObject = globalThis.RegisterInterface(["Configuration", "getWidth"], <any>IFormsObject);
// generated-code-end
