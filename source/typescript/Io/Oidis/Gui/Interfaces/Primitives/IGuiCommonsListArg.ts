/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IGuiCommonsArg } from "./IGuiCommonsArg.js";

export interface IGuiCommonsListArg extends IGuiCommonsArg {
    items : string[];
}

// generated-code-start
export const IGuiCommonsListArg = globalThis.RegisterInterface(["items"], <any>IGuiCommonsArg);
// generated-code-end
