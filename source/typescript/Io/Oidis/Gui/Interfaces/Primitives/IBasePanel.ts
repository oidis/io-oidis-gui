/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IArrayList.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { PanelContentType } from "../../Enums/PanelContentType.js";
import { IBasePanelEvents } from "../Events/IBasePanelEvents.js";
import { IIcon } from "../UserControls/IIcon.js";
import { ILabel } from "../UserControls/ILabel.js";
import { IBasePanelViewer } from "./IBasePanelViewer.js";
import { IBasePanelViewerArgs } from "./IBasePanelViewerArgs.js";
import { IBaseViewer } from "./IBaseViewer.js";
import { IFormsObject } from "./IFormsObject.js";
import { IGuiCommons } from "./IGuiCommons.js";
import { IGuiElement } from "./IGuiElement.js";
import { IResponsiveElement } from "./IResponsiveElement.js";

export interface IBasePanel extends IFormsObject {
    loaderIcon : IIcon;
    loaderText : ILabel;

    AddChild($element : IGuiCommons, $variableName? : string) : void;

    getChildPanelList() : ArrayList<IBasePanelViewer>;

    setChildPanelArgs($instanceOrId : string | IBasePanel, $args : IBasePanelViewerArgs) : void;

    ContentType($type? : PanelContentType) : PanelContentType;

    Scrollable($value? : boolean) : boolean;

    Width($value? : number) : number;

    Height($value? : number) : number;

    getScrollTop() : number;

    getScrollLeft() : number;

    getEvents() : IBasePanelEvents;

    Value($value? : string | IBasePanelViewerArgs) : string | IBasePanelViewerArgs;
}

export class IContainerSizeInfo {
    public elementsWithoutWidth : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>;
    public elementsWithoutHeight : IArrayList<IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer>;
    public remainingWidth : number;
    public remainingHeight : number;
}

export class IContainerVisibilityInfo {
    public commonsNum : number;
    public visibleCommons : number;
}

// generated-code-start
/* eslint-disable */
export const IBasePanel = globalThis.RegisterInterface(["loaderIcon", "loaderText", "AddChild", "getChildPanelList", "setChildPanelArgs", "ContentType", "Scrollable", "Width", "Height", "getScrollTop", "getScrollLeft", "getEvents", "Value"], <any>IFormsObject);
/* eslint-enable */
// generated-code-end
