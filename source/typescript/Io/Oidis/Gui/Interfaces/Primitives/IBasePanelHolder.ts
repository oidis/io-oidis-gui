/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBasePanelHolderEvents } from "../Events/IBasePanelHolderEvents.js";
import { IBasePanel } from "./IBasePanel.js";

export interface IBasePanelHolder extends IBasePanel {
    IsOpened($value ? : boolean) : boolean;

    getBody() : any;

    getEvents() : IBasePanelHolderEvents;
}

// generated-code-start
export const IBasePanelHolder = globalThis.RegisterInterface(["IsOpened", "getBody", "getEvents"], <any>IBasePanel);
// generated-code-end
