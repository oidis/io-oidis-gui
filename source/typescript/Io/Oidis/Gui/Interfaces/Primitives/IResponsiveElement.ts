/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { Alignment } from "../../Enums/Alignment.js";
import { FitToParent } from "../../Enums/FitToParent.js";
import { UnitType } from "../../Enums/UnitType.js";
import { VisibilityStrategy } from "../../Enums/VisibilityStrategy.js";
import { PropagableNumber } from "../../Structures/PropagableNumber.js";
import { IBaseViewer } from "./IBaseViewer.js";
import { IGuiCommons } from "./IGuiCommons.js";
import { IGuiElement } from "./IGuiElement.js";

export interface IResponsiveElement extends IGuiElement {

    StyleClassName($value : string | BaseEnum) : IGuiElement;

    WidthOfColumn($value : string, $isPropagated? : boolean) : IResponsiveElement;

    WidthOfColumn($value : () => PropagableNumber) : IResponsiveElement;

    WidthOfColumn($value : number, $unitType : UnitType, $isPropagated? : boolean) : IResponsiveElement;

    HeightOfRow($value : string, $isPropagated? : boolean) : IResponsiveElement;

    HeightOfRow($value : () => PropagableNumber) : IResponsiveElement;

    HeightOfRow($value : number, $unitType : UnitType, $isPropagated? : boolean) : IResponsiveElement;

    getWidthOfColumn() : PropagableNumber;

    getHeightOfRow() : PropagableNumber;

    Alignment($value : Alignment | (() => Alignment)) : IResponsiveElement;

    getAlignment() : Alignment;

    FitToParent($value : FitToParent | (() => FitToParent)) : IResponsiveElement;

    getFitToParent() : FitToParent;

    VisibilityStrategy($value : VisibilityStrategy | (() => VisibilityStrategy)) : IResponsiveElement;

    getVisibilityStrategy() : VisibilityStrategy;

    Add($value : string | IGuiElement | IResponsiveElement | IGuiCommons | IBaseViewer | HTMLElement) : IGuiElement;
}

// generated-code-start
/* eslint-disable */
export const IResponsiveElement = globalThis.RegisterInterface(["StyleClassName", "WidthOfColumn", "HeightOfRow", "getWidthOfColumn", "getHeightOfRow", "Alignment", "getAlignment", "FitToParent", "getFitToParent", "VisibilityStrategy", "getVisibilityStrategy", "Add"], <any>IGuiElement);
/* eslint-enable */
// generated-code-end
