/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPanelHolderStrategy } from "../Strategies/IPanelHolderStrategy.js";
import { IBasePanelViewerArgs } from "./IBasePanelViewerArgs.js";

export interface IBasePanelHolderViewerArgs extends IBasePanelViewerArgs {
    HeaderText($value? : string) : string;

    DescriptionText($value? : string) : string;

    IsOpened($value? : boolean) : boolean;

    BodyArgs() : any;

    HolderViewerClass($value? : any) : any;

    BodyViewerClass($value? : any) : any;

    Strategy($value? : IPanelHolderStrategy);
}

// generated-code-start
/* eslint-disable */
export const IBasePanelHolderViewerArgs = globalThis.RegisterInterface(["HeaderText", "DescriptionText", "IsOpened", "BodyArgs", "HolderViewerClass", "BodyViewerClass", "Strategy"], <any>IBasePanelViewerArgs);
/* eslint-enable */
// generated-code-end
