/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { INotification } from "../Components/INotification.js";
import { IBaseGuiObject } from "./IBaseGuiObject.js";

export interface IAbstractGuiObject extends IBaseGuiObject {
    Notification() : INotification;

    setSize($width : number, $height : number) : void;

    StyleAttributes($value? : string) : string;

    AppendStyleAttributes($value : string) : void;
}

// generated-code-start
/* eslint-disable */
export const IAbstractGuiObject = globalThis.RegisterInterface(["Notification", "setSize", "StyleAttributes", "AppendStyleAttributes"], <any>IBaseGuiObject);
/* eslint-enable */
// generated-code-end
