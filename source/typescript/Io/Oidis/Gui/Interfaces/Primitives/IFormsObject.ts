/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { INotification } from "../Components/INotification.js";
import { IFormsObjectEvents } from "../Events/IFormsObjectEvents.js";
import { IBaseGuiObject } from "./IBaseGuiObject.js";

export interface IFormsObject extends IBaseGuiObject {
    Notification() : INotification;

    Error($value? : boolean) : boolean;

    TabIndex($value? : number) : number;

    getSelectorEvents() : IFormsObjectEvents;

    getEvents() : IFormsObjectEvents;

    IsPersistent($value? : boolean) : boolean;
}

// generated-code-start
/* eslint-disable */
export const IFormsObject = globalThis.RegisterInterface(["Notification", "Error", "TabIndex", "getSelectorEvents", "getEvents", "IsPersistent"], <any>IBaseGuiObject);
/* eslint-enable */
// generated-code-end
