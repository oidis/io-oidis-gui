/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IToolTip } from "../Components/IToolTip.js";
import { IBasePanelViewerArgs } from "./IBasePanelViewerArgs.js";
import { IGuiCommons } from "./IGuiCommons.js";

export interface IBaseGuiObject extends IGuiCommons {
    Title() : IToolTip;

    Changed() : boolean;

    Value($value? : string | number | boolean | IBasePanelViewerArgs) : string | number | boolean | IBasePanelViewerArgs;
}

// generated-code-start
export const IBaseGuiObject = globalThis.RegisterInterface(["Title", "Changed", "Value"], <any>IGuiCommons);
// generated-code-end
