/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IArrayList.js";
import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { FitToParent } from "../../Enums/FitToParent.js";
import { PositionType } from "../../Enums/PositionType.js";
import { GuiOptionsManager } from "../../GuiOptionsManager.js";
import { ElementOffset } from "../../Structures/ElementOffset.js";
import { Size } from "../../Structures/Size.js";
import { IGuiCommonsEvents } from "../Events/IGuiCommonsEvents.js";
import { IBaseViewer } from "./IBaseViewer.js";
import { IGuiCommonsArg } from "./IGuiCommonsArg.js";
import { IGuiElement } from "./IGuiElement.js";

export interface IGuiCommons extends IBaseObject {
    getGuiOptions() : GuiOptionsManager;

    getEvents() : IGuiCommonsEvents;

    getGuiElementClass() : any;

    Visible($value? : boolean) : boolean;

    Enabled($value? : boolean) : boolean;

    StyleClassName($value? : string) : string;

    Id() : string;

    Parent($value? : IGuiCommons) : IGuiCommons;

    InstanceOwner($value? : IBaseViewer) : IBaseViewer;

    InstancePath($value? : string) : string;

    getChildElements() : IArrayList<IGuiCommons>;

    getScreenPosition() : ElementOffset;

    setPosition($top : number, $left : number, $type? : PositionType) : void;

    getSize() : Size;

    Width($value? : number) : number;

    Height($value? : number) : number;

    FitToParent($value : FitToParent) : IGuiCommons;

    getFitToParent() : FitToParent;

    IsCached() : boolean;

    IsPrepared() : boolean;

    IsLoaded() : boolean;

    IsCompleted() : boolean;

    DisableAsynchronousDraw() : void;

    Draw($EOL? : string) : string;

    getArgs() : IGuiCommonsArg[];

    setArg($value : IGuiCommonsArg, $force? : boolean) : void;

    getInnerHtmlMap() : IGuiElement;

    getWrappingElement() : IGuiElement;

    setWrappingElement($value : IGuiElement) : void;

    getWrappingContainer() : IGuiElement;

    setWrappingContainer($value : IGuiElement) : void;

    IsPreventingScroll() : boolean;
}

// generated-code-start
/* eslint-disable */
export const IGuiCommons = globalThis.RegisterInterface(["getGuiOptions", "getEvents", "getGuiElementClass", "Visible", "Enabled", "StyleClassName", "Id", "Parent", "InstanceOwner", "InstancePath", "getChildElements", "getScreenPosition", "setPosition", "getSize", "Width", "Height", "FitToParent", "getFitToParent", "IsCached", "IsPrepared", "IsLoaded", "IsCompleted", "DisableAsynchronousDraw", "Draw", "getArgs", "setArg", "getInnerHtmlMap", "getWrappingElement", "setWrappingElement", "getWrappingContainer", "setWrappingContainer", "IsPreventingScroll"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
