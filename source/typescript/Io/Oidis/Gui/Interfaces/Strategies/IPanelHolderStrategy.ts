/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBasePanelHolder } from "../Primitives/IBasePanelHolder.js";

/**
 * IPanelHolderStrategy interface represents strategy of BasePanelHolder resize behaviour
 */
export interface IPanelHolderStrategy {

    ResizeHolder($element : IBasePanelHolder, size? : number, $force? : boolean);

    getContentBasedSize($element : IBasePanelHolder) : number;

    getParentSize($element : IBasePanelHolder) : number;

    getHeaderSize($element : IBasePanelHolder) : number;

    getName() : string;
}

// generated-code-start
/* eslint-disable */
export const IPanelHolderStrategy = globalThis.RegisterInterface(["ResizeHolder", "getContentBasedSize", "getParentSize", "getHeaderSize", "getName"]);
/* eslint-enable */
// generated-code-end
