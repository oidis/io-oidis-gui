/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IEventsHandler } from "../IEventsHandler.js";
import { IDragBarEventsHandler } from "./IDragBarEventsHandler.js";
import { IFormsObjectEvents } from "./IFormsObjectEvents.js";
import { IResizeEventsHandler } from "./IResizeEventsHandler.js";

export interface IDialogEvents extends IFormsObjectEvents {
    setOnOpen($handler : IEventsHandler) : void;

    setOnClose($handler : IEventsHandler) : void;

    setOnDrag($handler : IDragBarEventsHandler) : void;

    setBeforeResize($handler : IResizeEventsHandler) : void;

    setOnResize($handler : IResizeEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const IDialogEvents = globalThis.RegisterInterface(["setOnOpen", "setOnClose", "setOnDrag", "setBeforeResize", "setOnResize"], <any>IFormsObjectEvents);
/* eslint-enable */
// generated-code-end
