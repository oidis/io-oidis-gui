/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IAudioEvents } from "./IAudioEvents.js";
import { IMediaEventsHandler } from "./IMediaEventsHandler.js";

export interface IVideoEvents extends IAudioEvents {

    setOnFullscreenStart($handler : IMediaEventsHandler) : void;

    setOnFullscreenEnd($handler : IMediaEventsHandler) : void;
}

// generated-code-start
export const IVideoEvents = globalThis.RegisterInterface(["setOnFullscreenStart", "setOnFullscreenEnd"], <any>IAudioEvents);
// generated-code-end
