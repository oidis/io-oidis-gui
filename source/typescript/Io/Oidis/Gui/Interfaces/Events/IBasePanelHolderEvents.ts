/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IEventsHandler } from "../IEventsHandler.js";
import { IBasePanelEvents } from "./IBasePanelEvents.js";

export interface IBasePanelHolderEvents extends IBasePanelEvents {
    setOnOpen($handler : IEventsHandler) : void;

    setOnClose($handler : IEventsHandler) : void;

    setBeforeOpen($handler : IEventsHandler) : void;

    setBeforeClose($handler : IEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const IBasePanelHolderEvents = globalThis.RegisterInterface(["setOnOpen", "setOnClose", "setBeforeOpen", "setBeforeClose"], <any>IBasePanelEvents);
/* eslint-enable */
// generated-code-end
