/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IEventsHandler } from "../IEventsHandler.js";
import { IErrorEventsHandler } from "./IErrorEventsHandler.js";
import { IFileUploadEventsHandler } from "./IFileUploadEventsHandler.js";
import { IGuiCommonsEvents } from "./IGuiCommonsEvents.js";

export interface IFileUploadEvents extends IGuiCommonsEvents {
    setOnError($handler : IErrorEventsHandler) : void;

    setOnChange($handler : IEventsHandler) : void;

    setOnAboard($handler : IFileUploadEventsHandler) : void;

    setOnUploadStart($handler : IFileUploadEventsHandler) : void;

    setOnUploadChange($handler : IFileUploadEventsHandler) : void;

    setOnUploadComplete($handler : IFileUploadEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const IFileUploadEvents = globalThis.RegisterInterface(["setOnError", "setOnChange", "setOnAboard", "setOnUploadStart", "setOnUploadChange", "setOnUploadComplete"], <any>IGuiCommonsEvents);
/* eslint-enable */
// generated-code-end
