/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IFormsObjectEvents } from "./IFormsObjectEvents.js";
import { IResizeEventsHandler } from "./IResizeEventsHandler.js";
import { IScrollEventsHandler } from "./IScrollEventsHandler.js";

export interface IBasePanelEvents extends IFormsObjectEvents {
    setOnScroll($handler : IScrollEventsHandler) : void;

    setBeforeResize($handler : IResizeEventsHandler) : void;

    setOnResize($handler : IResizeEventsHandler) : void;

    setOnResizeComplete($handler : IResizeEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const IBasePanelEvents = globalThis.RegisterInterface(["setOnScroll", "setBeforeResize", "setOnResize", "setOnResizeComplete"], <any>IFormsObjectEvents);
/* eslint-enable */
// generated-code-end
