/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IEventsHandler } from "../IEventsHandler.js";
import { IFormsObjectEvents } from "./IFormsObjectEvents.js";
import { IResizeEventsHandler } from "./IResizeEventsHandler.js";
import { IScrollEventsHandler } from "./IScrollEventsHandler.js";

export interface IDropDownListEvents extends IFormsObjectEvents {
    setOnResize($handler : IResizeEventsHandler) : void;

    setOnScroll($handler : IScrollEventsHandler) : void;

    setOnSelect($handler : IEventsHandler) : void;
}

// generated-code-start
export const IDropDownListEvents = globalThis.RegisterInterface(["setOnResize", "setOnScroll", "setOnSelect"], <any>IFormsObjectEvents);
// generated-code-end
