/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { MessageEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/MessageEventArgs.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { GuiObjectManager } from "../../GuiObjectManager.js";
import { IEventsHandler } from "../IEventsHandler.js";

export interface IMessageEventsHandler extends IEventsHandler {
    ($eventArgs : MessageEventArgs, $guiObjectManager : GuiObjectManager, ...$args : any[]) : void;

    ($eventArgs : MessageEventArgs, $guiObjectManager : GuiObjectManager, $reflection : Reflection) : void;
}

// generated-code-start
export const IMessageEventsHandler = globalThis.RegisterInterface([], <any>IEventsHandler);
// generated-code-end
