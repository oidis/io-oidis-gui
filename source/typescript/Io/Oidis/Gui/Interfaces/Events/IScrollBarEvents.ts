/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IGuiCommonsEvents } from "./IGuiCommonsEvents.js";
import { IScrollEventsHandler } from "./IScrollEventsHandler.js";

export interface IScrollBarEvents extends IGuiCommonsEvents {
    setOnArrow($handler : IScrollEventsHandler) : void;

    setOnButton($handler : IScrollEventsHandler) : void;

    setOnTracker($handler : IScrollEventsHandler) : void;

    setOnScroll($handler : IScrollEventsHandler) : void;

    setOnChange($handler : IScrollEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const IScrollBarEvents = globalThis.RegisterInterface(["setOnArrow", "setOnButton", "setOnTracker", "setOnScroll", "setOnChange"], <any>IGuiCommonsEvents);
/* eslint-enable */
// generated-code-end
