/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IEventsHandler } from "../IEventsHandler.js";
import { IFormsObjectEvents } from "./IFormsObjectEvents.js";
import { IMediaEventsHandler } from "./IMediaEventsHandler.js";

export interface IAudioEvents extends IFormsObjectEvents {

    setOnPlay($handler : IMediaEventsHandler) : void;

    setOnStop($handler : IMediaEventsHandler) : void;

    setOnPause($handler : IMediaEventsHandler) : void;

    setOnMute($handler : IMediaEventsHandler) : void;

    setOnUnmute($handler : IMediaEventsHandler) : void;

    setOnOpen($handler : IEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const IAudioEvents = globalThis.RegisterInterface(["setOnPlay", "setOnStop", "setOnPause", "setOnMute", "setOnUnmute", "setOnOpen"], <any>IFormsObjectEvents);
/* eslint-enable */
// generated-code-end
