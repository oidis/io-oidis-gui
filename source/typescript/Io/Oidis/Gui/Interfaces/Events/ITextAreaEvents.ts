/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IFormsObjectEvents } from "./IFormsObjectEvents.js";
import { IResizeEventsHandler } from "./IResizeEventsHandler.js";
import { IScrollEventsHandler } from "./IScrollEventsHandler.js";

export interface ITextAreaEvents extends IFormsObjectEvents {
    setBeforeResize($handler : IResizeEventsHandler) : void;

    setOnResize($handler : IResizeEventsHandler) : void;

    setOnScroll($handler : IScrollEventsHandler) : void;
}

// generated-code-start
export const ITextAreaEvents = globalThis.RegisterInterface(["setBeforeResize", "setOnResize", "setOnScroll"], <any>IFormsObjectEvents);
// generated-code-end
