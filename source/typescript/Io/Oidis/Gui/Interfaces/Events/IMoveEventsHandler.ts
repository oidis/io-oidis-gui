/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { MoveEventArgs } from "../../Events/Args/MoveEventArgs.js";
import { GuiObjectManager } from "../../GuiObjectManager.js";
import { IEventsHandler } from "../IEventsHandler.js";

export interface IMoveEventsHandler extends IEventsHandler {
    ($eventArgs : MoveEventArgs, $guiObjectManager : GuiObjectManager, ...$args : any[]) : void;

    ($eventArgs : MoveEventArgs, $guiObjectManager : GuiObjectManager, $reflection : Reflection) : void;
}

// generated-code-start
export const IMoveEventsHandler = globalThis.RegisterInterface([], <any>IEventsHandler);
// generated-code-end
