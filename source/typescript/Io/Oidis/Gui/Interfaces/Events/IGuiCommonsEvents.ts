/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IArrayList.js";
import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { IEventsHandler } from "../IEventsHandler.js";
import { IMouseEventsHandler } from "./IMouseEventsHandler.js";

export interface IGuiCommonsEvents extends IBaseObject {
    getOwner() : string;

    Subscriber($value? : string) : string;

    Exists($type : string) : boolean;

    getAll() : IArrayList<IArrayList<IEventsHandler>>;

    setEvent($type : string, $handler : IEventsHandler) : void;

    FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean) : number;

    FireAsynchronousMethod($handler : () => void, $waitForMilliseconds? : number) : number;

    FireAsynchronousMethod($handler : () => void, $clearBefore? : boolean, $waitForMilliseconds? : number) : number;

    RemoveHandler($type : string, $handler : IEventsHandler) : void;

    Clear($type? : string) : void;

    Subscribe($force? : boolean) : void;

    Subscribe($targetId? : string, $force? : boolean) : void;

    setOnStart($handler : IEventsHandler) : void;

    setBeforeLoad($handler : IEventsHandler) : void;

    setOnLoad($handler : IEventsHandler) : void;

    setOnComplete($handler : IEventsHandler) : void;

    setOnShow($handler : IEventsHandler) : void;

    setOnHide($handler : IEventsHandler) : void;

    setOnMouseOver($handler : IMouseEventsHandler) : void;

    setOnMouseOut($handler : IMouseEventsHandler) : void;

    setOnMouseMove($handler : IMouseEventsHandler) : void;

    setOnMouseDown($handler : IMouseEventsHandler) : void;

    setOnMouseUp($handler : IMouseEventsHandler) : void;

    setOnClick($handler : IMouseEventsHandler) : void;

    setOnDoubleClick($handler : IMouseEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const IGuiCommonsEvents = globalThis.RegisterInterface(["getOwner", "Subscriber", "Exists", "getAll", "setEvent", "FireAsynchronousMethod", "RemoveHandler", "Clear", "Subscribe", "setOnStart", "setBeforeLoad", "setOnLoad", "setOnComplete", "setOnShow", "setOnHide", "setOnMouseOver", "setOnMouseOut", "setOnMouseMove", "setOnMouseDown", "setOnMouseUp", "setOnClick", "setOnDoubleClick"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
