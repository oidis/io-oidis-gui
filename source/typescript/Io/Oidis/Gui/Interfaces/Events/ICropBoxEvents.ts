/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ICropBoxEventsHandler } from "./ICropBoxEventsHandler.js";
import { IGuiCommonsEvents } from "./IGuiCommonsEvents.js";

export interface ICropBoxEvents extends IGuiCommonsEvents {
    setOnResize($handler : ICropBoxEventsHandler) : void;

    setOnResizeStart($handler : ICropBoxEventsHandler) : void;

    setOnResizeChange($handler : ICropBoxEventsHandler) : void;

    setOnResizeComplete($handler : ICropBoxEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const ICropBoxEvents = globalThis.RegisterInterface(["setOnResize", "setOnResizeStart", "setOnResizeChange", "setOnResizeComplete"], <any>IGuiCommonsEvents);
/* eslint-enable */
// generated-code-end
