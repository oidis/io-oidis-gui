/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { GuiObjectManager } from "../../GuiObjectManager.js";
import { IEventsHandler } from "../IEventsHandler.js";

export interface IErrorEventsHandler extends IEventsHandler {
    ($eventArgs : ErrorEventArgs, $guiObjectManager : GuiObjectManager, ...$args : any[]) : void;

    ($eventArgs : ErrorEventArgs, $guiObjectManager : GuiObjectManager, $reflection : Reflection) : void;
}

// generated-code-start
export const IErrorEventsHandler = globalThis.RegisterInterface([], <any>IEventsHandler);
// generated-code-end
