/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IEventsHandler } from "../IEventsHandler.js";
import { IGuiCommonsEvents } from "./IGuiCommonsEvents.js";
import { IResizeEventsHandler } from "./IResizeEventsHandler.js";
import { IScrollEventsHandler } from "./IScrollEventsHandler.js";

export interface ISelectBoxEvents extends IGuiCommonsEvents {
    setOnResize($handler : IResizeEventsHandler) : void;

    setOnScroll($handler : IScrollEventsHandler) : void;

    setOnChange($handler : IEventsHandler) : void;

    setOnSelect($handler : IEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const ISelectBoxEvents = globalThis.RegisterInterface(["setOnResize", "setOnScroll", "setOnChange", "setOnSelect"], <any>IGuiCommonsEvents);
/* eslint-enable */
// generated-code-end
