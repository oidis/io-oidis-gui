/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IDragBarEventsHandler } from "./IDragBarEventsHandler.js";
import { IGuiCommonsEvents } from "./IGuiCommonsEvents.js";

export interface IDragBarEvents extends IGuiCommonsEvents {
    setOnDragStart($handler : IDragBarEventsHandler) : void;

    setOnDragChange($handler : IDragBarEventsHandler) : void;

    setOnDragComplete($handler : IDragBarEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const IDragBarEvents = globalThis.RegisterInterface(["setOnDragStart", "setOnDragChange", "setOnDragComplete"], <any>IGuiCommonsEvents);
/* eslint-enable */
// generated-code-end
