/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IGuiCommonsEvents } from "./IGuiCommonsEvents.js";
import { IResizeBarEventsHandler } from "./IResizeBarEventsHandler.js";

export interface IResizeBarEvents extends IGuiCommonsEvents {
    setOnResize($handler : IResizeBarEventsHandler) : void;

    setOnResizeStart($handler : IResizeBarEventsHandler) : void;

    setOnResizeChange($handler : IResizeBarEventsHandler) : void;

    setOnResizeComplete($handler : IResizeBarEventsHandler) : void;
}

// generated-code-start
/* eslint-disable */
export const IResizeBarEvents = globalThis.RegisterInterface(["setOnResize", "setOnResizeStart", "setOnResizeChange", "setOnResizeComplete"], <any>IGuiCommonsEvents);
/* eslint-enable */
// generated-code-end
