/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventArgs.js";
import { IEventsHandler as Parent } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventsHandler.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { GuiObjectManager } from "../GuiObjectManager.js";

export interface IEventsHandler extends Parent {
    ($eventArgs : IEventArgs, $guiObjectManager : GuiObjectManager, ...$args : any[]) : void;

    ($eventArgs : IEventArgs, $guiObjectManager : GuiObjectManager, $reflection : Reflection) : void;
}

// generated-code-start
export const IEventsHandler = globalThis.RegisterInterface([], <any>Parent);
// generated-code-end
