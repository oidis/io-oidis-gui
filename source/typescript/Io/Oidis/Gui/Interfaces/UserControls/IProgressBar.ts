/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IProgressBarEvents } from "../Events/IProgressBarEvents.js";
import { IBaseGuiObject } from "../Primitives/IBaseGuiObject.js";

export interface IProgressBar extends IBaseGuiObject {
    GuiType($progressBarType? : any) : any;

    Value($value? : number) : number;

    RangeStart($value? : number) : number;

    RangeEnd($value? : number) : number;

    Width($value? : number) : number;

    getEvents() : IProgressBarEvents;
}

// generated-code-start
/* eslint-disable */
export const IProgressBar = globalThis.RegisterInterface(["GuiType", "Value", "RangeStart", "RangeEnd", "Width", "getEvents"], <any>IBaseGuiObject);
/* eslint-enable */
// generated-code-end
