/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IAudio } from "./IAudio.js";

export interface IVideo extends IAudio {

    FullScreen($value? : boolean) : void;

    setSize($width : number, $height : number) : void;
}

// generated-code-start
export const IVideo = globalThis.RegisterInterface(["FullScreen", "setSize"], <any>IAudio);
// generated-code-end
