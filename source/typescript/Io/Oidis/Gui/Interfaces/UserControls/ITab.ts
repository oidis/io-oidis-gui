/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IButton } from "./IButton.js";

// eslint-disable-next-line @typescript-eslint/no-empty-object-type
export interface ITab extends IButton {
    // simple interface alias
}

// generated-code-start
export const ITab = globalThis.RegisterInterface([], <any>IButton);
// generated-code-end
