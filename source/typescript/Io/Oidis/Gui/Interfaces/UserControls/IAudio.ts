/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseGuiObject } from "../Primitives/IBaseGuiObject.js";
import { IMediaSource } from "./IMediaSource.js";

export interface IAudio extends IBaseGuiObject {

    Source($source? : IMediaSource | MediaStream) : IMediaSource | MediaStream;

    Play($inLoop? : boolean) : void;

    Pause() : void;

    Stop() : void;

    Mute($value? : boolean) : boolean;

    Ended() : boolean;

    Paused() : boolean;

    CurrentTime($value? : number) : number;

    Duration() : number;

    Looped($value? : boolean) : boolean;

    WithControls($value? : boolean) : boolean;

    Autoplay($value? : boolean) : boolean;

    Volume($value? : number) : number;
}

// generated-code-start
/* eslint-disable */
export const IAudio = globalThis.RegisterInterface(["Source", "Play", "Pause", "Stop", "Mute", "Ended", "Paused", "CurrentTime", "Duration", "Looped", "WithControls", "Autoplay", "Volume"], <any>IBaseGuiObject);
/* eslint-enable */
// generated-code-end
