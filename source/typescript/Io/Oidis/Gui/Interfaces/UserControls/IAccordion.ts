/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { IBasePanel } from "../Primitives/IBasePanel.js";

export interface IAccordion extends IBasePanel {
    GuiType($accordionType ? : any) : any;

    setPanelHoldersArgs($args : ArrayList<any>) : void;

    Clear() : void;

    Collapse(...$filter : Array<number | IClassName>) : void;

    Expand(...$filter : Array<number | IClassName>) : void;

    ExpandExclusive(...$filter : Array<number | IClassName>) : void;

    getPanel($filter : number | IClassName) : void;
}

// generated-code-start
/* eslint-disable */
export const IAccordion = globalThis.RegisterInterface(["GuiType", "setPanelHoldersArgs", "Clear", "Collapse", "Expand", "ExpandExclusive", "getPanel"], <any>IBasePanel);
/* eslint-enable */
// generated-code-end
