/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IFormsObject } from "../Primitives/IFormsObject.js";

export interface IImageButton extends IFormsObject {
    GuiType($imageButtonType? : any) : any;

    IconName($value? : any) : any;

    IsSelected($value? : boolean) : boolean;
}

// generated-code-start
export const IImageButton = globalThis.RegisterInterface(["GuiType", "IconName", "IsSelected"], <any>IFormsObject);
// generated-code-end
