/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { IFormsObject } from "../Primitives/IFormsObject.js";

export interface ITextField extends IFormsObject {
    GuiType($textFieldType? : any) : any;

    Value($value? : string) : string;

    Width($value? : number) : number;

    setAutocompleteDisable() : void;

    ReadOnly($value? : boolean) : boolean;

    setPasswordEnabled() : void;

    setOnlyNumbersAllowed() : void;

    IconName($value? : any) : any;

    Hint($value? : string) : string;

    LengthLimit($value? : number) : number;

    setAutocompleteData($value : ArrayList<any>) : void;
}

// generated-code-start
/* eslint-disable */
export const ITextField = globalThis.RegisterInterface(["GuiType", "Value", "Width", "setAutocompleteDisable", "ReadOnly", "setPasswordEnabled", "setOnlyNumbersAllowed", "IconName", "Hint", "LengthLimit", "setAutocompleteData"], <any>IFormsObject);
/* eslint-enable */
// generated-code-end
