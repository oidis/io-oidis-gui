/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { FileSystemItemType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/FileSystemItemType.js";
import { IFileSystemItemProtocol } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IFileSystemItemProtocol.js";
import { FileSystemFilter } from "@io-oidis-commons/Io/Oidis/Commons/Utils/FileSystemFilter.js";
import { IDirectoryBrowserEvents } from "../Events/IDirectoryBrowserEvents.js";
import { IFormsObject } from "../Primitives/IFormsObject.js";

export interface IDirectoryBrowser extends IFormsObject {
    Path($value? : string) : string;

    Filter($value? : Array<string | FileSystemFilter | FileSystemItemType>) : string;

    setStructure($data : IFileSystemItemProtocol[], $path? : string) : void;

    Clear() : void;

    getEvents() : IDirectoryBrowserEvents;

    Value($value? : string) : string;
}

// generated-code-start
/* eslint-disable */
export const IDirectoryBrowser = globalThis.RegisterInterface(["Path", "Filter", "setStructure", "Clear", "getEvents", "Value"], <any>IFormsObject);
/* eslint-enable */
// generated-code-end
