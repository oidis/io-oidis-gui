/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IMediaSource {
    src : string;
    type? : string;
    chunkId? : number;
    codecs? : string[];
}

// generated-code-start
export const IMediaSource = globalThis.RegisterInterface(["src", "type", "chunkId", "codecs"]);
// generated-code-end
