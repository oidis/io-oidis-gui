/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseGuiObject } from "../Primitives/IBaseGuiObject.js";
import { ITab } from "./ITab.js";

export interface ITabs extends IBaseGuiObject {
    Add($value : string) : boolean;

    Clear() : void;

    Select($value : string | number) : void;

    getItem($value : number) : ITab;
}

// generated-code-start
export const ITabs = globalThis.RegisterInterface(["Add", "Clear", "Select", "getItem"], <any>IBaseGuiObject);
// generated-code-end
