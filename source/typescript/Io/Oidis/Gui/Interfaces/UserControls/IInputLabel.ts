/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ILabel } from "./ILabel.js";

export interface IInputLabel extends ILabel {
    GuiType($textFieldType? : any) : any;

    Width($value? : number) : number;

    Error($value? : boolean) : boolean;
}

// generated-code-start
export const IInputLabel = globalThis.RegisterInterface(["GuiType", "Width", "Error"], <any>ILabel);
// generated-code-end
