/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IFormsObject } from "../Primitives/IFormsObject.js";

export interface ICheckBox extends IFormsObject {
    Text($value? : string) : string;

    Checked($value? : boolean) : boolean;
}

// generated-code-start
export const ICheckBox = globalThis.RegisterInterface(["Text", "Checked"], <any>IFormsObject);
// generated-code-end
