/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IDropDownListEvents } from "../Events/IDropDownListEvents.js";
import { IFormsObject } from "../Primitives/IFormsObject.js";

export interface IDropDownList extends IFormsObject {
    GuiType($dropDownListType? : any) : any;

    Hint($value? : string) : string;

    Value($value? : string | number) : string;

    Width($value? : number) : number;

    Height($value? : number) : number;

    MaxVisibleItemsCount($value? : number) : number;

    Add($text : string, $value? : string | number, $styleName? : string) : void;

    Select($value : string | number) : void;

    Clear() : void;

    ShowField($value? : boolean) : boolean;

    ShowButton($value? : boolean) : boolean;

    getItemsCount() : number;

    getEvents() : IDropDownListEvents;
}

// generated-code-start
/* eslint-disable */
export const IDropDownList = globalThis.RegisterInterface(["GuiType", "Hint", "Value", "Width", "Height", "MaxVisibleItemsCount", "Add", "Select", "Clear", "ShowField", "ShowButton", "getItemsCount", "getEvents"], <any>IFormsObject);
/* eslint-enable */
// generated-code-end
