/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ResizeableType } from "../../Enums/ResizeableType.js";
import { BasePanelViewerArgs } from "../../Primitives/BasePanelViewerArgs.js";
import { IDialogEvents } from "../Events/IDialogEvents.js";
import { IBasePanelViewer } from "../Primitives/IBasePanelViewer.js";
import { IFormsObject } from "../Primitives/IFormsObject.js";

export interface IDialog extends IFormsObject {
    GuiType($dialogType? : any) : any;

    TopOffset($value? : number) : number;

    MaxWidth($value? : number) : number;

    MaxHeight($value? : number) : number;

    AutoResize($value? : boolean) : boolean;

    AutoCenter($value? : boolean) : boolean;

    ResizeableType($type? : ResizeableType) : ResizeableType;

    Modal($value? : boolean) : boolean;

    Draggable($value? : boolean) : boolean;

    PanelViewer($instance? : IBasePanelViewer) : IBasePanelViewer;

    PanelViewerArgs($args? : BasePanelViewerArgs) : BasePanelViewerArgs;

    Width($value? : number) : number;

    Height($value? : number) : number;

    getEvents() : IDialogEvents;
}

// generated-code-start
/* eslint-disable */
export const IDialog = globalThis.RegisterInterface(["GuiType", "TopOffset", "MaxWidth", "MaxHeight", "AutoResize", "AutoCenter", "ResizeableType", "Modal", "Draggable", "PanelViewer", "PanelViewerArgs", "Width", "Height", "getEvents"], <any>IFormsObject);
/* eslint-enable */
// generated-code-end
