/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IFormsObject } from "../Primitives/IFormsObject.js";

export interface INumberPicker extends IFormsObject {
    GuiType($numberPickerType? : any) : any;

    Value($value? : number) : number;

    ValueStep($value? : number) : number;

    RangeStart($value? : number) : number;

    RangeEnd($value? : number) : number;

    Width($value? : number) : number;

    Text($value? : string) : string;

    TabIndex($value? : number) : number;

    DecimalPlaces($value? : number) : number;
}

// generated-code-start
/* eslint-disable */
export const INumberPicker = globalThis.RegisterInterface(["GuiType", "Value", "ValueStep", "RangeStart", "RangeEnd", "Width", "Text", "TabIndex", "DecimalPlaces"], <any>IFormsObject);
/* eslint-enable */
// generated-code-end
