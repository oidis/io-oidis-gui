/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseGuiObject } from "../Primitives/IBaseGuiObject.js";
import { ILabel } from "./ILabel.js";

export interface ILabelList extends IBaseGuiObject {
    IconName($value? : any) : any;

    Add($value : string | ILabel) : boolean;

    Clear() : void;
}

// generated-code-start
export const ILabelList = globalThis.RegisterInterface(["IconName", "Add", "Clear"], <any>IBaseGuiObject);
// generated-code-end
