/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ImageOutputArgs } from "../../ImageProcessor/ImageOutputArgs.js";
import { IBaseGuiObject } from "../Primitives/IBaseGuiObject.js";

export interface IImage extends IBaseGuiObject {
    Source($value? : any) : any;

    GuiType($imageType? : any) : any;

    Link($value? : string, $openNewWindow? : boolean) : void;

    LoadingSpinnerEnabled($value? : boolean) : boolean;

    OpacityShowEnabled($value? : boolean) : boolean;

    setSize($width : number, $height : number) : void;

    getWidth() : number;

    getHeight() : number;

    IsSelected($value? : boolean) : boolean;

    TabIndex($value? : number) : number;

    getStream() : string;

    OutputArgs($value? : ImageOutputArgs) : ImageOutputArgs;

    Version($value? : number) : number;
}

// generated-code-start
/* eslint-disable */
export const IImage = globalThis.RegisterInterface(["Source", "GuiType", "Link", "LoadingSpinnerEnabled", "OpacityShowEnabled", "setSize", "getWidth", "getHeight", "IsSelected", "TabIndex", "getStream", "OutputArgs", "Version"], <any>IBaseGuiObject);
/* eslint-enable */
// generated-code-end
