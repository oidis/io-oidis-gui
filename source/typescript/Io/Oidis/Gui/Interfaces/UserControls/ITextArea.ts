/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ResizeableType } from "../../Enums/ResizeableType.js";
import { ITextAreaEvents } from "../Events/ITextAreaEvents.js";
import { IFormsObject } from "../Primitives/IFormsObject.js";

export interface ITextArea extends IFormsObject {
    GuiType($textAreaType? : any) : any;

    Value($value? : string) : string;

    Width($value? : number) : number;

    MaxWidth($value? : number) : number;

    Height($value? : number) : number;

    MaxHeight($value? : number) : number;

    ResizeableType($type? : ResizeableType) : ResizeableType;

    ReadOnly($value? : boolean) : boolean;

    LengthLimit($value? : number) : number;

    Hint($value? : string) : string;

    getEvents() : ITextAreaEvents;
}

// generated-code-start
/* eslint-disable */
export const ITextArea = globalThis.RegisterInterface(["GuiType", "Value", "Width", "MaxWidth", "Height", "MaxHeight", "ResizeableType", "ReadOnly", "LengthLimit", "Hint", "getEvents"], <any>IFormsObject);
/* eslint-enable */
// generated-code-end
