/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { IFileUploadEvents } from "../Events/IFileUploadEvents.js";
import { IGuiCommons } from "../Primitives/IGuiCommons.js";

export interface IFileUpload extends IGuiCommons {
    getEvents() : IFileUploadEvents;

    setOpenElement($id : string | HTMLElement | IGuiCommons) : void;

    setDropZone($id : string | HTMLElement | IGuiCommons) : void;

    getStream($onSuccess : ($data : ArrayList<string>) => void) : void;

    Value() : string;

    Upload() : void;

    Aboard($fileHandle? : number) : void;

    MultipleSelectEnabled($value? : boolean) : boolean;

    Filter(...$value : string[]) : string;

    MaxFileSize($value? : number) : number;

    MaxChunkSize($value? : number) : number;
}

export interface IFileTransferProtocol {
    id : string;
    index : number;
    path : string;
    type? : string;
    error? : number;
    start : number;
    end : number;
    size : number;
    data? : string;
    metadata? : string;
}

export interface IRemoteFileTransferProtocol extends IFileTransferProtocol {
    cwd : string;
}

// generated-code-start
/* eslint-disable */
export const IFileUpload = globalThis.RegisterInterface(["getEvents", "setOpenElement", "setDropZone", "getStream", "Value", "Upload", "Aboard", "MultipleSelectEnabled", "Filter", "MaxFileSize", "MaxChunkSize"], <any>IGuiCommons);
export const IFileTransferProtocol = globalThis.RegisterInterface(["id", "index", "path", "type", "error", "start", "end", "size", "data", "metadata"]);
export const IRemoteFileTransferProtocol = globalThis.RegisterInterface(["cwd"], <any>IFileTransferProtocol);
/* eslint-enable */
// generated-code-end
