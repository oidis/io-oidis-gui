/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ResizeableType } from "../../Enums/ResizeableType.js";
import { IResizeBarEvents } from "../Events/IResizeBarEvents.js";
import { IGuiCommons } from "../Primitives/IGuiCommons.js";

export interface IResizeBar extends IGuiCommons {
    ResizeableType($notificationType? : ResizeableType) : ResizeableType;

    getEvents() : IResizeBarEvents;
}

// generated-code-start
export const IResizeBar = globalThis.RegisterInterface(["ResizeableType", "getEvents"], <any>IGuiCommons);
// generated-code-end
