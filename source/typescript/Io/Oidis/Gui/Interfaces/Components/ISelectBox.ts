/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { DirectionType } from "../../Enums/DirectionType.js";
import { ISelectBoxEvents } from "../Events/ISelectBoxEvents.js";
import { IGuiCommons } from "../Primitives/IGuiCommons.js";

export interface ISelectBox extends IGuiCommons {
    GuiType($selectBoxType? : any) : any;

    Value($value? : string | number) : string;

    Width($value? : number) : number;

    Height($value? : number) : number;

    MaxVisibleItemsCount($value? : number) : number;

    Add($text : string, $value? : string | number, $styleName? : string) : void;

    Select($value : string | number) : void;

    Clear() : void;

    OptionsCount() : number;

    DisableCharacterNavigation() : void;

    DisableAdvancedSelection() : void;

    setInitSize($width : number, $height : number) : void;

    setOpenDirection($verticalDirection : DirectionType, $horizontalDirection : DirectionType) : void;

    getEvents() : ISelectBoxEvents;
}

// generated-code-start
/* eslint-disable */
export const ISelectBox = globalThis.RegisterInterface(["GuiType", "Value", "Width", "Height", "MaxVisibleItemsCount", "Add", "Select", "Clear", "OptionsCount", "DisableCharacterNavigation", "DisableAdvancedSelection", "setInitSize", "setOpenDirection", "getEvents"], <any>IGuiCommons);
/* eslint-enable */
// generated-code-end
