/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IGuiCommons } from "../Primitives/IGuiCommons.js";

export interface INotification extends IGuiCommons {
    GuiType($notificationType? : any) : any;

    Width($value? : number) : number;

    Text($value? : string) : string;
}

// generated-code-start
export const INotification = globalThis.RegisterInterface(["GuiType", "Width", "Text"], <any>IGuiCommons);
// generated-code-end
