/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { OrientationType } from "../../Enums/OrientationType.js";
import { IScrollBarEvents } from "../Events/IScrollBarEvents.js";
import { IGuiCommons } from "../Primitives/IGuiCommons.js";

export interface IScrollBar extends IGuiCommons {
    GuiType($orientationType? : OrientationType) : OrientationType;

    OrientationType($orientationType? : OrientationType) : OrientationType;

    Size($value? : number) : number;

    getEvents() : IScrollBarEvents;
}

// generated-code-start
export const IScrollBar = globalThis.RegisterInterface(["GuiType", "OrientationType", "Size", "getEvents"], <any>IGuiCommons);
// generated-code-end
