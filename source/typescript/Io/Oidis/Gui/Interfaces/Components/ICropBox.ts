/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ICropBoxEvents } from "../Events/ICropBoxEvents.js";
import { IGuiCommons } from "../Primitives/IGuiCommons.js";

export interface ICropBox extends IGuiCommons {
    GuiType($cropBoxType? : any) : any;

    setDimensions($topX : number, $topY : number, $bottomX : number, $bottomY : number) : void;

    setSize($width : number, $height : number) : void;

    EnvelopOwner($owner? : string | IGuiCommons) : string;

    getEvents() : ICropBoxEvents;
}

// generated-code-start
/* eslint-disable */
export const ICropBox = globalThis.RegisterInterface(["GuiType", "setDimensions", "setSize", "EnvelopOwner", "getEvents"], <any>IGuiCommons);
/* eslint-enable */
// generated-code-end
