/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IArrayList.js";
import { IEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventArgs.js";
import { IEventsManager as Parent } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventsManager.js";
import { IEventsHandler } from "./IEventsHandler.js";
import { IGuiCommons } from "./Primitives/IGuiCommons.js";

export interface IEventsManager extends Parent {
    getAll() : IArrayList<IArrayList<IArrayList<IEventsHandler>>>;

    setEventArgs($owner : string | IGuiCommons, $type : string, $args : IEventArgs) : void;

    setEvent($owner : string | IGuiCommons, $type : string, $handler : IEventsHandler,
             $args? : IEventArgs) : void;

    FireEvent($owner : string | IGuiCommons, $type : string, $args? : any, $async? : boolean) : void;

    RemoveHandler($owner : string | IGuiCommons, $type : string, $handler : IEventsHandler) : void;

    Exists($owner : string | IGuiCommons, $type : string) : boolean;
}

// generated-code-start
/* eslint-disable */
export const IEventsManager = globalThis.RegisterInterface(["getAll", "setEventArgs", "setEvent", "FireEvent", "RemoveHandler", "Exists"], <any>Parent);
/* eslint-enable */
// generated-code-end
