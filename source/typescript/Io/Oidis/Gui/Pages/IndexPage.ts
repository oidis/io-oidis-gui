/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BasePanel } from "../Bootstrap/Primitives/BasePanel.js";
import { GuiCommons } from "../Bootstrap/Primitives/GuiCommons.js";

export class IndexPage extends BasePanel {
    public readonly headerTitle : GuiCommons;
    public readonly headerInfo : GuiCommons;
    public readonly pageIndex : GuiCommons;
    public readonly footerInfo : GuiCommons;

    constructor() {
        super();
        this.headerTitle = new GuiCommons();
        this.headerInfo = new GuiCommons();
        this.pageIndex = new GuiCommons();
        this.footerInfo = new GuiCommons();
    }

    protected innerHtml() : string {
        return `
<section>
    <div class="container-fluid d-flex flex-column h-100">
        <div class="row" style="background: rgb(232,232,232);border-bottom-style: none;padding: 15px;">
            <div class="col">
                <h1 style="margin: 10px;" data-oidis-bind="${this.headerTitle}">Oidis Framework Library</h1>
                <h4 class="text-muted" style="margin-left: 30px;margin-bottom: 30px;" data-oidis-bind="${this.headerInfo}"></h4>
            </div>
        </div>
        <div class="row justify-content-center" style="padding-top: 20px;">
            <div class="col-xl-11" data-oidis-bind="${this.pageIndex}"></div>
        </div>
        <div class="row mt-auto">
            <div class="col text-center" style="padding-bottom: 15px;">
                <p style="margin-top: 20px;color: var(--bs-gray);" data-oidis-bind="${this.footerInfo}">Copyright ${StringUtils.YearFrom(2019)} Oidis<br /></p><img src="resource/graphics/Io/Oidis/Commons/FooterLogo.png" />
            </div>
        </div>
    </div>
</section>`;
    }
}
