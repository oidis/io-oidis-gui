/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { PersistenceFactory as Parent } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { PersistenceType } from "./Enums/PersistenceType.js";

export class PersistenceFactory extends Parent {
    private static globalOwner : string;

    /**
     * @param {any} $persistenceType , which should be validated.
     * @param {string} $owner , which should be validated.
     * @returns {string} Returns generated session Id.
     */
    public static sessionIdGenerator($persistenceType : any, $owner : string) : string {
        if ($persistenceType === PersistenceType.AUTOFILL ||
            $persistenceType === PersistenceType.FORM_VALUES ||
            $persistenceType === PersistenceType.ERROR_FLAGS) {
            if (ObjectValidator.IsEmptyOrNull(PersistenceFactory.globalOwner)) {
                $owner = PersistenceFactory.globalOwner;
            } else {
                $owner = $persistenceType;
                $persistenceType = PersistenceType.BROWSER;
            }
        }

        return Parent.sessionIdGenerator($persistenceType, $owner);
    }

    /**
     * @param {string} $value , which should be validated.
     * @returns {string} Returns global Owner.
     */
    public static GlobalOwner($value? : string) : string {
        if (ObjectValidator.IsSet($value)) {
            PersistenceFactory.globalOwner = $value;
        }

        return PersistenceFactory.globalOwner;
    }
}
