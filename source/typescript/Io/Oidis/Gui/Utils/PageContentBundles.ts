/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EnvironmentArgs } from "@io-oidis-commons/Io/Oidis/Commons/EnvironmentArgs.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Loader } from "../Loader.js";
import { StaticPageContentManager } from "./StaticPageContentManager.js";

export class PageContentBundles extends BaseObject {
    private readonly buildTime : number;
    private readonly packageName : string;

    public static getInstance() : PageContentBundles {
        const instance : PageContentBundles = new this();
        this.getInstance = () : PageContentBundles => {
            return instance;
        };
        return instance;
    }

    constructor() {
        super();
        const env : EnvironmentArgs = Loader.getInstance().getEnvironmentArgs();
        this.buildTime = new Date(env.getBuildTime()).getTime();
        this.packageName = env.getProjectName() + "-" + env.getProjectVersion();
        this.packageName = StringUtils.Replace(this.packageName, ".", "-");
    }

    public Apply($type : PageContentBundleType) : void {
        switch ($type) {
        case PageContentBundleType.OIDIS:
            StaticPageContentManager.HeadScriptAppend("resource/javascript/" + this.packageName + ".min.js?v=" + this.buildTime);
            StaticPageContentManager.HeadScriptAppend("resource/javascript/loader.min.js?v=" + this.buildTime);
            break;
        case PageContentBundleType.BOOTSTRAP:
            StaticPageContentManager.BodyEventsEnabled(false);
            StaticPageContentManager.HeadLinkAppend("resource/libs/Bootstrap/bootstrap.min.css?v=" + this.buildTime);
            const viewPort : HTMLMetaElement = document.createElement("meta"); // eslint-disable-line no-case-declarations
            viewPort.name = "viewport";
            viewPort.content = "width=device-width, initial-scale=1.0, shrink-to-fit=no";
            StaticPageContentManager.HeadMetaDataAppend(viewPort);
            StaticPageContentManager
                .BodyAppend(`<script nonce="${StaticPageContentManager.Nonce()}" src="resource/libs/Bootstrap/bootstrap.min.js?v=${this.buildTime}"></script>`);
            break;
        case PageContentBundleType.REACT:
            StaticPageContentManager.BodyEventsEnabled(false);
            const viewPortR : HTMLMetaElement = document.createElement("meta"); // eslint-disable-line no-case-declarations
            viewPortR.name = "viewport";
            viewPortR.content = "width=device-width, initial-scale=1.0, shrink-to-fit=no";
            StaticPageContentManager.HeadMetaDataAppend(viewPortR);
            StaticPageContentManager
                .HeadScriptAppend("resource/libs/react/react.production.min.js?v=" + this.buildTime);
            StaticPageContentManager
                .HeadScriptAppend("resource/libs/react/react-dom.production.min.js?v=" + this.buildTime);
            break;
        case PageContentBundleType.FONTAWESOME:
            StaticPageContentManager.HeadLinkAppend("resource/css/fontawesome.css");
            break;
        case PageContentBundleType.SENTRY:
            StaticPageContentManager.BodyAppend(
                `<script nonce="${StaticPageContentManager.Nonce()}" src="resource/libs/Sentry/sentry.min.js?v=${this.buildTime}" crossorigin="anonymous"></script>`);
            break;
        case PageContentBundleType.QRCODE:
            StaticPageContentManager.HeadScriptAppend("resource/libs/qrcode/qrcode.js?v=" + this.buildTime);
            break;
        case PageContentBundleType.BARCODE:
            StaticPageContentManager.HeadScriptAppend("resource/libs/jsBarcode/JsBarcode.all.min.js?v=" + this.buildTime);
            break;
        case PageContentBundleType.CODE_SCANNER:
            StaticPageContentManager.HeadScriptAppend("resource/libs/html5QRcode/html5-qrcode.min.js?v=" + this.buildTime);
            break;
        default:
            LogIt.Error("Undefined content bundle type: {0}", $type);
            break;
        }
    }
}

export enum PageContentBundleType {
    OIDIS,
    BOOTSTRAP,
    FONTAWESOME,
    REACT,
    SENTRY,
    QRCODE,
    BARCODE,
    CODE_SCANNER
}
