/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BrowserType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/BrowserType.js";
import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Loader } from "../Loader.js";
import { ElementManager } from "./ElementManager.js";

/**
 * StaticPageContentManager class provides static methods for handling of page content.
 */
export class StaticPageContentManager extends BaseObject {
    private static isInitialized : boolean = false;
    private static isPrinted : boolean = false;
    private static charset : string;
    private static title : string;
    private static license : string;
    private static faviconSource : string;
    private static links : ArrayList<HTMLLinkElement>;
    private static scripts : ArrayList<HTMLScriptElement>;
    private static metaTags : ArrayList<HTMLMetaElement>;
    private static body : string;
    private static browserType : BrowserType;
    private static language : LanguageType;
    private static withBodyEvents : boolean = false;
    private static nonce : string;

    /**
     * Language type can affect page structure based on conditional css overriding.
     * @param {LanguageType} [$value] Specify language type of the page.
     * @returns {LanguageType} Returns language type of the page.
     */
    public static Language($value? : LanguageType) : LanguageType {
        this.init();
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.language = $value;
            if (this.isPrinted) {
                ElementManager.setClassName("Language", this.language.toString());
            }
        }

        return this.language;
    }

    /**
     * @param {string} [$value] Specify page title.
     * @returns {string} Returns page title.
     */
    public static Title($value? : string) : string {
        this.init();
        this.title = Property.String(this.title, $value);
        if (!ObjectValidator.IsEmptyOrNull($value) && this.isPrinted) {
            document.title = this.title;
        }

        return this.title;
    }

    /**
     * @param {string} [$value] Specify license text.
     * @returns {string} Returns license text.
     */
    public static License($value? : string) : string {
        this.init();
        return this.license = Property.String(this.license, $value);
    }

    /**
     * @param {string} [$value] Specify source path for favicon.
     * @returns {string} Returns source path for favicon.
     */
    public static FaviconSource($value? : string) : string {
        this.init();
        this.faviconSource = Property.String(this.faviconSource, $value);
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.links.foreach(($link : HTMLLinkElement) : boolean => {
                if ($link.rel === "icon" || $link.rel === "shortcut icon") {
                    let faviconSource : string = this.faviconSource;
                    if (!StringUtils.Contains(faviconSource, "?")) {
                        faviconSource += "?v=" + Loader.getInstance().getEnvironmentArgs().getProjectConfig().build.timestamp;
                    }
                    $link.href = faviconSource;
                    return false;
                }
                return true;
            });
        }

        return this.faviconSource;
    }

    /**
     * @param {string} [$value] Specify page encoding.
     * @returns {string} Returns page encoding.
     */
    public static Charset($value? : string) : string {
        this.init();
        this.charset = Property.String(this.charset, $value);
        return this.charset;
    }

    public static Nonce($value? : string) : string {
        return this.nonce = Property.String(this.nonce, $value);
    }

    /**
     * @param {boolean} [$value] Specify is body events should be statically defined or not.
     * @returns {boolean} Returns true if body events are statically included otherwise false.
     */
    public static BodyEventsEnabled($value? : boolean) : boolean {
        return this.withBodyEvents = Property.Boolean(this.withBodyEvents, $value);
    }

    /**
     * @param {string|HTMLLinkElement} $value String or link element value,
     * which should be appended to the page head at links position.
     * @param {string} [$rel] Set rel value, if link has been specified as string.
     * @param {string} [$type] Set type value, if link has been specified as string.
     * @returns {HTMLLinkElement} Returns created instance of HTMLLinkElement.
     */
    public static HeadLinkAppend($value : string | HTMLLinkElement, $rel? : string, $type? : string) : HTMLLinkElement {
        let link : HTMLLinkElement;
        if (ObjectValidator.IsObject($value)) {
            link = <HTMLLinkElement>$value;
        } else if (ObjectValidator.IsString($value)) {
            link = document.createElement("link");
            link.href = <string>$value;
            link.rel = "stylesheet";
            link.type = "text/css";
            if (ObjectValidator.IsSet($rel)) {
                link.rel = $rel;
            }
            if (ObjectValidator.IsSet($type)) {
                link.rel = $type;
            }
        }
        if (ObjectValidator.IsSet(link)) {
            link.nonce = "";
            const key : string = StringUtils.getSha1(link.outerHTML);
            link.nonce = this.nonce;
            if (!this.links.KeyExists(key)) {
                this.links.Add(link, key);
                if (this.isPrinted) {
                    window.document.body.appendChild(link);
                }
            }
        }
        return link;
    }

    /**
     * @returns {ArrayList<HTMLLinkElement>} Returns list of registered link tags.
     */
    public static getHeadLinks() : ArrayList<HTMLLinkElement> {
        return this.links;
    }

    /**
     * @param {string|HTMLMetaElement} $value String or metatag element value,
     * which should be appended to the page head at metatags position.
     * @param {string} [$httpEquiv] Set http-equiv value, if tag has been specified as string.
     * @returns {void}
     */
    public static HeadMetaDataAppend($value : string | HTMLMetaElement, $httpEquiv? : string) : void {
        let meta : HTMLMetaElement;
        if (ObjectValidator.IsObject($value)) {
            meta = <HTMLMetaElement>$value;
        } else if (ObjectValidator.IsString($value)) {
            meta = document.createElement("meta");
            meta.content = <string>$value;

            if (ObjectValidator.IsSet($httpEquiv)) {
                meta.httpEquiv = "Content-Type";
            }

        }
        if (ObjectValidator.IsSet(meta)) {
            const key : string = StringUtils.getSha1(meta.outerHTML);
            if (!this.metaTags.KeyExists(key)) {
                this.metaTags.Add(meta, key);
                if (this.isPrinted) {
                    window.document.body.appendChild(meta);
                }
            }
        }
    }

    /**
     * @returns {ArrayList<HTMLMetaElement>} Returns list of registered meta tags.
     */
    public static getHeadMetaData() : ArrayList<HTMLMetaElement> {
        return this.metaTags;
    }

    /**
     * @param {string|HTMLScriptElement} $value String or script element value,
     * which should be appended to the page head at scripts position.
     * @param {string} [$type] Set type value, if script has been specified as string.
     * @returns {void}
     */
    public static HeadScriptAppend($value : string | HTMLScriptElement, $type? : string) : void {
        let script : HTMLScriptElement;
        if (ObjectValidator.IsObject($value)) {
            script = <HTMLScriptElement>$value;
        } else if (ObjectValidator.IsString($value)) {
            script = document.createElement("script");
            script.src = <string>$value;

            if (ObjectValidator.IsSet($type)) {
                script.type = $type;
            }
        }
        if (ObjectValidator.IsSet(script)) {
            script.nonce = "";
            const key : string = StringUtils.getSha1(script.outerHTML);
            script.nonce = this.nonce;
            if (!this.scripts.KeyExists(key)) {
                this.scripts.Add(script, key);
                if (this.isPrinted) {
                    window.document.body.appendChild(script);
                }
            }
        }
    }

    /**
     * @returns {ArrayList<HTMLScriptElement>} Returns list of registered script tags.
     */
    public static getHeadScripts() : ArrayList<HTMLScriptElement> {
        return this.scripts;
    }

    /**
     * @param {string} $value String value, which should be appended to the page body.
     * @returns {void}
     */
    public static BodyAppend($value : string) : void {
        this.body = this.htmlAppend(this.body, $value);
        if (this.isPrinted) {
            ElementManager.AppendHtml("Content", $value);
        }
    }

    /**
     * @returns {string} Returns page body content in string format.
     */
    public static getBody() : string {
        this.init();
        return this.body;
    }

    /**
     * Clean up page content
     * @param {boolean} [$force=false] Specify, if all data including header tags should be cleaned up.
     * @returns {void}
     */
    public static Clear($force : boolean = false) : void {
        try {
            document.documentElement.innerHTML = "";
        } catch (ex) {
            document.body.innerHTML = "";
        }
        this.faviconSource = "";
        this.isInitialized = false;
        this.init($force);
    }

    /**
     * @returns {string} Returns whole page content buffer in string format.
     */
    public static ToString() : string {
        this.init();

        let events : string = "";
        if (this.withBodyEvents) {
            events = " " +
                "onfocus=\"Io.Oidis.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
                "onblur=\"Io.Oidis.Gui.Events.EventsManager.bodyBlurEventHandler();\">";
        }
        return "" +
            "<!DOCTYPE html>\n" +
            this.license + "\n" +
            "<html lang=\"" + StringUtils.ToLowerCase(<string>this.language) + "\">\n" +
            "<head>\n" +
            this.getDocumentHead() +
            "</head>\n" +
            "\n" +
            "<body" + events + ">\n" +
            this.getDocumentBody() +
            "</body>\n" +
            "</html>";
    }

    /**
     * Override current page content by new page content buffer.
     * @returns {void}
     */
    public static Draw() : void {
        if (ObjectValidator.IsSet(window)) {
            this.init();

            try {
                document.documentElement.innerHTML = this.ToString();
            } catch (ex) {
                if (ObjectValidator.IsSet(document.head)) {
                    this.removeFromHead(document.head.getElementsByTagName("meta"));
                    this.removeFromHead(document.head.getElementsByTagName("link"));
                    this.removeFromHead(document.head.getElementsByTagName("script"));

                    this.appendToHead(this.metaTags);
                    this.appendToHead(this.links);
                    this.appendToHead(this.scripts);
                } else {
                    document.title = this.title;
                }
                document.body.innerHTML = this.getDocumentBody();
            }

            const stream : string = Echo.getStream();
            Echo.Init("Content", true);
            if (!ObjectValidator.IsEmptyOrNull(stream)) {
                Echo.Print(stream);
            }
            this.isPrinted = true;
        }
    }

    private static init($force : boolean = false) : void {
        if (!this.isInitialized) {
            this.charset = "UTF-8";
            this.title = Loader.getInstance().getHttpManager().getRequest().getUrl();
            this.license = "";
            this.browserType = Loader.getInstance().getHttpManager().getRequest().getBrowserType();
            this.language = LanguageType.EN;
            if (ObjectValidator.IsEmptyOrNull(this.faviconSource)) {
                this.faviconSource = "resource/graphics/icon.ico";
            }

            if ($force || ObjectValidator.IsEmptyOrNull(this.links)) {
                let packageName : string =
                    Loader.getInstance().getEnvironmentArgs().getProjectName() + "-" +
                    Loader.getInstance().getEnvironmentArgs().getProjectVersion();
                packageName = StringUtils.Replace(packageName, ".", "-");
                const buildTime : number = Loader.getInstance().getEnvironmentArgs().getProjectConfig().build.timestamp;

                this.links = new ArrayList<HTMLLinkElement>();
                let faviconSource : string = this.faviconSource;
                if (!StringUtils.Contains(faviconSource, "?")) {
                    faviconSource += "?v=" + buildTime;
                }
                this.HeadLinkAppend(faviconSource, "shortcut icon");
                this.HeadLinkAppend("resource/libs/Bootstrap/bootstrap.min.css?v=" + buildTime);
                this.HeadLinkAppend("resource/css/" + packageName + ".min.css?v=" + buildTime);

                this.scripts = new ArrayList<HTMLScriptElement>();
                this.metaTags = new ArrayList<HTMLMetaElement>();
                this.HeadMetaDataAppend("text/html; charset=" + this.charset, "Content-Type");
            }

            this.body = "";

            this.isPrinted = false;
            this.isInitialized = true;
        }
    }

    private static htmlAppend($type : string, $value : string) : string {
        this.init();
        if (!ObjectValidator.IsSet($type)) {
            $type = "";
        }
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            $type += $value + "\n";
        }
        return $type;
    }

    private static getDocumentHead() : string {
        let metaTags : string = "";
        this.metaTags.foreach(($element : HTMLMetaElement) : void => {
            metaTags += $element.outerHTML + "\n";
        });
        if (!ObjectValidator.IsEmptyOrNull(metaTags)) {
            metaTags += "\n";
        }
        let links : string = "";
        this.links.foreach(($element : HTMLLinkElement) : void => {
            links += $element.outerHTML + "\n";
        });
        if (!ObjectValidator.IsEmptyOrNull(links)) {
            links += "\n";
        }
        let scripts : string = "";
        this.scripts.foreach(($element : HTMLScriptElement) : void => {
            scripts += $element.outerHTML + "\n";
        });
        return "<title>" + this.title + "</title>\n\n" +
            metaTags +
            links +
            scripts;
    }

    private static getDocumentBody() : string {
        return "" +
            "<div id=\"Browser\" class=\"" + BrowserType[this.browserType] + "\">\n" +
            "<div id=\"Language\" class=\"" + this.language + "\">\n" +
            "<div id=\"Content\" class=\"Content\" guiType=\"PageContent\">\n" +
            this.body + "\n" +
            "</div>\n" +
            "</div>\n" +
            "</div>\n";
    }

    private static removeFromHead($elements : any) : void {
        let index : number;
        for (index = $elements.length; index >= 0; index--) {
            if (ObjectValidator.IsSet($elements[index])) {
                $elements[index].parentNode.removeChild($elements[index]);
            }
        }
    }

    private static appendToHead($elements : ArrayList<any>) : void {
        $elements.foreach(($element : any) : void => {
            document.head.appendChild($element);
            document.head.appendChild(document.createTextNode("\n"));
        });
    }
}
