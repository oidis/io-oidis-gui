/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ThreadPool } from "@io-oidis-commons/Io/Oidis/Commons/Events/ThreadPool.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { DirectionType } from "../Enums/DirectionType.js";
import { ProgressType } from "../Enums/ProgressType.js";
import { UnitType } from "../Enums/UnitType.js";
import { AnimationThreadPool } from "../Events/AnimationThreadPool.js";
import { ValueProgressEventArgs } from "../Events/Args/ValueProgressEventArgs.js";
import { EventsManager } from "../Events/EventsManager.js";

/**
 * ValueProgressManager class provides asynchronous change of number value with warious progress types.
 */
export class ValueProgressManager extends BaseObject {
    private static eventArgsList : ArrayList<ValueProgressEventArgs>;
    private static timeStep : number = 10;

    /**
     * @param {ValueProgressEventArgs} $args Set progress event args, which should be processed and
     * provided by ValueProgressManager events.
     * @returns {void}
     */
    public static Execute($args : ValueProgressEventArgs) : void {
        if ($args.RangeStart() !== $args.RangeEnd()) {
            const threadPool : any = ValueProgressManager.getThreadPool();
            if (!ValueProgressManager.Exists($args.OwnerId())) {
                ValueProgressManager.eventArgsList.Add($args, $args.OwnerId());
            }
            EventsManager.getInstanceSingleton().FireEvent($args.OwnerId(), $args.StartEventType(), $args);
            $args.TimeStamp(ValueProgressManager.getTimeStamp());
            threadPool
                .AddThread($args.OwnerId(), EventType.ON_CHANGE + "_Value", ValueProgressManager.handler, $args);
            threadPool.Execute();
        }
    }

    /**
     * @param {string} $id Set manager id, which should be removed from processing thread.
     * @returns {void}
     */
    public static Remove($id : string) : void {
        ValueProgressManager.getThreadPool().RemoveThread($id, EventType.ON_CHANGE + "_Value");
    }

    /**
     * @param {string} $id Manager id, which should be validated.
     * @returns {boolean} Returns true, if ValueProgressManager has been registered, otherwise false.
     */
    public static Exists($id : string) : boolean {
        if (!ObjectValidator.IsSet(ValueProgressManager.eventArgsList)) {
            ValueProgressManager.eventArgsList = new ArrayList<ValueProgressEventArgs>();
        }
        return ValueProgressManager.eventArgsList.KeyExists($id);
    }

    /**
     * @param {string} $id Manager id, which should be get from value progress managers register.
     * @returns {ValueProgressEventArgs} Returns managers args associated with manager id.
     * If managers has not been found, this method will automatically register new manager with desired id.
     */
    public static get($id : string) : ValueProgressEventArgs {
        if (!ValueProgressManager.Exists($id)) {
            ValueProgressManager.eventArgsList.Add(new ValueProgressEventArgs($id), $id);
        }
        return ValueProgressManager.eventArgsList.getItem($id);
    }

    public static getTimeFromPct($pct : number, $functionType : ProgressType) : number {
        switch ($functionType) {
        case ProgressType.LINEAR:
            return ValueProgressManager.getPctFromTime($pct, $functionType);
        case ProgressType.FAST_START:
            return ValueProgressManager.getPctFromTime($pct, ProgressType.SLOW_START);
        case ProgressType.SLOW_START:
            return ValueProgressManager.getPctFromTime($pct, ProgressType.FAST_START);
        case ProgressType.FAST_END:
            return ValueProgressManager.getPctFromTime($pct, ProgressType.SLOW_END);
        case ProgressType.SLOW_END:
            return ValueProgressManager.getPctFromTime($pct, ProgressType.FAST_END);
        case ProgressType.FAST_TAILS:
            return ValueProgressManager.getPctFromTime($pct, ProgressType.SLOW_TAILS);
        case ProgressType.SLOW_TAILS:
            return ValueProgressManager.getPctFromTime($pct, ProgressType.FAST_TAILS);
        default:
            return 0;
        }
    }

    public static getPctFromTime($time : number, $functionType : ProgressType) : number {
        switch ($functionType) {
        case ProgressType.LINEAR:
            return $time;
        case ProgressType.FAST_START:
            return Math.sqrt($time);
        case ProgressType.SLOW_START:
            return $time * $time;
        case ProgressType.FAST_END:
            return 1 - Math.sqrt(-$time + 1);
        case ProgressType.SLOW_END:
            return $time * (2 - $time);
        case ProgressType.FAST_TAILS:
            return $time < .5 ?
                Math.sqrt($time / 2) :
                -((-4 + Math.sqrt(-8 * $time + 8)) / 4);
        case ProgressType.SLOW_TAILS:
            return $time < .5 ?
                (2 * $time * $time) :
                (-1 + (4 - 2 * $time) * $time);
        default:
            return 0;
        }
    }

    private static handler($eventArgs : ValueProgressEventArgs) : void {
        const threadPool : any = ValueProgressManager.getThreadPool();
        $eventArgs.FloatValue(ValueProgressManager.getValue($eventArgs));
        $eventArgs.TimeStamp(ValueProgressManager.getTimeStamp());
        const isComplete : boolean = $eventArgs.FloatValue() <= Math.min($eventArgs.RangeStart(), $eventArgs.RangeEnd()) &&
            $eventArgs.DirectionType() === DirectionType.DOWN ||
            $eventArgs.FloatValue() >= Math.max($eventArgs.RangeStart(), $eventArgs.RangeEnd()) &&
            $eventArgs.DirectionType() === DirectionType.UP;

        if (isComplete) {
            $eventArgs.FloatValue($eventArgs.CurrentValue());
        }
        threadPool.setThreadArgs($eventArgs.OwnerId(), EventType.ON_CHANGE + "_Value", $eventArgs);
        EventsManager.getInstanceSingleton()
            .FireEvent($eventArgs.OwnerId(), $eventArgs.ChangeEventType(), $eventArgs, false);

        if (isComplete) {
            threadPool.RemoveThread($eventArgs.OwnerId(), EventType.ON_CHANGE + "_Value");
            EventsManager.getInstanceSingleton()
                .FireEvent($eventArgs.OwnerId(), $eventArgs.CompleteEventType(), $eventArgs, false);
        }
    }

    private static getValue($eventArgs : ValueProgressEventArgs) : number {
        const minValue : number = Math.min($eventArgs.RangeStart(), $eventArgs.RangeEnd());
        const maxValue : number = Math.max($eventArgs.RangeStart(), $eventArgs.RangeEnd());
        const range : number = maxValue - minValue;

        const targetAnimTime : number = ($eventArgs.StepUnitType() === UnitType.PCT ?
            100 / $eventArgs.Step() : range / $eventArgs.Step()) * ValueProgressManager.timeStep;
        const timeStamp : number = ValueProgressManager.getTimeStamp();
        const frameTime : number = (timeStamp - $eventArgs.TimeStamp()) / targetAnimTime;
        const valuePct : number = ($eventArgs.FloatValue() - minValue) / range;

        const currentAnimTime : number = ValueProgressManager.getTimeFromPct($eventArgs.DirectionType() === DirectionType.UP ?
            valuePct : (1 - valuePct), $eventArgs.ProgressType());
        const unitTime = Math.min(1, currentAnimTime + frameTime);
        const animPct : number = ValueProgressManager.getPctFromTime(unitTime, $eventArgs.ProgressType());

        return minValue + ($eventArgs.DirectionType() === DirectionType.UP ? animPct : 1 - animPct) * range;
    }

    private static getTimeStamp() : number {
        if (ObjectValidator.IsSet(window.performance)) {
            ValueProgressManager.getTimeStamp = () : number => {
                return window.performance.now();
            };
            return window.performance.now();
        }
        ValueProgressManager.getTimeStamp = () : number => {
            return new Date().getTime();
        };
        return new Date().getTime();
    }

    private static getThreadPool() : any {
        const threadPool : any = ObjectValidator.IsSet(window.requestAnimationFrame) ? AnimationThreadPool : ThreadPool;
        ValueProgressManager.getThreadPool = () : any => {
            return threadPool;
        };
        return threadPool;
    }
}
