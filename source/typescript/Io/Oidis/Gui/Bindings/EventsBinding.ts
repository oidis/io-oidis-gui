/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { GuiCommons } from "../Bootstrap/Primitives/GuiCommons.js";

export class EventsBinding extends BaseObject {

    public static SingleClick($instance : GuiCommons, $handler : ($eventArgs? : EventArgs) => Promise<void>,
                              $timeout : number = 3500) : void {
        $instance.getEvents().setOnClick(($eventArgs : EventArgs) : void => {
            $instance.Enabled(false);
            const clickTimout : number = $instance.getEvents().FireAsynchronousMethod(() : void => {
                $instance.Enabled(true);
            }, $timeout);
            $instance.getEvents().FireAsynchronousMethod(async () : Promise<void> => {
                await $handler($eventArgs);
                clearTimeout(clickTimout);
                $instance.getEvents().FireAsynchronousMethod(() : void => {
                    $instance.Enabled(true);
                }, 200);
            }, true);
        });
    }
}
