/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseArgs } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseArgs.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";

/**
 * ElementOffset class provides structure for element offset parameters.
 */
export class ElementOffset extends BaseArgs {
    private top : number;
    private left : number;

    /**
     * @param {number} [$top] Set top offset value.
     * @param {number} [$left] Set left offset value.
     */
    constructor($top? : number, $left? : number) {
        super();
        this.top = 0;
        this.left = 0;
        this.Top($top);
        this.Left($left);
    }

    /**
     * @param {number} [$value] Set top offset value.
     * @returns {number} Returns top offset value.
     */
    public Top($value? : number) : number {
        return this.top = Property.Integer(this.top, $value);
    }

    /**
     * @param {number} [$value] Set left offset value.
     * @returns {number} Returns left offset value.
     */
    public Left($value? : number) : number {
        return this.left = Property.Integer(this.left, $value);
    }
}
