/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Exception } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { BaseArgs } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { UnitType } from "../Enums/UnitType.js";

/**
 * PropagableNumber class provides structure for handling of propagable number values.
 */
export class PropagableNumber extends BaseArgs {
    private readonly propagated : boolean;
    private readonly values : IPropagableNumberValue[];

    /**
     * @param {PropagableNumber} $parentValue Specify parent value.
     * @param {PropagableNumber} $childValue Specify child value.
     * @returns {PropagableNumber} Returns propagated value, if propagation has been allowed, otherwise null.
     */
    public static PriorityValue($parentValue : PropagableNumber, $childValue : PropagableNumber) : PropagableNumber {
        if (!ObjectValidator.IsEmptyOrNull($childValue)) {
            return $childValue;
        } else if (!ObjectValidator.IsEmptyOrNull($parentValue) && $parentValue.IsPropagated()) {
            return $parentValue;
        }
        return null;
    }

    /**
     * @param {string|string[]|IPropagableNumberValue|IPropagableNumberValue[]} $value Specify value/values.
     * @param {boolean} [$propagated] Specify propagable flag.
     */
    constructor($value : string | string[] | IPropagableNumberValue | IPropagableNumberValue[] | PropagableNumber,
                $propagated? : boolean) {
        super();
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            if (ObjectValidator.IsSet((<any>$value).IsPropagated)) {
                const copyFrom : PropagableNumber = <PropagableNumber>$value;
                this.values = JSON.parse(JSON.stringify(copyFrom.values));
                this.propagated = copyFrom.propagated;
            } else {
                const valArray : any[] =
                    ObjectValidator.IsArray($value) ? <string[] | IPropagableNumberValue[]>$value : [<IPropagableNumberValue>$value];
                this.values = [];
                valArray.forEach(($value : string | IPropagableNumberValue) : void => {
                    if (ObjectValidator.IsString($value)) {
                        const value : number = parseFloat($value.toString());
                        if (ObjectValidator.IsSet(value)) {
                            if (StringUtils.Contains($value.toString(), "px")) {
                                this.values.push({number: value, unitType: UnitType.PX});
                            } else if (StringUtils.Contains($value.toString(), "%")) {
                                this.values.push({number: value, unitType: UnitType.PCT});
                            } else {
                                // eslint-disable-next-line @typescript-eslint/only-throw-error
                                throw new Exception("Unit type of value not supported : " + $value);
                            }
                        } else {
                            // eslint-disable-next-line @typescript-eslint/only-throw-error
                            throw new Exception("Format of value not supported : " + $value);
                        }
                    } else {
                        this.values.push(<IPropagableNumberValue>$value);
                    }
                });
                this.propagated = ObjectValidator.IsSet($propagated) ? $propagated : false;
            }
        } else {
            this.values = [];
        }
    }

    /**
     * @returns {boolean} Returns propagable flag value.
     */
    public IsPropagated() : boolean {
        return this.propagated;
    }

    /**
     * @param {number} $parentPixelSize Specify value, which should be normalized.
     * @param {DisplayUnitType} $targetUnitType Specify targeted unit type for normalization.
     * @returns {number} Returns normalized value.
     */
    public Normalize($parentPixelSize : number, $targetUnitType : UnitType) : number {
        if (ObjectValidator.IsEmptyOrNull(this.values)) {
            return -1;
        }
        let returnVal : number = 0;
        this.values.forEach(($value : IPropagableNumberValue) : void => {
            let normalizedNumber : number = $value.number;
            if ($parentPixelSize > 0) {
                if ($value.unitType === UnitType.PX && $targetUnitType === UnitType.PCT) {
                    normalizedNumber = $value.number / $parentPixelSize * 100;
                } else if ($value.unitType === UnitType.PCT && $targetUnitType === UnitType.PX) {
                    normalizedNumber = $parentPixelSize * $value.number / 100;
                }
            }
            returnVal += normalizedNumber;
        });
        if ($targetUnitType === UnitType.PX) {
            return Math.round(returnVal);
        } else {
            return returnVal;
        }
    }

}

export class IPropagableNumberValue {
    public number : number;
    public unitType : UnitType;
}
