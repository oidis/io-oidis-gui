/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseArgs } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";

/**
 * FormValue class provides structure for HTML input element.
 */
export class FormValue extends BaseArgs {
    private readonly id : string;
    private readonly value : any;
    private readonly errorFlag : boolean;

    /**
     * @param {string} $id Set element id value.
     * @param {any} $value Set element value.
     * @param {boolean} [$errorFlag] Specify, if element is in error status or not.
     */
    constructor($id : string, $value : any, $errorFlag? : boolean) {
        super();
        this.id = $id;
        this.value = $value;
        if (ObjectValidator.IsSet($errorFlag)) {
            this.errorFlag = $errorFlag;
        } else {
            this.errorFlag = false;
        }
    }

    /**
     * @returns {string} Returns element's id.
     */
    public Id() : string {
        return this.id;
    }

    /**
     * @returns {any} Returns element's value.
     */
    public Value() : any {
        return this.value;
    }

    /**
     * @returns {boolean} Returns true, if element is in error state, otherwise false.
     */
    public ErrorFlag() : boolean {
        return this.errorFlag;
    }
}
