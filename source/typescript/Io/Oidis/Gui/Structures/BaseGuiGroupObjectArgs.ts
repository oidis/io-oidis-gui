/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseArgs } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { GuiOptionType } from "../Enums/GuiOptionType.js";
import { IBaseGuiGroupObjectArgs } from "../Interfaces/Primitives/IBaseGuiGroupObjectArgs.js";

/**
 * BaseGuiGroupObjectArgs is abstract structure handling look and feel of BaseGuiGroupObject primitive.
 */
export abstract class BaseGuiGroupObjectArgs extends BaseArgs implements IBaseGuiGroupObjectArgs {

    private readonly guiOptions : ArrayList<GuiOptionType>;
    private visible : boolean;
    private enabled : boolean;
    private error : boolean;
    private titleText : string;
    private forceSetValue : boolean;
    private resize : boolean;
    private value : string | number | boolean;
    private width : number;

    constructor() {
        super();

        this.visible = true;
        this.enabled = true;
        this.error = false;
        this.titleText = "";
        this.guiOptions = new ArrayList<GuiOptionType>();
        this.forceSetValue = false;
        this.resize = false;
        this.value = null;
        this.width = -1;
    }

    /**
     * @param {boolean} [$value] Specify, if user control should be visible at the screen.
     * @returns {boolean} Returns true, if user control will be visible at the screen, otherwise false.
     */
    public Visible($value? : boolean) : boolean {
        return this.visible = Property.Boolean(this.visible, $value);
    }

    /**
     * @param {boolean} [$value] Specify, if user control's value should be force set at runtime.
     * @returns {boolean} Returns true, if user control's value should be force set at runtime, otherwise false.
     */
    public ForceSetValue($value? : boolean) : boolean {
        return this.forceSetValue = Property.Boolean(this.forceSetValue, $value);
    }

    /**
     * @param {any} [$value] Specify element group's value.
     * @returns {any} Returns element group's value.
     */
    public Value($value? : any) : any {
        if (ObjectValidator.IsSet($value)) {
            this.value = $value;
        }
        return this.value;
    }

    /**
     * @param {boolean} [$value] Specify, if user control should be at disabled or enabled mode.
     * @returns {boolean} Returns true, if user control is at enabled mode, otherwise false.
     */
    public Enabled($value? : boolean) : boolean {
        return this.enabled = Property.Boolean(this.enabled, $value);
    }

    /**
     * @param {boolean} [$value] Specify, if user control should be at error state.
     * @returns {boolean} Returns true, if user control is at error state, otherwise false.
     */
    public Error($value? : boolean) : boolean {
        return this.error = Property.Boolean(this.error, $value);
    }

    /**
     * @param {GuiOptionType} $value Specify gui option type, which should be passed to the user controls group.
     * @returns {void}
     */
    public AddGuiOption($value : GuiOptionType) : void {
        this.guiOptions.Add($value);
    }

    /**
     * @returns {ArrayList<GuiOptionType>} Returns list of options which should be passed to the user controls group.
     */
    public getGuiOptionsList() : ArrayList<GuiOptionType> {
        return this.guiOptions;
    }

    /**
     * @param {string} [$value] Specify value, which should be used as user controls tooltip text.
     * @returns {string} Returns user controls group tooltip text value.
     */
    public TitleText($value? : string) : string {
        return this.titleText = Property.String(this.titleText, $value);
    }

    /**
     * @param {number} [$value] Specify required element group's width value.
     * @returns {number} Returns element group's width.
     */
    public Width($value? : number) : number {
        if (ObjectValidator.IsInteger($value)) {
            this.resize = this.width !== $value;
        }
        return this.width = Property.Integer(this.width, $value, 50);
    }

    /**
     * @param {boolean} [$value] Specify, if user control's size should be force set at runtime.
     * @returns {boolean} Returns true, if user control's size should be force set at runtime, otherwise false.
     */
    public Resize($value? : boolean) : boolean {
        return this.resize = Property.Boolean(this.resize, $value);
    }
}
