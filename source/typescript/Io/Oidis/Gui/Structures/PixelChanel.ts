/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";

export class PixelChanel extends BaseObject {
    public RED : number;
    public GREEN : number;
    public BLUE : number;
    public ALPHA : number;

    constructor() {
        super();

        this.RED = 0;
        this.GREEN = 0;
        this.BLUE = 0;
        this.ALPHA = 0;
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        let output : string = "";
        output += $prefix + "[Red] " + Convert.ObjectToString(this.RED) + StringUtils.NewLine($htmlTag);
        output += $prefix + "[Green] " + Convert.ObjectToString(this.GREEN) + StringUtils.NewLine($htmlTag);
        output += $prefix + "[Blue] " + Convert.ObjectToString(this.BLUE) + StringUtils.NewLine($htmlTag);
        output += $prefix + "[Alpha] " + Convert.ObjectToString(this.ALPHA) + StringUtils.NewLine($htmlTag);
        return output;
    }

    public toString() : string {
        return this.ToString();
    }
}
