/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IClassName } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IGuiCommons } from "../Interfaces/Primitives/IGuiCommons.js";

/**
 * GuiObjectsMap is structure, which provides management of GUI objects register.
 */
export class GuiObjectsMap extends BaseObject {
    private static guiClasses : any[];
    private static membersRegister : object;
    private objects : IGuiCommons[];
    private size : number;
    private lastIndex : number;
    private refresh : boolean;
    private typesMap : ArrayList<ArrayList<IGuiCommons>>;

    constructor() {
        super();
        this.objects = [];
        this.size = 0;
        this.refresh = true;
        this.typesMap = new ArrayList<ArrayList<IGuiCommons>>();
    }

    /**
     * @returns {number} Returns count of registered GUI objects.
     */
    public Length() : number {
        return this.size;
    }

    /**
     * @returns {boolean} Returns true, if none of GUI object has been registered yet, otherwise false.
     */
    public IsEmpty() : boolean {
        return this.size === 0;
    }

    /**
     * @param {IGuiCommons} $object Element, which should be validated.
     * @returns {boolean} Returns true, if element has been registered, otherwise false.
     */
    public Exists($object : IGuiCommons) : boolean {
        let index : number;
        for (index = 0; index < this.size; index++) {
            if (this.objects[index].Id() === $object.Id()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add element to the GUI objects register, if element is not already in register.
     * @param {IGuiCommons} [$object] Element, which should be registered.
     * @returns {void}
     */
    public Add($object : IGuiCommons) : void {
        if (!this.Exists(<IGuiCommons>$object) && ObjectValidator.IsSet($object.Id)) {
            this.objects[this.size] = (<IGuiCommons>$object);
            this.size++;
            this.refresh = true;
        }
    }

    /**
     * @param {IGuiCommons|IClassName} $className Element class name, which should be searched.
     * @returns {ArrayList<IGuiCommons>} Returns array list of elements with desired element type.
     */
    public getType($className : IGuiCommons | IClassName) : ArrayList<IGuiCommons> {
        if (ObjectValidator.IsSet($className) &&
            ObjectValidator.IsSet((<any>$className).ClassName) && Reflection.getInstance().Exists((<any>$className).ClassName())) {
            if (this.refresh || !this.typesMap.KeyExists((<any>$className).ClassName())) {
                this.reindex();
            }
            const items : ArrayList<IGuiCommons> = this.typesMap.getItem((<any>$className).ClassName());
            if (items !== null) {
                return items;
            }
        }
        return new ArrayList<IGuiCommons>();
    }

    /**
     * @returns {ArrayList<IGuiCommons>} Returns array list of all registered GUI objects.
     */
    public getAll() : ArrayList<IGuiCommons> {
        const objectsMap : string[] = [];
        let index : number;
        for (index = 0; index < this.size; index++) {
            objectsMap[index] = this.objects[index].Id();
        }
        return Reflection.getInstanceOf(ArrayList.ClassName(), {
            data: this.objects,
            getClassName() : string {
                return ArrayList.ClassName();
            },
            keys: objectsMap,
            size: this.size
        });
    }

    /**
     * Clean up all registered elements, if $object has not been specified.
     * Clean up element from the registered GUI objects, if $object has been specified and is registered.
     * @param {IGuiCommons} [$object] Element, which should be removed.
     * @returns {void}
     */
    public Clear($object? : IGuiCommons) : void {
        if (ObjectValidator.IsSet($object)) {
            const objects : IGuiCommons[] = [];
            let size : number = 0;
            let index : number;
            for (index = 0; index < this.size; index++) {
                if (this.objects[index] !== $object) {
                    objects[size] = this.objects[index];
                    size++;
                }
            }
            this.objects = objects;
            this.size = size;
            this.lastIndex = size;

            if (ObjectValidator.IsSet(GuiObjectsMap.membersRegister)) {
                const members : string[] = GuiObjectsMap.membersRegister[($object!).getClassName()];
                if (!ObjectValidator.IsEmptyOrNull(members)) {
                    let memberIndex : number;
                    for (memberIndex = 0; memberIndex < members.length; memberIndex++) {
                        const typesMap : ArrayList<IGuiCommons> = this.typesMap.getItem(members[memberIndex]);
                        if (!ObjectValidator.IsEmptyOrNull(typesMap)) {
                            typesMap.RemoveAt(typesMap.IndexOf(($object!)));
                            if (typesMap.IsEmpty()) {
                                this.typesMap.RemoveAt(this.typesMap.IndexOf(typesMap));
                            }
                        }
                    }
                }
            }
        } else {
            this.objects = [];
            this.size = 0;
            this.lastIndex = 0;
            this.typesMap.Clear();
        }
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        if (this.refresh || !this.typesMap.IsEmpty()) {
            this.reindex();
        }
        let output : string = "";
        let index : number;
        for (index = 0; index < this.size; index++) {
            output += $prefix + this.objectInfo(this.objects[index]) + StringUtils.NewLine($htmlTag);
        }
        const objectInfo : ($object : IGuiCommons) => void = this.objectInfo;
        this.typesMap.foreach(($members : ArrayList<IGuiCommons>, $key? : string) : void => {
            let group : string = $key;
            if ($htmlTag) {
                group = "<b>" + group + "</b>";
            }
            group = $prefix + group + StringUtils.NewLine($htmlTag);
            $members.foreach(($member : IGuiCommons) : void => {
                group += $prefix + StringUtils.Tab(1, $htmlTag) + objectInfo($member) + StringUtils.NewLine($htmlTag);
            });
            output += group;
        });
        return output;
    }

    private reindex() : void {
        const reflection : Reflection = Reflection.getInstance();
        let classesCount : number = 0;
        let classIndex : number;
        if (!ObjectValidator.IsSet(GuiObjectsMap.guiClasses)) {
            const classes : string[] = reflection.getAllClasses();
            let guiClassIndex : number = 0;
            GuiObjectsMap.guiClasses = [];
            classesCount = classes.length;
            for (classIndex = 0; classIndex < classesCount; classIndex++) {
                if (reflection.ClassHasInterface(classes[classIndex], IGuiCommons)) {
                    GuiObjectsMap.guiClasses[guiClassIndex] = reflection.getClass(classes[classIndex]);
                    guiClassIndex++;
                }
            }
        }
        classesCount = GuiObjectsMap.guiClasses.length;

        let index : number;
        let startIndex : number = 0;

        if (this.lastIndex > this.size) {
            this.typesMap.Clear();
        } else if (this.lastIndex > 0) {
            startIndex = this.lastIndex - 1;
        }

        if (!ObjectValidator.IsSet(GuiObjectsMap.membersRegister)) {
            GuiObjectsMap.membersRegister = {};
        }

        let memberIndex : number;
        for (index = startIndex; index < this.size; index++) {
            const element : any = this.objects[index];
            if (element !== null) {
                if (!GuiObjectsMap.membersRegister.hasOwnProperty(element.getClassName())) {
                    GuiObjectsMap.membersRegister[element.getClassName()] = [];
                    for (classIndex = 0; classIndex < classesCount; classIndex++) {
                        if (element instanceof GuiObjectsMap.guiClasses[classIndex]) {
                            GuiObjectsMap.membersRegister[element.getClassName()].push(
                                GuiObjectsMap.guiClasses[classIndex].ClassName());
                        }
                    }
                }

                const members : string[] = GuiObjectsMap.membersRegister[element.getClassName()];
                for (memberIndex = 0; memberIndex < members.length; memberIndex++) {
                    if (!this.typesMap.KeyExists(members[memberIndex])) {
                        this.typesMap.Add(new ArrayList<IGuiCommons>(), members[memberIndex]);
                    }
                    this.typesMap.getItem(members[memberIndex]).Add(element, element.Id());
                }
            }
        }

        this.lastIndex = this.size;
        this.refresh = false;
    }

    private objectInfo($object : IGuiCommons) : string {
        let childOut : string = "";
        if (!ObjectValidator.IsEmptyOrNull($object.getChildElements())) {
            $object.getChildElements().foreach(($child : IGuiCommons) : void => {
                if (!ObjectValidator.IsEmptyOrNull(childOut)) {
                    childOut += ",";
                }
                childOut += $child.Id();
            });
            if (!ObjectValidator.IsEmptyOrNull(childOut)) {
                childOut = "[" + childOut + "]";
            }
        } else {
            childOut = "EMPTY";
        }
        return "id: " + $object.Id() +
            ", parentId: " + (ObjectValidator.IsEmptyOrNull($object.Parent()) ? "not registered" : $object.Parent().Id()) +
            ", type: " + $object.getClassName() + ", childrenList: " + childOut;

    }
}
