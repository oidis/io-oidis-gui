/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { Exception } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/Exception.js";
import { AsyncHttpResolver } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/AsyncHttpResolver.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { DesignerTaskType } from "../Enums/DesignerTaskType.js";
import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { IDesignerProtocol } from "../Interfaces/Designer/IDesignerProtocol.js";

/**
 * DesignerErrorHandler class provides redirection of exceptions handling to the GUI Designer layer.
 */
export class DesignerErrorHandler extends AsyncHttpResolver {
    private exceptionsList : ArrayList<Exception>;

    protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
        if (ObjectValidator.IsEmptyOrNull(this.exceptionsList)) {
            if ($POST.KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
                this.exceptionsList = $POST.getItem(HttpRequestConstants.EXCEPTIONS_LIST);
            } else {
                this.exceptionsList = new ArrayList<Exception>();
            }
        }
    }

    protected getExceptionsList() : ArrayList<Exception> {
        return this.exceptionsList;
    }

    protected resolver() : void {
        try {
            Echo.ClearAll();
            this.sendData(<IDesignerProtocol>{
                data: Echo.getStream(),
                task: DesignerTaskType.ECHO
            });
            this.sendData(<IDesignerProtocol>{
                data: this.getExceptionsList().ToString(),
                task: DesignerTaskType.FAIL
            });
        } catch (ex) {
            try {
                this.sendData(<IDesignerProtocol>{
                    data: ex.message,
                    task: DesignerTaskType.FAIL
                });
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }
    }

    private sendData($data : IDesignerProtocol) : void {
        try {
            window.parent.postMessage($data, "*");
        } catch (ex) {
            window.parent.postMessage(<IDesignerProtocol>{
                data: "Failed to send designer data: " + ex.message,
                task: DesignerTaskType.FAIL
            }, "*");
        }
    }
}
