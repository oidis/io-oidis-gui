/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BuilderConnector } from "@io-oidis-commons/Io/Oidis/Commons/Connectors/BuilderConnector.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { MessageEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/MessageEventArgs.js";
import { RuntimeTestRunner } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { IEventsHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IEventsHandler.js";
import { IWebServiceResponseHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IWebServiceClient.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { DesignerProtocolConstants } from "../Enums/DesignerProtocolConstants.js";
import { DesignerTaskType } from "../Enums/DesignerTaskType.js";
import { EventType } from "../Enums/Events/EventType.js";
import { GuiCommonsArgType } from "../Enums/GuiCommonsArgType.js";
import { GuiOptionType } from "../Enums/GuiOptionType.js";
import { HttpRequestConstants } from "../Enums/HttpRequestConstants.js";
import { DrawGuiObject } from "../HttpProcessor/Resolvers/DrawGuiObject.js";
import { RuntimeTestRunner as GuiRuntimeTestRunner } from "../HttpProcessor/Resolvers/RuntimeTestRunner.js";
import {
    IDesignerEventProtocol,
    IDesignerInstanceMapItem,
    IDesignerItemArgProtocol, IDesignerItemArgsProtocol,
    IDesignerItemProtocol, IDesignerProjectMapProtocol,
    IDesignerProtocol
} from "../Interfaces/Designer/IDesignerProtocol.js";
import { IBaseViewer } from "../Interfaces/Primitives/IBaseViewer.js";
import { IFormsObject } from "../Interfaces/Primitives/IFormsObject.js";
import { IGuiCommons } from "../Interfaces/Primitives/IGuiCommons.js";
import { IGuiCommonsArg } from "../Interfaces/Primitives/IGuiCommonsArg.js";
import { BasePanel } from "../Primitives/BasePanel.js";
import { BasePanelViewer } from "../Primitives/BasePanelViewer.js";
import { BaseViewer } from "../Primitives/BaseViewer.js";
import { GuiCommons } from "../Primitives/GuiCommons.js";
import { GuiElement } from "../Primitives/GuiElement.js";
import { ElementOffset } from "../Structures/ElementOffset.js";
import { Size } from "../Structures/Size.js";
import { ElementManager } from "../Utils/ElementManager.js";
import { StaticPageContentManager } from "../Utils/StaticPageContentManager.js";
import { WindowManager } from "../Utils/WindowManager.js";
import { ViewerManager } from "../ViewerManager.js";
import { BlankPanelViewer } from "./BlankPanelViewer.js";
import { DesignerElement } from "./DesignerElement.js";
import { DesignerErrorHandler } from "./DesignerErrorHandler.js";

/**
 * DesignerRunner class provides handling of requests for rendering of GUI objects with focus on GUI Designer needs.
 */
export class DesignerRunner extends DrawGuiObject {

    /**
     * @param {string} [$value] Object class name in string format, which should be used by the designer.
     * @returns {string} Returns object class name.
     */
    public ObjectClassName($value? : string) : string {
        if (ObjectValidator.IsEmptyOrNull($value)) {
            if (!ObjectValidator.IsSet(this.objectName)) {
                this.objectName = this.getRequest().getScriptPath();
                this.objectName = StringUtils.Remove(this.objectName, "/" + HttpRequestConstants.TEST_MODE);
                this.objectName = StringUtils.Remove(this.objectName, "/" + HttpRequestConstants.FORCE_REFRESH);
                this.objectName = StringUtils.Substring(this.objectName,
                    StringUtils.IndexOf(this.objectName, "design/") + 7, StringUtils.Length(this.objectName));
                this.objectName = StringUtils.Replace(this.objectName, "/", ".");
            }
        } else {
            this.objectName = Property.String(this.objectName, $value);
        }

        return this.objectName;
    }

    /**
     * Execute implementation.
     * @returns {void}
     */
    public Process() : void {
        Echo.ClearAll();
        this.resolver();
    }

    protected testMode() : boolean {
        return true;
    }

    protected refresh() : boolean {
        return true;
    }

    protected includeChildren() : boolean {
        return true;
    }

    protected navigator() : string {
        return "";
    }

    protected resolver() : void {
        this.overrideResolver("/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}", DesignerErrorHandler);

        const connector : BuilderConnector = BuilderConnector.Connect();
        connector.getEvents().OnError(($args : ErrorEventArgs) : void => {
            Echo.Println($args.Message());
        });

        const errorHandler : IWebServiceResponseHandler = ($message : string) : void => {
            this.fireOnBuild("fail");
            Echo.Println($message);
        };

        connector.getEvents().OnStart(() : void => {
            connector.getEvents().OnBuildStart(() : void => {
                this.fireOnBuild("start");
            });
            connector.getEvents().OnBuildComplete(() : void => {
                this.fireOnBuild("complete");
            });

            connector.getEvents().OnFail(errorHandler);
            connector.getEvents().OnWarning(errorHandler);
        });

        connector.getEvents().OnMessage(($data : string) : void => {
            Echo.Println($data);
        });

        if (!ObjectValidator.IsEmptyOrNull(this.ObjectClassName())) {
            GuiCommons.GuiElementClass(DesignerElement);
            const reflection : Reflection = Reflection.getInstance();
            const newObjectsMap : ArrayList<IGuiCommons> = new ArrayList<IGuiCommons>();
            let viewer : BaseViewer;
            let instance : IGuiCommons = null;
            const instanceMap : ArrayList<string> = new ArrayList<string>();

            const getInstance : () => void = () : void => {
                if (ObjectValidator.IsEmptyOrNull(viewer)) {
                    viewer = ViewerManager.getLoadedViewers().getItem(this.loadGuiObject().getId());
                    instance = viewer.getInstance();
                }
            };

            const variableNameExists : ($name : string) => boolean = ($name : string) : boolean => {
                let index : number;
                if (!ObjectValidator.IsEmptyOrNull(instance)) {
                    const properties : string[] = instance.getProperties();
                    for (index = 0; index < properties.length; index++) {
                        if (properties[index] === $name) {
                            return true;
                        }
                    }
                }
                for (index = 0; index < newObjectsMap.Length(); index++) {
                    if (newObjectsMap.getItem(index)[DesignerProtocolConstants.VARIABLE_NAME] === $name) {
                        return true;
                    }
                }
                return false;
            };

            const getInstanceMap : ($element : IGuiCommons) => void = ($element : IGuiCommons) : void => {
                const elements : ArrayList<IGuiCommons> = this.guiManager.getAll();
                instanceMap.Clear();
                let elementMap : IDesignerInstanceMapItem[] =
                    <IDesignerInstanceMapItem[]>(<DesignerElement>$element.getInnerHtmlMap()).getMap();
                let position : ElementOffset = instance.getScreenPosition();
                const size : Size = instance.getSize();

                const dataLookup : ($data : IDesignerInstanceMapItem[]) => void = ($data : IDesignerInstanceMapItem[]) : void => {
                    $data.forEach(($item : IDesignerInstanceMapItem) : void => {
                        if (ObjectValidator.IsSet($item.name)) {
                            instanceMap.Add($item.name, $item.id);
                            let position : ElementOffset;
                            if (elements.KeyExists($item.id)) {
                                const guiElement : IGuiCommons = elements.getItem($item.id);
                                position = guiElement.getScreenPosition();
                                const size : Size = guiElement.getSize();
                                $item.top = position.Top();
                                $item.left = position.Left();
                                $item.width = size.Width();
                                $item.height = size.Height();
                            }
                        } else if (ElementManager.Exists($item.id)) {
                            const htmlElement : HTMLElement = ElementManager.getElement($item.id);
                            position = ElementManager.getAbsoluteOffset(htmlElement);
                            $item.top = position.Top();
                            $item.left = position.Left();
                            $item.width = htmlElement.offsetWidth;
                            $item.height = htmlElement.offsetHeight;
                        }
                        if (ObjectValidator.IsSet($item.map)) {
                            dataLookup(<IDesignerInstanceMapItem[]>$item.map);
                        }
                    });
                };

                elementMap = <IDesignerInstanceMapItem[]>[
                    <IDesignerInstanceMapItem>{
                        height: size.Height(),
                        id    : $element.Id(),
                        left  : position.Left(),
                        map   : elementMap,
                        name  : DesignerProtocolConstants.ROOT_ELEMENT_VARIABLE,
                        top   : position.Top(),
                        type  : DesignerProtocolConstants.GUI_OBJECT,
                        width : size.Width()
                    }
                ];
                dataLookup(elementMap);
                this.sendData(<IDesignerProtocol>{
                    data: elementMap,
                    task: DesignerTaskType.GET_INSTANCE_MAP
                });
            };

            const getBlankInstanceMap : () => void = () : void => {
                const map : IDesignerInstanceMapItem[] = [];
                newObjectsMap.foreach(($element : IGuiCommons) : void => {
                    const position : ElementOffset = $element.getScreenPosition();
                    const size : Size = $element.getSize();
                    map.push(
                        <IDesignerInstanceMapItem>{
                            height: size.Height(),
                            id    : $element.Id(),
                            left  : position.Left(),
                            name  : $element[DesignerProtocolConstants.VARIABLE_NAME],
                            top   : position.Top(),
                            type  : DesignerProtocolConstants.GUI_OBJECT,
                            width : size.Width()
                        }
                    );
                });
                this.sendData(<IDesignerProtocol>{
                    data: map,
                    task: DesignerTaskType.GET_INSTANCE_MAP
                });
            };

            const markRootElement : IEventsHandler = () : void => {
                getInstance();
                if (!ObjectValidator.IsEmptyOrNull(instance) && instance.IsMemberOf(BasePanel)) {
                    const args : any[] = instance.getArgs();
                    args.push({
                        name : DesignerProtocolConstants.VARIABLE_NAME,
                        type : GuiCommonsArgType.TEXT,
                        value: DesignerProtocolConstants.ROOT_ELEMENT_VARIABLE
                    });
                    this.fireShowProperties(instance.Id(), args);
                    const position : ElementOffset = instance.getScreenPosition();
                    const size : Size = instance.getSize();
                    this.fireMarkItem(<IDesignerItemProtocol>{
                        draggable : false,
                        height    : size.Height(),
                        id        : instance.Id(),
                        left      : position.Left(),
                        resizeable: true,
                        top       : position.Top(),
                        width     : size.Width()
                    });
                }
            };

            const moveElement : ($elementId : string, $top : number, $left : number) => void =
                ($elementId : string, $top : number, $left : number) : void => {
                    const element : IGuiCommons = this.guiManager.getAll().getItem($elementId);
                    let wrapperPosition : ElementOffset;
                    if (!ObjectValidator.IsEmptyOrNull(element)) {
                        wrapperPosition = ElementManager.getScreenPosition(
                            ElementManager.getElement($elementId).parentElement.parentElement);
                        const contentElement : HTMLElement = ElementManager.getElement((<any>element).guiContentId());
                        element.setPosition(
                            $top - wrapperPosition.Top() - contentElement.offsetTop,
                            $left - wrapperPosition.Left() - contentElement.offsetLeft);
                    } else if (ElementManager.Exists($elementId)) {
                        const positionType : string = ElementManager.getCssValue($elementId, "position");
                        if (ObjectValidator.IsEmptyOrNull(positionType) ||
                            positionType === "static" ||
                            positionType === "relative") {
                            if (positionType !== "relative") {
                                ElementManager.setCssProperty($elementId, "position", "relative");
                            }
                        }
                        wrapperPosition = ElementManager.getScreenPosition(ElementManager.getElement($elementId).parentElement);
                        ElementManager.setCssProperty($elementId, "top", $top - wrapperPosition.Top());
                        ElementManager.setCssProperty($elementId, "left", $left - wrapperPosition.Left());
                    }
                };

            WindowManager.getEvents().setOnMessage(($eventArgs : MessageEventArgs) : void => {
                const data : IDesignerProtocol = <IDesignerProtocol>$eventArgs.NativeEventArgs().data;
                let elementId : string;
                let element : IGuiCommons;
                let htmlElement : HTMLElement;
                switch (data.task) {
                case DesignerTaskType.RELOAD:
                    this.getHttpManager().Reload();
                    break;
                case DesignerTaskType.BUILD:
                    connector.Send("Cmd", {
                        args: ["build", "dev", "--skip-test"],
                        cmd : "wui"
                    });
                    break;
                case DesignerTaskType.ADD_ITEM:
                    // eslint-disable-next-line no-case-declarations
                    const addItemData : IDesignerItemProtocol = <IDesignerItemProtocol>data.data;
                    elementId = addItemData.id;
                    if (reflection.Exists(elementId)) {
                        Echo.Printf("Add item: {0}", elementId);

                        getInstance();
                        const newObjectClass : any = reflection.getClass(elementId);
                        let newObject : IGuiCommons;
                        if (reflection.ClassHasInterface(newObjectClass, BasePanelViewer.ClassName())) {
                            const newViewer : BasePanelViewer = new newObjectClass(newObjectClass.getTestViewerArgs());
                            newViewer.PrepareImplementation();
                            newViewer.getInstance().getChildPanelList().foreach(($child : BasePanelViewer) : void => {
                                $child.PrepareImplementation();
                            });
                            newObject = newViewer.getInstance();
                        } else {
                            newObject = new newObjectClass();
                        }
                        newObject.getGuiOptions().Add(GuiOptionType.ALL);

                        let variableName : string = newObject.getClassNameWithoutNamespace();
                        variableName = StringUtils.ToLowerCase(StringUtils.Substring(variableName, 0, 1)) +
                            StringUtils.Substring(variableName, 1);
                        let variableIncrement : number = 1;
                        while (variableNameExists(variableName + variableIncrement)) {
                            variableIncrement++;
                        }
                        newObject[DesignerProtocolConstants.VARIABLE_NAME] = variableName + variableIncrement;

                        let addToPanel : boolean = false;
                        let target : HTMLElement;
                        let positionType : string = "fixed";
                        if (!ObjectValidator.IsEmptyOrNull(instance)) {
                            if (instance.IsMemberOf(BasePanel)) {
                                addToPanel = true;
                                (<BasePanel>instance).AddChild(newObject, newObject[DesignerProtocolConstants.VARIABLE_NAME]);
                                positionType = "relative"; // eslint-disable-line @typescript-eslint/no-unused-vars
                            } else {
                                newObject.Parent(instance);
                                target = ElementManager.getElement(instance).parentElement.parentElement.parentElement;
                            }
                        } else {
                            target = ElementManager.getElement("Content");
                        }

                        if (!ObjectValidator.IsEmptyOrNull(addItemData.top)) {
                            newObject.getEvents().setOnComplete(() : void => {
                                moveElement(newObject.Id(), addItemData.top, addItemData.left);
                            });
                        }

                        newObjectsMap.Add(newObject);

                        if (!addToPanel) {
                            newObject.InstanceOwner(viewer);
                            newObject.Visible(false);
                            target.appendChild(new GuiElement().Add(newObject).ToDOMElement(StringUtils.NewLine(false)));
                            newObject.Visible(true);
                        }

                        if (!ObjectValidator.IsEmptyOrNull(instance)) {
                            if (!addToPanel) {
                                instance[newObject[DesignerProtocolConstants.VARIABLE_NAME]] = newObject;
                                instance.getChildElements().Add(newObject, newObject.Id());
                                instance.getInnerHtmlMap().Add(newObject);
                            }
                            getInstanceMap(instance);
                        } else {
                            getBlankInstanceMap();
                        }
                    } else {
                        Echo.Printf("Required item class does not exist: {0}", elementId);
                    }
                    break;
                case DesignerTaskType.DELETE_ITEM:
                    elementId = (<IDesignerItemProtocol>data.data).id;
                    element = this.guiManager.getAll().getItem(elementId);
                    Echo.Printf("Delete: {0}", elementId);
                    htmlElement = null;
                    if (!ObjectValidator.IsEmptyOrNull(element)) {
                        element.Visible(false);
                        newObjectsMap.RemoveAt(newObjectsMap.IndexOf(element));
                        htmlElement = ElementManager.getElement(elementId).parentElement.parentElement;
                        this.guiManager.Clear(element);
                    } else if (ElementManager.Exists(elementId)) {
                        htmlElement = ElementManager.getElement(elementId);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(htmlElement)) {
                        htmlElement.parentElement.removeChild(htmlElement);
                        ElementManager.CleanElementCache(elementId);
                    }
                    break;
                case DesignerTaskType.ITEM_EXISTS:
                    getInstance();
                    this.fireItemExists(elementId, variableNameExists(data.data));
                    break;
                case DesignerTaskType.SELECT_ROOT_ITEM:
                    markRootElement();
                    break;
                case DesignerTaskType.SELECT_ITEM:
                    elementId = data.data;
                    element = this.guiManager.getAll().getItem(elementId);
                    let args : IGuiCommonsArg[]; // eslint-disable-line no-case-declarations
                    if (!ObjectValidator.IsEmptyOrNull(element)) {
                        args = element.getArgs();
                        args.push({
                            name : DesignerProtocolConstants.VARIABLE_NAME,
                            type : GuiCommonsArgType.TEXT,
                            value: instanceMap.getItem(elementId)
                        });
                        this.fireShowProperties(elementId, args);
                        const position : ElementOffset = element.getScreenPosition();
                        const size : Size = element.getSize();
                        this.fireMarkItem(<IDesignerItemProtocol>{
                            draggable : ObjectValidator.IsEmptyOrNull(instance) ||
                                !ObjectValidator.IsEmptyOrNull(instance) && elementId !== instance.Id(),
                            height    : size.Height(),
                            id        : elementId,
                            left      : position.Left(),
                            resizeable: true,
                            top       : position.Top(),
                            width     : size.Width()
                        });
                    } else if (ElementManager.Exists(elementId)) {
                        args = [
                            {
                                name : "Id",
                                type : GuiCommonsArgType.TEXT,
                                value: elementId
                            },
                            {
                                name : DesignerProtocolConstants.VARIABLE_NAME,
                                type : GuiCommonsArgType.TEXT,
                                value: DesignerProtocolConstants.HTML_ELEMENT_VARIABLE
                            },
                            {
                                name : "StyleClassName",
                                type : GuiCommonsArgType.TEXT,
                                value: ""
                            },
                            {
                                name : "Visible",
                                type : GuiCommonsArgType.BOOLEAN,
                                value: true
                            }
                        ];
                        this.fireShowProperties(elementId, args);
                        htmlElement = ElementManager.getElement(elementId);
                        const position : ElementOffset = ElementManager.getAbsoluteOffset(htmlElement);
                        this.fireMarkItem(<IDesignerItemProtocol>{
                            draggable : true,
                            height    : htmlElement.offsetHeight,
                            id        : elementId,
                            left      : position.Left(),
                            resizeable: true,
                            top       : position.Top(),
                            width     : htmlElement.offsetWidth
                        });
                    }
                    break;
                case DesignerTaskType.MOVE_ITEM:
                    // eslint-disable-next-line no-case-declarations
                    const moveItemData : IDesignerItemProtocol = <IDesignerItemProtocol>data.data;
                    moveElement(moveItemData.id, moveItemData.top, moveItemData.left);
                    break;
                case DesignerTaskType.GET_INSTANCE_MAP:
                    if (!ObjectValidator.IsEmptyOrNull(instance)) {
                        getInstanceMap(instance);
                    } else {
                        getBlankInstanceMap();
                    }
                    break;
                case DesignerTaskType.SET_PROPERTY:
                    // eslint-disable-next-line no-case-declarations
                    const setItemData : IDesignerItemArgProtocol = <IDesignerItemArgProtocol>data.data;
                    elementId = setItemData.id;
                    getInstance();

                    Echo.Printf("Set item: {0}[{1}] - {2}", elementId, setItemData.name, setItemData.value);

                    element = this.guiManager.getAll().getItem(elementId);
                    if (!ObjectValidator.IsEmptyOrNull(element)) {
                        if (setItemData.name === DesignerProtocolConstants.VARIABLE_NAME) {
                            if (!variableNameExists(<string>setItemData.value)) {
                                element[DesignerProtocolConstants.VARIABLE_NAME] = setItemData.value;
                            } else {
                                this.fireItemExists(elementId, true);
                            }
                        } else {
                            element.setArg(setItemData, true);
                        }
                    } else if (ElementManager.Exists(elementId)) {
                        if (setItemData.name === "Visible") {
                            if (<boolean>setItemData.value) {
                                ElementManager.Show(elementId);
                            } else {
                                ElementManager.Hide(elementId);
                            }
                        } else {
                            ElementManager.setCssProperty(elementId, setItemData.name,
                                setItemData.type === GuiCommonsArgType.NUMBER ?
                                    Convert.StringToInteger(<string>setItemData.value) : <string>setItemData.value);
                        }
                    }
                    break;
                case DesignerTaskType.SET_SIZE:
                    // eslint-disable-next-line no-case-declarations
                    const sizeItemData : IDesignerItemProtocol = <IDesignerItemProtocol>data.data;
                    const value : number = sizeItemData.zoom; // eslint-disable-line no-case-declarations
                    ElementManager.setZoom(document.body, 100);
                    ElementManager.setZoom("Designer_Background", 100);
                    ElementManager.setZoom(document.body, value);
                    if (value <= 100) {
                        const newWidth : number = Convert.ToFixed(sizeItemData.width * 100 / value, 0);
                        const newHeight : number = Convert.ToFixed(sizeItemData.height * 100 / value, 0);
                        ElementManager.setWidth(document.body, newWidth);
                        ElementManager.setHeight("Designer_Background", newHeight);
                    }
                    break;
                default:
                    Echo.Println("Unrecognized Designer task: \"" + data.task + "\"");
                    break;
                }
            });

            WindowManager.getEvents().setOnDoubleClick(markRootElement);

            WindowManager.getEvents().setOnLoad(() : void => {
                Echo.setOnPrint(($message : string) : void => {
                    this.sendData(<IDesignerProtocol>{
                        data: $message,
                        task: DesignerTaskType.ECHO
                    });
                });

                StaticPageContentManager.BodyAppend(
                    "<div guiType=\"DeveloperCorner\" class=\"DeveloperCorner\">" +
                    "   <div class=\"Designer\">" +
                    "       <div id=\"Designer_Background\" class=\"Background\"></div>" +
                    "   </div>" +
                    "</div>");
                ElementManager.setHeight("Designer_Background", WindowManager.getSize().Height());

                if (ObjectValidator.IsEmptyOrNull(viewer)) {
                    getInstance();
                    if (!ObjectValidator.IsEmptyOrNull(instance)) {
                        instance.getEvents().setOnComplete(($eventArgs : EventArgs) : void => {
                            getInstanceMap($eventArgs.Owner());
                            this.fireEvent(EventType.ON_LOAD, "root");
                        });
                    }
                }

                this.fireEvent(EventType.ON_LOAD, "body");
            });

            super.resolver();
        } else {
            const host : string = this.getRequest().getHostUrl();
            const reflection : Reflection = Reflection.getInstance();
            const allClasses : string[] = reflection.getAllClasses();
            let index : number;
            const projectMap : IDesignerProjectMapProtocol = {
                blankViewer : host + BlankPanelViewer.CallbackLink(),
                guiObjects  : {
                    components  : {
                        names: []
                    },
                    panels      : {
                        names: []
                    },
                    userControls: {
                        names: []
                    }
                },
                host,
                runtimeTests: {
                    links: [],
                    names: []
                },
                viewers     : {
                    links: [],
                    names: []
                }
            };

            for (index = 0; index < allClasses.length; index++) {
                const className : string = allClasses[index];
                if (className !== RuntimeTestRunner.ClassName() &&
                    className !== GuiRuntimeTestRunner.ClassName() &&
                    !reflection.ClassHasInterface(className, BasePanel.ClassName())) {
                    const classObject : any = reflection.getClass(className);
                    if (reflection.ClassHasInterface(className, IBaseViewer)) {
                        if (StringUtils.Contains(className, "RuntimeTests")) {
                            projectMap.runtimeTests.names.push(className);
                            projectMap.runtimeTests.links.push(host + classObject.CallbackLink());
                        } else {
                            projectMap.viewers.names.push(className);
                            projectMap.viewers.links.push(host + classObject.CallbackLink());
                        }
                        projectMap.guiObjects.panels.names.push(className);
                    } else if (reflection.ClassHasInterface(className, RuntimeTestRunner.ClassName())) {
                        projectMap.runtimeTests.names.push(className);
                        const linkName : string = "/" + HttpRequestConstants.RUNTIME_TEST + "/" +
                            StringUtils.Replace(className, ".", "/");
                        this.registerResolver(linkName, classObject);
                        projectMap.runtimeTests.links.push(host + "#" + linkName);
                    } else if (reflection.ClassHasInterface(className, GuiRuntimeTestRunner.ClassName())) {
                        projectMap.runtimeTests.names.push(className);
                        projectMap.runtimeTests.links.push(host + "#" + classObject.CallbackLink());
                    } else if (reflection.ClassHasInterface(className, IFormsObject)) {
                        projectMap.guiObjects.userControls.names.push(className);
                    } else if (reflection.ClassHasInterface(className, IGuiCommons)) {
                        projectMap.guiObjects.components.names.push(className);
                    }
                }
            }

            StaticPageContentManager.Clear();
            this.sendData(<IDesignerProtocol>{
                data: projectMap,
                task: DesignerTaskType.GET_PROJECT_MAP
            });

            WindowManager.getEvents().setOnMessage(($eventArgs : MessageEventArgs) : void => {
                const data : IDesignerProtocol = <IDesignerProtocol>$eventArgs.NativeEventArgs().data;
                const fileSystemRequestHandler : any =
                    ($taskType : DesignerTaskType, $apiName : string, $data : IDesignerItemProtocol) : void => {
                        connector.Send("FileSystem", {
                            type : $apiName,
                            value: $data
                        }, ($data : any) : void => {
                            this.sendData(<IDesignerProtocol>{
                                data: JSON.parse(ObjectDecoder.Base64($data.data)),
                                task: $taskType
                            });
                        });
                    };
                const task : DesignerTaskType = data.task;

                switch (task) {
                case DesignerTaskType.GET_FILESYSTEM_PATH:
                    fileSystemRequestHandler(task, "getPath",
                        <IDesignerItemProtocol>data.data);
                    break;
                case DesignerTaskType.GET_FILESYSTEM_DIRECTORY:
                    fileSystemRequestHandler(task, "getDirectory",
                        <IDesignerItemProtocol>data.data);
                    break;

                default:
                    Echo.Println("Unrecognized Designer task: \"" + task + "\"");
                    break;
                }
            });
        }
    }

    private sendData($data : IDesignerProtocol) : void {
        try {
            window.parent.postMessage($data, "*");
        } catch (ex) {
            window.parent.postMessage(<IDesignerProtocol>{
                data: "Failed to send designer data: " + ex.message,
                task: DesignerTaskType.FAIL
            }, "*");
        }
    }

    private fireEvent($type : string, $args : string) : void {
        this.sendData(<IDesignerProtocol>{
            data: <IDesignerEventProtocol>{
                args: $args,
                type: $type
            },
            task: DesignerTaskType.EVENT
        });
    }

    private fireOnBuild($type : string) : void {
        this.fireEvent("onBuild", $type);
    }

    private fireItemExists($elementId : string, $value : boolean) : void {
        this.sendData(<IDesignerProtocol>{
            data: <IDesignerItemArgProtocol>{
                id   : $elementId,
                name : "Exists",
                type : GuiCommonsArgType.BOOLEAN,
                value: $value
            },
            task: DesignerTaskType.ITEM_EXISTS
        });
    }

    private fireShowProperties($elementId : string, $properties : IGuiCommonsArg[]) : void {
        this.sendData(<IDesignerProtocol>{
            data: <IDesignerItemArgsProtocol>{
                id   : $elementId,
                value: $properties
            },
            task: DesignerTaskType.SHOW_PROPERTIES
        });
    }

    private fireMarkItem($data : IDesignerItemProtocol) : void {
        this.sendData(<IDesignerProtocol>{
            data: $data,
            task: DesignerTaskType.MARK_ITEM
        });
    }
}
