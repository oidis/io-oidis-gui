/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { DesignerProtocolConstants } from "../Enums/DesignerProtocolConstants.js";
import { IDesignerElementMap } from "../Interfaces/Designer/IDesignerElementMap.js";
import { IBaseViewer } from "../Interfaces/Primitives/IBaseViewer.js";
import { IGuiCommons } from "../Interfaces/Primitives/IGuiCommons.js";
import { IGuiElement } from "../Interfaces/Primitives/IGuiElement.js";
import { GuiCommons } from "../Primitives/GuiCommons.js";
import { GuiElement } from "../Primitives/GuiElement.js";

/**
 * DesignerElement class provides structure for handling of element rendering focused on GUI Designer needs.
 */

export class DesignerElement extends GuiElement {
    private static elementIndex : number = 0;
    private readonly designerId : string;

    constructor() {
        super();
        DesignerElement.elementIndex++;
        this.designerId = DesignerProtocolConstants.DESIGNER_ELEMENT + DesignerElement.elementIndex;
    }

    /**
     * @param {string|IGuiElement|IGuiCommons|IBaseViewer|HTMLElement} $value Specify element's content.
     * @returns {IGuiElement} Returns reference to current GuiElement instance.
     */
    public Add($value : string | IGuiElement | IGuiCommons | IBaseViewer | HTMLElement) : IGuiElement {
        DesignerElement.elementIndex++;
        return super.Add($value);
    }

    /**
     * @returns {IDesignerElementMap[]} Returns object's map, which describes content of the GUI Element.
     */
    public getMap() : IDesignerElementMap[] {
        const map : IDesignerElementMap[] = [];
        const reflection : Reflection = Reflection.getInstance();
        this.getChildElements().foreach(($element : any, $key : number) : void => {
            const item : IDesignerElementMap = <IDesignerElementMap>{};
            if (reflection.IsMemberOf($element, GuiElement)) {
                item.id = $element.designerId;
                item.type = DesignerProtocolConstants.GUI_ELEMENT;
                item.map = $element.getMap();
            } else if (reflection.IsMemberOf($element, GuiCommons)) {
                const element : GuiCommons = <GuiCommons>$element;
                const parent : GuiCommons = element.Parent();
                item.id = element.Id();
                item.type = DesignerProtocolConstants.GUI_OBJECT;
                if (!ObjectValidator.IsEmptyOrNull(parent)) {
                    parent.getProperties().forEach(($value : string) : void => {
                        if (parent.hasOwnProperty($value) && parent[$value] === element) {
                            item.name = $value;
                        }
                    });
                }
            } else {
                item.id = this.designerId + "_" + $key;
                item.type = DesignerProtocolConstants.HTML_ELEMENT;
            }
            map.push(item);
        });
        return map;
    }

    protected itemToString($item : string | IGuiElement | IGuiCommons | IBaseViewer | HTMLElement, $EOL : string,
                           $key? : number) : string {
        if (ObjectValidator.IsEmptyOrNull($item)) {
            return "";
        }
        if (ObjectValidator.IsString($item) || ObjectValidator.IsSet((<HTMLElement>$item).outerHTML)) {
            return $EOL + "<span id=\"" + this.designerId + "_" + $key + "\">" + super.itemToString($item, $EOL + "    ") + "</span>";
        }
        return super.itemToString($item, $EOL);
    }
}
