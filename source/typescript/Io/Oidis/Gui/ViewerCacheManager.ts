/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { TimeoutManager } from "@io-oidis-commons/Io/Oidis/Commons/Events/TimeoutManager.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { BasePersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/Handlers/BasePersistenceHandler.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EventType } from "./Enums/Events/EventType.js";
import { ElementEventsManager } from "./Events/ElementEventsManager.js";
import { EventsManager } from "./Events/EventsManager.js";
import { GuiObjectManager } from "./GuiObjectManager.js";
import { IGuiCommons } from "./Interfaces/Primitives/IGuiCommons.js";
import { Loader } from "./Loader.js";
import { BaseViewer } from "./Primitives/BaseViewer.js";
import { GuiCommons } from "./Primitives/GuiCommons.js";
import { Size } from "./Structures/Size.js";
import { ElementManager } from "./Utils/ElementManager.js";
import { WindowManager } from "./Utils/WindowManager.js";
import { ViewerManager } from "./ViewerManager.js";

/**
 * ViewerCacheManager class provides creation and loading of viewer's cache.
 */
export class ViewerCacheManager extends BaseObject {
    private storage : IPersistenceHandler;
    private htmlCache : string;
    private jsCache : string;
    private readonly cacheFilePath : string;
    private viewerInstance : BaseViewer;
    private isLoaded : boolean;

    /**
     * Content should be replaced by cache execution
     * @returns {BaseViewer} Returns instance of BaseViewer which has been resolved from cache
     */
    private static getViewerInstance() : BaseViewer {
        return null;
    }

    /**
     * Content should be replaced by cache execution
     * @returns {IGuiCommons[]} Returns list of cached elements
     */
    private static getCachedElements() : IGuiCommons[] {
        return [];
    }

    /**
     * Content should be replaced by cache execution
     * @returns {number} Returns current events count.
     */
    private static getProgressEventsCount() : number {
        return 0;
    }

    /**
     * @param {string} $viewerManagerId Specify viewer's manager id, by which is viewer handled.
     * @param {string} [$cacheFilePath] Specify file path, which should be used for store and load of raw cache data.
     */
    constructor($viewerManagerId : string, $cacheFilePath? : string) {
        super();

        this.storage = PersistenceFactory.getPersistence($viewerManagerId);
        this.storage.DisableCRC();
        this.cacheFilePath = $cacheFilePath;
        this.htmlCache = "";
        this.jsCache = "";
        this.isLoaded = false;
    }

    /**
     * @param {BaseViewer} $value Specify viewer instance, which should be manged by cache manager.
     * @returns {void}
     */
    public setViewerInstance($value : BaseViewer) : void {
        this.viewerInstance = $value;
    }

    /**
     * Load cache data from desired source, but do not provide instantiation of cached viewer.
     * @param {Callback} [$successHandler] Handler, which should be called on successfully loaded cache.
     * @param {Callback} [$errorHandler] Handler, which should be called on cache loading error.
     * @returns {void}
     */
    public Load($successHandler? : () => void, $errorHandler? : () => void) : void {
        this.storage.LoadPersistenceAsynchronously(() : void => {
            this.isLoaded = true;
            if (this.storage.Exists("HTML") && this.storage.Exists("JS")) {
                this.htmlCache = this.storage.Variable("HTML");
                this.jsCache = this.storage.Variable("JS");
                if (ObjectValidator.IsSet($successHandler)) {
                    $successHandler();
                }
            } else {
                if (ObjectValidator.IsSet($errorHandler)) {
                    $errorHandler();
                }
            }
        }, this.cacheFilePath);
    }

    /**
     * Create cache from viewer handled by the manager.
     * @param {Callback} [$handler] Handler, which should be called after successfully created cache.
     * @returns {void}
     */
    public Create($handler? : () => void) : void {
        this.htmlCache = this.getHtmlCode();
        this.getJsCode(($code : string) : void => {
            this.jsCache = $code;
            if (ObjectValidator.IsSet($handler)) {
                $handler();
            }
        });
    }

    /**
     * Load cache data and prepare viewer instance.
     * @param {Callback} [$handler] Handler, which should be called after successfully instantiation of cached viewer.
     * @returns {void}
     */
    public Process($handler? : ($viewer : BaseViewer, $elements : IGuiCommons[]) => void) : void {
        const events : EventsManager = <EventsManager>EventsManager.getInstanceSingleton();

        const loadViewerCache : () => void = () : void => {
            const progressBar : HTMLElement = document.createElement("div");
            progressBar.id = "CacheLoadProgress";
            progressBar.className = "CacheLoaderProgressBar";
            document.getElementById("Content").appendChild(progressBar);

            const pageWidth : number = Loader.getInstance().getHttpManager().getRequest().IsJre(true) ?
                new Size("Browser", true).Width() : WindowManager.getSize().Width();
            let progressScale : number = 0;
            let progressTick : number = 0;
            events.setEvent(ViewerCacheManager.ClassName(), EventType.ON_CHANGE, () : void => {
                const progressBarWidth : number = Math.round(progressScale * progressTick) -
                    ElementManager.getCssIntegerValue(progressBar.id, "left") * 2;
                progressTick++;
                ElementManager.setWidth(progressBar.id, progressBarWidth);
            });

            const script : HTMLScriptElement = document.createElement("script");
            events.setEvent(ViewerCacheManager.ClassName(), EventType.ON_COMPLETE, () : void => {
                ElementManager.setWidth(progressBar.id, pageWidth - ElementManager.getCssIntegerValue(progressBar.id, "left") * 2);
                this.viewerInstance = ViewerCacheManager.getViewerInstance();
                if (!ObjectValidator.IsEmptyOrNull(script.parentNode)) {
                    script.parentNode.removeChild(script);
                }
                if (ObjectValidator.IsSet($handler)) {
                    $handler(this.viewerInstance, ViewerCacheManager.getCachedElements());
                }
                document.body.style.cursor = "default";
                if (!ObjectValidator.IsEmptyOrNull(progressBar.parentNode)) {
                    progressBar.parentNode.removeChild(progressBar);
                }
            });

            document.body.style.cursor = "wait";
            events.FireAsynchronousMethod(() : void => {
                script.type = "text/javascript";
                script.text = this.jsCache;
                if (!ObjectValidator.IsEmptyOrNull(globalThis.nonce)) {
                    script.nonce = globalThis.nonce;
                }
                document.body.appendChild(script);
                progressScale = pageWidth / ViewerCacheManager.getProgressEventsCount();
            }, 100);
        };

        if (!this.isLoaded) {
            this.Load(loadViewerCache);
        } else {
            loadViewerCache();
        }
    }

    /**
     * Try to save created cache data to the desired cache storage.
     * @param {Callback} [$handler] Handler, which should be called after try to save the cache.
     * @returns {void}
     */
    public Store($handler? : () => void) : void {
        this.storage.LoadPersistenceAsynchronously(() : void => {
            try {
                this.storage.Variable("HTML", this.htmlCache);
                this.storage.Variable("JS", this.jsCache);
            } catch (ex) {
                LogIt.Error("Viewer cache has not been stored correctly", ex);
            }
            if (ObjectValidator.IsSet($handler)) {
                $handler();
            }
        });
    }

    /**
     * Clean up cache data from cache storage.
     * @returns {void}
     */
    public Clear() : void {
        this.storage.Clear();
    }

    /**
     * @returns {string} Returns HTML output provided by the cache, if the cache has been loaded successfully, otherwise empty string.
     */
    public Show() : string {
        return this.htmlCache;
    }

    /**
     * Provides raw cache data in format specified by the cache storage.
     * It can also provides saving of the cache for ability to read it's raw data.
     * @param {Callback} $handler Handler, which should be called when the cache data are ready.
     * @returns {void}
     */
    public getRawData($handler : ($data : string) => void) : void {
        if (!this.isLoaded) {
            this.Load(() : void => {
                this.storage.getRawData($handler);
            });
        } else {
            this.Store(() : void => {
                this.storage.getRawData($handler);
            });
        }
    }

    public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
        /* dev:start */
        let output : string = "";
        if (!ObjectValidator.IsEmptyOrNull(this.cacheFilePath)) {
            output += "Cache file path: " + this.cacheFilePath + StringUtils.NewLine($htmlTag);
        }

        let cacheReport : string = "";
        if (!ObjectValidator.IsEmptyOrNull(this.htmlCache) && !ObjectValidator.IsEmptyOrNull(this.jsCache)) {
            cacheReport +=
                "HTML cache size: " + (StringUtils.Length(this.htmlCache) / 1000) + " KB" + StringUtils.NewLine() +
                "JavaScript cache size: " + (StringUtils.Length(this.jsCache) / 1000) + " KB";

            if ($htmlTag) {
                let html : string = StringUtils.Replace(this.htmlCache, "<", "&lt;");
                html = StringUtils.Replace(html, ">", "&gt;");
                let js : string = StringUtils.Replace(this.jsCache, ";", ";" + StringUtils.NewLine(false));
                js = StringUtils.Replace(js, ",", "," + StringUtils.NewLine(false));
                js = StringUtils.Replace(js, "{", "{" + StringUtils.NewLine(false));
                js = StringUtils.Replace(js, "}", "}" + StringUtils.NewLine(false));
                js = StringUtils.Replace(js, "}" + StringUtils.NewLine(false) + ")", "})");
                js = StringUtils.Replace(js, "}" + StringUtils.NewLine(false) + ";", "};");
                js = StringUtils.Replace(js, "," + StringUtils.NewLine(false) + "\"", ",\"");
                js = StringUtils.Replace(js, "," + StringUtils.NewLine(false) + "false", ",false");
                js = StringUtils.Replace(js, "}" + StringUtils.NewLine(false) + ",\"", "}," + StringUtils.NewLine(false) + "\"");
                js = StringUtils.Replace(js, "\",\"", "\"," + StringUtils.NewLine(false) + "\"");
                js = StringUtils.Replace(js, ",\"progressEventsCount\"", "," + StringUtils.NewLine(false) + "\"progressEventsCount\"");
                js = StringUtils.Replace(js, ",\"Load\"", "," + StringUtils.NewLine(false) + "\"Load\"");
                js = StringUtils.Replace(js, "<", "&lt;");
                js = StringUtils.Replace(js, ">", "&gt;");

                cacheReport +=
                    "<hr>HTML<hr>" +
                    "<pre>" + html + "</pre>" +
                    "<hr>JavaScript<hr>" +
                    "<pre>" + js + "</pre>";
            }
        }

        if (ObjectValidator.IsEmptyOrNull(cacheReport)) {
            output += "Cache status: EMPTY";
        } else {
            output += "Cache status: GENERATED" + StringUtils.NewLine($htmlTag) + cacheReport;
        }

        return output;
        /* dev:end */
    }

    private getHtmlCode() : string {
        /* dev:start */
        const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
        const resetItems : ArrayList<any> = new ArrayList<any>();
        const excludeViewers : string[] = [];
        ViewerManager.getLoadedViewers().foreach(($viewer : BaseViewer) : void => {
            if ($viewer !== this.viewerInstance) {
                if (!ObjectValidator.IsEmptyOrNull($viewer.getInstance())) {
                    excludeViewers.push($viewer.getInstance().Id());
                }
            }
        });

        manager.getAll().foreach(($element : GuiCommons) : void => {
            (<any>$element).beforeCacheCreation(($id : string, $value : any) : void => {
                resetItems.Add({element: $element, id: $id, value: $value});
            });
        });

        let pageContent : HTMLElement = document.createElement("span");
        pageContent.innerHTML = document.body.innerHTML;

        resetItems.foreach(($item : any) : void => {
            $item.element.afterCacheCreation($item.id, $item.value);
        });

        let nodes : any;
        let index : number;
        let child : any;
        let attributeValue : string;
        const attributeKey : string = "guitype";

        nodes = pageContent.getElementsByTagName("div");
        for (index = 0; index < nodes.length; index++) {
            child = nodes[index];
            if (child.nodeType === 1 && ObjectValidator.IsSet(child.attributes[attributeKey])) {
                attributeValue = ObjectValidator.IsSet(child.attributes[attributeKey].value) ?
                    child.attributes[attributeKey].value : child.attributes[attributeKey].nodeValue;
                if (attributeValue === "DeveloperCorner") {
                    if (!ObjectValidator.IsEmptyOrNull(child.parentNode)) {
                        child.parentNode.removeChild(child);
                    }
                    break;
                }
            }
        }
        for (index = 0; index < nodes.length; index++) {
            child = nodes[index];
            if (child.nodeType === 1 && ObjectValidator.IsSet(child.attributes[attributeKey])) {
                attributeValue = ObjectValidator.IsSet(child.attributes[attributeKey].value) ?
                    child.attributes[attributeKey].value : child.attributes[attributeKey].nodeValue;
                if (attributeValue === "PageContent") {
                    nodes = child.getElementsByTagName("span");
                    break;
                }
            }
        }

        let output : string = "";
        for (index = 0; index < nodes.length; index++) {
            child = nodes[index];
            if (child.nodeType === 1 && ObjectValidator.IsSet(child.attributes[attributeKey])) {
                attributeValue = ObjectValidator.IsSet(child.attributes[attributeKey].value) ?
                    child.attributes[attributeKey].value : child.attributes[attributeKey].nodeValue;
                if (attributeValue === "HtmlAppender") {
                    let excludeIndex : number;
                    let exclude : boolean = false;
                    for (excludeIndex = 0; excludeIndex < excludeViewers.length; excludeIndex++) {
                        if (StringUtils.Contains(child.innerHTML, excludeViewers[excludeIndex])) {
                            exclude = true;
                            break;
                        }
                    }
                    if (!exclude) {
                        output += child.innerHTML + StringUtils.NewLine(false);
                    }
                }
            }
        }

        output = StringUtils.Remove(output, "<span guitype=\"HtmlAppender\"></span>");
        pageContent = null;
        return output;
        /* dev:end */
    }

    private getJsCode($handler : ($data : string) => void) : void {
        /* dev:start */
        const variableName : string = "i";
        let asyncManager : TimeoutManager = new TimeoutManager();
        let classIndex : number;
        let objectIndex : number = 0;
        let arrayItemIndex : number = 0;
        const classMap : string[] = [];
        let classMapSize : number = 0;
        let classes : string = "";
        let parsedElements : string = "";
        let parsedObjects : ArrayList<any> = new ArrayList<any>();
        const parsedStrings : ArrayList<string> = new ArrayList<string>();
        let postprocessors : string[][] = [];
        let postprocessor : string = "";
        let variablesSubscribe : string = "";
        const excludeList : string[] = [
            "objectClass"
        ];

        let output : string = "try{" +
            ViewerCacheManager.ClassName() + ".Loader={" +
            "\"" + variableName + "\":null," +
            "\"Load\":function(){" +
            "var a=" + Reflection.ClassName() + "," +
            "b=" + PersistenceFactory.ClassName() + "," +
            "c=" + ElementEventsManager.ClassName() + "," +
            "d=" + GuiObjectManager.ClassName() + ".getInstanceSingleton()," +
            "e=" + EventsManager.ClassName() + ".getInstanceSingleton()," +
            "f=" + EventType.ClassName() + "," +
            "g=\"" + ArrayList.ClassName() + "\"," +
            "h=new " + TimeoutManager.ClassName() + "()," +
            variableName + "=a.getInstanceOf(\"" + this.viewerInstance.getClassName() + "\")," +
            "j=" + StringUtils.ClassName() + "," +
            "eol=j.NewLine(false)," +
            "pc=\"" + ViewerCacheManager.ClassName() + "\"," +
            "ob=[]," +
            "ai=[]," +
            "si=[];";
        output += variableName + ".Show=function(){return \"\";};";

        const events : EventsManager = <EventsManager>EventsManager.getInstanceSingleton();
        const cmdLines : ArrayList<string> = new ArrayList<string>();
        const getCode : any = ($object : any, $paramName : string) : void => {
            asyncManager.Add(($index? : number) : void => {
                events.FireEvent(ViewerCacheManager.ClassName(), EventType.ON_CHANGE);
                postprocessors[$index] = [];
                objectIndex++;
                cmdLines.Add("ob[" + objectIndex + "]=" + $paramName + ";");
                $paramName = "ob[" + objectIndex + "]";
                parsedObjects.Add($object, $paramName);
                let parameterName : string;
                let parameterValue : any;
                let objectExclude : string[] = [];
                if (ObjectValidator.IsSet($object.excludeCacheData)) {
                    objectExclude = $object.excludeCacheData();
                }
                for (parameterName in $object) {
                    if ($object.hasOwnProperty(parameterName) &&
                        excludeList.indexOf(parameterName) === -1 &&
                        objectExclude.indexOf(parameterName) === -1) {
                        parameterValue = $object[parameterName];
                        if (!ObjectValidator.IsFunction(parameterValue)) {
                            let cmdLine : string = "";
                            if (ObjectValidator.IsString(parameterName)) {
                                cmdLine += $paramName + "[\"" + parameterName + "\"]";
                            } else {
                                cmdLine += $paramName + "[\"" + parameterName + "\"]";
                            }
                            if (ObjectValidator.IsClass(parameterValue)) {
                                if ((<BasePersistenceHandler>parameterValue).IsMemberOf(BasePersistenceHandler)) {
                                    if (parsedObjects.Contains(parameterValue)) {
                                        cmdLine += "=" + parsedObjects.getKey(parameterValue) + ";";
                                    } else {
                                        let sessionId : string = (<BasePersistenceHandler>parameterValue).getSessionId();
                                        if (ObjectValidator.IsString(sessionId)) {
                                            sessionId = "\"" + sessionId + "\"";
                                        }
                                        objectIndex++;
                                        parsedObjects.Add(parameterValue, "ob[" + objectIndex + "]");
                                        variablesSubscribe += "ob[" + objectIndex + "]=b.getPersistenceById(" + sessionId + ");";
                                        cmdLine += "=ob[" + objectIndex + "];";
                                    }
                                } else if ((<BaseObject>parameterValue).IsTypeOf(ElementEventsManager)) {
                                    cmdLine += "=new c(";
                                    const ownerKey : string = "owner";
                                    const subscriberKey : string = "subscriber";
                                    if (!ObjectValidator.IsEmptyOrNull(parameterValue[ownerKey])) {
                                        if (parsedObjects.Contains(parameterValue[ownerKey])) {
                                            cmdLine += parsedObjects.getKey(parameterValue[ownerKey]);
                                        } else {
                                            cmdLine += "\"" + parameterValue[ownerKey] + "\"";
                                        }
                                        if (!ObjectValidator.IsEmptyOrNull(parameterValue[subscriberKey])) {
                                            cmdLine += ",\"" + parameterValue[subscriberKey] + "\"";
                                        }
                                    }
                                    cmdLine += ");";
                                } else {
                                    if (parsedObjects.Contains(parameterValue)) {
                                        cmdLine += "=" + parsedObjects.getKey(parameterValue) + ";";
                                    } else {
                                        if ((<BaseObject>parameterValue).IsTypeOf(ArrayList)) {
                                            cmdLine += "=a.getInstanceOf(g);";
                                        } else {
                                            classIndex = classMap.indexOf(parameterValue.getClassName());
                                            if (classIndex === -1) {
                                                classIndex = classMapSize;
                                                classMap[classMapSize] = parameterValue.getClassName();
                                                classMapSize++;
                                                if (!ObjectValidator.IsEmptyOrNull(classes)) {
                                                    classes += ",";
                                                }
                                                classes += "\"" + classMap[classIndex] + "\"";
                                            }
                                            cmdLine += "=a.getInstanceOf(cm[" + classIndex + "]);";
                                        }
                                        getCode(parameterValue, $paramName + "[\"" + parameterName + "\"]");
                                    }
                                }
                            } else {
                                if (!ObjectValidator.IsSet(parameterValue)) {
                                    cmdLine += ";";
                                } else {
                                    if (ObjectValidator.IsString(parameterValue)) {
                                        classIndex = classMap.indexOf(parameterValue);
                                        if (classIndex > -1) {
                                            cmdLine += "=cm[" + classIndex + "];";
                                        } else if (ObjectValidator.IsEmptyOrNull(parameterValue)) {
                                            cmdLine += "=\"\";";
                                        } else if (parameterName === "outputEndOfLine" ||
                                            (StringUtils.StartsWith(parameterValue, StringUtils.NewLine(false)) &&
                                                ObjectValidator.IsEmptyOrNull(StringUtils.Remove(
                                                    StringUtils.Remove(parameterValue, StringUtils.NewLine(false)), " ")))) {
                                            cmdLine += "=eol+j.Space(" + StringUtils.OccurrenceCount(parameterValue, " ") + ",false);";
                                        } else {
                                            let stringVariableName : string;
                                            if (parsedStrings.KeyExists(parameterValue)) {
                                                stringVariableName = parsedStrings.getItem(parameterValue);
                                            } else {
                                                stringVariableName = "si[" + parsedStrings.Length() + "]";
                                                parsedStrings.Add(stringVariableName, parameterValue);
                                                variablesSubscribe +=
                                                    stringVariableName + "=decodeURI(\"" + encodeURI(parameterValue) + "\");";
                                            }
                                            cmdLine += "=" + stringVariableName + ";";
                                        }
                                    } else if (ObjectValidator.IsNativeArray(parameterValue)) {
                                        cmdLine += "=[];";
                                        if (!ObjectValidator.IsEmptyOrNull(parameterValue)) {
                                            if (parameterValue.length > 0) {
                                                postprocessor = "";
                                                let index : number;
                                                for (index = 0; index < parameterValue.length; index++) {
                                                    const item : any = parameterValue[index];
                                                    let itemToString : string = "";
                                                    if (ObjectValidator.IsString(item)) {
                                                        itemToString = "\"" + item + "\"";
                                                    } else if (ObjectValidator.IsFunction(item)) {
                                                        itemToString = Convert.FunctionToString(item);
                                                    } else if (ObjectValidator.IsClass(item)) {
                                                        arrayItemIndex++;
                                                        itemToString = "ai[" + arrayItemIndex + "]";
                                                        classIndex = classMap.indexOf(item.getClassName());
                                                        if (classIndex === -1) {
                                                            classIndex = classMapSize;
                                                            classMap[classMapSize] = item.getClassName();
                                                            classMapSize++;
                                                            if (!ObjectValidator.IsEmptyOrNull(classes)) {
                                                                classes += ",";
                                                            }
                                                            classes += "\"" + classMap[classIndex] + "\"";
                                                        }
                                                        variablesSubscribe +=
                                                            itemToString + "=a.getInstanceOf(cm[" + classIndex + "]);";
                                                        getCode(item, itemToString);
                                                    } else if (!ObjectValidator.IsSet(item)) {
                                                        itemToString = "null";
                                                    } else {
                                                        itemToString = item;
                                                    }
                                                    postprocessor += $paramName + "[\"" + parameterName + "\"][" + index + "]=" +
                                                        itemToString + ";";
                                                }
                                                postprocessors[$index].push(postprocessor);
                                            }
                                        }
                                    } else {
                                        cmdLine += "=" + parameterValue + ";";
                                    }
                                }
                            }
                            cmdLines.Add(cmdLine);
                        }
                    }
                }
                if (ObjectValidator.IsClass($object)) {
                    if ((<BaseObject>$object).IsMemberOf(GuiCommons)) {
                        postprocessor = "d.Add(" + $paramName + ");";
                        postprocessors[$index].push(postprocessor);
                        if (!ObjectValidator.IsEmptyOrNull(parsedElements)) {
                            parsedElements += ",";
                        }
                        parsedElements += $paramName;
                    } else if ((<BaseObject>$object).IsMemberOf(BaseViewer)) {
                        cmdLines.Add($paramName + ".IsCached(true);");
                    }
                }
            });
        };
        getCode(this.viewerInstance, variableName);

        events.setEvent(asyncManager.getId(), EventType.ON_COMPLETE, () : void => {
            output += "var cm=[" + classes + "];";
            output += variablesSubscribe;
            let index : number;
            let cmdLineIndex : number = 0;
            const cmdLineBlocSize : number = 500;
            let progressEventsCount : number = 0;
            const progressBlockSize : number = 100;

            cmdLines.foreach(($line : string) : void => {
                if (cmdLineIndex % cmdLineBlocSize === 0) {
                    if (cmdLineIndex > 0) {
                        output += "});";
                    }
                    output += "h.Add(function(){";
                }
                if (cmdLineIndex % progressBlockSize === 0) {
                    progressEventsCount++;
                    output += "e.FireEvent(pc,\"" + EventType.ON_CHANGE + "\",false);";
                }
                output += $line;
                cmdLineIndex++;
            });
            let processIndex : number = 0;
            for (index = postprocessors.length - 1; index >= 0; index--) {
                for (processIndex = 0; processIndex < postprocessors[index].length; processIndex++) {
                    if (cmdLineIndex % cmdLineBlocSize === 0) {
                        if (cmdLineIndex > 0) {
                            output += "});";
                        }
                        output += "h.Add(function(){";
                    }
                    if (cmdLineIndex % progressBlockSize === 0) {
                        progressEventsCount++;
                        output += "e.FireEvent(pc,\"" + EventType.ON_CHANGE + "\",false);";
                    }
                    output += postprocessors[index][processIndex];
                    cmdLineIndex++;
                }
            }
            if (!cmdLines.IsEmpty() || postprocessors.length > 0) {
                output += "});";
            }

            classes = "";
            variablesSubscribe = "";
            postprocessors = [];
            cmdLines.Clear();
            cmdLineIndex = 0;
            asyncManager = new TimeoutManager();
            events.setEvent(asyncManager.getId(), EventType.ON_COMPLETE, () : void => {
                if (!ObjectValidator.IsEmptyOrNull(classes)) {
                    output += "cm.push(" + classes + ");";
                }
                output += variablesSubscribe;
                cmdLines.foreach(($line : string) : void => {
                    if (cmdLineIndex % cmdLineBlocSize === 0) {
                        if (cmdLineIndex > 0) {
                            output += "});";
                        }
                        output += "h.Add(function(){";
                    }
                    if (cmdLineIndex % progressBlockSize === 0) {
                        progressEventsCount++;
                        output += "e.FireEvent(pc,\"" + EventType.ON_CHANGE + "\",false);";
                    }
                    output += $line;
                    cmdLineIndex++;
                });
                for (index = postprocessors.length - 1; index >= 0; index--) {
                    for (processIndex = 0; processIndex < postprocessors[index].length; processIndex++) {
                        if (cmdLineIndex % cmdLineBlocSize === 0) {
                            if (cmdLineIndex > 0) {
                                output += "});";
                            }
                            output += "h.Add(function(){";
                        }
                        if (cmdLineIndex % progressBlockSize === 0) {
                            progressEventsCount++;
                            output += "e.FireEvent(pc,\"" + EventType.ON_CHANGE + "\",false);";
                        }
                        output += postprocessors[index][processIndex];
                        cmdLineIndex++;
                    }
                }

                if (!cmdLines.IsEmpty() || postprocessors.length > 0) {
                    output += "});";
                }

                output +=
                    "h.Add(function(){" +
                    ViewerCacheManager.ClassName() + ".Loader." + variableName + "=" + variableName + ";" +
                    ViewerCacheManager.ClassName() + ".Loader.elements=[" + parsedElements + "];" +
                    "ai=null;" +
                    "si=null;" +
                    "ob=null;" +
                    "cm=null;" +
                    "e.FireEvent(pc,\"" + EventType.ON_COMPLETE + "\",false);});" +
                    "h.Execute();" +
                    "}," +
                    "\"elements\":[]," +
                    "\"progressEventsCount\":" + progressEventsCount +
                    "};" +
                    ViewerCacheManager.ClassName() + ".Loader.Load();" +
                    ViewerCacheManager.ClassName() + ".getViewerInstance=" +
                    "function(){return " + ViewerCacheManager.ClassName() + ".Loader." + variableName + ";};" +
                    ViewerCacheManager.ClassName() + ".getCachedElements=" +
                    "function(){return " + ViewerCacheManager.ClassName() + ".Loader.elements;};" +
                    ViewerCacheManager.ClassName() + ".getProgressEventsCount=" +
                    "function(){return " + ViewerCacheManager.ClassName() + ".Loader.progressEventsCount;};" +
                    "}catch(ex){" + ExceptionsManager.ClassName() + ".HandleException(ex);}";

                parsedObjects = null;
                parsedElements = null;
                $handler(output);
            });
            asyncManager.Execute();
        });
        asyncManager.Execute();
        /* dev:end */
    }
}
