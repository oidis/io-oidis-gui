/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export class CursorType extends BaseEnum {
    public static readonly NS_RESIZE : string = "ns-resize";
    public static readonly EW_RESIZE : string = "ew-resize";
    public static readonly NESW_RESIZE : string = "nesw-resize";
    public static readonly NWSE_RESIZE : string = "nwse-resize";
}
