/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";

export abstract class BasePropagableEnum extends BaseEnum {
    /**
     * @param {BasePropagableEnum} $parentValue Specify parent value.
     * @param {BasePropagableEnum} $childValue Specify child value.
     * @returns {BasePropagableEnum} Returns propagated value, if propagation has been allowed, otherwise default value is returned.
     */
    public static PriorityValue<T extends BasePropagableEnum>($parentValue : T, $childValue? : T) : T {
        const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
        if (!ObjectValidator.IsEmptyOrNull($childValue)) {
            return $childValue;
        } else if (!ObjectValidator.IsEmptyOrNull($parentValue) && thisClass.IsPropagated($parentValue)) {
            return $parentValue;
        }
        return thisClass.getDefaultValue();
    }

    /**
     * @param {BasePropagableEnum} $value Specify value, which should be normalized.
     * @returns {string} Returns normalized BasePropagableEnum value.
     */
    public static Normalize($value : BasePropagableEnum) : string {
        const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
        if (thisClass.Contains($value)) {
            return StringUtils.Remove($value.toString(), "Propagated");
        }
        return thisClass.getDefaultValue();
    }

    /**
     * @param {BasePropagableEnum} $value Specify value for, which should be generated CSS style class.
     * @param {string} $initialClass Specify initial class which should be modified.
     * @returns {string} Returns CSS style class for specified BasePropagableEnum value.
     */
    public static StyleClassName($value : BasePropagableEnum, $initialClass? : string) : string {
        const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
        const classPrefix : string = ObjectValidator.IsEmptyOrNull($initialClass) ? "" : $initialClass
            .replace(new RegExp(`\\s*${thisClass.getPrefix()}([a-zA-Z])*`, "g"), "");
        const normalizedPrefix : string = ObjectValidator.IsEmptyOrNull(classPrefix) ? "" : classPrefix + " ";
        if (thisClass.Contains($value)) {
            return normalizedPrefix + thisClass.getPrefix() + thisClass.Normalize($value);
        }
        return normalizedPrefix + thisClass.getDefaultValue();
    }

    /**
     * @param {BasePropagableEnum} $value Specify value, which should be validated.
     * @returns {boolean} Returns true if BasePropagableEnum value is propagated, otherwise false.
     */
    public static IsPropagated($value : BasePropagableEnum) : boolean {
        const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
        if (thisClass.Contains($value)) {
            return StringUtils.Contains($value.toString(), "Propagated");
        }
        return false;
    }

    /**
     * @returns {string} Returns prefix for BasePropagableEnum enum value.
     */
    protected static getPrefix() : string {
        return "";
    }

    /**
     * @returns {string} Returns default BasePropagableEnum enum value.
     */
    protected static getDefaultValue() : string {
        return "";
    }
}
