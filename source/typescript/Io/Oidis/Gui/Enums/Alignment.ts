/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePropagableEnum } from "./BasePropagableEnum.js";

export class Alignment extends BasePropagableEnum {
    public static readonly CENTER : string = "Center";
    public static readonly TOP : string = "Top";
    public static readonly TOP_RIGHT : string = "TopRight";
    public static readonly RIGHT : string = "Right";
    public static readonly BOTTOM_RIGHT : string = "BottomRight";
    public static readonly BOTTOM : string = "Bottom";
    public static readonly BOTTOM_LEFT : string = "BottomLeft";
    public static readonly LEFT : string = "Left";
    public static readonly TOP_LEFT : string = "TopLeft";
    public static readonly CENTER_PROPAGATED : string = "CenterPropagated";
    public static readonly TOP_PROPAGATED : string = "TopPropagated";
    public static readonly TOP_RIGHT_PROPAGATED : string = "TopRightPropagated";
    public static readonly RIGHT_PROPAGATED : string = "RightPropagated";
    public static readonly BOTTOM_RIGHT_PROPAGATED : string = "BottomRightPropagated";
    public static readonly BOTTOM_PROPAGATED : string = "BottomPropagated";
    public static readonly BOTTOM_LEFT_PROPAGATED : string = "BottomLeftPropagated";
    public static readonly LEFT_PROPAGATED : string = "LeftPropagated";
    public static readonly TOP_LEFT_PROPAGATED : string = "TopLeftPropagated";

    /**
     * @returns {string} Returns prefix for Alignment enum value.
     */
    public static getPrefix() : string {
        return "Align";
    }

    /**
     * @returns {string} Returns default Alignment enum value.
     */
    public static getDefaultValue() : string {
        return Alignment.TOP_LEFT;
    }
}
