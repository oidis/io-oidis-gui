/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export class GeneralCssNames extends BaseEnum {
    public static readonly GUI_SELECTOR : string = "GuiSelector";
    public static readonly GUI_FIXED_HOLDER : string = "GuiFixedHolder";

    public static readonly BACKGROUND : string = "Background";
    public static readonly FOREGROUND : string = "Foreground";

    public static readonly TOP : string = "Top";
    public static readonly MIDDLE : string = "Middle";
    public static readonly BOTTOM : string = "Bottom";
    public static readonly LEFT : string = "Left";
    public static readonly CENTER : string = "Center";
    public static readonly RIGHT : string = "Right";

    public static readonly ON : string = "On";
    public static readonly OFF : string = "Off";
    public static readonly ACTIVE : string = "Active";
    public static readonly ENABLE : string = "Enable";
    public static readonly DISABLE : string = "Disable";
    public static readonly ERROR : string = "Error";
    public static readonly HIDDEN : string = "Hidden";
    public static readonly READONLY : string = "ReadOnly";

    public static readonly TEXT : string = "Text";
    public static readonly ICON : string = "Icon";
    public static readonly SPLITTER : string = "Splitter";

    public static readonly ROW : string = "GuiRow";
    public static readonly COLUMN : string = "GuiCol";
    public static readonly PADDING_BEFORE : string = "Before";
    public static readonly PADDING_AFTER : string = "After";

    public static readonly DNONE : string = "d-none";
}

export class GeneralCSS extends GeneralCssNames {
    // alias just for simplification
}
