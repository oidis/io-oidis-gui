/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BasePropagableEnum } from "./BasePropagableEnum.js";

export class VisibilityStrategy extends BasePropagableEnum {
    public static readonly NONE : string = "None";
    public static readonly COLLAPSE_EMPTY : string = "CollapseEmpty";
    public static readonly COLLAPSE_EMPTY_PROPAGATED : string = "CollapseEmptyPropagated";

    /**
     * @returns {string} Returns default VisibilityStrategy enum value.
     */
    public static getDefaultValue() : string {
        return VisibilityStrategy.NONE;
    }
}
