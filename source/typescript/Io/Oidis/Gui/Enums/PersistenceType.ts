/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { PersistenceType as Parent } from "@io-oidis-commons/Io/Oidis/Commons/Enums/PersistenceType.js";

export class PersistenceType extends Parent {
    public static readonly GUI_COMMONS : string = "guicommons";
    public static readonly VIEWER : string = "viewer";
    public static readonly AUTOFILL : string = "autofill";
    public static readonly FORM_VALUES : string = "formvalues";
    public static readonly ERROR_FLAGS : string = "errorflags";
}
