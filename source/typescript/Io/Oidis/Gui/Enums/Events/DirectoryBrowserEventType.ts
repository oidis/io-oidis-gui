/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "./EventType.js";

export class DirectoryBrowserEventType extends EventType {
    public static ON_PATH_REQUEST : string = "OnPathRequest";
    public static ON_DIRECTORY_REQUEST : string = "OnDirectoryRequest";
    public static ON_CREATE_DIRECTORY_REQUEST : string = "OnCreateDirectoryRequest";
    public static ON_RENAME_REQUEST : string = "OnRenameRequest";
    public static ON_DELETE_REQUEST : string = "OnDeleteRequest";
}
