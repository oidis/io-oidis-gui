/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "./EventType.js";

export class AudioEventType extends EventType {
    public static readonly ON_STOP : string = "OnStop";
    public static readonly ON_PLAY : string = "OnPlay";
    public static readonly ON_PAUSE : string = "OnPause";
    public static readonly ON_MUTE : string = "OnMute";
    public static readonly ON_UNMUTE : string = "OnUnmute";
    public static readonly ON_OPEN : string = "OnOpen";
}
