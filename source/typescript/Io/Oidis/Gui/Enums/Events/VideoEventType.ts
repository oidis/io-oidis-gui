/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { AudioEventType } from "./AudioEventType.js";

export class VideoEventType extends AudioEventType {
    public static readonly ON_FULLSCREEN_START : string = "OnFullScreenStart";
    public static readonly ON_FULLSCREEN_END : string = "OnFullScreenEnd";
}
