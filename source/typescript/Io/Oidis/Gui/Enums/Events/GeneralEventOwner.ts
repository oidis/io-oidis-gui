/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GeneralEventOwner as Parent } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/GeneralEventOwner.js";

export class GeneralEventOwner extends Parent {
    public static readonly MOUSE_MOVE : string = "mousemove";
}
