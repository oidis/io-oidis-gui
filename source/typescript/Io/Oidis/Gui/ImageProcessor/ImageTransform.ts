/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { OutOfRangeException } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/Type/OutOfRangeException.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { ObjectDecoder } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectDecoder.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ImageCorners } from "../Structures/ImageCorners.js";
import { ImageCropDimensions } from "../Structures/ImageCropDimensions.js";
import { PixelChanel } from "../Structures/PixelChanel.js";
import { Size } from "../Structures/Size.js";

/**
 * ImageTransform class provides methods for image transformation based on HTML5 Canvas.
 */
export class ImageTransform extends BaseObject {

    /**
     * @returns {boolean} Returns true, if image transform based on HTML5 Canvas is supported by runtime environment, otherwise false.
     */
    public static IsSupported() : boolean {
        const canvas : HTMLCanvasElement = this.getCanvas();
        return ObjectValidator.IsSet(canvas) && ObjectValidator.IsSet(canvas.getContext) &&
            !ObjectValidator.IsEmptyOrNull(this.getContext(canvas));
    }

    /**
     * @param {HTMLImageElement} $input Specify input data, which should be converted.
     * @returns {HTMLCanvasElement} Returns image data converted to the HTML5 Canvas.
     */
    public static ToCanvas($input : HTMLImageElement) : HTMLCanvasElement {
        const output : HTMLCanvasElement = this.getCanvas();
        output.width = $input.naturalWidth;
        output.height = $input.naturalHeight;
        this.getContext(output).drawImage($input, 0, 0, $input.naturalWidth, $input.naturalHeight);
        return output;
    }

    /**
     * @param {HTMLCanvasElement} $input Specify input data, which should be transformed.
     * @param {number} $width Specify image envelop width.
     * @param {number} $height Specify image envelop height.
     * @returns {HTMLCanvasElement} Returns transformed image.
     */
    public static ToCenter($input : HTMLCanvasElement, $width : number, $height : number) : HTMLCanvasElement {
        const output : HTMLCanvasElement = this.getCanvas();
        output.width = $width;
        output.height = $height;
        this.getContext(output).drawImage($input, (output.width - $input.width) / 2, (output.height - $input.height) / 2);
        return output;
    }

    /**
     * @param {HTMLCanvasElement} $input Specify input data, which should be transformed.
     * @param {number} $width Specify required image width.
     * @param {number} $height Specify required image height.
     * @param {boolean} [$fillEnabled=true] Specify, if image size should be scaled exactly to required width and height or,
     * if original scale should be preserved.
     * @param {number} [$rotation=0] Specify rotation value in degrees for, which should be image scaled before rotation with ability
     * to fit required output envelop size.
     * @returns {HTMLCanvasElement} Returns transformed image.
     */
    public static Resize($input : HTMLCanvasElement, $width : number, $height : number,
                         $fillEnabled : boolean = true, $rotation : number = 0) : HTMLCanvasElement {
        while ($rotation / 90 > 4) {
            $rotation -= 360;
        }
        if ($width > 0 && $height > 0 && (
            $rotation !== 0 || $rotation === 0 && ($input.width !== $width || $input.height !== $height))) {
            let newWidth : number;
            let newHeight : number;
            let scaleWidth : number;
            let scaleHeight : number;
            let scale : number;

            if ($rotation === 0 || $rotation === 180 || $rotation === 360) {
                if ($fillEnabled) {
                    newWidth = $width;
                    newHeight = $height;
                } else {
                    if ($input.height > $input.width) {
                        newHeight = $height;
                        newWidth = Math.ceil($height / $input.height * $input.width);
                    } else {
                        newWidth = $width;
                        newHeight = Math.ceil($width / $input.width * $input.height);
                    }
                }
            } else if ($rotation === 90 || $rotation === 270) {
                if ($fillEnabled) {
                    newWidth = $height;
                    newHeight = $width;
                } else {
                    if ($input.width > $input.height) {
                        newWidth = $height;
                        newHeight = Math.ceil($height / $input.width * $input.height);
                    } else {
                        newHeight = $width;
                        newWidth = Math.ceil($width / $input.height * $input.width);
                    }
                }
            } else if ($rotation !== 0 && $rotation / 90 !== 2 && $rotation / 90 !== 4) {
                if ($fillEnabled) {
                    scaleWidth = $width / $input.width;
                    scaleHeight = $height / $input.height;
                } else {
                    if ($height > $width) {
                        scaleWidth = $width / $input.width;
                        scaleHeight = $height / $input.height;
                    } else {
                        scaleWidth = $height / $input.height;
                        scaleHeight = $width / $input.width;
                    }
                    scale = scaleWidth < scaleHeight ? scaleWidth : scaleHeight;
                    scaleWidth = scaleHeight = scale;
                }
                let zeroRotation : number = $rotation;
                if (zeroRotation > 90 && zeroRotation < 180) {
                    zeroRotation = 180 - zeroRotation;
                } else if (zeroRotation > 180 && zeroRotation < 270) {
                    zeroRotation -= 180;
                } else if (zeroRotation > 270 && zeroRotation < 360) {
                    zeroRotation = 360 - zeroRotation;
                }
                const alpha : number = Convert.DegToRad(90 - zeroRotation);
                const beta : number = Convert.DegToRad(zeroRotation);
                const cosBeta : number = Math.cos(beta);
                const zeroSize : Size = new Size();
                const angleSize : Size = new Size();
                zeroSize.Width(Math.ceil(scaleWidth * $input.width));
                zeroSize.Height(Math.ceil(scaleHeight * $input.height));
                angleSize.Width(cosBeta * zeroSize.Width() + Math.cos(alpha) * zeroSize.Height());
                angleSize.Height(Math.sin(beta) * zeroSize.Width() + cosBeta * zeroSize.Height());
                const overflowWidth : number = Math.abs(angleSize.Width() - $width);
                const overflowHeight : number = Math.abs(angleSize.Height() - $height);
                const overflow : number = (overflowWidth > overflowHeight ? overflowWidth : overflowHeight) / 2;
                let iteration : number = 0;
                let maxIteration : number = overflow * 3;
                if (maxIteration < 500) {
                    maxIteration = 500;
                }
                let approximation : number = overflow / 2;
                let step : number = overflow - approximation;
                const lastSize : Size = new Size();
                let lastStep : number;
                let isApproximated : boolean = false;
                do {
                    lastStep = Convert.ToFixed(step, 5);
                    lastSize.Width(angleSize.Width());
                    lastSize.Height(angleSize.Height());
                    iteration++;
                    if ($input.height > $input.width) {
                        newWidth = Math.abs(Math.ceil(zeroSize.Width() - step / cosBeta));
                        newHeight = Math.abs(Math.ceil(zeroSize.Height() * newWidth / zeroSize.Width()));
                    } else {
                        newHeight = Math.abs(Math.ceil((cosBeta * zeroSize.Height() - step) / cosBeta));
                        newWidth = Math.abs(Math.ceil(zeroSize.Width() * newHeight / zeroSize.Height()));
                    }
                    angleSize.Width(cosBeta * newWidth + Math.cos(alpha) * newHeight);
                    angleSize.Height(Math.sin(beta) * newWidth + cosBeta * newHeight);

                    if (angleSize.Width() > $width || angleSize.Height() > $height) {
                        step += approximation;
                        approximation /= 2;
                    } else {
                        step -= approximation;
                    }
                    if (Convert.ToFixed(approximation, 5) === 0) {
                        approximation = 1;
                    }

                    isApproximated = angleSize.Width() <= $width && angleSize.Height() <= $height && (
                        (lastSize.Width() === angleSize.Width() && lastSize.Height() === angleSize.Height()) ||
                        lastStep === Convert.ToFixed(step, 5));
                } while (!isApproximated && iteration < maxIteration);
            }

            const output : HTMLCanvasElement = this.getCanvas();
            output.width = newWidth;
            output.height = newHeight;
            this.getContext(output).drawImage($input, 0, 0, newWidth, newHeight);
            return output;
        }
        return $input;
    }

    /**
     * @param {HTMLCanvasElement} $input Specify input data, which should be transformed.
     * @param {number} $value Specify image quality in percentage format.
     * @returns {HTMLCanvasElement} Returns transformed image.
     */
    public static Quality($input : HTMLCanvasElement, $value : number) : HTMLCanvasElement {
        if ($value > 1 && $value < 100) {
            $value /= 100;
        }
        if ($value > 1 || $value < 0) {
            ExceptionsManager.Throw(ImageTransform.ClassName(), new OutOfRangeException("Quality value must be in range <0;100>"));
        }
        const sqScale : number = $value * $value;
        const outputWidth : number = Math.floor($input.width * $value);
        const outputHeight : number = Math.floor($input.height * $value);

        const inputData : Uint8ClampedArray = this.getContext($input).getImageData(0, 0, $input.width, $input.height).data;
        const outputData : number[] = [];
        let index : number;
        for (index = 0; index < 3 * outputWidth * outputHeight; index++) {
            outputData[index] = 0;
        }

        let inputY : number;
        let inputX : number;
        let inputIndex : number = 0;
        const chanel : PixelChanel = new PixelChanel();
        for (inputY = 0; inputY < $input.height; inputY++) {
            const sourceY : number = inputY * $value;
            const outputY : number = 0 | sourceY;
            const inputYindex : number = 3 * outputY * outputWidth;
            const isCrossY : boolean = (outputY !== (0 | sourceY + $value));
            let widthY : number = 0;
            let newWidthY : number = 0;
            if (isCrossY) {
                widthY = (outputY + 1 - sourceY);
                newWidthY = (sourceY + $value - outputY - 1);
            }
            for (inputX = 0; inputX < $input.width; inputX++, inputIndex += 4) {
                const sourceX : number = inputX * $value;
                const outputX : number = 0 | sourceX;
                const outputIndex : number = inputYindex + outputX * 3;
                const isCrossX : boolean = (outputX !== (0 | sourceX + $value));
                let widthX : number = 0;
                let newWidthX : number = 0;
                if (isCrossX) {
                    widthX = (outputX + 1 - sourceX);
                    newWidthX = (sourceX + $value - outputX - 1);
                }
                chanel.RED = inputData[inputIndex];
                chanel.GREEN = inputData[inputIndex + 1];
                chanel.BLUE = inputData[inputIndex + 2];

                let width : number = 0;
                let newWidth : number = 0;
                if (!isCrossX && !isCrossY) {
                    outputData[outputIndex] += chanel.RED * sqScale;
                    outputData[outputIndex + 1] += chanel.GREEN * sqScale;
                    outputData[outputIndex + 2] += chanel.BLUE * sqScale;
                } else if (isCrossX && !isCrossY) {
                    width = widthX * $value;
                    outputData[outputIndex] += chanel.RED * width;
                    outputData[outputIndex + 1] += chanel.GREEN * width;
                    outputData[outputIndex + 2] += chanel.BLUE * width;
                    newWidth = newWidthX * $value;
                    outputData[outputIndex + 3] += chanel.RED * newWidth;
                    outputData[outputIndex + 4] += chanel.GREEN * newWidth;
                    outputData[outputIndex + 5] += chanel.BLUE * newWidth;
                } else if (isCrossY && !isCrossX) {
                    width = widthY * $value;
                    outputData[outputIndex] += chanel.RED * width;
                    outputData[outputIndex + 1] += chanel.GREEN * width;
                    outputData[outputIndex + 2] += chanel.BLUE * width;
                    newWidth = newWidthY * $value;
                    outputData[outputIndex + 3 * outputWidth] += chanel.RED * newWidth;
                    outputData[outputIndex + 3 * outputWidth + 1] += chanel.GREEN * newWidth;
                    outputData[outputIndex + 3 * outputWidth + 2] += chanel.BLUE * newWidth;
                } else {
                    width = widthX * widthY;
                    outputData[outputIndex] += chanel.RED * width;
                    outputData[outputIndex + 1] += chanel.GREEN * width;
                    outputData[outputIndex + 2] += chanel.BLUE * width;
                    newWidth = newWidthX * widthY;
                    outputData[outputIndex + 3] += chanel.RED * newWidth;
                    outputData[outputIndex + 4] += chanel.GREEN * newWidth;
                    outputData[outputIndex + 5] += chanel.BLUE * newWidth;
                    newWidth = widthX * newWidthY;
                    outputData[outputIndex + 3 * outputWidth] += chanel.RED * newWidth;
                    outputData[outputIndex + 3 * outputWidth + 1] += chanel.GREEN * newWidth;
                    outputData[outputIndex + 3 * outputWidth + 2] += chanel.BLUE * newWidth;
                    newWidth = newWidthX * newWidthY;
                    outputData[outputIndex + 3 * outputWidth + 3] += chanel.RED * newWidth;
                    outputData[outputIndex + 3 * outputWidth + 4] += chanel.GREEN * newWidth;
                    outputData[outputIndex + 3 * outputWidth + 5] += chanel.BLUE * newWidth;
                }
            }
        }

        const output : HTMLCanvasElement = this.getCanvas();
        output.width = outputWidth;
        output.height = outputHeight;
        const context : CanvasRenderingContext2D = this.getContext(output);
        const image : ImageData = context.getImageData(0, 0, outputWidth, outputHeight);
        let pixelIndex : number = 0;
        let outputIndex : number;
        for (inputIndex = 0, outputIndex = 0;
             pixelIndex < outputWidth * outputHeight;
             inputIndex += 3, outputIndex += 4, pixelIndex++) {
            image.data[outputIndex] = Math.ceil(outputData[inputIndex]);
            image.data[outputIndex + 1] = Math.ceil(outputData[inputIndex + 1]);
            image.data[outputIndex + 2] = Math.ceil(outputData[inputIndex + 2]);
            image.data[outputIndex + 3] = 255;
        }
        context.putImageData(image, 0, 0);

        return this.restoreSize(output, $input);
    }

    /**
     * @param {HTMLCanvasElement} $input Specify input data, which should be transformed.
     * @param {number} $value Specify zoom scale in percentage format.
     * @returns {HTMLCanvasElement} Returns transformed image.
     */
    public static Zoom($input : HTMLCanvasElement, $value : number) : HTMLCanvasElement {
        if ($value > 100) {
            const output : HTMLCanvasElement = this.getCanvas();
            if ($value === 200) {
                $value = 199;
            }
            const scale : number = $value / 100;
            const cropTopX : number = Math.floor((($input.width * scale) - $input.width) / 2);
            const cropTopY : number = Math.floor((($input.height * scale) - $input.height) / 2);
            const cropWith : number = $input.width - cropTopX * 2;
            const cropHeight : number = $input.height - cropTopY * 2;
            output.width = cropWith;
            output.height = cropHeight;
            this.getContext(output).drawImage($input, -cropTopX, -cropTopY);
            return this.restoreSize(output, $input);
        } else {
            return $input;
        }
    }

    /**
     * @param {HTMLCanvasElement} $input Specify input data, which should be transformed.
     * @param {number} $value Specify image rotation in degrees format.
     * @param {number} [$width] Specify required width after rotation.
     * @param {number} [$height] Specify required height after rotation.
     * @returns {HTMLCanvasElement} Returns transformed image.
     */
    public static Rotate($input : HTMLCanvasElement, $value : number, $width ? : number, $height? : number) : HTMLCanvasElement {
        if ($value !== 0) {
            const output : HTMLCanvasElement = this.getCanvas();
            output.width = ObjectValidator.IsSet($width) ? $width : $input.width;
            output.height = ObjectValidator.IsSet($height) ? $height : $input.height;
            const context : CanvasRenderingContext2D = this.getContext(output);
            context.translate(output.width / 2, output.height / 2);
            context.rotate(Convert.DegToRad($value));
            context.drawImage($input, -$input.width / 2, -$input.height / 2, $input.width, $input.height);
            return output;
        } else {
            return $input;
        }
    }

    /**
     * @param {HTMLCanvasElement} $input Specify input data, which should be transformed.
     * @param {ImageCropDimensions} $value Specify image crop dimensions, which should be applied.
     * @returns {HTMLCanvasElement} Returns transformed image.
     */
    public static Crop($input : HTMLCanvasElement, $value : ImageCropDimensions) : HTMLCanvasElement {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            let cropTopX : number = $value.TopX();
            let cropTopY : number = $value.TopY();
            let cropBottomX : number = $value.BottomX();
            let cropBottomY : number = $value.BottomY();
            if (cropTopX < 0) {
                cropTopX = 0;
            }
            if (cropTopY < 0) {
                cropTopY = 0;
            }
            if (cropBottomX < 0) {
                cropBottomX = 0;
            }
            if (cropBottomY < 0) {
                cropBottomY = 0;
            }
            if (cropTopX > $input.width) {
                cropTopX = $input.width;
            }
            if (cropTopY > $input.height) {
                cropTopY = $input.height;
            }
            if (cropBottomX > $input.width) {
                cropBottomX = $input.width;
            }
            if (cropBottomY > $input.height) {
                cropBottomY = $input.height;
            }

            const cropWidth : number = cropBottomX - cropTopX;
            const cropHeight : number = cropBottomY - cropTopY;
            if (cropWidth > 0 && cropHeight > 0) {
                const output : HTMLCanvasElement = this.getCanvas();
                output.width = cropWidth;
                output.height = cropHeight;
                this.getContext(output).putImageData(
                    this.getContext($input).getImageData(cropTopX, cropTopY, cropWidth, cropHeight),
                    0, 0);
                return this.restoreSize(output, $input);
            }
        }
        return $input;
    }

    /**
     * @param {HTMLCanvasElement} $input Specify input data, which should be transformed.
     * @param {ImageCorners} $value Specify image quality in percentage format.
     * @param {number} [$rotation=0] Specify rotation in degrees format for ability to freeze corner position in case of,
     * that input data will be rotated after modification of corners.
     * @returns {HTMLCanvasElement} Returns transformed image.
     */
    public static ModifyCorners($input : HTMLCanvasElement, $value : ImageCorners, $rotation : number = 0) : HTMLCanvasElement {
        const topLeft : number = $value.TopLeft();
        const topRight : number = $value.TopRight();
        const bottomLeft : number = $value.BottomLeft();
        const bottomRight : number = $value.BottomRight();
        if (topLeft > 0 || topRight > 0 || bottomLeft > 0 || bottomRight > 0) {
            const output : HTMLCanvasElement = this.getCanvas();
            const context : CanvasRenderingContext2D = this.getContext(output);
            const width : number = $input.width;
            const height : number = $input.height;
            output.width = width;
            output.height = height;

            let corners : number[] = [];
            if ($rotation >= 0 && $rotation < 45 || $rotation > 305 && $rotation <= 360) {
                corners = [topLeft, topRight, bottomLeft, bottomRight];
            } else if ($rotation >= 45 && $rotation < 125) {
                corners = [topRight, bottomRight, topLeft, bottomLeft];
            } else if ($rotation >= 125 && $rotation < 215) {
                corners = [bottomRight, bottomLeft, topRight, topLeft];
            } else if ($rotation >= 215 && $rotation <= 305) {
                corners = [bottomLeft, topLeft, bottomRight, topRight];
            }

            context.beginPath();
            context.moveTo(corners[1], 0);
            context.lineTo(width - corners[1], 0);
            context.arcTo(width, 0, width, corners[1], corners[1]);
            context.lineTo(width, height - corners[3]);
            context.arcTo(width, height, width - corners[3], height, corners[3]);
            context.lineTo(corners[2], height);
            context.arcTo(0, height, 0, height - corners[2], corners[2]);
            context.lineTo(0, corners[0]);
            context.arcTo(0, 0, corners[0], 0, corners[0]);
            context.closePath();
            context.clip();
            context.drawImage($input, 0, 0, $input.width, $input.height);

            return output;
        }
        return $input;
    }

    /**
     * @param {HTMLCanvasElement} $input Specify input data, which should be transformed.
     * @param {HTMLCanvasElement} $source Specify input data, which should be used as for watermark source.
     * @param {boolean} [$fillEnabled=true] Specify, if watermark size should be scaled exactly to input width and height or,
     * if original scale should be preserved.
     * @param {number} [$rotation=0] Specify rotation in degrees format for ability to freeze watermark orientation in case of,
     * that input data will be rotated after watermark application.
     * @returns {HTMLCanvasElement} Returns transformed image.
     */
    public static AddWatermark($input : HTMLCanvasElement, $source : HTMLCanvasElement, $fillEnabled : boolean = true,
                               $rotation : number = 0) : HTMLCanvasElement {
        if ($rotation >= 45 && $rotation < 125) {
            $rotation = 270;
        } else if ($rotation >= 125 && $rotation < 215) {
            $rotation = 180;
        } else if ($rotation >= 215 && $rotation <= 305) {
            $rotation = 90;
        } else {
            $rotation = 0;
        }
        const output : HTMLCanvasElement = this.getCanvas();
        const context : CanvasRenderingContext2D = this.getContext(output);
        output.width = $input.width;
        output.height = $input.height;
        context.drawImage($input, 0, 0);
        context.drawImage(
            this.Rotate(
                this.Resize($source, $source.width, $source.height, $fillEnabled, $rotation),
                $rotation),
            0, 0, output.width, output.height);
        return output;
    }

    /**
     * @param {HTMLCanvasElement} $input Specify input data, which should be transformed.
     * @param {boolean} [$encoded=false] Specify, if data should be in Base64 format.
     * @returns {string} Returns raw image data.
     */
    public static getStream($input : HTMLCanvasElement, $encoded : boolean = false) : string {
        let stream : string = StringUtils.Split($input.toDataURL("image/png"), ",")[1];
        if ($encoded) {
            stream = StringUtils.Replace(stream, "+", "-");
            stream = StringUtils.Replace(stream, "/", "_");
            stream = StringUtils.Replace(stream, "=", ".");
            return stream;
        }
        return ObjectDecoder.Base64(stream);
    }

    private static getCanvas() : HTMLCanvasElement {
        return document.createElement("canvas");
    }

    private static getContext($input : HTMLCanvasElement) : CanvasRenderingContext2D {
        return $input.getContext("2d");
    }

    private static restoreSize($input : HTMLCanvasElement, $original : HTMLCanvasElement) : HTMLCanvasElement {
        const output : HTMLCanvasElement = this.getCanvas();
        output.width = $original.width;
        output.height = $original.height;
        this.getContext(output).drawImage($input, 0, 0, $original.width, $original.height);
        return output;
    }
}
