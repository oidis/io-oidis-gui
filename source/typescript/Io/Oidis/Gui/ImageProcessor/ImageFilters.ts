/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { GrayscaleType } from "../Enums/GrayscaleType.js";
import { PixelChanel } from "../Structures/PixelChanel.js";

/**
 * ImageFilters class provides methods for manipulation with image data.
 */
export class ImageFilters extends BaseObject {

    private static laplaceKernel : number[] = (<any>ImageFilters).getFloat32Array(
        [
            -1, -1, -1,
            -1, 8, -1,
            -1, -1, -1
        ]);
    private static sobelSignVector : number[] = (<any>ImageFilters).getFloat32Array([-1, 0, 1]);
    private static sobelScaleVector : number[] = (<any>ImageFilters).getFloat32Array([1, 2, 1]);
    private static sharpKernel : number[] = (<any>ImageFilters).getFloat32Array(
        [
            0, -1, 0,
            -1, 5, -1,
            0, -1, 0
        ]);

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @param {GrayscaleType} [$type=GrayscaleType.DEFAULT] Specify type of grayscale filter.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static Grayscale($input : ImageData, $type : GrayscaleType = GrayscaleType.DEFAULT) : ImageData {
        return this.applyFilter($input, ($chanel : PixelChanel) : number => {
            let value : number;
            switch ($type) {
            case GrayscaleType.AVERAGE:
                value = ($chanel.RED + $chanel.GREEN + $chanel.BLUE) / 3;
                break;
            case GrayscaleType.LUMINANCE:
                value = 0.2126 * $chanel.RED + 0.7152 * $chanel.GREEN + 0.0722 * $chanel.BLUE;
                break;
            default:
                value = 0.3 * $chanel.RED + 0.59 * $chanel.GREEN + 0.11 * $chanel.BLUE;
                break;
            }
            return value;
        });
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @param {number} $threshold Specify threshold value, which should be applied.
     * @param {number} [$high] Specify color for pixel value over or equal to the threshold in 8bit format with range <0;255>.
     * @param {number} [$low] Specify color for pixel value under the threshold in 8bit format with range <0;255>.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static Threshold($input : ImageData, $threshold : number, $high? : number, $low? : number) : ImageData {
        if (ObjectValidator.IsEmptyOrNull($high)) {
            $high = 255;
        }
        if (ObjectValidator.IsEmptyOrNull($low)) {
            $low = 0;
        }
        return this.applyFilter($input, ($chanel : PixelChanel) : number => {
            return (0.3 * $chanel.RED + 0.59 * $chanel.GREEN + 0.11 * $chanel.BLUE) >= $threshold ? $high : $low;
        });
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static Invert($input : ImageData) : ImageData {
        return this.applyFilter($input,
            ($chanel : PixelChanel) : number => {
                return 255 - $chanel.RED;
            },
            ($chanel : PixelChanel) : number => {
                return 255 - $chanel.GREEN;
            },
            ($chanel : PixelChanel) : number => {
                return 255 - $chanel.BLUE;
            });
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @param {number} $value Specify brightness value in 8bit format with range <-255;255>.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static Brightness($input : ImageData, $value : number) : ImageData {
        return this.applyFilter($input,
            ($chanel : PixelChanel) : number => {
                return $chanel.RED + $value;
            },
            ($chanel : PixelChanel) : number => {
                return $chanel.GREEN + $value;
            },
            ($chanel : PixelChanel) : number => {
                return $chanel.BLUE + $value;
            });
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @param {number} $value Specify contrast value in 8bit format with range <-255;255>.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static Contrast($input : ImageData, $value : number) : ImageData {
        const chanelFilter : any = ($chanelValue : number) : number => {
            if ($value > 0) {
                $chanelValue += (255 - $chanelValue) * $value / 255;
            } else if ($value < 0) {
                $chanelValue += $chanelValue * $value / 255;
            }
            return $chanelValue;
        };
        return this.applyFilter($input,
            ($chanel : PixelChanel) : number => {
                return chanelFilter($chanel.RED);
            },
            ($chanel : PixelChanel) : number => {
                return chanelFilter($chanel.GREEN);
            },
            ($chanel : PixelChanel) : number => {
                return chanelFilter($chanel.BLUE);
            });
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static HorizontalFlip($input : ImageData) : ImageData {
        return this.flip($input, true);
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static VerticalFlip($input : ImageData) : ImageData {
        return this.flip($input, false);
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @param {number[]} $kernel Specify convolve kernel, which should be applied.
     * @param {boolean} [$opacityEnabled=true] Specify, if alpha chanel should be preserved.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static Convolve($input : ImageData, $kernel : number[], $opacityEnabled : boolean = true) : ImageData {
        return this.generalConvolve($input, $kernel, $opacityEnabled, ConvolveType.DEFAULT);
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @param {number} $diameter Specify diameter of bluer effect in pixel format.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static GaussianBlur($input : ImageData, $diameter : number) : ImageData {
        $diameter = Math.abs($diameter);
        let index : number;
        if ($diameter <= 1) {
            const output : ImageData = this.createImageData($input.width, $input.height);
            for (index = 0; index < $input.data.length; index++) {
                output.data[index] = $input.data[index];
            }
            return output;
        }
        const size : number = Math.ceil($diameter) + (1 - (Math.ceil($diameter) % 2));
        const kernel : number[] = this.getFloat32Array(size);
        const rho : number = ($diameter / 2 + 0.5) / 3;
        const gaussianFactor : number = 1 / Math.sqrt(2 * Math.PI * rho * rho);
        const rhoFactor : number = -1 / (2 * rho * rho);
        let weight : number = 0;
        const middle : number = Math.floor(size / 2);
        for (index = 0; index < size; index++) {
            const x : number = index - middle;
            const gx : number = gaussianFactor * Math.exp(x * x * rhoFactor);
            kernel[index] = gx;
            weight += gx;
        }
        for (index = 0; index < kernel.length; index++) {
            kernel[index] /= weight;
        }
        return this.separableConvolve($input, kernel, kernel, false);
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static Laplace($input : ImageData) : ImageData {
        return this.Convolve($input, this.laplaceKernel, true);
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static Sobel($input : ImageData) : ImageData {
        $input = this.Grayscale($input);
        const vertical : ImageData = this.sobelVerticalGradient($input);
        const horizontal : ImageData = this.sobelHorizontalGradient($input);
        const output : ImageData = this.createImageData(vertical.width, vertical.height);
        let index : number;
        for (index = 0; index < output.data.length; index += 4) {
            const verticalValue : number = Math.abs(vertical.data[index]);
            output.data[index] = verticalValue;
            const horizontalValue : number = Math.abs(horizontal.data[index]);
            output.data[index + 1] = horizontalValue;
            output.data[index + 2] = (verticalValue + horizontalValue) / 4;
            output.data[index + 3] = 255;
        }
        return output;
    }

    /**
     * @param {ImageData} $input Specify input data, which should be manipulated.
     * @returns {ImageData} Returns image data with applied filter.
     */
    public static Sharp($input : ImageData) : ImageData {
        return this.Convolve($input, this.sharpKernel, true);
    }

    private static getFloat32Array($input : number | number[]) : number[] {
        if (ObjectValidator.IsArray($input)) {
            return (<number[]>$input).slice(0);
        }
        return <number[]>new Array(<number>$input);
    }

    private static createImageData($width : number, $height : number) : ImageData {
        return document.createElement("canvas").getContext("2d").createImageData($width, $height);
    }

    private static createImageDataFloat32($width : number, $height : number) : ImageData {
        return <ImageData>{data: <any>this.getFloat32Array($width * $height * 4), height: $height, width: $width};
    }

    private static applyFilter($input : ImageData,
                               $redFilter : ($chanel? : PixelChanel) => number,
                               $greenFilter? : ($chanel? : PixelChanel) => number,
                               $blueFilter? : ($chanel? : PixelChanel) => number,
                               $alphaFilter? : ($chanel? : PixelChanel) => number) : ImageData {
        const output : ImageData = this.createImageData($input.width, $input.height);
        let index : number;
        const chanel : PixelChanel = new PixelChanel();
        for (index = 0; index < $input.data.length; index += 4) {
            chanel.RED = $input.data[index];
            chanel.GREEN = $input.data[index + 1];
            chanel.BLUE = $input.data[index + 2];
            chanel.ALPHA = $input.data[index + 3];

            let value : number = $redFilter(chanel);
            if (value > 255) {
                value = 255;
            }
            if (value < 0) {
                value = 0;
            }
            output.data[index] = value;
            if (ObjectValidator.IsSet($greenFilter)) {
                value = $greenFilter(chanel);
                if (value > 255) {
                    value = 255;
                }
                if (value < 0) {
                    value = 0;
                }
            }
            output.data[index + 1] = value;
            if (ObjectValidator.IsSet($blueFilter)) {
                value = $blueFilter(chanel);
                if (value > 255) {
                    value = 255;
                }
                if (value < 0) {
                    value = 0;
                }
            }
            output.data[index + 2] = value;
            if (ObjectValidator.IsSet($alphaFilter)) {
                value = $alphaFilter(chanel);
                if (value > 255) {
                    value = 255;
                }
                if (value < 0) {
                    value = 0;
                }
                output.data[index + 3] = value;
            } else {
                output.data[index + 3] = $input.data[index + 3];
            }
        }
        return output;
    }

    private static flip($input : ImageData, $isHorizontal : boolean) : ImageData {
        const output : ImageData = this.createImageData($input.width, $input.height);
        let y : number;
        let x : number;
        for (y = 0; y < $input.height; y++) {
            for (x = 0; x < $input.width; x++) {
                const inputOffset : number = (y * $input.width + x) * 4;
                const outputOffset : number = $isHorizontal ?
                    (y * $input.width + ($input.width - x - 1)) * 4 :
                    (($input.height - y - 1) * $input.width + x) * 4;
                output.data[outputOffset] = $input.data[inputOffset];
                output.data[outputOffset + 1] = $input.data[inputOffset + 1];
                output.data[outputOffset + 2] = $input.data[inputOffset + 2];
                output.data[outputOffset + 3] = $input.data[inputOffset + 3];
            }
        }
        return output;
    }

    private static generalConvolve($input : ImageData, $kernel : number[], $opacityEnabled : boolean = true,
                                   $type : ConvolveType = ConvolveType.DEFAULT) : ImageData {
        let kernelSize : number = $kernel.length;
        if ($type === ConvolveType.DEFAULT || $type === ConvolveType.FLOAT32) {
            kernelSize = Math.round(Math.sqrt($kernel.length));
        }
        const padding : number = Math.floor(kernelSize / 2);
        const alphaFactor : number = $opacityEnabled ? 1 : 0;
        let kernelHeight : number = kernelSize;
        let kernelWidth : number = kernelSize;
        if ($type === ConvolveType.HORIZONTAL || $type === ConvolveType.HORIZONTAL_FLOAT32) {
            kernelHeight = 1;
        } else if ($type === ConvolveType.VERTICAL || $type === ConvolveType.VERTICAL_FLOAT32) {
            kernelWidth = 1;
        }

        let output : ImageData;
        if ($type === ConvolveType.FLOAT32 || $type === ConvolveType.VERTICAL_FLOAT32 || $type === ConvolveType.HORIZONTAL_FLOAT32) {
            output = this.createImageDataFloat32($input.width, $input.height);
        } else {
            output = this.createImageData($input.width, $input.height);
        }

        const chanel : PixelChanel = new PixelChanel();
        let inputY : number;
        let inputX : number;
        let kernelY : number;
        let kernelX : number;
        for (inputY = 0; inputY < $input.height; inputY++) {
            for (inputX = 0; inputX < $input.width; inputX++) {
                const outputOffset : number = (inputY * $input.width + inputX) * 4;
                chanel.RED = 0;
                chanel.GREEN = 0;
                chanel.BLUE = 0;
                chanel.ALPHA = 0;
                for (kernelY = 0; kernelY < kernelHeight; kernelY++) {
                    for (kernelX = 0; kernelX < kernelWidth; kernelX++) {
                        let outputY : number = Math.min($input.height - 1, Math.max(0, inputY + kernelY - padding));
                        let outputX : number = Math.min($input.width - 1, Math.max(0, inputX + kernelX - padding));
                        if ($type === ConvolveType.VERTICAL || $type === ConvolveType.VERTICAL_FLOAT32) {
                            outputX = inputX;
                        } else if ($type === ConvolveType.HORIZONTAL || $type === ConvolveType.HORIZONTAL_FLOAT32) {
                            outputY = inputY;
                        }
                        const inputOffset : number = (outputY * $input.width + outputX) * 4;
                        let value : number = $kernel[kernelY * kernelSize + kernelX];
                        if ($type === ConvolveType.VERTICAL || $type === ConvolveType.VERTICAL_FLOAT32) {
                            value = $kernel[kernelY];
                        } else if ($type === ConvolveType.HORIZONTAL || $type === ConvolveType.HORIZONTAL_FLOAT32) {
                            value = $kernel[kernelX];
                        }
                        chanel.RED += $input.data[inputOffset] * value;
                        chanel.GREEN += $input.data[inputOffset + 1] * value;
                        chanel.BLUE += $input.data[inputOffset + 2] * value;
                        chanel.ALPHA += $input.data[inputOffset + 3] * value;
                    }
                }
                output.data[outputOffset] = chanel.RED;
                output.data[outputOffset + 1] = chanel.GREEN;
                output.data[outputOffset + 2] = chanel.BLUE;
                output.data[outputOffset + 3] = chanel.ALPHA + alphaFactor * (255 - chanel.ALPHA);
            }
        }
        return output;
    }

    private static verticalConvolve($input : ImageData, $kernel : number[], $opacityEnabled : boolean = true) : ImageData {
        return this.generalConvolve($input, $kernel, $opacityEnabled, ConvolveType.VERTICAL);
    }

    private static horizontalConvolve($input : ImageData, $kernel : number[], $opacityEnabled : boolean = true) : ImageData {
        return this.generalConvolve($input, $kernel, $opacityEnabled, ConvolveType.HORIZONTAL);
    }

    private static separableConvolve($input : ImageData, $horizontalKernel : number[], $verticalKernel : number[],
                                     $opacityEnabled : boolean = true) : ImageData {
        return this.horizontalConvolve(
            this.verticalConvolveFloat32($input, $verticalKernel, $opacityEnabled), $horizontalKernel, $opacityEnabled);
    }

    private static verticalConvolveFloat32($input : ImageData, $kernel : number[], $opacityEnabled : boolean = true) : ImageData {
        return this.generalConvolve($input, $kernel, $opacityEnabled, ConvolveType.VERTICAL_FLOAT32);
    }

    private static horizontalConvolveFloat32($input : ImageData, $kernel : number[], $opacityEnabled : boolean = true) : ImageData {
        return this.generalConvolve($input, $kernel, $opacityEnabled, ConvolveType.HORIZONTAL_FLOAT32);
    }

    private static separableConvolveFloat32($input : ImageData, $horizontalKernel : number[], $verticalKernel : number[],
                                            $opacityEnabled : boolean = true) : ImageData {
        return this.horizontalConvolveFloat32(
            this.verticalConvolveFloat32($input, $verticalKernel, $opacityEnabled), $horizontalKernel, $opacityEnabled);
    }

    private static sobelVerticalGradient($input : ImageData) : ImageData {
        return this.separableConvolveFloat32($input, this.sobelSignVector, this.sobelScaleVector);
    }

    private static sobelHorizontalGradient($input : ImageData) : ImageData {
        return this.separableConvolveFloat32($input, this.sobelScaleVector, this.sobelSignVector);
    }
}

enum ConvolveType {
    DEFAULT,
    FLOAT32,
    HORIZONTAL,
    VERTICAL,
    HORIZONTAL_FLOAT32,
    VERTICAL_FLOAT32
}
