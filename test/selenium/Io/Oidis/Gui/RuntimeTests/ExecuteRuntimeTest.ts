/* ********************************************************************************************************* *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../../../../reference.d.ts" />
// eslint-disable-next-line @typescript-eslint/no-namespace
namespace Io.Oidis.Gui.RuntimeTests {
    "use strict";
    import SeleniumTestRunner = Io.Oidis.Commons.SeleniumTestRunner;

    export class ExecuteRuntimeTest extends SeleniumTestRunner {

        public testWindowManager() : void {
            this.driver.findElement(this.by.linkText("Window Manager")).click().then(() : void => {
                this.validate();
            });
        }

        public testStaticPageContentManager() : void {
            this.driver.findElement(this.by.linkText("Static Page Content Manager")).click().then(() : void => {
                this.validate();
            });
        }

        public testGuiObjectManager() : void {
            this.driver.findElement(this.by.linkText("GUI Object Manager")).click().then(() : void => {
                this.validate();
            });
        }

        public testElementEventsManager() : void {
            this.driver.findElement(this.by.linkText("Element Events Manager")).click().then(() : void => {
                this.validate();
            });
        }

        public testGuiPrimitives() : void {
            this.driver.findElement(this.by.linkText("GUI Primitives")).click().then(() : void => {
                this.validate();
            });
        }

        public testElementManager() : void {
            this.driver.findElement(this.by.linkText("Element Manager")).click().then(() : void => {
                this.validate();
            });
        }

        public testGuiPrimitivesSerialization() : void {
            this.driver.findElement(this.by.linkText("GUI Primitives Serialization")).click().then(() : void => {
                this.validate();
            });
        }

        protected setUp() : void {
            this.driver.get("file:///" + this.getAbsoluteRoot() + "/build/target/index.html");
        }

        protected after() : void {
            this.driver.quit();
        }

        private validate() : void {
            this.driver.findElement(this.by.className("Result")).getText().then(($value : string) : void => {
                assert.equal($value, "SUCCESS");
            });
        }
    }
}
