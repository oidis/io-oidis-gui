/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { UnitType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/UnitType.js";
import { PropagableNumber } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class PropagableNumberTest extends UnitTestRunner {
    public testPriorityNumber() : void {
        const propagnumber : PropagableNumber = new PropagableNumber({number: 5, unitType: UnitType.PCT}, true);
        const childnumber : PropagableNumber = new PropagableNumber({number: 10, unitType: UnitType.PCT}, true);
        assert.equal(PropagableNumber.PriorityValue(propagnumber, childnumber), childnumber);
    }

    public testPriorityNumber2() : void {
        const propagnumber2 : PropagableNumber = new PropagableNumber({number: 0, unitType: null}, false);
        assert.equal(PropagableNumber.PriorityValue(propagnumber2, null), null);
    }

    public testPriorityNumber3() : void {
        const propagnumber3 : PropagableNumber = new PropagableNumber({number: 10, unitType: UnitType.PX}, true);
        assert.equal(PropagableNumber.PriorityValue(propagnumber3, null), propagnumber3);
    }

    public testIsPropagated() : void {
        const propagnumber : PropagableNumber = new PropagableNumber({number: 35, unitType: UnitType.PCT}, true);
        assert.equal(propagnumber.IsPropagated(), true);
    }

    public testNormalize() : void {
        const propagnumber : PropagableNumber = new PropagableNumber({number: 45, unitType: UnitType.PCT}, true);
        assert.equal(propagnumber.Normalize(30, UnitType.PCT), 45);
        const propagnumber2 : PropagableNumber = new PropagableNumber({number: 10, unitType: UnitType.PX}, true);
        assert.equal(propagnumber2.Normalize(10, UnitType.PCT), 100);
        const propagnumber3 : PropagableNumber = new PropagableNumber([
            {number: 75, unitType: UnitType.PCT},
            {number: 25, unitType: UnitType.PX}
        ], true);
        assert.equal(propagnumber3.Normalize(100, UnitType.PX), 100);
    }
}
