/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ImageCropDimensions } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/ImageCropDimensions.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class ImageCropDimensionsTest extends UnitTestRunner {

    public testConstructor() : void {
        const cropDimension : ImageCropDimensions = new ImageCropDimensions(10, 10, 10, 10);
        assert.equal(cropDimension.TopX(), 10);
        assert.equal(cropDimension.TopY(), 10);
        assert.equal(cropDimension.BottomX(), 10);
        assert.equal(cropDimension.BottomY(), 10);
    }

    public testsetDimensions() : void {
        const cropDimension : ImageCropDimensions = new ImageCropDimensions(50, 50, 50, 50);
        assert.equal(cropDimension.TopY(), 50);
        assert.equal(cropDimension.BottomY(), 50);
    }

    public testTopX() : void {
        const cropDimension : ImageCropDimensions = new ImageCropDimensions();
        assert.equal(cropDimension.TopX(30), 30);
        assert.equal(cropDimension.TopX(0), 0);
    }

    public testTopY() : void {
        const cropDimension : ImageCropDimensions = new ImageCropDimensions();
        assert.equal(cropDimension.TopY(30), 30);
        assert.equal(cropDimension.TopY(0), 0);
    }

    public testButtomX() : void {
        const cropDimension : ImageCropDimensions = new ImageCropDimensions();
        assert.equal(cropDimension.BottomX(30), 30);
        assert.equal(cropDimension.BottomX(0), 0);
    }

    public testButtomY() : void {
        const cropDimension : ImageCropDimensions = new ImageCropDimensions();
        assert.equal(cropDimension.BottomY(30), 30);
        assert.equal(cropDimension.BottomY(0), 0);
    }
}
