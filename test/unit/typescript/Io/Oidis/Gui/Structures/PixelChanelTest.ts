/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { PixelChanel } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/PixelChanel.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class PixelChanelTest extends UnitTestRunner {

    public testConstructor() : void {
        const pixel : PixelChanel = new PixelChanel();
        assert.equal(pixel.getClassName(), "Io.Oidis.Gui.Structures.PixelChanel");
    }

    public testToString() : void {
        const pixel : PixelChanel = new PixelChanel();
        assert.equal(pixel.ToString(""), "[Red] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
            "[Green] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
            "[Blue] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
            "[Alpha] <i>Object type:</i> number. <i>Return value:</i> 0<br/>");
    }

    public testtoString() : void {
        const pixel : PixelChanel = new PixelChanel();
        assert.equal(pixel.toString(), "[Red] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
            "[Green] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
            "[Blue] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
            "[Alpha] <i>Object type:</i> number. <i>Return value:</i> 0<br/>");
    }
}
