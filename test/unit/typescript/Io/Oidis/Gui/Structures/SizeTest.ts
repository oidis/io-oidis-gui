/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Size } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/Size.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class SizeTest extends UnitTestRunner {

    public testConstructor() : void {
        const size : Size = new Size();
        assert.equal(size.Height(20), 20);
        const size2 : Size = new Size("div");
        assert.equal(size2.getClassName(), "Io.Oidis.Gui.Structures.Size");
    }

    public testWidth() : void {
        const size : Size = new Size();
        assert.equal(size.Width(30), 30);
        const size2 : Size = new Size();
        assert.equal(size2.Width(-2), 0);
    }

    public testHeight() : void {
        const size : Size = new Size();
        assert.equal(size.Height(60), 60);
    }
}
