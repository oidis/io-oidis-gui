/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Borders } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/Borders.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class BordersTest extends UnitTestRunner {

    public testConstructor() : void {
        const border : Borders = new Borders();
        assert.equal(border.Bottom(4), 4);
    }

    public testTop() : void {
        const border : Borders = new Borders();
        assert.equal(border.Top(4), 4);
        assert.equal(border.Top(0), 0);
        assert.equal(border.Top(), 0);
    }

    public testLeft() : void {
        const border : Borders = new Borders();
        assert.equal(border.Left(), 0);
        assert.equal(border.Left(4), 4);
        assert.equal(border.Left(-5), -5);
    }

    public testBottom() : void {
        const border : Borders = new Borders();
        assert.equal(border.Bottom(), 0);
        assert.equal(border.Bottom(4), 4);
        assert.equal(border.Bottom(-5), -5);
    }

    public testRight() : void {
        const border : Borders = new Borders();
        assert.equal(border.Right(4), 4);
        assert.equal(border.Right(-5), -5);
    }
}
