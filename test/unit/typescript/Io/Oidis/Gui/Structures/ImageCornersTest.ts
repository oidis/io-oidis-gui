/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ImageCorners } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/ImageCorners.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class ImageCornersTest extends UnitTestRunner {

    public testConstructor() : void {
        const corner : ImageCorners = new ImageCorners(10, 10, 10, 10);
        assert.equal(corner.BottomLeft(), 10);
        assert.equal(corner.BottomRight(), 10);
        assert.equal(corner.TopRight(), 10);
    }

    public testsetCorners() : void {
        const corner : ImageCorners = new ImageCorners(50, 50, 50, 50);
        assert.equal(corner.BottomLeft(), 50);
        assert.equal(corner.TopRight(), 50);
    }

    public testTopLeft() : void {
        const corner : ImageCorners = new ImageCorners();
        assert.equal(corner.TopLeft(30), 30);
        assert.equal(corner.TopLeft(0), 0);
    }

    public testTopRight() : void {
        const corner : ImageCorners = new ImageCorners();
        assert.equal(corner.TopRight(30), 30);
        assert.equal(corner.TopRight(0), 0);
    }

    public testButtomLeft() : void {
        const corner : ImageCorners = new ImageCorners();
        assert.equal(corner.BottomLeft(30), 30);
        assert.equal(corner.BottomLeft(0), 0);
    }

    public testButtomRight() : void {
        const corner : ImageCorners = new ImageCorners();
        assert.equal(corner.BottomRight(30), 30);
        assert.equal(corner.BottomRight(0), 0);
    }
}
