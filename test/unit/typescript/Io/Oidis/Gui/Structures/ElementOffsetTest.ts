/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ElementOffset } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/ElementOffset.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class ElementOffsetTest extends UnitTestRunner {
    public testConstructor() : void {
        const offset : ElementOffset = new ElementOffset(10, 3);
        assert.equal(offset.Top(), 10);
    }

    public testTop() : void {
        const offset : ElementOffset = new ElementOffset(10, 3);
        assert.equal(offset.Top(15), 15);
    }

    public testLeft() : void {
        const offset : ElementOffset = new ElementOffset();
        assert.equal(offset.Left(0), 0);
        assert.equal(offset.Left(3), 3);
        assert.equal(offset.Left(8), 8);
    }
}
