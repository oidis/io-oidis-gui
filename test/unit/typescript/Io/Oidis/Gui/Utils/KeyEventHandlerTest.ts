/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { KeyMap } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/KeyMap.js";
import { KeyEventHandler } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/KeyEventHandler.js";
import { TextSelectionManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/TextSelectionManager.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class KeyEventHandlerTest extends UnitTestRunner {

    public testIsEdit() : void {
        const keyboardEvent : any = {altKey: true, char: "T", ctrlKey: true, keyCode: KeyMap.DOT};
        assert.equal(KeyEventHandler.IsEdit(keyboardEvent), true);
        const keyboardEvent2 : any = {altKey: true, char: "T", ctrlKey: true, keyCode: KeyMap.A};
        assert.equal(KeyEventHandler.IsEdit(keyboardEvent2), false);
        const keyboardEvent3 : any = {altKey: true, char: "T", ctrlKey: true, keyCode: KeyMap.C};
        assert.equal(KeyEventHandler.IsEdit(keyboardEvent3), false);
    }

    public testIsInteger() : void {
        const keyboardEvent : any = {altKey: true, char: "T", ctrlKey: true, keyCode: 20};

        (<any>KeyEventHandler).IsNavigate(keyboardEvent);
        assert.equal(KeyEventHandler.IsInteger(keyboardEvent, "232", 10), false);

        const keyboardEvent2 : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 10};
        assert.equal(KeyEventHandler.IsInteger(keyboardEvent2, "454", 10), false);

        const keyEvent : any = {altKey: true, char: "T", ctrlKey: true, keyCode: 48};
        this.registerElement("id123");
        const element : HTMLInputElement = <HTMLInputElement>document.getElementById("id123");
        element.value = "test";
        assert.equal(KeyEventHandler.IsInteger(keyEvent, "id123", 10), true);
        assert.equal(KeyEventHandler.IsInteger(keyEvent, "id123"), true);
        element.value = "value10test2";
        assert.equal(KeyEventHandler.IsInteger(keyEvent, "id123", 2), TextSelectionManager.Clear(element));

        const key : any = {altKey: true, char: "T", ctrlKey: true, keyCode: 45};
        (<any>KeyEventHandler).IsNavigate(key);
        this.registerElement("id124");
        const element2 : HTMLInputElement = <HTMLInputElement>document.getElementById("id124");
        element2.value = "value10test2";
        TextSelectionManager.getSelectedText(element2);
        assert.equal(KeyEventHandler.IsInteger(key), true);

        const key2 : any = {altKey: true, char: "T", ctrlKey: true, keyCode: KeyMap.MINUS};
        this.registerElement("id656");
        const element3 : HTMLInputElement = <HTMLInputElement>document.getElementById("id656");
        element3.value = "value10test2";
        assert.equal(KeyEventHandler.IsInteger(key2, "id656"), false);
        element3.value = "12345";
        assert.equal(KeyEventHandler.IsInteger(key2, "id656"), false);

        const key3 : any = {altKey: true, char: "T", ctrlKey: true, keyCode: 9};
        this.registerElement("id787");
        const element4 : HTMLInputElement = <HTMLInputElement>document.getElementById("id787");
        element4.value = "-123456789458";
        assert.equal(KeyEventHandler.IsInteger(key3, "id787", 11), true);
        element4.value = "12345";
        assert.equal(KeyEventHandler.IsInteger(key3, "id787"), true);

        const key4 : any = {altKey: true, char: "T", ctrlKey: true, keyCode: KeyMap.DOT};
        this.registerElement("id989");
        assert.equal(KeyEventHandler.IsInteger(key4, "id989", 5), false);
    }

    public testIsIntegerSecond() : void {
        const key5 : any = {altKey: true, char: "T", ctrlKey: true, keyCode: 48};
        let element : HTMLElement;
        assert.equal(KeyEventHandler.IsInteger(key5, undefined, 10), true);
    }

    public testIsDouble() : void {
        const keyboardEvent : any = {altKey: true, char: "T", ctrlKey: true, keyCode: KeyMap.DOT};
        (<any>KeyEventHandler).IsNavigate(keyboardEvent);
        assert.equal(KeyEventHandler.IsDouble(keyboardEvent), true);

        const boardEvent : any = {altKey: true, char: "T", ctrlKey: true, keyCode: 20};
        assert.equal(KeyEventHandler.IsDouble(boardEvent), false);
    }

    public testIsPositiveIntegerOrSpace() : void {
        const keyboardEvent : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 20};
        assert.equal(KeyEventHandler.IsPositiveIntegerOrSpace(keyboardEvent), false);

        const keyboard : any = {altKey: true, char: "T", ctrlKey: true, keyCode: KeyMap.SPACE};
        (<any>KeyEventHandler).IsNavigate(keyboard);
        assert.equal(KeyEventHandler.IsPositiveIntegerOrSpace(keyboard), true);
        assert.equal(KeyEventHandler.IsPositiveIntegerOrSpace(keyboard), true);
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
