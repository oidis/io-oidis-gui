/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ThreadPool } from "@io-oidis-commons/Io/Oidis/Commons/Events/ThreadPool.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { DirectionType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/DirectionType.js";
import { ProgressType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/ProgressType.js";
import { ValueProgressEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/ValueProgressEventArgs.js";
import { ValueProgressManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/ValueProgressManager.js";

export class ValueProgressManagerTest extends UnitTestRunner {

    public __IgnoretestExecute20() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : ValueProgressEventArgs = new ValueProgressEventArgs("166");
            args.DirectionType(DirectionType.UP);
            args.ProgressType(ProgressType.FAST_START);
            ValueProgressManager.Execute(args);
            ThreadPool.AddThread("166", "onstart", ($eventArgs : EventArgs) : void => {
                ThreadPool.RemoveThread("166", "onstart");
                assert.deepEqual($eventArgs, args);
                $done();
            });
            ThreadPool.setThreadArgs("166", "onstart", args);
            ThreadPool.Execute();
        };
    }

    public __IgnoretestExecute30() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : ValueProgressEventArgs = new ValueProgressEventArgs("266");
            args.DirectionType(DirectionType.DOWN);
            args.ProgressType(ProgressType.FAST_START);
            ValueProgressManager.Execute(args);
            ThreadPool.AddThread("266", "valueProgressFastStart", ($eventArgs : EventArgs) : void => {
                ThreadPool.RemoveThread("266", "valueProgressFastStart");
                assert.deepEqual($eventArgs, args);
                $done();
            });
            ThreadPool.setThreadArgs("266", "valueProgressFastStart", args);
            ThreadPool.Execute();
        };
    }

    public __IgnoretestExecute40() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : ValueProgressEventArgs = new ValueProgressEventArgs("366");
            args.DirectionType(DirectionType.UP);
            args.ProgressType(ProgressType.SLOW_START);
            ValueProgressManager.Execute(args);
            ThreadPool.AddThread("366", "onstart", ($eventArgs : EventArgs) : void => {
                ThreadPool.RemoveThread("366", "onstart");
                assert.deepEqual($eventArgs, args);
                $done();
            });
            ThreadPool.setThreadArgs("366", "onstart", args);
            ThreadPool.Execute();
        };
    }

    public __IgnoretestExecute50() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : ValueProgressEventArgs = new ValueProgressEventArgs("466");
            args.DirectionType(DirectionType.DOWN);
            args.ProgressType(ProgressType.SLOW_START);
            ValueProgressManager.Execute(args);
            ThreadPool.AddThread("466", "valueProgressSlowStart", ($eventArgs : EventArgs) : void => {
                ThreadPool.RemoveThread("466", "valueProgressSlowStart");
                assert.deepEqual($eventArgs, args);
                $done();
            });
            ThreadPool.setThreadArgs("466", "valueProgressSlowStart", args);
            ThreadPool.Execute();
        };
    }

    public __IgnoretestExecute60() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : ValueProgressEventArgs = new ValueProgressEventArgs("566");
            args.DirectionType(DirectionType.UP);
            args.ProgressType(ProgressType.LINEAR);
            ValueProgressManager.Execute(args);
            ThreadPool.AddThread("566", "valueProgressSlowStart", ($eventArgs : EventArgs) : void => {
                ThreadPool.RemoveThread("566", "valueProgressSlowStart");
                assert.equal(args.DirectionType(DirectionType.UP), DirectionType.UP);
                $done();
            });
            ThreadPool.setThreadArgs("66", "valueProgressSlowStart", args);
            ThreadPool.Execute();
        };
    }

    public __IgnoretestExecute70() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : ValueProgressEventArgs = new ValueProgressEventArgs("666");
            args.DirectionType(DirectionType.DOWN);
            args.ProgressType(ProgressType.LINEAR);
            ValueProgressManager.Execute(args);
            ThreadPool.AddThread("666", "valueProgressSlowStart", ($eventArgs : EventArgs) : void => {
                ThreadPool.RemoveThread("666", "valueProgressSlowStart");
                assert.deepEqual(args.DirectionType(DirectionType.DOWN), DirectionType.DOWN);
                $done();
            });
            ThreadPool.setThreadArgs("666", "valueProgressSlowStart", args);
            ThreadPool.Execute();
        };
    }

    public __IgnoretestExecute80() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : ValueProgressEventArgs = new ValueProgressEventArgs("766");
            ValueProgressManager.Execute(args);
            ThreadPool.AddThread("766", "valueProgressSlowStart", ($eventArgs : EventArgs) : void => {
                ThreadPool.RemoveThread("766", "valueProgressSlowStart");
                assert.deepEqual($eventArgs, args);
                $done();
            });
            ThreadPool.setThreadArgs("766", "valueProgressSlowStart", args);
            ThreadPool.Execute();
        };
    }

    public __IgnoretestRemove() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : ValueProgressEventArgs = new ValueProgressEventArgs("666");
            args.DirectionType(DirectionType.DOWN);
            args.ProgressType(ProgressType.LINEAR);
            args.RangeEnd(9);
            args.Step(10);
            ValueProgressManager.Execute(args);
            ThreadPool.AddThread("666", "onchange", ($eventArgs : EventArgs) : void => {
                ValueProgressManager.Remove("666");
                assert.deepEqual(args.DirectionType(DirectionType.DOWN), DirectionType.DOWN);
                $done();
            });
            ThreadPool.setThreadArgs("666", "onchange", args);
            ThreadPool.Execute();
        };
    }

    public testget() : void {
        const valueprogress2 : ValueProgressEventArgs = new ValueProgressEventArgs("264");
        valueprogress2.DirectionType(DirectionType.RIGHT);
        assert.deepEqual(ValueProgressManager.get(valueprogress2.OwnerId()), valueprogress2);
        assert.deepEqual(ValueProgressManager.get("364"), new ValueProgressEventArgs("364"));
        assert.equal(ValueProgressManager.Exists("264Width"), false);
        assert.deepEqual(ValueProgressManager.get(null), null);
        assert.deepEqual(ValueProgressManager.get(undefined), null);
    }

    public testgetSecond() : void {
        let valueprogress : ValueProgressEventArgs;
        assert.deepEqual(ValueProgressManager.get(""), null);
        //  assert.deepEqual(ValueProgressManager.get(valueprogress.OwnerId()),null);
    }

    public testExists() : void {
        assert.deepEqual(ValueProgressManager.Exists("220"), false);
        assert.deepEqual(ValueProgressManager.Exists("110"), false);
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
