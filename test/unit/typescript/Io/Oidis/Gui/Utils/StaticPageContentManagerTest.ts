/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { GuiCommons } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ElementManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/ElementManager.js";
import { StaticPageContentManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/StaticPageContentManager.js";

class MockGuiCommons extends GuiCommons {
}

export class StaticPageContentManagerTest extends UnitTestRunner {

    public testLanguage() : void {
        assert.equal(StaticPageContentManager.Language(LanguageType.CZ), LanguageType.CZ);
        assert.equal(StaticPageContentManager.Language(null), "Cz");
        (<any>StaticPageContentManager).isPrinted = true;
        assert.equal(StaticPageContentManager.Language(LanguageType.EN), LanguageType.EN);
    }

    public testTitle() : void {
        assert.equal(StaticPageContentManager.Title(), "UnitTestLoader");
        assert.equal(StaticPageContentManager.Title("testTitle"), "testTitle");
        assert.equal(StaticPageContentManager.Title(), "testTitle");
    }

    public testFaviconSource() : void {
        assert.equal(StaticPageContentManager.FaviconSource(), "resource/graphics/icon.ico");
        assert.equal(StaticPageContentManager.FaviconSource("testFavicon"), "testFavicon");
    }

    public testCharset() : void {
        assert.equal(StaticPageContentManager.Charset("charset=UTF-8"), "charset=UTF-8");
    }

    public testHeadLinkAppend() : void {
        const linkElement : HTMLLinkElement = document.createElement("link");
        StaticPageContentManager.HeadLinkAppend(linkElement, "forward link types", "advisory content type");
        const linkElement2 : HTMLLinkElement = document.createElement("link");
        StaticPageContentManager.HeadLinkAppend(linkElement2, "alternate");
        StaticPageContentManager.HeadLinkAppend(linkElement2, "text/html");
        const linkElement3 : HTMLLinkElement = document.createElement("link");
        StaticPageContentManager.HeadLinkAppend(linkElement3);
        StaticPageContentManager.HeadLinkAppend("http://someplace.com/manual/postscript.ps");
        StaticPageContentManager.HeadLinkAppend(null);
        StaticPageContentManager.HeadLinkAppend("http://someplace.com/manual/postscript.ps", "alternate", "text/html");
    }

    public testHeadMetaDataAppend() : void {
        const metaElement : HTMLMetaElement = document.createElement("meta");
        StaticPageContentManager.HeadMetaDataAppend(metaElement, "http response header");
        StaticPageContentManager.HeadMetaDataAppend("The Federal Aviation Administration is an operating mode of the U.S." +
            " Department of Transportation.", "http response header");
        StaticPageContentManager.HeadMetaDataAppend(null);
        StaticPageContentManager.HeadMetaDataAppend(metaElement);
        StaticPageContentManager.HeadMetaDataAppend("The Federal Aviation Administration is an operating mode of the U.S." +
            " Department of Transportation.");
    }

    public testHeadScriptAppend() : void {
        const scriptElement : HTMLScriptElement = document.createElement("script");
        StaticPageContentManager.HeadScriptAppend(scriptElement, "text/javascript");
        StaticPageContentManager.HeadScriptAppend("url", "text/javascript");
        StaticPageContentManager.HeadScriptAppend(null);
        StaticPageContentManager.HeadScriptAppend(scriptElement);
        StaticPageContentManager.HeadScriptAppend("url");
    }

    public testBodyAppend() : void {
        StaticPageContentManager.BodyAppend(null);
        assert.equal(StaticPageContentManager.getBody(), "");
    }

    public __IgnoretestgetBodyAppend() : void {
        StaticPageContentManager.BodyAppend("stringBody");
        assert.patternEqual(StaticPageContentManager.getBody(),
            "*<h1>WUI Framework base GUI Library</h1>" +
            "<h3>Common and abstract classes or interfaces connected with creation and handling of GUI.</h3>" +
            "*<H3>Runtime tests</H3><H4>Page content</H4>" +
            "<a *</a><br/>" +
            "<H4>GUI elements</H4>" +
            "<a *</a><br/>" +
            "<H4>GUI utils</H4>" +
            "<a *</a><br/>" +
            "<H4>Draw GUI resolvers</H4>" +
            "<a *<H4>Io.Oidis.Gui.Primitives</H4>" +
            "<a *</a><br/>" +
            "</div></div><div*</div>\r\nstringBody\r\n");
        StaticPageContentManager.BodyAppend("stringBody");
        StaticPageContentManager.BodyAppend(undefined);
    }

    public testBodyAppendSecond() : void {
        const baseguiNext : GuiCommons = new MockGuiCommons("id006");
        StaticPageContentManager.BodyAppend(baseguiNext.Draw());
        StaticPageContentManager.Draw();
        baseguiNext.Visible(true);
        baseguiNext.Visible(true);
        ElementManager.AppendHtml("id006", "HelloSubstring");
        assert.equal(ElementManager.getElement(baseguiNext, true).outerHTML,
            "<div id=\"id006\" class=\"GuiCommons\" style=\"display: none;\">" +
            "<span guitype=\"HtmlAppender\">HelloSubstring</span></div>");
        assert.equal(baseguiNext.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"id006_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id006\" class=\"GuiCommons\" style=\"display: none;\"></div>\r\n" +
            "   </div>\r\n</div>");
    }

    public testClearSecond() : void {
        // StaticPageContentManager.getBody();
        // StaticPageContentManager.Clear();
        assert.throws(() : void => {
            StaticPageContentManager.Clear();
            throw new Error("Static Clean");
        }, /Static Clean/);
    }

    public __IgnoretestToString() : void {
        assert.patternEqual(StaticPageContentManager.ToString(),
            "<!DOCTYPE html>\n\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "<title>UnitTestLoader</title>\n\n" +
            "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
            "<link href=\"resource/graphics/icon.ico?v=*\" rel=\"shortcut icon\" type=\"text/css\">\n" +
            "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css?v=*\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
            "</head>\n\n" +
            "<body onfocus=\"Io.Oidis.Gui.Events.EventsManager.bodyFocusEventHandler();" +
            "\" onblur=\"Io.Oidis.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
            "<div id=\"Browser\" class=\"FIREFOX\">\n" +
            "<div id=\"Language\" class=\"En\">\n" +
            "<div id=\"Content\" class=\"Content\" guiType=\"PageContent\">\n\n" +
            "</div>\n" +
            "</div>\n" +
            "</div>\n" +
            "</body>\n" +
            "</html>");
    }

    public testDrawSecond() : void {
        Echo.PrintCode("<html> <head>" +
            "<title>The Hello World Program</title>" +
            "<meta name=\"keywords\" content=\"computer science, programming, code, STEM, tutorials, education\" />" +
            "<meta name=\"description\" content=\"The Hello World Program is an online collection of programming, computer science," +
            " and web development videos and tutorials combining technology and art.\" />" +
            "<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"styles.css\" />" +
            "<link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"print.css\" />" +
            "<script src=\"js/main.js\"></script>\n" +
            "1\n" +
            "<script src=\"js/main.js\"></script>" +
            "</head><body>" +
            "<div id=\"hello\"></div>\n" +
            "\t\t<script src=\"js/main.js\"></script>" +
            "</body>" +
            "</html>");

        assert.equal(StaticPageContentManager.Draw(), undefined);
        const element : HTMLElement = document.createElement("head");
        this.registerElement("id007", "span");
        StaticPageContentManager.HeadMetaDataAppend("id007");
    }

    protected tearDown() : void {
        StaticPageContentManager.Clear(true);
        this.initSendBox();
    }
}
