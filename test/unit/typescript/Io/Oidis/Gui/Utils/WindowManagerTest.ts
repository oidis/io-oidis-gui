/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventsManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/EventsManager.js";
import { WindowEventsManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/WindowEventsManager.js";
import { GuiCommons } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { Size } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/Size.js";
import { WindowManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/WindowManager.js";

class MockGuiCommons extends GuiCommons {
}

export class WindowManagerTest extends UnitTestRunner {

    public testgetEvents() : void {
        const events : WindowEventsManager = new WindowEventsManager();
        const gui : GuiCommons = new MockGuiCommons("id5");
        const handler : any = () : void => {
            // test event handler
        };
        gui.getEvents().setEvent("TestEvent", handler);
        gui.getEvents().setEvent("TestNext", handler);
        assert.equal(WindowManager.getEvents(), (<any>WindowManager).events);
        assert.equal(WindowManager.getEvents(), (<any>WindowManager).events);
    }

    public testIsDropSupported() : void {
        assert.equal(WindowManager.IsDropSupported(), true);
        assert.equal(WindowManager.IsDropSupported(), true);
    }

    public testgetMouseX() : void {
        const mouseEvent : any = {metaKey: true, movementX: 5, movementY: 6, offsetX: 7, offsetY: 8, pageX: 9, pageY: 10};
        assert.equal(WindowManager.getMouseX(mouseEvent, false), 9);
        assert.equal(WindowManager.getMouseY().toString(), "NaN");

        const element : HTMLElement = document.createElement("body");
        element.scrollLeft = 10;
        const mouseEvent2 : any = {altKey: true, button: 2, buttons: 3, clientX: 4, clientY: 5, ctrlKey: true, fromElement: element};
        assert.equal(WindowManager.getMouseX(mouseEvent2, false), 4);
    }

    public testgetMouseY() : void {
        const mouseEvent : any = {metaKey: true, movementY: 5, movementX: 6, offsetY: 7, offsetX: 8, pageX: 9, pageY: 10};
        assert.equal(WindowManager.getMouseY(mouseEvent, false), 10);
        assert.equal(WindowManager.getMouseX().toString(), "NaN");

        const element : HTMLElement = document.createElement("body");
        element.scrollLeft = 10;
        const mouseEvent2 : any = {altKey: true, button: 2, buttons: 3, clientX: 4, clientY: 5, ctrlKey: true, fromElement: element};
        assert.equal(WindowManager.getMouseY(mouseEvent2, true), 5);
    }

    public testGetSize() : void {
        const size : Size = WindowManager.getSize();
        assert.equal(size.Width(), 1024);
        assert.equal(size.Height(), 768);

        const bodyOffsetWidth = document.body.offsetWidth;
        (<any>document.body).__defineGetter__("offsetWidth", function () {
            return 30;
        });
        const size2 : Size = WindowManager.getSize();
        assert.equal(size2.Width(), 1024);
        assert.equal(size2.Height(), 768);

        (<any>document.body).__defineGetter__("offsetWidth", function () {
            return bodyOffsetWidth;
        });

        const documentOffsetWidth : number = document.documentElement.offsetWidth;
        (<any>document.documentElement).__defineGetter__("offsetWidth", function () {
            return 10;
        });
        const compatMode : string = document.compatMode;
        (<any>document).__defineGetter__("compatMode", function () {
            return "CSS1Compat";
        });
        const size3 : Size = WindowManager.getSize();
        assert.equal(size3.Width(), 1024);
        assert.equal(size3.Height(), 768);

        (<any>document).__defineGetter__("compatMode", function () {
            return compatMode;
        });
        (<any>document.documentElement).__defineGetter__("offsetWidth", function () {
            return documentOffsetWidth;
        });

        const windowInnerWidth : number = window.innerWidth;
        delete (<any>window).innerWidth;
        const size4 : Size = WindowManager.getSize();
        assert.equal(size4.Width(), 1024);
        assert.equal(size4.Height(), 768);

        (<any>window).__defineGetter__("innerWidth", function () {
            return windowInnerWidth;
        });
    }

    public __IgnoretestGetMousePosition() : void {
        Echo.Println("<div style=\"position: relative; top: 500px; left: 1200px; border: 1px solid red; width: 200px;\" " +
            "id=\"GetPositionButton\">Get mouse position</div>");
        EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
            const element : HTMLElement = document.getElementById("GetPositionButton");
            element.onclick = ($eventArgs : MouseEvent) : any => {
                Echo.Printf(WindowManager.getMouseX($eventArgs));
                Echo.Printf(WindowManager.getMouseY($eventArgs));
            };
        });
    }

    public testViewHTMLCode() : void {
        const element : GuiCommons = new MockGuiCommons("id300");
        element.Width(100);
        element.Height(200);
        element.Visible(true);
        element.Enabled(true);
        element.DisableAsynchronousDraw();
        assert.patternEqual(element.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"id300_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id300\" class=\"GuiCommons\" style=\"display: block;\"></div>\r\n" +
            "   </div>\r\n" +
            "</div>");

        assert.equal(WindowManager.ViewHTMLCode(), "<div class=\"ViewHtmlCode\"><pre><div class=\"Line  Odd\">" +
            "<span class=\"Number\">1</span><span class=\"Tag\"><span class=\"Tag\">&lt;head&gt;</span><span class=\"Tag\">" +
            "&lt;/head&gt;</span><span class=\"Tag\">&lt;body&gt;</span><span class=\"Tag\">&lt;div <span class=\"Attribute\">" +
            "id=</span><span class=\"String\">\"Content\"</span>&gt;</span><span class=\"Tag\">&lt;/div&gt;</span>" +
            "<span class=\"Tag\">&lt;/body&gt;</span></span></div><div class=\"Line  Even\">" +
            "<span class=\"Number\">2</span></div></pre></div>");

        assert.equal(WindowManager.ViewHTMLCode(),
            "<div class=\"ViewHtmlCode\"><pre><div class=\"Line  Odd\"><span class=\"Number\">1</span><span class=\"Tag\">" +
            "<span class=\"Tag\">&lt;head&gt;</span><span class=\"Tag\">&lt;/head&gt;</span><span class=\"Tag\">&lt;body&gt;</span>" +
            "<span class=\"Tag\">&lt;div <span class=\"Attribute\">id=</span><span class=\"String\">\"Content\"</span>&gt;</span>" +
            "<span class=\"Tag\">&lt;/div&gt;</span><span class=\"Tag\">&lt;/body&gt;</span></span></div><div class=\"Line  Even\">" +
            "<span class=\"Number\">2</span></div></pre></div>");
    }

    public testViewHTMLCodeSecond() : void {
        assert.equal(WindowManager.ViewHTMLCode(), "<div class=\"ViewHtmlCode\"><pre><div class=\"Line  Odd\">" +
            "<span class=\"Number\">1</span><span class=\"Tag\"><span class=\"Tag\">&lt;head&gt;</span><span class=\"Tag\">" +
            "&lt;/head&gt;</span><span class=\"Tag\">&lt;body&gt;</span><span class=\"Tag\">&lt;div <span class=\"Attribute\">" +
            "id=</span><span class=\"String\">\"Content\"</span>&gt;</span><span class=\"Tag\">" +
            "&lt;/div&gt;</span><span class=\"Tag\">" +
            "&lt;/body&gt;</span></span></div><div class=\"Line  Even\"><span class=\"Number\">2</span></div></pre></div>");
    }

    public testViewHTMLCodeThird() : void {
        Echo.PrintCode("<script type=\"text/tcl\">proc my_onload {} {. . .}set win " +
            "[window open \"some/other/URI\"]if {$win != \"\"} {$win onload my_onload} </script>" +
            "input<div class=\"IoOidisGuiPrimitives\">" +
            "<h5 id=\"HEOLProjectsIndex\"><span>EOL Projects Index</span></h5>" +
            "input   <div id=\"BaseGuiObject*_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "input      <div id=\"BaseGuiObject*\" class=\"BaseGuiObject\" style=\"display: block;\"></div>" +
            "<p>Hi<b>Hi &lt;Some text=\"\"&gt; and other text</b></b>" +
            "input   </div>" +
            "input</div>"
        );
        assert.equal(WindowManager.ViewHTMLCode(),
            "<div class=\"ViewHtmlCode\"><pre><div class=\"Line  Odd\"><span class=\"Number\">1</span>" +
            "<span class=\"Tag\"><span class=\"Tag\">&lt;head&gt;</span>" +
            "<span class=\"Tag\">&lt;/head&gt;</span><span class=\"Tag\">&lt;body&gt;</span><span class=\"Tag\">&lt;div " +
            "<span class=\"Attribute\">id=</span><span class=\"String\">\"Content\"</span>&gt;</span><span class=\"Tag\">&lt;span " +
            "<span class=\"Attribute\">guitype=</span><span class=\"String\">\"HtmlAppender\"</span>&gt;</span>" +
            "<span class=\"Tag\">&lt;br&gt;</span></span></div><div class=\"Line  Even\"><span class=\"Number\">2</span>" +
            "<span class=\"Tag\"><span class=\"Tag\">&lt;pre&gt;</span>&lt;script <span class=\"Attribute\">type=</span>" +
            "<span class=\"String\">\"text/tcl\"</span>&gt;proc my_onload {} {. . .}set win [window open " +
            "<span class=\"String\">\"some/other/URI\"</span>]if {$win <span class=\"Attribute\">!=</span>" +
            " \"\"} {$win onload my_onload} &lt;/script&gt;input&lt;div " +
            "<span class=\"Attribute\">class=</span><span class=\"String\">\"IoOidisGuiPrimitives\"</span>&gt;&lt;h5 " +
            "<span class=\"Attribute\">id=</span><span class=\"String\">\"HEOLProjectsIndex\"</span>&gt;&lt;span&gt;EOL " +
            "Projects Index&lt;/span&gt;&lt;/h5&gt;input   &lt;div <span class=\"Attribute\">id=</span><span class=\"String\">" +
            "\"BaseGuiObject*_GuiWrapper\"</span> <span class=\"Attribute\">guiType=</span>" +
            "<span class=\"String\">\"GuiWrapper\"</span>&gt;input      &lt;div <span class=\"Attribute\">id=</span>" +
            "<span class=\"String\">\"BaseGuiObject*\"</span> <span class=\"Attribute\">class=</span><span class=\"String\">" +
            "\"BaseGuiObject\"</span> <span class=\"Attribute\">style=</span>" +
            "<span class=\"String\">\"display: block;\"</span>&gt;&lt;/div&gt;&lt;p&gt;Hi&lt;b&gt;Hi &lt;Some text=\"\"&gt;" +
            " and other text&lt;/b&gt;&lt;/b&gt;input   &lt;/div&gt;input&lt;/div&gt;<span class=\"Tag\">&lt;/pre&gt;</span>" +
            "<span class=\"Tag\">&lt;/span&gt;</span><span class=\"Tag\">&lt;/div&gt;</span>" +
            "<span class=\"Tag\">&lt;/body&gt;</span></span></div><div class=\"Line  Odd\"><span class=\"Number\">3</span>" +
            "</div></pre></div>");
    }

    public testViewHTMLCodeFourth() : void {
        Echo.Printf(
            "<HTML> <HEAD> <TITLE>Your Title Here</TITLE><script type=\"text/javascript\">var test;</script>" +
            " </HEAD> <BODY BGCOLOR=\"FFFFFF\"> " +
            "<CENTER><IMG SRC=\"clouds.jpg\" ALIGN=\"BOTTOM\"> </CENTER> <HR> <a href=\"http://somegreatsite.com\">" +
            "Link Name</a>is a link to another nifty site<H1>This is a Header</H1><H2>This is a Medium Header</H2>" +
            "Send me mail at <a href=\"mailto:support@yourcompany.com\">support@yourcompany.com</a>.<P> This is a new paragraph!<P> " +
            "<B>This is a new paragraph!</B><BR> <B><I>This is a new sentence without a paragraph break, in bold italics.</I></B>" +
            "<HR></BODY></HTML>"
        );

        WindowManager.ViewHTMLCode();
        assert.equal(WindowManager.ViewHTMLCode(),
            "<div class=\"ViewHtmlCode\"><pre><div class=\"Line  Odd\"><span class=\"Number\">1</span>" +
            "<span class=\"Tag\"><span class=\"Tag\">&lt;head&gt;</span><span class=\"Tag\">&lt;/head&gt;</span>" +
            "<span class=\"Tag\">&lt;body&gt;</span><span class=\"Tag\">&lt;div <span class=\"Attribute\">id=</span>" +
            "<span class=\"String\">\"Content\"</span>&gt;</span><span class=\"Tag\">&lt;span " +
            "<span class=\"Attribute\">guitype=</span><span class=\"String\">\"HtmlAppender\"</span>&gt;</span>" +
            "<span class=\"Tag\">&lt;br&gt;</span></span></div><div class=\"Line  Even\"><span class=\"Number\">2</span>" +
            "<span class=\"Tag\">  <span class=\"Tag\">&lt;title&gt;" +
            "<span class=\"TagContent\">Your Title Here</span>&lt;/title&gt;</span><span class=\"Tag\">&lt;script " +
            "<span class=\"Attribute\">type=</span><span class=\"String\">\"text/javascript\"</span>&gt;</span>" +
            "<span class=\"Script\">var test;</span></div><div class=\"Line  Odd\"><span class=\"Number\">3</span>" +
            "<span class=\"Tag\">&lt;/script&gt;<span class=\"TagContent\">   </span>&lt;center&gt;</span>" +
            "<span class=\"Tag\">&lt;img <span class=\"Attribute\">src=</span><span class=\"String\">\"clouds.jpg\"</span> " +
            "<span class=\"Attribute\">align=</span><span class=\"String\">\"BOTTOM\"</span>&gt;</span> " +
            "<span class=\"Tag\">&lt;/center&gt;</span> <span class=\"Tag\">&lt;hr&gt;</span></span></div>" +
            "<div class=\"Line  Even\"><span class=\"Number\">4</span><span class=\"Tag\"> <span class=\"Tag\">&lt;a " +
            "<span class=\"Attribute\">href=</span><span class=\"String\">\"http://somegreatsite.com\"</span>&gt;" +
            "<span class=\"TagContent\">Link Name</span>&lt;/a&gt;" +
            "<span class=\"TagContent\">is a link to another nifty site</span>&lt;h1&gt;" +
            "<span class=\"TagContent\">This is a Header</span>&lt;/h1&gt;</span><span class=\"Tag\">&lt;h2&gt;" +
            "<span class=\"TagContent\">This is a Medium Header</span>&lt;/h2&gt;" +
            "<span class=\"TagContent\">Send me mail at </span>&lt;a <span class=\"Attribute\">href=</span>" +
            "<span class=\"String\">\"mailto:support@yourcompany.com\"</span>&gt;" +
            "<span class=\"TagContent\">support@yourcompany.com</span>&lt;/a&gt;<span class=\"TagContent\">.</span>&lt;p&gt;" +
            "<span class=\"TagContent\"> This is a new paragraph!</span>&lt;/p&gt;</span><span class=\"Tag\">&lt;p&gt;</span> " +
            "<span class=\"Tag\">&lt;b&gt;<span class=\"TagContent\">This is a new paragraph!</span>&lt;/b&gt;</span>" +
            "<span class=\"Tag\">&lt;br&gt;</span></span></div><div class=\"Line  Odd\"><span class=\"Number\">5</span>" +
            "<span class=\"Tag\"> <span class=\"Tag\">&lt;b&gt;</span><span class=\"Tag\">&lt;i&gt;" +
            "<span class=\"TagContent\">This is a new sentence without a paragraph break, in bold italics.</span>&lt;/i&gt;</span>" +
            "<span class=\"Tag\">&lt;/b&gt;</span><span class=\"Tag\">&lt;/p&gt;</span>" +
            "<span class=\"Tag\">&lt;hr&gt;</span></span></div><div class=\"Line  Even\"><span class=\"Number\">6</span>" +
            "<span class=\"Tag\"><span class=\"Tag\">&lt;/span&gt;</span><span class=\"Tag\">&lt;/div&gt;</span>" +
            "<span class=\"Tag\">&lt;/body&gt;</span></span></div><div class=\"Line  Odd\">" +
            "<span class=\"Number\">7</span></div></pre></div>");
    }

    public testViewHTMLCodeFifth() : void {
        Echo.PrintCode("<HTML></HTML>");
        assert.equal(WindowManager.ViewHTMLCode(), "<div class=\"ViewHtmlCode\"><pre><div class=\"Line  Odd\">" +
            "<span class=\"Number\">1</span><span class=\"Tag\"><span class=\"Tag\">&lt;head&gt;</span><span class=\"Tag\">" +
            "&lt;/head&gt;</span><span class=\"Tag\">&lt;body&gt;</span><span class=\"Tag\">&lt;div <span class=\"Attribute\">" +
            "id=</span><span class=\"String\">\"Content\"</span>&gt;</span><span class=\"Tag\">&lt;span <span class=\"Attribute\">" +
            "guitype=</span><span class=\"String\">\"HtmlAppender\"</span>&gt;</span><span class=\"Tag\">&lt;br&gt;</span></span>" +
            "</div><div class=\"Line  Even\"><span class=\"Number\">2</span><span class=\"Tag\"><span class=\"Tag\">&lt;pre&gt;" +
            "<span class=\"TagContent\">&lt;HTML&gt;&lt;/HTML&gt;</span>&lt;/pre&gt;</span><span class=\"Tag\">&lt;/span&gt;</span>" +
            "<span class=\"Tag\">&lt;/div&gt;</span><span class=\"Tag\">&lt;/body&gt;</span></span></div><div class=\"Line  Odd\">" +
            "<span class=\"Number\">3</span></div></pre></div>");
    }

    public testViewHTMLCodeSixth() : void {
        Echo.Printf(
            "<HTML> <HEAD> <TITLE>Your Title Here</TITLE><script></script>" +
            " </HEAD> <BODY BGCOLOR=\"FFFFFF\"> " +
            "<CENTER><IMG SRC=\"clouds.jpg\" ALIGN=\"BOTTOM\"> </CENTER> <HR> <a href=\"http://somegreatsite.com\">" +
            "Link Name</a>is a link to another nifty site" +
            "Send me mail at .<P> This is a new paragraph!<P> " +
            "<B>This is a new paragraph!</B><BR> <B><I>This is a new sentence without a paragraph break, in bold italics.</I></B>" +
            "<HR></BODY></HTML>"
        );
        assert.equal(WindowManager.ViewHTMLCode(),
            "<div class=\"ViewHtmlCode\"><pre><div class=\"Line  Odd\"><span class=\"Number\">1</span><span class=\"Tag\">" +
            "<span class=\"Tag\">&lt;head&gt;</span><span class=\"Tag\">&lt;/head&gt;</span><span class=\"Tag\">&lt;body&gt;</span>" +
            "<span class=\"Tag\">&lt;div <span class=\"Attribute\">id=</span><span class=\"String\">\"Content\"</span>&gt;</span>" +
            "<span class=\"Tag\">&lt;span <span class=\"Attribute\">guitype=</span>" +
            "<span class=\"String\">\"HtmlAppender\"</span>&gt;</span><span class=\"Tag\">&lt;br&gt;</span></span></div>" +
            "<div class=\"Line  Even\"><span class=\"Number\">2</span><span class=\"Tag\">  <span class=\"Tag\">&lt;title&gt;" +
            "<span class=\"TagContent\">Your Title Here</span>&lt;/title&gt;</span><span class=\"Tag\">&lt;script&gt;</span>" +
            "<span class=\"Tag\">&lt;/script&gt;<span class=\"TagContent\">   </span>&lt;center&gt;</span>" +
            "<span class=\"Tag\">&lt;img <span class=\"Attribute\">src=</span><span class=\"String\">\"clouds.jpg\"</span> " +
            "<span class=\"Attribute\">align=</span><span class=\"String\">\"BOTTOM\"</span>&gt;</span> " +
            "<span class=\"Tag\">&lt;/center&gt;</span> <span class=\"Tag\">&lt;hr&gt;</span></span></div><div class=\"Line  Odd\">" +
            "<span class=\"Number\">3</span><span class=\"Tag\"> <span class=\"Tag\">&lt;a <span class=\"Attribute\">href=</span>" +
            "<span class=\"String\">\"http://somegreatsite.com\"</span>&gt;<span class=\"TagContent\">Link Name</span>&lt;/a&gt;" +
            "<span class=\"TagContent\">is a link to another nifty siteSend me mail at .</span>&lt;p&gt;" +
            "<span class=\"TagContent\"> This is a new paragraph!</span>&lt;/p&gt;</span><span class=\"Tag\">&lt;p&gt;</span> " +
            "<span class=\"Tag\">&lt;b&gt;<span class=\"TagContent\">This is a new paragraph!</span>&lt;/b&gt;</span>" +
            "<span class=\"Tag\">&lt;br&gt;</span></span></div><div class=\"Line  Even\"><span class=\"Number\">4</span>" +
            "<span class=\"Tag\"> <span class=\"Tag\">&lt;b&gt;</span><span class=\"Tag\">&lt;i&gt;" +
            "<span class=\"TagContent\">This is a new sentence without a paragraph break, in bold italics.</span>&lt;/i&gt;</span>" +
            "<span class=\"Tag\">&lt;/b&gt;</span><span class=\"Tag\">&lt;/p&gt;</span>" +
            "<span class=\"Tag\">&lt;hr&gt;</span></span></div><div class=\"Line  Odd\"><span class=\"Number\">5</span>" +
            "<span class=\"Tag\"><span class=\"Tag\">&lt;/span&gt;</span><span class=\"Tag\">&lt;/div&gt;</span>" +
            "<span class=\"Tag\">&lt;/body&gt;</span></span></div><div class=\"Line  Even\">" +
            "<span class=\"Number\">6</span></div></pre></div>");
    }

    public testScrolltoButton() : void {
        WindowManager.ScrollToBottom();
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
