/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Http403ForbiddenPage } from "../../../../../../../source/typescript/Io/Oidis/Gui/ErrorPages/Http403ForbiddenPage.js";

export class Http403ForbiddenPageTest extends UnitTestRunner {
    public __IgnoretestgetMessageBody() : void {
        assert.resolveEqual(Http403ForbiddenPage, "" +
            "<head>\n<title>WUI - HTTP 403</title>\n\n" +
            "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
            "<link href=\"resource/graphics/Io/Oidis/Gui/ForbiddenIcon.ico?v=*\" rel=\"shortcut icon\" type=\"text/css\">\n" +
            "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css?v=*\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
            "</head>\n\n" +
            "<body onfocus=\"Io.Oidis.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
            "onblur=\"Io.Oidis.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
            "<div id=\"Browser\" class=\"FIREFOX\">\n" +
            "<div id=\"Language\" class=\"En\">\n" +
            "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
            "<div class=\"Exception\">\n   " +
            "<h1>HTTP status 403</h1>\n" +
            "   <div class=\"Message\">\n" +
            "Access denied.\n" +
            "   </div>\n" +
            "</div>\n" +
            "<div class=\"Logo\">\n" +
            "   <div class=\"WUI\"></div>\n" +
            "</div>\n\n" +
            "</div>\n" +
            "</div>\n" +
            "</div>\n\n" +
            "</body>");
    }
}
