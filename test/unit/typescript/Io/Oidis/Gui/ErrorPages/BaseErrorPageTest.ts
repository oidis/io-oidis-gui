/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BaseErrorPage } from "../../../../../../../source/typescript/Io/Oidis/Gui/ErrorPages/BaseErrorPage.js";

class MockBaseErrorPage extends BaseErrorPage {
}

class MockBaseErrorPage2 extends BaseErrorPage {
    protected getFaviconSource() : string {
        return "";
    }
}

export class BaseErrorPageTest extends UnitTestRunner {
    public testgetEchoOutput() : void {
        Echo.ClearAll();
        const baseerror : BaseErrorPage = new MockBaseErrorPage();
        const output : string = (<any>baseerror).getEchoOutput();
        assert.equal(output, "Nothing has been printed by Echo yet.");
    }

    public __IgnoretestgetPageBody() : void {
        assert.resolveEqual(BaseErrorPage,
            "<head>\n" +
            "<title>WUI - Error</title>\n\n" +
            "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
            "<link href=\"resource/graphics/Io/Oidis/Gui/ErrorIcon.ico?v=*\" rel=\"shortcut icon\" type=\"text/css\">\n" +
            "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css?v=*\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
            "</head>\n\n" +
            "<body onfocus=\"Io.Oidis.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
            "onblur=\"Io.Oidis.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
            "<div id=\"Browser\" class=\"FIREFOX\">\n" +
            "<div id=\"Language\" class=\"En\">\n" +
            "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
            "<div class=\"Exception\">\n" +
            "   <h1>Error</h1>\n" +
            "   <div class=\"Message\">\n" +
            "Something has went wrong\n" +
            "   </div>\n" +
            "</div>\n" +
            "<div class=\"Logo\">\n" +
            "   <div class=\"WUI\"></div>\n" +
            "</div>\n\n" +
            "</div>\n" +
            "</div>\n" +
            "</div>\n\n" +
            "</body>");
    }
}
