/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { HttpRequestConstants } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/HttpRequestConstants.js";
import { Http301MovedPage } from "../../../../../../../source/typescript/Io/Oidis/Gui/ErrorPages/Http301MovedPage.js";

export class Http301MovedPageTest extends UnitTestRunner {
    public __IgnoretestHttp301MovedPage() : void {
        assert.resolveEqual(Http301MovedPage, "" +
            "<head>\n" +
            "<title>WUI - HTTP 301</title>\n\n" +
            "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
            "<link href=\"resource/graphics/Io/Oidis/Gui/MovedIcon.ico?v=*\" rel=\"shortcut icon\" type=\"text/css\">\n" +
            "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css?v=*\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
            "</head>\n\n" +
            "<body onfocus=\"Io.Oidis.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
            "onblur=\"Io.Oidis.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
            "<div id=\"Browser\" class=\"FIREFOX\">\n" +
            "<div id=\"Language\" class=\"En\">\n" +
            "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
            "<div class=\"Exception\">\n" +
            "   <h1>HTTP status 301</h1>\n" +
            "   <div class=\"Message\">\n" +
            "File has been moved. New address is:<br>" +
            "<a href=\"#\">file://*/index.html#</a>\n" +
            "   </div>\n" +
            "</div>\n" +
            "<div class=\"Logo\">\n" +
            "   <div class=\"WUI\"></div>\n" +
            "</div>\n\n" +
            "</div>\n" +
            "</div>\n" +
            "</div>\n\n" +
            "</body>");

        const data : ArrayList<any> = new ArrayList<any>();
        data.Add("Io/Oidis/GuiErrorPages/Http301MovedPage.ts", HttpRequestConstants.HTTP301_LINK);
        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
        assert.equal(args.POST().getItem(HttpRequestConstants.HTTP301_LINK),
            "Io/Oidis/GuiErrorPages/Http301MovedPage.ts");
        assert.resolveEqual(Http301MovedPage, "" +
            "<head>\n" +
            "<title>WUI - HTTP 301</title>\n\n" +
            "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
            "<link href=\"resource/graphics/Io/Oidis/Gui/MovedIcon.ico?v=*\" rel=\"shortcut icon\" type=\"text/css\">\n" +
            "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css?v=*\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
            "</head>\n\n" +
            "<body onfocus=\"Io.Oidis.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
            "onblur=\"Io.Oidis.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
            "<div id=\"Browser\" class=\"FIREFOX\">\n" +
            "<div id=\"Language\" class=\"En\">\n" +
            "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
            "<div class=\"Exception\">\n" +
            "   <h1>HTTP status 301</h1>\n" +
            "   <div class=\"Message\">\n" +
            "File has been moved. New address is:<br>" +
            "<a href=\"#Io/Oidis/GuiErrorPages/Http301MovedPage.ts\">" +
            "file://*/index.html#Io/Oidis/GuiErrorPages/Http301MovedPage.ts</a>\n" +
            "   </div>\n" +
            "</div>\n" +
            "<div class=\"Logo\">\n" +
            "   <div class=\"WUI\"></div>\n" +
            "</div>\n\n" +
            "</div>\n" +
            "</div>\n" +
            "</div>\n\n" +
            "</body>", args);
    }
}
