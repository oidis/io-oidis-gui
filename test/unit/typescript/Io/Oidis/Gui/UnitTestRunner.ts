/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseUnitTestRunner } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { UnitTestEnvironmentArgs } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestRunner.js";
import { EventType } from "../../../../../../source/typescript/Io/Oidis/Gui/Enums/Events/EventType.js";
import { ElementEventsManager } from "../../../../../../source/typescript/Io/Oidis/Gui/Events/ElementEventsManager.js";
import { EventsManager } from "../../../../../../source/typescript/Io/Oidis/Gui/Events/EventsManager.js";
import { IEventsManager } from "../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/IEventsManager.js";
import { IGuiCommons } from "../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { Loader } from "../../../../../../source/typescript/Io/Oidis/Gui/Loader.js";
import { ElementManager } from "../../../../../../source/typescript/Io/Oidis/Gui/Utils/ElementManager.js";

export class UnitTestLoader extends Loader {
    protected initEnvironment() : UnitTestEnvironmentArgs {
        return new UnitTestEnvironmentArgs();
    }
}

export class UnitTestRunner extends BaseUnitTestRunner {

    protected assertFired($mock : IGuiCommons, $eventTypes : EventType[], $done? : () => void) : void {
        const assertEvents : IEventsManager = new EventsManager();
        const incompleteEvents : ArrayList<string> = ArrayList.ToArrayList($eventTypes);

        $eventTypes.forEach(($eventType : string) => {
            [$mock.getEvents(), EventsManager.getInstanceSingleton()]
                .forEach(($events : EventsManager | ElementEventsManager) : void => {
                    const argList : any[] = [];
                    if (!($events instanceof ElementEventsManager)) {
                        argList.push($mock);
                    }
                    argList.push($eventType,
                        () : void => {
                            incompleteEvents.RemoveAt(incompleteEvents.IndexOf($eventType));
                            if (incompleteEvents.IsEmpty()) {
                                assertEvents.Clear();
                                if (ObjectValidator.IsSet($done)) {
                                    $done();
                                }
                            }
                        });
                    // eslint-disable-next-line prefer-spread
                    $events.setEvent.apply($events, argList);
                });
        });

        assertEvents.FireAsynchronousMethod(() : void => {
            assert.doesNotThrow(() : void => {
                throw new Error("Events \"" + incompleteEvents.ToArray() + "\" expected to fire.");
            });
        }, 999);
    }

    protected assertNotFired($mock : IGuiCommons, $eventTypes : EventType[], $callback? : ($complete : () => void) => void) : void {
        let isComplete : boolean = false;
        [$mock.getEvents(), EventsManager.getInstanceSingleton()]
            .forEach(($events : EventsManager | ElementEventsManager) : void => {
                $eventTypes.forEach(($eventType : string) : void => {
                    const argList : any[] = [];
                    if (!($events instanceof ElementEventsManager)) {
                        argList.push($mock);
                    }
                    argList.push(
                        $eventType, () : void => {
                            if (!isComplete) {
                                assert.doesNotThrow(() : void => {
                                    throw new Error("Unexpected event \"" + $eventType + "\" fired.");
                                });
                            }
                        }
                    );
                    // eslint-disable-next-line prefer-spread
                    $events.setEvent.apply($events, argList);
                });
            });
        if (ObjectValidator.IsSet($callback)) {
            $callback(() : void => {
                isComplete = true;
            });
        }
    }

    protected assertSize($id : string, $width? : number, $height? : number, $message? : string) {
        if (ObjectValidator.IsSet($width)) {
            assert.equal(ElementManager.getCssIntegerValue($id, "width"), $width,
                "test width of element with id " + $id + (ObjectValidator.IsSet($message) ? " " + $message : ""));
        }
        if (ObjectValidator.IsSet($height)) {
            assert.equal(ElementManager.getCssIntegerValue($id, "height"), $height,
                "test height of element with id " + $id + (ObjectValidator.IsSet($message) ? " " + $message : ""));
        }
    }

    protected assertVisible($id : string, $value : boolean = true) {
        assert.equal(ElementManager.IsVisible($id), $value,
            "test if element with id " + $id + " is visible");
    }

    protected assertExists($id : string, $value : boolean = true) {
        assert.equal(ElementManager.Exists($id, true), $value,
            "test if element with id " + $id + " exists");
    }

    protected initLoader() : void {
        super.initLoader(UnitTestLoader);
    }
}
