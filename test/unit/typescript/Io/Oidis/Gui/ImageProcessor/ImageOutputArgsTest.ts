/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ImageOutputArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/ImageProcessor/ImageOutputArgs.js";

export class ImageOutputArgsTest extends UnitTestRunner {
    public testsetSize() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        imageargs.setSize(40, 20);
        assert.equal(imageargs.getHeight(), 20);
        assert.equal(imageargs.getWidth(), 40);
    }

    public testFillEnabled() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.FillEnabled(true), true);
        assert.equal(imageargs.FillEnabled(), true);
    }

    public testCornerOnEnvelop() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.CornerOnEnvelop(true), true);
    }

    public testZoom() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.Zoom(100), 100);
        const imageargs2 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs2.Zoom(), 100);
        const imageargs3 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs3.Zoom(60), 160);
        const imageargs4 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs4.Zoom(0.45), 145);
    }

    public testBlurEnabled() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.BlurEnabled(true), true);
    }

    public testGrayscaleEnabled() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.GrayscaleEnabled(true), true);
    }

    public testInvertEnabled() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.InvertEnabled(true), true);
    }

    public testBrightness() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.Brightness(-150), -150);
        const imageargs2 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs2.Brightness(200), 200);
    }

    public testRotation() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.Rotation(60, true), 60);
        const imageargs2 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs2.Rotation(4.55, false), 355);
        const imageargs3 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs3.Rotation(380, true), 20);
        const imageargs4 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs4.Rotation(null), 0);
    }

    public testContrast() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.Contrast(37), 37);
        const imageargs2 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs2.Contrast(-200), -200);
    }

    public testWaterMarkEnabled() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.WaterMarkEnabled(true), true);
        const imageargs2 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs2.WaterMarkEnabled(), false);
    }

    public testWaterMarkSource() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.WaterMarkSource("/img/coffee.jpg"), "/img/coffee.jpg");
    }

    public testForntEndCacheEnabled() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.FrontEndCacheEnabled(true), true);
        const imageargs2 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs2.FrontEndCacheEnabled(), true);
    }

    public testBackEndCacheEnabled() : void {
        const imageargs : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs.BackEndCacheEnabled(true), true);
        const imageargs2 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imageargs2.BackEndCacheEnabled(), false);
    }

    public __IgnoretestExpireTime() : void {
        const imagears : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imagears.ExpireTime("Sunday, May 07, 2017 12:10 PM"), "Sun, 07 May 2017 10:10:00 GMT");
        const imagears2 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imagears2.ExpireTime(), "+10 days");
    }

    public testQuality() : void {
        const imagears : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imagears.Quality(90), 90);
        const imagears2 : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imagears2.Quality(), 100);
    }

    public __IgnoretestToUrlData() : void {
        const imagears : ImageOutputArgs = new ImageOutputArgs();
        assert.equal(imagears.ToUrlData(), "Jnd0PTEmd3M9dW5kZWZpbmVkJmJlYz1mYWxzZQ==");
    }

    public testToUrlData20() : void {
        const imagears : ImageOutputArgs = new ImageOutputArgs();
        imagears.setSize(20, 40);
        imagears.FillEnabled(false);
        imagears.CornerOnEnvelop(false);
        imagears.Corners(30, 60, 30, 60);
        imagears.CropDimension(90, 90, 90, 90);
        imagears.WaterMarkEnabled();
        assert.equal(imagears.ToUrlData(),
            "Jnc9MjAmaD00MCZmPWZhbHNlJmN0bD0zMCZjdHI9NjAmY2JsPTMwJmNicj02MCZjZT1mYWxzZSZjdHg9OTAmY3R5PTkwJmNieD05MCZj" +
            "Ynk9OTAmd3Q9MSZ3cz11bmRlZmluZWQmYmVjPWZhbHNl");
    }

    public __IgnoretestToUrlData30() : void {
        const imagears : ImageOutputArgs = new ImageOutputArgs();
        imagears.Zoom(23);
        imagears.Rotation(180);
        imagears.BlurEnabled(true);
        imagears.GrayscaleEnabled(true);
        imagears.InvertEnabled(true);
        imagears.Brightness(100);
        imagears.Contrast(100);
        imagears.WaterMarkEnabled(true);
        imagears.Quality(90);
        imagears.FrontEndCacheEnabled(false);
        imagears.BackEndCacheEnabled(true);
        imagears.ExpireTime("Sunday, May 07, 2017 12:10 PM");
        assert.equal(imagears.ToUrlData(),
            "Jno9MTIzJnI9MTgwJmI9dHJ1ZSZnPXRydWUmaT10cnVlJmJyPTEwMCZjdD0xMDAmd2U9dHJ1ZSZ3dD0xJndzPXVuZGVmaW5lZCZxPTkwJmZlYz1mY" +
            "WxzZSZlPVN1biwgMDcgTWF5IDIwMTcgMTA6MTA6MDAgR01U");
    }
}
