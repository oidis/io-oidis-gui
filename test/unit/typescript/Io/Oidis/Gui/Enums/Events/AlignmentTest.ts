/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Alignment } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Alignment.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class AlignmentTest extends UnitTestRunner {
    public testgetPrefix() : void {
        assert.equal(Alignment.getPrefix(), "Align");
    }

    public testNormalizedValue() : void {
        const alignment : Alignment = new Alignment();
        assert.equal(Alignment.Normalize(alignment), "TopLeft");
        assert.equal(Alignment.Normalize(undefined), "TopLeft");
        assert.equal(Alignment.Normalize(Alignment.CENTER_PROPAGATED), "Center");
        assert.equal(Alignment.Normalize(Alignment.BOTTOM_LEFT), "BottomLeft");
    }

    public testClassSelectorName() : void {
        const alignment : Alignment = new Alignment();
        assert.equal(Alignment.StyleClassName(alignment), "TopLeft");
        assert.equal(Alignment.StyleClassName(Alignment.BOTTOM_LEFT), "AlignBottomLeft");
        assert.equal(Alignment.StyleClassName(undefined), "TopLeft");
    }

    public testIsPropagated() : void {
        const alignment : Alignment = new Alignment();
        assert.equal(Alignment.IsPropagated(Alignment.CENTER), false);
        assert.equal(Alignment.IsPropagated(alignment), false);
        assert.equal(Alignment.IsPropagated(undefined), false);
    }
}
