/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { UnitTestLoader, UnitTestRunner } from "./UnitTestRunner.js";

export class LoadTest extends UnitTestRunner {
    public testLoad() : void {
        UnitTestLoader.Load(<any>{
            build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-gui", version: "1.0.0"
        });
        assert.equal(this.getEnvironmentArgs().getProjectName(), "com-wui-framework-gui");
        this.initSendBox();
    }
}
