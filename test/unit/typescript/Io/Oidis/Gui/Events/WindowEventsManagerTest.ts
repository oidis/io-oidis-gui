/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Events/EventType.js";
import { EventsManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/EventsManager.js";
import { WindowEventsManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/WindowEventsManager.js";
import {
    IAsyncRequestEventsHandler
} from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IAsyncRequestEventsHandler.js";
import { IErrorEventsHandler } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IErrorEventsHandler.js";
import {
    IHttpRequestEventsHandler
} from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IHttpRequestEventsHandler.js";
import { IKeyEventsHandler } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IKeyEventsHandler.js";
import { IMessageEventsHandler } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IMessageEventsHandler.js";
import { IMouseEventsHandler } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IMouseEventsHandler.js";
import { IMoveEventsHandler } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IMoveEventsHandler.js";
import { IResizeEventsHandler } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IResizeEventsHandler.js";
import { IScrollEventsHandler } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IScrollEventsHandler.js";
import { IEventsHandler } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/IEventsHandler.js";

export class WindowEventsManagerTest extends UnitTestRunner {

    public __IgnoretestConstructor() : void {
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        assert.deepEqual(windowmanager.getAll().Length(), 1);
    }

    public testExists() : void {
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        assert.equal(windowmanager.Exists("set_up"), false);
        assert.equal(windowmanager.Exists("set_up"), false);
    }

    public testsetEvent() : void {
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        const handler : IEventsHandler = () : void => {
            LogIt.Debug("this is event test");
        };
        windowmanager.setEvent("baseGuiObject", handler);
        assert.equal(windowmanager.Exists("baseGuiObject"), true);
        EventsManager.getInstanceSingleton().Clear("body", "baseGuiObject");
    }

    public __IgnoretestgetAll() : void {
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        assert.equal(windowmanager.getAll().Length(), 2);
    }

    public testFireAsynchronousMethod() : void {
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        const handler : any = () : void => {
            LogIt.Debug("handler script");
        };
        windowmanager.FireAsynchronousMethod(handler, true);
        windowmanager.FireAsynchronousMethod(handler, 1);
        windowmanager.FireAsynchronousMethod(handler, false);
    }

    public testRemoveHandler() : void {
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        const handler : any = () : void => {
            LogIt.Debug("handler script");
        };
        windowmanager.setEvent("onblur", handler);
        windowmanager.setEvent("onload", handler);
        assert.equal(windowmanager.Exists("onload"), true);
        windowmanager.RemoveHandler("onload", handler);
        assert.equal(windowmanager.Exists("onload"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onblur");
        EventsManager.getInstanceSingleton().Clear("body", "onload");
    }

    public testClear() : void {
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        const handler : any = () : void => {
            LogIt.Debug("handler script");
        };
        windowmanager.setEvent("onblur", handler);
        assert.equal(windowmanager.Exists("onblur"), true);
        windowmanager.Clear("onblur");
        assert.equal(windowmanager.Exists("onblur"), false);
    }

    public __IgnoretestToString() : void {
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        assert.equal(windowmanager.ToString("", false),
            "Registered events list:\r\n" +
            "    [\"baseGuiObject\"] hooked handlers count: 1\r\n" +
            "    [\"onload\"] hooked handlers count: 0\r\n");
    }

    public __IgnoretestToString20() : void {
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        assert.equal(windowmanager.ToString("", true),
            "<b>Registered events list:</b><br/>&nbsp;&nbsp;&nbsp;" +
            "[\"baseGuiObject\"] hooked handlers count: 1<br/>&nbsp;&nbsp;&nbsp;" +
            "[\"onload\"] hooked handlers count: 0<br/>");
    }

    public __IgnoretesttoString() : void {
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        assert.equal(windowmanager.toString(),
            "<b>Registered events list:</b><br/>&nbsp;&nbsp;&nbsp;" +
            "[\"baseGuiObject\"] hooked handlers count: 1<br/>&nbsp;&nbsp;&nbsp;" +
            "[\"onload\"] hooked handlers count: 0<br/>");
    }

    public testsetOnError() : void {
        const handler : IErrorEventsHandler = () : void => {
            LogIt.Debug("this is event test error");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnError(handler);
        assert.equal(windowmanager.Exists("onerror"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onerror");
    }

    public testsetBeforeRefresh() : void {
        const handler : IEventsHandler = () : void => {
            LogIt.Debug("this is event test refresh");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setBeforeRefresh(handler);
        assert.equal(windowmanager.Exists("beforerefresh"), true);
        EventsManager.getInstanceSingleton().Clear("body", "beforerefresh");
    }

    public testsetOnLoad() : void {
        const handler : IEventsHandler = () : void => {
            LogIt.Debug("this is event test onload");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnLoad(handler);
        assert.equal(windowmanager.Exists("onload"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onload");
    }

    public testsetOnBlur() : void {
        const handler : IEventsHandler = () : void => {
            LogIt.Debug("this is event test onblur");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnBlur(handler);
        assert.equal(windowmanager.Exists("onblur"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onblur");
    }

    public testsetOnFocus() : void {
        const handler : IEventsHandler = () : void => {
            LogIt.Debug("this is event test onfocus");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnFocus(handler);
        assert.equal(windowmanager.Exists("onfocus"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onfocus");
    }

    public testsetOnClick() : void {
        const handler : IMouseEventsHandler = () : void => {
            LogIt.Debug("this is event test onclick");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnClick(handler);
        assert.equal(windowmanager.Exists("onclick"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onclick");
    }

    public testsetOnRightClick() : void {
        const handler : IMouseEventsHandler = () : void => {
            LogIt.Debug("this is event test onrightclick");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnRightClick(handler);
        assert.equal(windowmanager.Exists("onrightclick"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onrightclick");
    }

    public testsetOnDoubleClick() : void {
        const handler : IMouseEventsHandler = () : void => {
            LogIt.Debug("this is event test ondblclick");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnDoubleClick(handler);
        assert.equal(windowmanager.Exists("ondblclick"), true);
        EventsManager.getInstanceSingleton().Clear("body", "ondblclick");
    }

    public testsetOnMouseOver() : void {
        const handler : IMouseEventsHandler = () : void => {
            LogIt.Debug("this is event test ondblclick");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnMouseOver(handler);
        assert.equal(windowmanager.Exists("onmouseover"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onmouseover");
    }

    public testsetOnMouseOut() : void {
        const handler : IMouseEventsHandler = () : void => {
            LogIt.Debug("this is event test onmouseout");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnMouseOut(handler);
        assert.equal(windowmanager.Exists("onmouseout"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onmouseout");
    }

    public testsetOnMove() : void {
        const handler : IMoveEventsHandler = () : void => {
            LogIt.Debug("this is event onchange");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setEvent(EventType.ON_MOUSE_MOVE, handler);
        windowmanager.setOnMove(handler);
        assert.equal(windowmanager.Exists(EventType.ON_MOUSE_MOVE), true);
        EventsManager.getInstanceSingleton().Clear("mousemove", "onmousemove");
        EventsManager.getInstanceSingleton().Clear("mousemove", "onchange");
    }

    public testsetOnMouseDown() : void {
        const handler : IMouseEventsHandler = () : void => {
            LogIt.Debug("this is event test onmousedown");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnMouseDown(handler);
        assert.equal(windowmanager.Exists(EventType.ON_MOUSE_DOWN), true);
        EventsManager.getInstanceSingleton().Clear("body", "onmousedown");
    }

    public testsetOnMouseUp() : void {
        const handler : IMouseEventsHandler = () : void => {
            LogIt.Debug("this is event test onmouseup");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnMouseUp(handler);
        assert.equal(windowmanager.Exists("onmouseup"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onmouseup");
    }

    public testsetOnKeyDown() : void {
        const handler : IKeyEventsHandler = () : void => {
            LogIt.Debug("this is event test onkeydown");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnKeyDown(handler);
        assert.equal(windowmanager.Exists("onkeydown"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onkeydown");
    }

    public testsetOnKeyUp() : void {
        const handler : IKeyEventsHandler = () : void => {
            LogIt.Debug("this is event test onkeyup");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnKeyUp(handler);
        assert.equal(windowmanager.Exists("onkeyup"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onkeyup");
    }

    public testsetOnKeyPress() : void {
        const handler : IKeyEventsHandler = () : void => {
            LogIt.Debug("this is event test onkeypress");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnKeyPress(handler);
        assert.equal(windowmanager.Exists("onkeypress"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onkeypress");

    }

    public testsetOnResize() : void {
        const handler : IResizeEventsHandler = () : void => {
            LogIt.Debug("this is event test onresize");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnResize(handler);
        assert.equal(windowmanager.Exists("onresize"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onresize");
    }

    public testsetOnScroll() : void {
        const handler : IScrollEventsHandler = () : void => {
            LogIt.Debug("this is event test onscroll");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnScroll(handler);
        assert.equal(windowmanager.Exists("onscroll"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onscroll");
    }

    public testsetOnRequest() : void {
        const handler : IHttpRequestEventsHandler = () : void => {
            LogIt.Debug("this is event test onhttprequest");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnRequest(handler);
        assert.equal(windowmanager.Exists("onhttprequest"), true);
        this.initSendBox();
    }

    public testsetOnAsyncRequest() : void {
        const handler : IAsyncRequestEventsHandler = () : void => {
            LogIt.Debug("this is event test onasyncrequest");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnAsyncRequest(handler);
        assert.equal(windowmanager.Exists("onasyncrequest"), true);
        this.initSendBox();
    }

    public testsetOnMessage() : void {
        const handler : IMessageEventsHandler = () : void => {
            LogIt.Debug("this is event test onmessage");
        };
        const windowmanager : WindowEventsManager = new WindowEventsManager();
        windowmanager.setOnMessage(handler);
        assert.equal(windowmanager.Exists("onmessage"), true);
        EventsManager.getInstanceSingleton().Clear("body", "onmessage");
    }
}
