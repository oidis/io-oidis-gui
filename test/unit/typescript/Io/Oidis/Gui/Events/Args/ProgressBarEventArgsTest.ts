/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { DirectionType } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Enums/DirectionType.js";
import { ProgressBarEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/ProgressBarEventArgs.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class ProgressBarEventArgsTest extends UnitTestRunner {
    public testDirectionType() : void {
        const barevent : ProgressBarEventArgs = new ProgressBarEventArgs();
        assert.equal(barevent.DirectionType(DirectionType.LEFT), "Left");
        const barevent2 : ProgressBarEventArgs = new ProgressBarEventArgs();
        assert.equal(barevent2.DirectionType(), "Up");
    }

    public testPercentage() : void {
        const barevent : ProgressBarEventArgs = new ProgressBarEventArgs();
        assert.equal(barevent.Percentage(65), 65);
    }
}
