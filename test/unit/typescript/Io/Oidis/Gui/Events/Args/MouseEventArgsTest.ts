/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { MouseEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class MouseEventArgsTest extends UnitTestRunner {
    public testConstructor() : void {
        const mouse : MouseEventArgs = new MouseEventArgs();
        assert.equal(mouse.getClassName(), "Io.Oidis.Gui.Events.Args.MouseEventArgs");
    }
}
