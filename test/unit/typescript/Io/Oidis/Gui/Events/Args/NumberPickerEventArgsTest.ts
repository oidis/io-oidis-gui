/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { NumberPickerEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/NumberPickerEventArgs.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class NumberPickerEventArgsTest extends UnitTestRunner {
    public testRangeStart() : void {
        const numberpicker : NumberPickerEventArgs = new NumberPickerEventArgs();
        assert.equal(numberpicker.RangeStart(58), 58);
        const numberpicker2 : NumberPickerEventArgs = new NumberPickerEventArgs();
        assert.equal(numberpicker2.RangeStart(), 0);
    }

    public testRangeEnd() : void {
        const numberpicker : NumberPickerEventArgs = new NumberPickerEventArgs();
        assert.equal(numberpicker.RangeEnd(78), 78);
        const numberpicker2 : NumberPickerEventArgs = new NumberPickerEventArgs();
        assert.equal(numberpicker2.RangeEnd(), 100);
    }

    public testCurrentValue() : void {
        const numberpicker : NumberPickerEventArgs = new NumberPickerEventArgs();
        assert.equal(numberpicker.CurrentValue(56), 56);
        const numberpicker2 : NumberPickerEventArgs = new NumberPickerEventArgs();
        assert.equal(numberpicker2.CurrentValue(), 0);
    }

    public testPercentage() : void {
        const numberpicker : NumberPickerEventArgs = new NumberPickerEventArgs();
        assert.equal(numberpicker.Percentage(50), 50);
        const numberpicker2 : NumberPickerEventArgs = new NumberPickerEventArgs();
        assert.equal(numberpicker2.Percentage(), 0);
    }
}
