/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ViewerManagerEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/ViewerManagerEventArgs.js";
import { BasePanel } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanel.js";
import { BaseViewer } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { BaseViewerArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class ViewerManagerEventArgsTest extends UnitTestRunner {
    public testViewerClass() : void {
        const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        assert.equal(viewermanager.ViewerClass(), null);
        const object : BaseViewer = new BaseViewer();
        assert.equal(viewermanager.ViewerClass(object), object);
    }

    public testViewerArgs() : void {
        const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        const args : BaseViewerArgs = new BaseViewerArgs();
        assert.equal(viewermanager.ViewerArgs(args), args);
        const viewermanager2 : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        assert.equal(viewermanager2.ViewerArgs(), null);
    }

    public testChildElement() : void {
        const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        const gui : BasePanel = new BasePanel();
        assert.equal(viewermanager.ChildElement(gui), gui);
        const viewermanager2 : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        assert.equal(viewermanager2.ChildElement(), null);
        this.initSendBox();
    }

    public testReloadCache() : void {
        const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        assert.equal(viewermanager.ReloadCache(true), true);
    }

    public testTestMode() : void {
        const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        assert.equal(viewermanager.TestMode(true), true);
    }

    public testIncludeChildren() : void {
        const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        assert.equal(viewermanager.IncludeChildren(true), true);
    }

    public testAsyncCallback() : void {
        const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        assert.equal(viewermanager.AsyncCallback(false), false);
    }

    public testResult() : void {
        const viewermanager : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        const baseviewer : BaseViewer = new BaseViewer();
        assert.equal(viewermanager.Result(baseviewer), baseviewer);
        const viewermanager2 : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        assert.equal(viewermanager2.Result(), null);
    }
}
