/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ResizeableType } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Enums/ResizeableType.js";
import { ResizeBarEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/ResizeBarEventArgs.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class ResizeBarEventArgsTest extends UnitTestRunner {
    public testConstructor() : void {
        const resize : ResizeBarEventArgs = new ResizeBarEventArgs();
        assert.equal(resize.DistanceX(25), 25);
        assert.equal(resize.DistanceY(65), 65);
    }

    public testNativeEventArgs() : void {
        const mouseEventInit : any = {screenX: 20, screenY: 40, clientX: 50, clientY: 60, button: 2, buttons: 4};
        const mouseEvent : any = {typeArgs: "onclick", eventInitDict: mouseEventInit};
        const resize : ResizeBarEventArgs = new ResizeBarEventArgs();
        assert.equal(resize.NativeEventArgs(mouseEvent), mouseEvent);
    }

    public testResizeableType() : void {
        const resize : ResizeBarEventArgs = new ResizeBarEventArgs();
        assert.equal(resize.ResizeableType("XY"), ResizeableType.HORIZONTAL_AND_VERTICAL);
        const resize2 : ResizeBarEventArgs = new ResizeBarEventArgs();
        assert.equal(resize2.ResizeableType(), undefined);
    }
}
