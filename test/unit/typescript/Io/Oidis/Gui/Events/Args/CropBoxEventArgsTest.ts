/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { CropBoxEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/CropBoxEventArgs.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class CropBoxEventArgsTest extends UnitTestRunner {
    public testOffsetLeft() : void {
        const cropbox : CropBoxEventArgs = new CropBoxEventArgs();
        assert.equal(cropbox.OffsetLeft(), null);
        assert.equal(cropbox.OffsetLeft(100), 100);
    }

    public testOffsetTop() : void {
        const cropbox : CropBoxEventArgs = new CropBoxEventArgs();
        assert.equal(cropbox.OffsetTop(), null);
        assert.equal(cropbox.OffsetTop(100), 100);
    }
}
