/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { DirectoryBrowserEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/DirectoryBrowserEventArgs.js";
import { BasePanel } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanel.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class DirectoryBrowserEventArgsTest extends UnitTestRunner {
    public testOwner() : void {
        const directory : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
        const owner : any = new BasePanel();
        directory.Owner(owner);
        assert.equal(directory.Owner(), owner);
        this.initSendBox();
    }

    public testValue() : void {
        const directory : DirectoryBrowserEventArgs = new DirectoryBrowserEventArgs();
        assert.equal(directory.Value("string"), "string");
    }
}
