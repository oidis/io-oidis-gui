/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ResizeEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/ResizeEventArgs.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class ResizeEventArgsTest extends UnitTestRunner {
    public testWidth() : void {
        const resizeevent : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent.Width(50), 50);
        const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent2.Width(), null);
    }

    public testHeight() : void {
        const resizeevent : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent.Height(100), 100);
        const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent2.Height(), null);
    }

    public testAvailableWidth() : void {
        const resizeevent : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent.AvailableWidth(120), 120);
        const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent2.AvailableWidth(), null);
    }

    public testAvailableHeight() : void {
        const resizeevent : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent.AvailableHeight(120), 120);
        const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent2.AvailableHeight(), null);
    }

    public testScrollBarWidth() : void {
        const resizeevent : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent.ScrollBarWidth(120), 120);
        const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent2.ScrollBarWidth(), null);
    }

    public testScrollBarHeight() : void {
        const resizeevent : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent.ScrollBarHeight(120), 120);
        const resizeevent2 : ResizeEventArgs = new ResizeEventArgs();
        assert.equal(resizeevent2.ScrollBarHeight(), null);
    }
}
