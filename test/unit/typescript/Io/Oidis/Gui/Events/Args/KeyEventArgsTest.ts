/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { KeyEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { UnitTestRunner } from "../../UnitTestRunner.js";

export class KeyEventArgsTest extends UnitTestRunner {
    public testConstructor() : void {
        const keyevent : KeyEventArgs = new KeyEventArgs();
        assert.equal(keyevent.getKeyChar(), null);
    }

    public testgetKeyCode() : void {
        const keyboardevent : any = {altKey: true, char: "H", ctrlKey: true, key: "D5", keyCode: 38};
        const keyevent : KeyEventArgs = new KeyEventArgs(keyboardevent);
        assert.equal(keyevent.getKeyCode(), 38);
    }

    public testgetKeyChar() : void {
        const keyboard : any = {altKey: true, char: "T", ctrlKey: true, keyCode: 0};
        const keyevent : KeyEventArgs = new KeyEventArgs(keyboard);
        assert.equal(keyevent.getKeyChar(), null);
        const keyboard2 : any = {altKey: true, char: "T", charCode: 5, ctrlKey: true, keyCode: 2};
        const keyevent2 : KeyEventArgs = new KeyEventArgs(keyboard2);
        assert.equal(keyevent2.getKeyChar(), "\u0002");
        const keyboard3 : any = {altKey: true, char: "T", charCode: 0, ctrlKey: true};
        const keyevent3 : KeyEventArgs = new KeyEventArgs(keyboard3);
        assert.equal(keyevent3.getKeyChar(), "\u0000");
        const keyboard4 : any = {
            altKey: true, char: "H", charCode: 219, ctrlKey: true, key: "1212", keyCode: 2,
            locale: "", location: 3, metaKey: true, repeat: false, shiftKey: true, which: 0
        };
        const keyevent4 : KeyEventArgs = new KeyEventArgs(keyboard4);
        assert.equal(keyevent4.getKeyChar(), "\u0002");
        const keyboard5 : any = {altKey: true, char: "5", charCode: 101, ctrlKey: true};
        const keyevent5 : KeyEventArgs = new KeyEventArgs(keyboard5);
        assert.equal(keyevent5.getKeyChar(), "5");
    }
}
