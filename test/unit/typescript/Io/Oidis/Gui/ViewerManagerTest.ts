/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { HttpRequestConstants } from "../../../../../../source/typescript/Io/Oidis/Gui/Enums/HttpRequestConstants.js";
import { ViewerManagerEventArgs } from "../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/ViewerManagerEventArgs.js";
import { GuiObjectManager } from "../../../../../../source/typescript/Io/Oidis/Gui/GuiObjectManager.js";
import { BasePanel } from "../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanel.js";
import { BasePanelViewer } from "../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { BaseViewer } from "../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { BaseViewerArgs } from "../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { FormsObject } from "../../../../../../source/typescript/Io/Oidis/Gui/Primitives/FormsObject.js";
import { GuiCommons } from "../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ViewerManager } from "../../../../../../source/typescript/Io/Oidis/Gui/ViewerManager.js";
import { UnitTestRunner } from "./UnitTestRunner.js";

class MockGuiCommons extends GuiCommons {
}

class MockFormsObject extends FormsObject {
}

export class ViewerManagerTest extends UnitTestRunner {
    private manager : ViewerManager;

    public testExists() : void {
        const args : BaseViewerArgs = new BaseViewerArgs();
        const viewers : BaseViewer = new BaseViewer(args);
        const basepanelviewers : BasePanelViewer = new BasePanelViewer();
        const element : GuiCommons = new MockGuiCommons("id41");
        const manager : ViewerManager = new ViewerManager("viewers", element, args);
        assert.equal(element.InstanceOwner(viewers), viewers);
        assert.equal(ViewerManager.Exists(manager.getId()), false);
        this.initSendBox();
    }

    public testgetLoadedViewers() : void {
        const args : BaseViewerArgs = new BaseViewerArgs();
        const viewers : BaseViewer = new BaseViewer(args);
        const basepanelviewers : BasePanelViewer = new BasePanelViewer();
        const element : GuiCommons = new MockGuiCommons("id41");
        const panel : BasePanel = new BasePanel("id42");
        assert.equal(panel.InstanceOwner(basepanelviewers), basepanelviewers);
        this.resetCounters();
        assert.deepEqual(ViewerManager.getLoadedViewers().toString(),
            "<i>Io.Oidis.Commons.Primitives.ArrayList object</i> " +
            "<span onclick=\"" +
            "document.getElementById(\'ContentBlock_0\').style.display=" +
            "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
            "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
            "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>");
        this.initSendBox();
    }

    public testReloadCacheEnabled() : void {
        const args : BaseViewerArgs = new BaseViewerArgs();
        const viewers : BaseViewer = new BaseViewer(args);
        const basepanelviewers : BasePanelViewer = new BasePanelViewer();
        const element : GuiCommons = new MockGuiCommons("id41");
        const panel : BasePanel = new BasePanel("id43");
        assert.equal(panel.InstanceOwner(basepanelviewers), basepanelviewers);
        const manager : ViewerManager = new ViewerManager("viewers", element, args);
        assert.equal(manager.ReloadCacheEnabled(), false);
        assert.equal(manager.ReloadCacheEnabled(true), true);
        this.initSendBox();
    }

    public __IgnoretestClearCache() : void {
        const args : BaseViewerArgs = new BaseViewerArgs();
        const viewers : BaseViewer = new BaseViewer(args);
        const basepanelviewers : BasePanelViewer = new BasePanelViewer();
        const element : GuiCommons = new MockGuiCommons("id41");
        const panel : BasePanel = new BasePanel("id43");
        assert.equal(element.InstanceOwner(viewers), viewers);
        assert.equal(panel.InstanceOwner(basepanelviewers), basepanelviewers);
        const manager : ViewerManager = new ViewerManager("viewers", element, args);
        manager.ReloadCacheEnabled(true);
        manager.ClearCache(true);
        manager.Process();
        this.initSendBox();
    }

    public testTestModeEnabled() : void {
        const args : BaseViewerArgs = new BaseViewerArgs();
        const viewers : BaseViewer = new BaseViewer(args);
        viewers.TestModeEnabled(true);
        const element : GuiCommons = new MockGuiCommons("id44");
        assert.equal(element.InstanceOwner(viewers), viewers);
        const manager : ViewerManager = new ViewerManager("id44", element, args);
        manager.TestModeEnabled(true);
        assert.equal(manager.TestModeEnabled(), true);
        this.initSendBox();
    }

    public testIncludeChildren() : void {
        const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
        panelViewerArgs.AsyncEnabled(true);
        const viewer : BasePanelViewer = new BasePanelViewer(panelViewerArgs);
        const panel : BasePanel = new BasePanel();
        panel.getChildPanelList().Add(viewer);
        const manager : ViewerManager = new ViewerManager(panel.Id(), panel, panelViewerArgs);
        manager.IncludeChildren(true);
        assert.equal(manager.IncludeChildren(), true);
        this.initSendBox();
    }

    public testAsyncCallback() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const form : FormsObject = new MockFormsObject();
            const form2 : FormsObject = new MockFormsObject();
            const gui : GuiCommons = new MockGuiCommons("id46");
            form.Parent(gui);
            form2.Parent(gui);
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            data.Add(form2, HttpRequestConstants.HIDE_CHILDREN);
            data.Add(form, HttpRequestConstants.ELEMENT_INSTANCE);
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form2);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);
            assert.equal(args.POST().getItem(HttpRequestConstants.ELEMENT_INSTANCE), form);
            this.initSendBox();
            $done();
        };
    }

    public testsetCacheFilePath() : void {
        const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
        panelViewerArgs.AsyncEnabled(true);
        const viewer : BasePanelViewer = new BasePanelViewer(panelViewerArgs);
        const panel : BasePanel = new BasePanel();
        panel.getChildPanelList().Add(viewer);
        assert.equal(panel.InstanceOwner(viewer), viewer);
        const manager : ViewerManager = new ViewerManager(panel.Id(), panel, panelViewerArgs);
        manager.setCacheFilePath("filePath");
        assert.equal(ViewerManager.Exists(manager.getId()), false);
        assert.equal(ViewerManager.Exists("filePath"), false);
        this.initSendBox();
    }

    public testgetId() : void {
        const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
        panelViewerArgs.AsyncEnabled(true);
        const viewer : BasePanelViewer = new BasePanelViewer(panelViewerArgs);
        viewer.TestModeEnabled(true);
        const panel : BasePanel = new BasePanel("id47");
        panel.getChildPanelList().Add(viewer);
        assert.equal(panel.InstanceOwner(viewer), viewer);
        const manager : ViewerManager = new ViewerManager(panel.Id(), panel, panelViewerArgs);
        manager.setCacheFilePath("filePath");
        manager.TestModeEnabled(true);
        manager.Process();
        assert.notEqual(manager.getId().toString(), "*T");
        this.initSendBox();
    }

    public __IgnoretestgetCache() : void {
        const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
        const viewer : BasePanelViewer = new BasePanelViewer(panelViewerArgs);
        viewer.TestModeEnabled(true);
        const panel : BasePanel = new BasePanel("id48");
        assert.equal(panel.InstanceOwner(viewer), viewer);
        const viewerManager : ViewerManager = new ViewerManager(panel.Id(), panel, panelViewerArgs);
        const handler : any = ($eventArgs : ViewerManagerEventArgs) : void => {
            const args : ViewerManagerEventArgs = new ViewerManagerEventArgs();
        };
        viewerManager.getCache(handler);
        this.initSendBox();
    }

    public __IgnoretestToString() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const args : BaseViewerArgs = new BaseViewerArgs();
            const viewers : BaseViewer = new BaseViewer(args);
            const element : GuiCommons = new MockGuiCommons("id49");
            assert.equal(element.ToString("", false), "Io.Oidis.Gui.Primitives.GuiCommons (id49)");
            const manager : ViewerManager = new ViewerManager(element.Id(), element, args);
            assert.equal(manager.ToString("", true), "");
            this.initSendBox();
            $done();
        };
    }

    public __IgnoretestgetCacheRawData() : void {
        const guiobject : GuiObjectManager = new GuiObjectManager();
        const args : BasePanelViewerArgs = new BasePanelViewerArgs();
        const viewers : BasePanelViewer = new BasePanelViewer(args);
        const element : BasePanel = new BasePanel("id50");
        element.InstanceOwner(viewers);
        const handler : any = () => {
            const str : string = "cacheData";
        };
        const manager : ViewerManager = new ViewerManager(element.Id(), element, args);
        manager.getCacheRawData();
    }
}
