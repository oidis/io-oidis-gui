/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { AbstractGuiObject } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/AbstractGuiObject.js";

class MockAbstractGuiObject extends AbstractGuiObject {

    public testData() : string[] {
        const object : AbstractGuiObject = new MockAbstractGuiObject();
        return this.excludeSerializationData();
    }

    public testCacheData() : string[] {
        const object : AbstractGuiObject = new MockAbstractGuiObject();
        return this.excludeCacheData();
    }
}

export class AbstractGuiObjectTest extends UnitTestRunner {

    public testConstructor() : void {
        const abstractgui : AbstractGuiObject = new AbstractGuiObject("12345");
        assert.equal(abstractgui.StyleAttributes("attributes"), "attributes");
        const guiobject : AbstractGuiObject = new AbstractGuiObject("id_333");
        assert.equal(guiobject.Id(), "id_333", "validate Id string");
    }

    public testNotification() : void {
        const abstractgui : AbstractGuiObject = new AbstractGuiObject("12345");
        assert.equal(abstractgui.Notification(), (<any>AbstractGuiObject).notification);
    }

    public testsetSize() : void {
        const guiobject : AbstractGuiObject = new AbstractGuiObject("id_333");
        guiobject.setSize(30, 60);
        assert.equal(guiobject.getSize().Height(60), 60);
        assert.equal(guiobject.getSize().Width(30), 30);
    }

    public testStyleAttributes() : void {
        const guiobject : AbstractGuiObject = new AbstractGuiObject("id2");
        guiobject.DisableAsynchronousDraw();
        guiobject.StyleAttributes("border-radius: 2px");
        assert.equal(guiobject.StyleAttributes(), "border-radius: 2px", "validate attributes");
        assert.equal(guiobject.ToString("", true),
            "Io.Oidis.Gui.Primitives.AbstractGuiObject (id2)");
    }

    public testAbstractGuiObject() : void {
        const element : AbstractGuiObject = new MockAbstractGuiObject("id543");
        element.DisableAsynchronousDraw();
        Echo.PrintCode(element.Draw());
        element.setSize(200, 200);
        element.StyleClassName("TestClassName");
        element.StyleAttributes("position: absolute;");
        element.AppendStyleAttributes("left: 100px;");
        Echo.Println("<i>After set element properties</i>");
        Echo.Println("<div style=\"width: 200px; height: 200px; position: relative;\">" + element.Draw() + "</div>");
        assert.patternEqual(element.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"id543_GuiWrapper\" guiType=\"GuiWrapper\" class=\"TestClassName\">\r\n" +
            "      <div id=\"id543\" class=\"AbstractGuiObject\" style=\"display: block;\">\r\n" +
            "         <div style=\"position: absolute; left: 100px; border: 1px solid red; color: red; height: 200px;" +
            " overflow-x: hidden; overflow-y: hidden; width: 200px;\">id543</div>\r\n" +
            "      </div>\r\n" +
            "   </div>\r\n" +
            "</div>");
    }

    public testAppendStyleAttributes() : void {
        const guiobject : AbstractGuiObject = new MockAbstractGuiObject("abstractGuiId");
        guiobject.DisableAsynchronousDraw();
        guiobject.AppendStyleAttributes("background-color: green");
        assert.patternEqual(guiobject.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">" +
            "\r\n   <div id=\"abstractGuiId_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n      <div id=\"abstractGuiId\" class=\"AbstractGuiObject\" style=\"display: block;\">" +
            "\r\n         <div style=\"background-color: green; border: 1px solid red; color: red; height: 20px; overflow-x: hidden;" +
            " overflow-y: hidden; width: 100px;\">abstractGuiId</div>" +
            "\r\n      </div>" +
            "\r\n   </div>" +
            "\r\n</div>");
    }

    public testAppendStyleAttributesSecond() : void {
        const guiobject : AbstractGuiObject = new MockAbstractGuiObject("abstractGuiId");
        guiobject.DisableAsynchronousDraw();
        guiobject.AppendStyleAttributes("background-color: green");
        guiobject.AppendStyleAttributes(null);
        guiobject.AppendStyleAttributes("");
        assert.equal(guiobject.StyleAttributes("background-color: green"), "background-color: green");
        assert.patternEqual(guiobject.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"abstractGuiId_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"abstractGuiId\" class=\"AbstractGuiObject\" style=\"display: block;\">\r\n" +
            "         <div style=\"background-color: green; border: 1px solid red; color: red; height: 20px; overflow-x: hidden;" +
            " overflow-y: hidden; width: 100px;\">abstractGuiId</div>\r\n" +
            "      </div>\r\n" +
            "   </div>\r\n" +
            "</div>");
    }

    public testSerialization() : void {
        const data : MockAbstractGuiObject = new MockAbstractGuiObject();
        assert.deepEqual(data.testData(), [
            "objectNamespace", "objectClassName", "options", "availableOptionsList",
            "parent", "owner", "guiPath", "visible", "enabled", "prepared", "completed", "interfaceClassName", "styleClassName",
            "containerClassName", "loaded", "asyncDrawEnabled", "contentLoaded", "waitFor", "outputEndOfLine", "innerHtmlMap",
            "events", "changed", "width", "height", "attributes"
        ]);
    }

    public testExclude() : void {
        const cachedata : MockAbstractGuiObject = new MockAbstractGuiObject();
        assert.deepEqual(cachedata.testCacheData(), [
            "options", "availableOptionsList", "events", "childElements",
            "waitFor", "cached", "prepared", "completed", "parent", "owner", "guiPath", "interfaceClassName", "styleClassName",
            "containerClassName", "innerHtmlMap", "loaded", "title", "changed", "notification", "attributes"
        ]);
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
