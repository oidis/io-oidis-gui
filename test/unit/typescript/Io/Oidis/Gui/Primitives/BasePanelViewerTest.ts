/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { ExceptionsManager } from "@io-oidis-commons/Io/Oidis/Commons/Exceptions/ExceptionsManager.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Events/GeneralEventOwner.js";
import { HttpRequestConstants } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/HttpRequestConstants.js";
import { PanelContentType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/PanelContentType.js";
import { EventsManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/EventsManager.js";
import { IBasePanel } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IBasePanel.js";
import { BasePanel } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanel.js";
import { BasePanelViewer } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { GuiCommons } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { GuiElement } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiElement.js";
import { ElementManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/ElementManager.js";
import { ViewerManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/ViewerManager.js";

class MockBasePanelViewer extends BasePanelViewer {
    constructor($args? : BasePanelViewerArgs) {
        super($args);
        this.setInstance(new BasePanel("id2"));
    }
}

class MockViewer extends BasePanelViewer {
}

class MockGuiCommons extends GuiCommons {
}

export class BasePanelViewerTest extends UnitTestRunner {

    public testConstructor() : void {
        const panelviewer : BasePanelViewerArgs = new BasePanelViewerArgs();
        const panel : BasePanelViewer = new BasePanelViewer(panelviewer);
        assert.equal(panel.getLayersType(), "Io.Oidis.Gui.Enums.BaseViewerLayerType");
    }

    public testShow() : void {
        const panelviewer : BasePanelViewer = new MockBasePanelViewer();
        const panel : IBasePanel = panelviewer.getInstance();

        assert.equal(panelviewer.Show(panel.Id()),
            "id2" +
            "<div id=\"id2_PanelEnvelop\" class=\"Panel\" style=\"display: block;\">id2" +
            "    <div class=\"IoOidisGuiPrimitives\">id2" +
            "       <div id=\"id2_GuiWrapper\" guiType=\"GuiWrapper\">id2" +
            "          <div id=\"id2\" class=\"BasePanel\" style=\"display: none;\"></div>id2" +
            "       </div>id2" +
            "    </div>id2" +
            "</div>");
        this.initSendBox();
    }

    public testPrepareImplementation() : void {
        const panelviewer : BasePanelViewerArgs = new BasePanelViewerArgs();
        panelviewer.AsyncEnabled(true);
        panelviewer.Visible(true);
        assert.equal(panelviewer.AsyncEnabled(), true);
        assert.equal(panelviewer.Visible(), true);
    }

    public testgetChildElement() : void {
        const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
        panelViewerArgs.AsyncEnabled(true);
        panelViewerArgs.Visible(true);
        const viewer : BasePanelViewer = new MockBasePanelViewer(panelViewerArgs);
        const viewer2 : BasePanelViewer = new MockBasePanelViewer(null);
        const panel : IBasePanel = viewer.getInstance();

        panel.ContentType(PanelContentType.WITH_ELEMENT_WRAPPER);
        panel.getChildPanelList().Add(viewer);
        panel.getChildPanelList().Add(viewer2);
        panel.IsPrepared();

        assert.deepEqual(panel.getChildPanelList().getItem(0), viewer);
        assert.deepEqual(panel.getChildPanelList().getItem(0).InstanceOwner(), null);
        assert.deepEqual(panel.getChildPanelList().getLast(), viewer2);
        this.initSendBox();
    }

    public __IgnoretestPrepareImplementationEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            panelViewerArgs.AsyncEnabled(true);
            const viewer : BasePanelViewer = new MockBasePanelViewer(panelViewerArgs);
            const panel : BasePanel = new BasePanel();
            const viewerManager : ViewerManager = new ViewerManager("viewer", panel, panelViewerArgs);
            panel.getChildPanelList().Add(viewer);
            panel.ContentType(PanelContentType.ASYNC_LOADER);
            Reflection.getInstance();

            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            data.Add(panel.Id(), HttpRequestConstants.ELEMENT_INSTANCE);
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);

            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE,
                ($eventArgs : AsyncRequestEventArgs) : void => {
                    assert.equal(args.POST().KeyExists(HttpRequestConstants.ELEMENT_INSTANCE), true);
                    assert.equal(args.POST().KeyExists(HttpRequestConstants.ELEMENT_INSTANCE), true);
                    this.initSendBox();
                    ExceptionsManager.Clear();
                    $done();
                });
            viewerManager.IncludeChildren(true);
            viewerManager.Process();
            viewer.PrepareImplementation();
            EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE);
        };
    }

    public testT() : void {
        const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
        panelViewerArgs.AsyncEnabled(true);
        const viewer : BasePanelViewer = new MockBasePanelViewer(panelViewerArgs);
        const panel : BasePanel = <BasePanel>BasePanel.getInstance();
        panel.getChildPanelList().Add(viewer);
        Reflection.getInstance();
        viewer.PrepareImplementation();
        this.initSendBox();
    }

    public __IgnoretestPrepareImplementation3() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panelViewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            const viewer : BasePanelViewer = new BasePanelViewer(panelViewerArgs);
            const panel : BasePanel = new BasePanel("id65");
            Reflection.getInstance();
            panel.getChildPanelList().Add(viewer);
            assert.onGuiComplete(panel,
                () : void => {
                    viewer.PrepareImplementation();
                },
                $done, viewer);
        };
    }

    public __IgnoretestPrepareImplementation4() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const viewerArgs : BasePanelViewerArgs = new BasePanelViewerArgs();
            viewerArgs.AsyncEnabled();
            const viewer : BasePanelViewer = new MockBasePanelViewer(viewerArgs);

            const panel : BasePanel = new BasePanel("id75");
            panel.getChildPanelList().Add(viewer);

            Reflection.getInstance();
            assert.onGuiComplete(panel,
                () : void => {
                    viewer.PrepareImplementation();
                },
                $done, viewer);
        };
    }

    public __IgnoretestgetInstance() : void {
        const args : BasePanelViewerArgs = new BasePanelViewerArgs();
        const panel : BasePanel = new BasePanel();
        const viewer : BasePanelViewer = new BasePanelViewer(args);
        panel.ContentType(PanelContentType.HIDDEN);
        viewer.PrepareImplementation();
    }

    public __IgnoretestShowSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const viewer : BasePanelViewer = new MockBasePanelViewer(new BasePanelViewerArgs());
            const panel : IBasePanel = viewer.getInstance();
            const gui : GuiElement = new GuiElement();
            panel.Visible(true);
            panel.Scrollable(true);
            Reflection.getInstance();
            ElementManager.setSize("id22", panel.Width(500), panel.Height(500));
            panel.ContentType(PanelContentType.WITHOUT_ELEMENT_WRAPPER);
            panel.getChildPanelList().Add(viewer);
            panel.getEvents().setOnComplete(() : void => {
                //  assert.equal(panel.Draw().toString(), "");
                $done();
            });
            viewer.Show();
            this.initSendBox();
        };
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
