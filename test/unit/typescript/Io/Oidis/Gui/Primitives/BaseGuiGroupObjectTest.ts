/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { GeneralCssNames } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiOptionType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { IToolTip } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Components/IToolTip.js";
import { IGuiElement } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { BaseGuiGroupObject } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseGuiGroupObject.js";
import { BaseViewer } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { FormsObject } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/FormsObject.js";
import { GuiCommons } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { BaseGuiGroupObjectArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/BaseGuiGroupObjectArgs.js";
import { Size } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/ElementManager.js";
import { StaticPageContentManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/StaticPageContentManager.js";

class MockBaseGuiGroupObjectArgs extends BaseGuiGroupObjectArgs {
}

class MockBaseGuiGroupObject extends BaseGuiGroupObject {
    public testCacheData() : string[] {
        return this.excludeCacheData();
    }

    public testElement<T>() : T {
        return this.getElement("50");
    }

    protected getTitleClass() : any {
        return MockTitle;
    }
}

class MockBaseGuiGroupObjectPlain extends BaseGuiGroupObject {

}

class MockFormsObject extends FormsObject {
}

class MockBaseGuiGroupObjectSecond extends MockBaseGuiGroupObject {
    protected registerGroup() : void {
        this.registerElement(new MockFormsObject("id51"), "input");
    }

    protected getInputElement() : FormsObject {
        return this.getElement <FormsObject>("input");
    }
}

class MockGuiCommons extends GuiCommons {
}

class MockBaseViewer extends BaseViewer {
}

class MockTitle extends GuiCommons implements IToolTip {
    private text : string;
    private guiType : any;

    public GuiType($toolTipType? : any) : any {
        return this.guiType = Property.String(this.guiType, $toolTipType);
    }

    public Text($value? : string) : string {
        return this.text = Property.String(this.text, $value);
    }
}

class MockFormsObject3 extends MockBaseGuiGroupObjectSecond {
    protected innerHtml() : IGuiElement {
        return this.addElement(this.Id() + "_Enabled").StyleClassName(GeneralCssNames.ON);
    }
}

class MockregisterElement extends MockBaseGuiGroupObjectSecond {
    public testregisterEl() : void {
        const gui : GuiCommons = new MockGuiCommons("id5");
        this.registerElement(gui, "width", 0);
        assert.deepEqual(this.getElement(0), gui);
        assert.deepEqual(this.getElement(4), null);
    }
}

class MockregisterElementNext extends MockBaseGuiGroupObjectSecond {
    public testregisterElement() : void {
        const form : FormsObject = new MockFormsObject();
        const form2 : FormsObject = new MockFormsObject();
        const gui : GuiCommons = new MockGuiCommons();
        const gui2 : GuiCommons = new MockGuiCommons();
        this.registerElement(form, "input");
        this.registerElement(form2);
        this.registerElement(gui, "input-flag", 5);
        this.registerElement(gui2, null, 3);
        assert.deepEqual(this.getElement("input-flag"), gui);
        assert.deepEqual(this.getElement("input"), form);
        assert.deepEqual(this.getElement(3), gui2);
        assert.deepEqual(this.getElement(8), null);
        assert.deepEqual(this.getElement("width"), null);
    }
}

export class BaseGuiGroupObjectTest extends UnitTestRunner {
    public testConstructor() : void {
        const groupsargs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObject(groupsargs, "666");
        let guigroup2 : BaseGuiGroupObject;
        let guigroup3 : BaseGuiGroupObject;
        assert.equal(guigroup.Id(), "666");
        assert.equal(guigroup.toString(), "Io.Oidis.Gui.Primitives.BaseGuiGroupObject (666)");
        assert.equal(guigroup2, guigroup3);
    }

    public testConfiguration() : void {
        const groupsargs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        groupsargs.Visible(false);
        groupsargs.Error(false);
        groupsargs.TitleText("WuiFramework");
        const args : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();

        args.Visible(false);
        const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObjectSecond(groupsargs, "666");
        guigroup.Enabled(true);
        (<any>GuiCommons).completed = true;
        guigroup.Visible(true);
        assert.equal(guigroup.Configuration(groupsargs), groupsargs);
    }

    public ConfigurationSecond() : IUnitTestRunnerPromise {
        const groupsargs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        groupsargs.TitleText("WuiFramework");
        groupsargs.Visible(false);
        const groupsargs2 : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        groupsargs2.TitleText("WuiFramework");
        groupsargs2.Visible(false);
        const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObjectSecond(groupsargs, "888");
        (<any>GuiCommons).completed = true;
        guigroup.InstanceOwner(new MockBaseViewer());
        StaticPageContentManager.BodyAppend(guigroup.Draw());
        StaticPageContentManager.Draw();

        return ($done : () => void) : void => {
            guigroup.getEvents().setOnComplete(() : void => {
                guigroup.Configuration(groupsargs2);
                assert.throws(() : void => {
                    guigroup.Configuration(groupsargs2.Value("value"));
                }, /Configuration type can not be changed at run time./);
                $done();
            });
            guigroup.Visible(true);
        };
    }

    public testEneblad() : void {
        const groupsargs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObject(groupsargs, "666");
        assert.equal(guigroup.Enabled(true), true);
    }

    public testEnebladSecond() : void {
        const groupsargs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObject(groupsargs, "222");
        assert.equal(guigroup.Enabled(), true);
    }

    public testGuiGroupError() : void {
        const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObjectSecond(new MockBaseGuiGroupObjectArgs(), "777");
        guigroup.Error(true);
        assert.equal(guigroup.Error(), true);
    }

    public __IgnoretestValue() : void {
        const groupsargs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObjectSecond(groupsargs, "777");
        guigroup.InstanceOwner(new MockBaseViewer());
        StaticPageContentManager.BodyAppend(guigroup.Draw());
        StaticPageContentManager.Draw();
        guigroup.Enabled(true);
        guigroup.Visible(true);
        guigroup.Value("value");
        assert.deepEqual(guigroup.Value(), "value");
        guigroup.DisableAsynchronousDraw();
        assert.equal(guigroup.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"777_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"777\" class=\"BaseGuiGroupObject\" style=\"display: block;\">\r\n" +
            "         <div id=\"777_Envelop\" class=\"BaseGuiGroupObject\">\r\n" +
            "            <div class=\"IoOidisGuiPrimitives\">\r\n" +
            "               <div id=\"id51_GuiWrapper\" guiType=\"GuiWrapper\" class=\"UserControlInput\">\r\n" +
            "                  <div id=\"id51\" class=\"FormsObject\" style=\"display: none;\"></div>\r\n" +
            "               </div>\r\n" +
            "            </div>\r\n" +
            "         </div>\r\n" +
            "      </div>\r\n" +
            "   </div>\r\n" +
            "</div>");
        assert.equal(guigroup.Configuration().Value(10), 10);
    }

    public __IgnoretestgetWidth() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const groupsargs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObjectSecond(groupsargs, "545");
            ElementManager.setSize("545", guigroup.Width(200), guigroup.Height(200));
            StaticPageContentManager.BodyAppend(guigroup.Draw());
            StaticPageContentManager.Draw();
            guigroup.InstanceOwner(new MockBaseViewer());
            guigroup.DisableAsynchronousDraw();
            assert.equal(guigroup.getSize(), new Size());
            assert.equal(guigroup.Draw(),
                "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
                "   <div id=\"545_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                "      <div id=\"545\" class=\"BaseGuiGroupObject\" style=\"display: block;\">\r\n" +
                "         <div id=\"545_Envelop\" class=\"BaseGuiGroupObject\">\r\n" +
                "            <div class=\"IoOidisGuiPrimitives\">\r\n" +
                "               <div id=\"id51_GuiWrapper\" guiType=\"GuiWrapper\" class=\"UserControlInput\">\r\n" +
                "                  <div id=\"id51\" class=\"FormsObject\" style=\"display: none;\"></div>\r\n" +
                "               </div>\r\n" +
                "            </div>\r\n" +
                "         </div>\r\n" +
                "      </div>\r\n" +
                "   </div>\r\n" +
                "</div>"
            );

            //  assert.equal(guigroup.getSize().Width(), 200);
            this.initSendBox();
            $done();
        };
    }

    public __IgnoretestConfigurationNext() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const groupsargs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            groupsargs.Visible(false);
            groupsargs.TitleText("text");
            groupsargs.Value("testValue");
            const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObjectSecond(groupsargs, "454");
            guigroup.InstanceOwner(new MockBaseViewer());
            StaticPageContentManager.BodyAppend(guigroup.Draw());
            StaticPageContentManager.Draw();
            guigroup.Enabled(true);
            guigroup.Visible(true);
            guigroup.Value("VALUE");
            guigroup.Title().Text();
            guigroup.Changed();
            guigroup.Height(100);
            guigroup.DisableAsynchronousDraw();

            assert.onGuiComplete(guigroup,
                () : void => {
                    assert.patternEqual(guigroup.Draw(),
                        "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
                        "   <div id=\"454_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                        "      <div id=\"454\" class=\"BaseGuiGroupObject\" style=\"display: block;\">\r\n" +
                        "         <div id=\"454_Envelop\" class=\"BaseGuiGroupObject\">\r\n" +
                        "            <div class=\"IoOidisGuiPrimitives\">\r\n" +
                        "               <div id=\"id51_GuiWrapper\" guiType=\"GuiWrapper\" class=\"UserControlInput\">\r\n" +
                        "                  <div id=\"id51\" class=\"FormsObject\" style=\"display: none;\"></div>\r\n" +
                        "               </div>\r\n" +
                        "            </div>\r\n" +
                        "         </div>\r\n" +
                        "      </div>\r\n" +
                        "   </div>\r\n" +
                        "</div>"
                    );
                },
                () : void => {
                    assert.equal(guigroup.Configuration(), groupsargs);
                    this.initSendBox();
                    $done();
                });
        };
    }

    public __IgnoretestgetSize() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const groupsargs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
            groupsargs.Visible(false);
            groupsargs.TitleText("text");
            groupsargs.Value("testValue");
            const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObjectSecond(groupsargs, "323");
            StaticPageContentManager.BodyAppend(guigroup.Draw());
            StaticPageContentManager.Draw();
            guigroup.InstanceOwner(new MockBaseViewer());

            ElementManager.setSize("323", guigroup.Width(250), guigroup.Height(200));
            assert.equal(ElementManager.getElement("323").outerHTML,
                "<div id=\"323\" class=\"BaseGuiGroupObject\" style=\"display: block;\"></div>");
            $done();
            this.initSendBox();
        };
    }

    public testConfigurationNotCompleted() : void {
        const groupsargs : BaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        groupsargs.Visible(false);
        groupsargs.TitleText("text");
        groupsargs.Value("testValue");
        const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObjectSecond(groupsargs, "454");
        assert.throws(() : void => {
            guigroup.Configuration();
            throw new Error("Configuration type can not be changed at run time.");
        }, /Configuration type can not be changed at run time./);
    }

    public testexcludeCacheData() : void {
        const args : MockBaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const data : MockBaseGuiGroupObject = new MockBaseGuiGroupObject(args);
        assert.deepEqual(data.testCacheData(), [
            "options", "availableOptionsList", "events", "childElements",
            "waitFor", "cached", "prepared", "completed", "parent", "owner",
            "guiPath", "interfaceClassName", "styleClassName", "containerClassName",
            "innerHtmlMap", "visible", "loaded", "title", "changed", "notification",
            "selectorEvents", "tabIndex", "isHovered"
        ]);
    }

    public testregisterElement() : void {
        const args : MockBaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const register : MockregisterElement = new MockregisterElement(args, "id55");
        register.testregisterEl();
    }

    public testregisterElementNext() : void {
        const args : MockBaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const register : MockregisterElementNext = new MockregisterElementNext(args, "id6");
        register.testregisterElement();
    }

    public testgetElement() : void {
        const args : MockBaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        const data : MockBaseGuiGroupObject = new MockBaseGuiGroupObject(args);
        assert.equal(data.testElement(), null);
    }

    public testgetElementNext() : void {
        const args : MockBaseGuiGroupObjectArgs = new MockBaseGuiGroupObjectArgs();
        args.Value("value");
        args.TitleText("testofText");
        args.Visible(true);
        const data : MockBaseGuiGroupObject = new MockBaseGuiGroupObject(args, "65");
        ElementManager.setCssProperty("65", "width", 200);
        ElementManager.setCssProperty("65", "height", 400);
        ElementManager.setInnerHtml("65", "How are you?");
        ElementManager.setWidth("65" + "_Envelop", 200);
        ElementManager.getCssIntegerValue("65", "margin-left");
    }

    public testGuiGroupObject() : void {
        const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObject(
            new MockBaseGuiGroupObjectArgs(), "64");
        assert.equal(guigroup.Visible(true), true);
        assert.equal(guigroup.Value(true), null);
        assert.equal(guigroup.Error(), false);
        assert.equal(guigroup.Enabled(true), true);
        assert.equal(guigroup.IsCompleted(), false);
        assert.equal(guigroup.Configuration().TitleText("Text").toString(), "Text");
        assert.equal(guigroup.Configuration().Value("testValue").toString(), "testValue");
        guigroup.Configuration().AddGuiOption(GuiOptionType.ACTIVED);
        assert.equal(guigroup.Configuration().ForceSetValue(true).toString(), "true");
        assert.equal(guigroup.Configuration().Resize(true).toString(), "true");
    }

    public testVal() : void {
        const guigroup : BaseGuiGroupObject = new MockBaseGuiGroupObject(
            new MockBaseGuiGroupObjectArgs(), "64");
        assert.equal(guigroup.Visible(false), false);
        assert.equal(guigroup.Enabled(false), false);
        assert.equal(guigroup.Value(undefined), undefined);
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
