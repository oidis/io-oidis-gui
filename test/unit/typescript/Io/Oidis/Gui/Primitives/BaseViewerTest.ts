/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BaseViewerLayerType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/BaseViewerLayerType.js";
import { BaseViewer } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { BaseViewerArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";

export class BaseViewerTest extends UnitTestRunner {
    public __IgnoretestCallbackLink() : void {
        assert.equal(BaseViewer.CallbackLink(false),
            "#/com-wui-framework-builder/web/Io/Oidis/Gui/Primitives/BaseViewer");
        assert.equal(BaseViewer.CallbackLink(true), "" +
            "#/com-wui-framework-builder/web/Io/Oidis/Gui/Primitives" +
            "/BaseViewer/TestMode");
    }

    public testConstructor() : void {
        const viewArgs : BaseViewerArgs = new BaseViewerArgs();
        const view : BaseViewer = new BaseViewer(viewArgs);
        assert.equal(view.ToString(""), "Io.Oidis.Gui.Primitives.BaseViewer (instance NOT DEFINED)");
    }

    public testInstanceOwner() : void {
        const view : BaseViewer = new BaseViewer();
        assert.equal(view.InstanceOwner(), null);
    }

    public testIsCached() : void {
        const view : BaseViewer = new BaseViewer();
        assert.equal(view.IsCached(true), true);
    }

    public testIsPrinted() : void {
        const view : BaseViewer = new BaseViewer();
        assert.equal(view.IsPrinted(), false);
    }

    public testTestModeEnabled() : void {
        const view : BaseViewer = new BaseViewer();
        assert.equal(view.TestModeEnabled(false), false);
    }

    public testViewerArgs() : void {
        const viewArgs : BaseViewerArgs = new BaseViewerArgs();
        const view : BaseViewer = new BaseViewer();
        assert.equal(view.ViewerArgs(viewArgs), viewArgs);
    }

    public testgetLayersType() : void {
        const view : BaseViewer = new BaseViewer();
        assert.equal(view.getLayersType(), "Io.Oidis.Gui.Enums.BaseViewerLayerType");
    }

    public testShow() : void {
        const view : BaseViewer = new BaseViewer();
        assert.equal(view.Show(), "");
    }

    public testSerializationData() : void {
        const view : BaseViewer = new BaseViewer();
        assert.deepEqual(view.SerializationData(), {isCached: false, instance: null});
    }

    public testToString() : void {
        const view : BaseViewer = new BaseViewer();
        assert.equal(view.ToString(""), "Io.Oidis.Gui.Primitives.BaseViewer (instance NOT DEFINED)");
    }

    public testtoString() : void {
        const view : BaseViewer = new BaseViewer();
        assert.equal(view.toString(), "Io.Oidis.Gui.Primitives.BaseViewer (instance NOT DEFINED)");
    }

    public testsetLayer() : void {
        const layertype : BaseViewerLayerType = new BaseViewerLayerType();
        const view : BaseViewer = new BaseViewer();
        view.setLayer(layertype);
        assert.equal((<any>BaseViewer).layer, undefined);
    }
}
