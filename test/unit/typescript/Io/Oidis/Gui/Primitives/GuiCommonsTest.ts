/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GuiCommonsArgType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { GuiOptionType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { PositionType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/PositionType.js";
import { ElementEventsManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/ElementEventsManager.js";
import { EventsManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/EventsManager.js";
import { IGuiCommonsArg } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { BaseViewer } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { BaseViewerArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";
import { FormsObject } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/FormsObject.js";
import { GuiCommons } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { GuiElement } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiElement.js";
import { Size } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/ElementManager.js";

class MockGuiCommons extends GuiCommons {
}

class MockBaseViewer extends BaseViewer {
}

class MockFormObject extends FormsObject {
}

export class GuiCommonsTest extends UnitTestRunner {

    public __IgnoretestHide() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            EventsManager.getInstanceSingleton().setEvent("guicommons", EventType.ON_HIDE,
                ($eventArgs : EventArgs) : void => {
                    assert.equal($eventArgs.Owner, "guicommons");
                    assert.equal($eventArgs.Type, EventType.ON_HIDE);
                    $done();
                });
            const guicommons : GuiCommons = new MockGuiCommons("65432");
            GuiCommons.Hide(guicommons);
        };
    }

    public testHideSecond() : void {
        const gui : GuiCommons = new MockGuiCommons("id301");
        gui.DisableAsynchronousDraw();
        Echo.Print(gui.Draw() + "<div style=\"clear: both;\"></div>");
        GuiCommons.Hide(gui);
        gui.DisableAsynchronousDraw();
        assert.patternEqual(gui.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"id301_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id301\" class=\"GuiCommons\" style=\"display: block;\"></div>\r\n" +
            "   </div>\r\n" +
            "</div>");
        assert.equal(ElementManager.getClassName(gui.Id()), "GuiCommons");
    }

    public testgetInstance() : void {
        assert.equal(ObjectValidator.IsEmptyOrNull(GuiCommons.getInstance()), false);
        assert.equal(GuiCommons.getInstance().getClassName(), "Io.Oidis.Gui.Primitives.GuiCommons");
    }

    public __IgnoretestGuiElementClass() : void {
        assert.equal(GuiCommons.GuiElementClass(), (<any>GuiCommons).guiElementClass);

        const element : GuiElement = new GuiElement();
        assert.equal(GuiCommons.GuiElementClass(element), GuiElement);
    }

    public __IgnoretestShow() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            EventsManager.getInstanceSingleton().setEvent("guicommons2", EventType.ON_SHOW,
                ($eventArgs : EventArgs) : void => {
                    assert.equal($eventArgs.Owner, () : void => {
                        // default owner
                    });
                    assert.equal($eventArgs.Type, EventType.ON_SHOW);
                    EventsManager.getInstanceSingleton().Clear("guicommons2", "onshow");
                    $done();
                });
            EventsManager.getInstanceSingleton().FireEvent("guicommons2", EventType.ON_SHOW);
            const guicommons : GuiCommons = new MockGuiCommons("12345");
            GuiCommons.Show(guicommons);

            const guicommons2 : GuiCommons = new MockGuiCommons("45678");
            ElementManager.Show("45678");
        };
    }

    public testShowSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const gui : GuiCommons = new MockGuiCommons("id888");
            gui.DisableAsynchronousDraw();
            assert.patternEqual(gui.Draw(),
                "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
                "   <div id=\"id888_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                "      <div id=\"id888\" class=\"GuiCommons\" style=\"display: block;\"></div>\r\n" +
                "   </div>\r\n" +
                "</div>");
            GuiCommons.Show(gui);
            assert.equal(EventsManager.getInstanceSingleton().Exists("gui", EventType.ON_SHOW), false);
            $done();
        };
    }

    public testgetEvents() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.getEvents().IsTypeOf(ElementEventsManager), true);
    }

    public testgetGuiElementClass() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.getGuiElementClass(), (<any>GuiCommons).guiElementClass);
    }

    public testVisible() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.Visible(true), true);
        const guicommons2 : GuiCommons = new MockGuiCommons("54321");
        assert.equal(guicommons2.Visible(false), false);
        const guicommons3 : GuiCommons = new MockGuiCommons("33333");
        assert.equal(guicommons3.Visible(null), null);

        const guicommons4 : GuiCommons = new MockGuiCommons("99999");
        const gui1 : GuiCommons = new MockGuiCommons("99999");
        const gui2 : GuiCommons = new MockGuiCommons("99999");
        const gui3 : GuiCommons = new MockGuiCommons("99999");
        guicommons4.getChildElements().Add(gui1);
        guicommons4.getChildElements().Add(gui2);
        guicommons4.getChildElements().Add(gui3);

        assert.equal(guicommons4.Visible(), true);
    }

    public testEnabled() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.Enabled(true), true);
        const guicommons2 : GuiCommons = new MockGuiCommons("54321");
        assert.equal(guicommons2.Enabled(false), false);
        const guicommons3 : GuiCommons = new MockGuiCommons("33333");
        assert.equal(guicommons3.Enabled(null), true);
    }

    public testStyleClassName() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.StyleClassName("GuiCommons"), "GuiCommons");
        const guicommons2 : GuiCommons = new MockGuiCommons("54321");
        assert.equal(guicommons2.StyleClassName(), false);
        const guicommons3 : GuiCommons = new MockGuiCommons("33333");
        assert.equal(guicommons3.StyleClassName(""), "");
    }

    public testParent() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.Parent(guicommons), guicommons);
        const guicommons2 : GuiCommons = new MockGuiCommons("54321");
        const baseviewer : BaseViewer = new BaseViewer();
        guicommons2.InstanceOwner(baseviewer);
        assert.equal(guicommons2.Parent(guicommons2), guicommons2);
    }

    public testInstanceOwner() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        const baseviewer : BaseViewer = new BaseViewer();
        assert.equal(guicommons.InstanceOwner(baseviewer), baseviewer);
        const guicommons2 : GuiCommons = new MockGuiCommons("54321");
        const baseviewer2 : BaseViewer = new BaseViewer();
        guicommons2.Parent(guicommons2);
        assert.equal(guicommons2.InstanceOwner(baseviewer2), baseviewer2);
    }

    public testgetChildElements() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        const guicommons10 : GuiCommons = new MockGuiCommons("98765");
        const guicommons20 : GuiCommons = new MockGuiCommons("54789");
        const guicommons30 : GuiCommons = new MockGuiCommons("14253");
        guicommons.getChildElements().Add(guicommons10);
        guicommons.getChildElements().Add(guicommons20);
        guicommons.getChildElements().Add(guicommons30);
        assert.deepEqual(guicommons.getChildElements().getAll(), [guicommons10, guicommons20, guicommons30]);
    }

    public testInstancePath() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        const baseviewer : BaseViewer = new BaseViewer();
        guicommons.InstanceOwner(baseviewer);
        assert.equal(guicommons.InstancePath("instancePath"), "instancePath");
        const guicommons2 : GuiCommons = new MockGuiCommons("54321");
        assert.equal(guicommons2.InstancePath(), "Io.Oidis.Gui.Primitives.GuiCommons");
        const guicommons3 : GuiCommons = new MockGuiCommons("33333");
        assert.equal(guicommons3.InstancePath(null), "Io.Oidis.Gui.Primitives.GuiCommons");
    }

    public testsetPosition() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        guicommons.setPosition(20, 20, PositionType.ABSOLUTE);
        assert.equal(ElementManager.getCssValue("12345", "position"), null);
        guicommons.DisableAsynchronousDraw();
        Echo.Println(
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"12345_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"12345\" class=\"GuiCommons\" style=\"display: block;\"></div>\r\n" +
            "   </div>\r\n" +
            "</div>"
        );

        assert.equal(guicommons.getScreenPosition().Top(), 0);
        assert.equal(guicommons.getScreenPosition().Left(), 0);
    }

    public testIsCached() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.IsCached(), false);
    }

    public testIsPrepared() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.IsPrepared(), false);
    }

    public testIsLoaded() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.IsPrepared(), false);
    }

    public testIsCompleted() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.IsCompleted(), false);
    }

    public testDraw() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.Draw(), "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"12345_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"12345\" class=\"GuiCommons\" style=\"display: none;\"></div>\r\n" +
            "   </div>\r\n</div>");
    }

    public testgetInnerHtmlMap() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const guicommons : GuiCommons = new MockGuiCommons("12345");
            guicommons.Width(50);
            guicommons.setPosition(30, 120, PositionType.RELATIVE);
            guicommons.Enabled(true);
            guicommons.DisableAsynchronousDraw();
            assert.patternEqual(guicommons.Draw(),
                "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
                "   <div id=\"12345_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                "      <div id=\"12345\" class=\"GuiCommons\" style=\"display: block;\"></div>\r\n" +
                "   </div>\r\n</div>");
            assert.patternEqual(guicommons.getInnerHtmlMap().Draw(""), "");
            $done();
        };
    }

    public testsetWarpingElement() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12346");
        const form : FormsObject = new MockFormObject("id909");
        guicommons.setWrappingElement(null);
        assert.equal(guicommons.getWrappingElement(), null);
    }

    public testToString() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.ToString(""), "Io.Oidis.Gui.Primitives.GuiCommons (12345)");
        assert.equal(guicommons.ToString("", false), "Io.Oidis.Gui.Primitives.GuiCommons (12345)");
    }

    public testtoString() : void {
        const guicommons : GuiCommons = new MockGuiCommons("12345");
        assert.equal(guicommons.toString(), "Io.Oidis.Gui.Primitives.GuiCommons (12345)");
    }

    public testFitToParent() : void {
        const guicommons : GuiCommons = new MockGuiCommons("7896");
        assert.equal(guicommons.FitToParent("Full"), guicommons);
    }

    public testgetFitToParent() : void {
        const guicommons : GuiCommons = new MockGuiCommons("7899");
        guicommons.FitToParent("Full");
        assert.equal(guicommons.getFitToParent(), "Full");
    }

    public testsetArgs() : void {
        const guicommons : GuiCommons = new MockGuiCommons("2345");
        guicommons.setArg(<IGuiCommonsArg>{
            name : "Height",
            type : GuiCommonsArgType.NUMBER,
            value: 80
        }, true);

        guicommons.setArg(<IGuiCommonsArg>{
            name : "Width",
            type : GuiCommonsArgType.NUMBER,
            value: 160
        }, true);

        guicommons.setArg(<IGuiCommonsArg>{
            name : "Enabled",
            type : GuiCommonsArgType.BOOLEAN,
            value: true
        }, true);

        guicommons.setArg(<IGuiCommonsArg>{
            name : "Visible",
            type : GuiCommonsArgType.BOOLEAN,
            value: false
        }, false);

        guicommons.setArg(<IGuiCommonsArg>{
            name : "StyleClassName",
            type : GuiCommonsArgType.TEXT,
            value: "TestClass"
        }, true);

        guicommons.setArg(<IGuiCommonsArg>{
            name : "Top",
            type : GuiCommonsArgType.NUMBER,
            value: 20
        }, true);

        guicommons.setArg(<IGuiCommonsArg>{
            name : "Left",
            type : GuiCommonsArgType.NUMBER,
            value: 20
        }, true);
        assert.equal(guicommons.getArgs().length, 8);
    }

    public testConstructorSecond() : void {
        const element : GuiCommons = new MockGuiCommons();
        Echo.Println("<div class=\"IoOidisGuiPrimitives\"> " +
            "<div id=\"GuiCommons1488787840472650_GuiWrapper\" guiType=\"GuiWrapper\"> " +
            "<div id=\"GuiCommons1488787840472650\" class=\"GuiCommons\" style=\"display: none;\"></div> </div> </div>");
        assert.equal(element.Enabled(true), true);
        Echo.Println("<div class=\"IoOidisGuiPrimitives\"> " +
            "<div id=\"GuiCommons1488787840472650_GuiWrapper\" guiType=\"GuiWrapper\"> " +
            "<div id=\"GuiCommons1488787840472650\" class=\"GuiCommons\" style=\"display: none;\"></div> </div> </div>");
    }

    public testgetGuiElementClassSecond() : void {
        const element : GuiCommons = new MockGuiCommons();
        Echo.Println("<div class=\"IoOidisGuiPrimitives\"> " +
            "<div id=\"GuiCommons1488789078814758_GuiWrapper\" guiType=\"GuiWrapper\"> " +
            "<div id=\"GuiCommons1488789078814758\" class=\"GuiCommons\" style=\"display: none;\"></div> </div> </div>");
        assert.equal(ElementManager.getClassName(element), null);
    }

    public testVisibleSecond() : void {
        const element : GuiCommons = new MockGuiCommons("testId1");
        element.Width(100);
        element.Height(50);
        element.StyleClassName("Tooltip");
        element.DisableAsynchronousDraw();
        element.Visible(true);
        assert.equal(element.Visible(), true);
        assert.patternEqual(element.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"testId1_GuiWrapper\" guiType=\"GuiWrapper\" class=\"Tooltip\">\r\n" +
            "      <div id=\"testId1\" class=\"GuiCommons\" style=\"display: block;\"></div>\r\n" +
            "   </div>\r\n" +
            "</div>"
        );
        const element2 : GuiCommons = new MockGuiCommons("testId5");
        element2.Visible(false);
        assert.equal(element2.Visible(), false);
    }

    public testEnabledSecond() : void {
        const element : GuiCommons = new MockGuiCommons("testId7");
        Echo.Println("<div id=\"testId7\" class=\"testClass\">test element</div>");
        element.Enabled(true);
        assert.patternEqual(element.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"testId7_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"testId7\" class=\"GuiCommons\" style=\"display: none;\"></div>\r\n" +
            "   </div>\r\n" +
            "</div>");
        assert.deepEqual(element.Enabled(), true);
    }

    public testgetGuiOptionsSecond() : void {
        const element : GuiCommons = new MockGuiCommons("testId2");
        element.DisableAsynchronousDraw();
        element.getGuiOptions().Add(GuiOptionType.ALL);
        assert.equal(element.getGuiOptions().Contains(GuiOptionType.ALL), false);
        assert.patternEqual(element.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"testId2_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"testId2\" class=\"GuiCommons\" style=\"display: block;\"></div>\r\n" +
            "   </div>\r\n</div>");
        const element2 : GuiCommons = new MockGuiCommons("testId2");
        element2.DisableAsynchronousDraw();
        element2.getGuiOptions().Add(GuiOptionType.ALL);
        assert.patternEqual(element2.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"testId2_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"testId2\" class=\"GuiCommons\" style=\"display: block;\"></div>\r\n" +
            "   </div>\r\n</div>"
        );
    }

    public testParentSecond() : void {
        const elementparent : GuiCommons = new MockGuiCommons();
        const element : GuiCommons = new MockGuiCommons();
        assert.equal(element.Parent(elementparent), elementparent);
    }

    public testInstanceOwnerSecond() : void {
        const baseviewerargs : BaseViewerArgs = new BaseViewerArgs();
        const baseviewer : BaseViewer = new BaseViewer(baseviewerargs);
        const element : GuiCommons = new MockGuiCommons();
        assert.equal(element.InstanceOwner(baseviewer), baseviewer);
    }

    public testInstancePathSecond() : void {
        const element : GuiCommons = new MockGuiCommons("testId3");
        assert.equal(element.InstancePath("rootElement/ChildElement/SubChildElement/testId3"),
            "rootElement/ChildElement/SubChildElement/testId3");
    }

    public testgetChildElementsSecond() : void {
        const element : GuiCommons = new MockGuiCommons("testId9");
        const element2 : GuiCommons = new MockGuiCommons("testId10");
        const element3 : GuiCommons = new MockGuiCommons("testId11");
        element.getChildElements().Add(element2);
        element.getChildElements().Add(element3);
        const child : ArrayList<GuiCommons> = new ArrayList<GuiCommons>();
        child.Add(element2);
        child.Add(element3);
        assert.equal(element.getChildElements().Equal(child), true);
    }

    public testgetSize() : void {
        const element : GuiCommons = new MockGuiCommons("testId12");
        const origin : Size = element.getSize();
        const golden : Size = new Size();
        assert.equal(origin.Width(), golden.Width());
        assert.equal(origin.Height(), golden.Height());
    }

    public testsetArgSecond() : void {
        const element : GuiCommons = new MockGuiCommons("testId15");
        element.DisableAsynchronousDraw();
        element.Id();
        element.StyleClassName("ToolTip");
        element.Enabled(true);
        element.Visible(true);
        assert.patternEqual(
            ArrayList.ToArrayList(element.getArgs()).Equal(ArrayList.ToArrayList([
                {name: "Id", type: "Text", value: "testId15"},
                {name: "StyleClassName", type: "Text", value: "ToolTip"},
                {name: "Enabled", type: "Bool", value: true},
                {name: "Visible", type: "Bool", value: true},
                {name: "Width", type: "Number", value: 0},
                {name: "Height", type: "Number", value: 0},
                {name: "Top", type: "Number", value: 0},
                {name: "Left", type: "Number", value: 0}
            ])), false);
    }

    public testDraw2() : void {
        const element : GuiCommons = new MockGuiCommons("id300");
        element.DisableAsynchronousDraw();
        assert.patternEqual(element.Draw(),
            "\r\n<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"id300_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id300\" class=\"GuiCommons\" style=\"display: block;\"></div>\r\n" +
            "   </div>\r\n" +
            "</div>");
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
