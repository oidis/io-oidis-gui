/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Alignment } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Alignment.js";
import { DirectionType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/DirectionType.js";
import { FitToParent } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/FitToParent.js";
import { GeneralCssNames } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { GuiCommonsArgType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { GuiOptionType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { KeyMap } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/KeyMap.js";
import { OrientationType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/OrientationType.js";
import { PanelContentType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/PanelContentType.js";
import { UnitType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/UnitType.js";
import { KeyEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/KeyEventArgs.js";
import { MouseEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/MouseEventArgs.js";
import { ScrollEventArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/ScrollEventArgs.js";
import { GuiObjectManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/GuiObjectManager.js";
import { GuiOptionsManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/GuiOptionsManager.js";
import { IScrollBar } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Components/IScrollBar.js";
import { IToolTip } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Components/IToolTip.js";
import { ILabelEvents } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/ILabelEvents.js";
import { IScrollBarEvents } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IScrollBarEvents.js";
import { IGuiCommonsArg } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { IGuiElement } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { IResponsiveElement } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IResponsiveElement.js";
import { IIcon } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/UserControls/IIcon.js";
import { ILabel } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/UserControls/ILabel.js";
import { BaseGuiObject } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseGuiObject.js";
import { BasePanel } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanel.js";
import { BasePanelViewer } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { BasePanelViewerArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";
import { BaseViewer } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { FormsObject } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/FormsObject.js";
import { GuiCommons } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { GuiElement } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiElement.js";
import { PropagableNumber } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { Size } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/ElementManager.js";

class MockBasePanelViewer extends BasePanelViewer {
    constructor($args? : BasePanelViewerArgs) {
        super($args);
        this.setInstance(new BasePanel("id2"));
    }
}

class MockexcludeCacheData extends BasePanel {
    public testexcludeCacheData() : string[] {
        return this.excludeCacheData();
    }

    public testexcludeSerialization() : string[] {
        return this.excludeSerializationData();
    }
}

class MockBasePanel extends BasePanel {
    public testContent() : string {
        const panel : BasePanel = new BasePanel("44");
        ElementManager.setInnerHtml("44", this.guiContent().Draw("44"));
        return "";
    }

    protected getTitleClass() : any {
        return MockTitle;
    }

    protected getScrollBarClass() : any {
        return MockScrollBar;
    }

    protected getLoaderTextClass() : any {
        return MockLabel;
    }

    protected getLoaderIconClass() : any {
        return MockIcon;
    }

    protected innerCode() : IGuiElement {
        this.getEvents().setOnMouseOver(MockBasePanel.onHoverEventHandler);
        this.getEvents().setOnClick(MockBasePanel.onHoverEventHandler);
        return super.innerCode();
    }
}

class MockIcon extends FormsObject implements IIcon {
    private iconType : string;

    public IconType($type? : any) : any {
        return this.iconType = Property.String(this.iconType, $type);
    }
}

class MockLabel extends BaseGuiObject implements ILabel {
    private text : string;

    public Text($value? : string) : string {
        return this.text = Property.String(this.text, $value);
    }

    public getEvents() : ILabelEvents {
        return undefined;
    }

}

class MockTitle extends GuiCommons implements IToolTip {
    private text : string;
    private guiType : any;

    public GuiType($toolTipType? : any) : any {
        return this.guiType = Property.String(this.guiType, $toolTipType);
    }

    public Text($value? : string) : string {
        return this.text = Property.String(this.text, $value);
    }
}

class MockScrollBar extends GuiCommons implements IScrollBar {
    private guiType : any;
    private size : number;
    private id : string;

    public GuiType($orientationType? : any) : any {
        return this.guiType = Property.String(this.guiType, $orientationType);
    }

    public OrientationType($orientationType? : any) : any {
        return this.GuiType($orientationType);
    }

    public Size($value? : number) : number {
        return this.size = Property.Integer(this.size, $value);
    }

    public getEvents() : IScrollBarEvents {
        return <IScrollBarEvents>super.getEvents();
    }
}

class MockGuiCommons extends GuiCommons {
}

export class BasePanelTest extends UnitTestRunner {

    public testConstructor() : void {
        const basepanel : BasePanel = new BasePanel();
        assert.equal(basepanel.getScrollLeft(), -1);
        const basepanel20 : BasePanel = new BasePanel("888");
        assert.equal(basepanel20.Id(), "888");
        const panel : BasePanel = new BasePanel("id5");
        ElementManager.setClassName(panel.Id(), "PanelScrollBar");

        const panel40 : BasePanel = new MockBasePanel("id455");
        assert.equal(panel40.Id(), "id455");
    }

    public testCascheData() : void {
        const panel : MockexcludeCacheData = new MockexcludeCacheData("id2");
        panel.Scrollable(true);
        assert.deepEqual(panel.testexcludeCacheData(),
            [
                "options", "availableOptionsList", "events", "childElements", "waitFor", "cached", "prepared", "completed",
                "parent", "owner", "guiPath", "interfaceClassName", "styleClassName", "containerClassName", "innerHtmlMap", "loaded",
                "title", "changed", "notification", "selectorEvents", "tabIndex", "asyncChildLoaders", "asyncChildrenLoader",
                "contentType", "asyncInnerCodeOutput"
            ]);
        this.initSendBox();
    }

    public testSerializationData() : void {
        const panel : MockexcludeCacheData = new MockexcludeCacheData("id8");
        assert.deepEqual(panel.testexcludeSerialization(),
            [
                "objectNamespace", "objectClassName", "options", "availableOptionsList", "parent", "owner", "guiPath",
                "visible", "enabled", "prepared", "completed", "interfaceClassName", "styleClassName", "containerClassName", "loaded",
                "asyncDrawEnabled", "contentLoaded", "waitFor", "outputEndOfLine", "innerHtmlMap", "events", "changed", "error",
                "tabIndex", "autofillPersistence", "valuesPersistence", "errorFlags", "isPersistent", "asyncChildLoaders",
                "asyncChildrenLoader", "contentType", "scrollable", "width", "height"
            ]);
        this.initSendBox();
    }

    public testBasePanel() : void {
        const element : BasePanel = new BasePanel();
        element.ContentType(PanelContentType.WITH_ELEMENT_WRAPPER);
        Echo.PrintCode(element.Draw());
        element.ContentType(PanelContentType.WITHOUT_ELEMENT_WRAPPER);
        Echo.Println("<i>Panel with out element wrapper</i>");
        Echo.PrintCode(element.Draw());
        element.ContentType(PanelContentType.HIDDEN);
        Echo.Println("<i>Hidden Panel</i>");
        Echo.PrintCode(element.Draw());
        Echo.Println("<div style=\"clear: both;\">" + element.Draw() + "</div>");
        element.ContentType(PanelContentType.ASYNC_LOADER);
        Echo.Println("<i>Panel with async loading</i>");
        Echo.PrintCode(element.Draw());
        Echo.Println("<div style=\"clear: both;\">" + element.Draw() + "</div>");
    }

    public testgetEventsSecond() : void {
        const basepanel : BasePanel = new BasePanel();
        const handler : any = () : void => {
            // test event handler
        };
        basepanel.getEvents().setEvent("test", handler);
        assert.equal(basepanel.getEvents().Exists("test"), true);
    }

    public testContentTypeSecond() : void {
        const basepanel : BasePanel = new BasePanel();
        Echo.PrintCode(JSON.stringify(basepanel));
        assert.equal(basepanel.ContentType(PanelContentType.WITH_ELEMENT_WRAPPER),
            PanelContentType.WITH_ELEMENT_WRAPPER);
        Echo.Println("<i>Panel with out element wrapper</i>");
    }

    public testShow() : void {
        const basepanel : BasePanel = new BasePanel();
        BasePanel.Show(basepanel);
        assert.equal(basepanel.Visible(), true);

        const basepanel2 : BasePanel = new BasePanel("3636");
        assert.equal(basepanel2.Id(), "3636");
        basepanel2.Width(40);
        basepanel2.Height(80);
        assert.equal(ElementManager.Exists("3636"), false);
        BasePanel.Show(basepanel2);
        assert.equal(basepanel2.Visible(), true);
    }

    public testContentType() : void {
        const basepanel : BasePanel = new BasePanel();
        assert.equal(basepanel.ContentType(PanelContentType.HIDDEN), PanelContentType.HIDDEN);
        const basepanel2 : BasePanel = new BasePanel();
        assert.equal(basepanel2.ContentType(), 1);
        const basepanel3 : BasePanel = new BasePanel();
        Echo.PrintCode(JSON.stringify(basepanel3));
        assert.equal(basepanel3.ContentType(PanelContentType.WITH_ELEMENT_WRAPPER),
            PanelContentType.WITH_ELEMENT_WRAPPER);
        Echo.Println("<i>Panel with out element wrapper</i>");
    }

    public testgetChildPanelList() : void {
        const basepanel : BasePanel = new BasePanel();
        assert.deepEqual(basepanel.getChildPanelList().getItem(0), null);
    }

    public testsetChildPanelArgs() : void {
        const basepanel : BasePanel = new BasePanel("45");
        const viewargs : BasePanelViewerArgs = new BasePanelViewerArgs();
        basepanel.setChildPanelArgs(basepanel, viewargs);

        const panel : BasePanel = new MockBasePanel("48");
        const gui : GuiCommons = new MockGuiCommons("id6");
        const viewerargs2 : BasePanelViewerArgs = new BasePanelViewerArgs();
        panel.Parent(gui);
        panel.AddChild(gui);
        panel.setChildPanelArgs("id6", viewerargs2);

        const panel3 : BasePanel = new MockBasePanel("id50");
        const gui3 : GuiCommons = new MockGuiCommons("id45");
        panel.Parent(gui);
        panel.AddChild(gui);
        panel.setChildPanelArgs("id45", null);
    }

    public __IgnoretestValue() : void {
        const basepanel : BasePanel = new BasePanel();
        const viewargs : BasePanelViewerArgs = new BasePanelViewerArgs();
        assert.equal(basepanel.Value(viewargs), null);
        const basepanel2 : BasePanel = new BasePanel();
        assert.equal(basepanel2.Value("arguments"), null);
        const basepanel3 : BasePanel = new BasePanel();
        const viewerargs : BasePanelViewerArgs = new BasePanelViewerArgs();
        const viewer : BasePanelViewer = new BasePanelViewer();
        const gui : GuiCommons = new MockGuiCommons();
        basepanel3.Parent(gui);
        basepanel3.AddChild(gui);
        assert.equal(basepanel3.Value(viewerargs), null);
    }

    public __IgnoretestScrollable() : void {
        const basepanel : BasePanel = new BasePanel();
        assert.equal(basepanel.Scrollable(true), true);
        assert.equal(basepanel.Scrollable(false), false);
        assert.equal(basepanel.Scrollable(null), false);
        const basepanel2 : BasePanel = new BasePanel();
        assert.equal(basepanel2.Scrollable(true), true);
    }

    public __IgnoretestWidth() : void {
        const panelviewer : BasePanelViewer = new MockBasePanelViewer();
        const basepanel : BasePanel = new BasePanel();
        assert.equal(basepanel.Width(50), 50);
        const basepanel2 : BasePanel = new BasePanel();
        (<any>BasePanel).loaded = true;
        BasePanel.Show(basepanel2);
        assert.equal(basepanel2.Visible(), true);
        assert.equal(basepanel2.Width(30), 30);
        const basepanel3 : BasePanel = new BasePanel();
        assert.equal(basepanel3.Width(8), 8);
    }

    public __IgnoretestHeight() : void {
        const panelviewer : BasePanelViewer = new MockBasePanelViewer();
        const basepanel : BasePanel = new BasePanel();
        assert.equal(basepanel.Height(100), 100);
        const basepanel4 : BasePanel = new BasePanel();
        (<any>BasePanel).loaded = true;
        BasePanel.Show(basepanel4);
        assert.equal(basepanel4.Visible(), true);
        assert.equal(basepanel4.Width(60), 60);
        const basepanel5 : BasePanel = new BasePanel();
        basepanel5.Height(16);
        assert.equal(basepanel5.Height(), 16);
    }

    public __IgnoretestgetScrollTop() : void {
        const basepanel : BasePanel = new BasePanel();
        assert.equal(basepanel.getScrollTop(), -1);
    }

    public __IgnoretestscrollTopSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id44");
            const gui1 : GuiCommons = new MockGuiCommons("id6");
            const gui2 : GuiCommons = new MockGuiCommons("id7");
            panel.Parent(gui1);
            panel.Parent(gui2);
            const viewer : BasePanelViewer = new BasePanelViewer();
            const args : EventArgs = new EventArgs();
            assert.onGuiComplete(panel,
                () : void => {
                    (<any>BasePanel).scrollTop(panel, 120);
                }, $done, viewer);
        };
    }

    public __IgnoretestscrollTopThird() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id135");
            const gui1 : GuiCommons = new MockGuiCommons("id6");
            const gui2 : GuiCommons = new MockGuiCommons("id7");
            panel.Parent(gui1);
            panel.Parent(gui2);
            const viewer : BasePanelViewer = new BasePanelViewer();
            const args : EventArgs = new EventArgs();
            assert.onGuiComplete(panel,
                () : void => {
                    ElementManager.setCssProperty("id135", "scrollTop", 100);
                    (<any>BasePanel).scrollTop(panel, null);
                }, $done, viewer);
        };
    }

    public __IgnoretestscrollLeftSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id44");
            const gui1 : GuiCommons = new MockGuiCommons("id6");
            const gui2 : GuiCommons = new MockGuiCommons("id7");
            panel.Parent(gui1);
            panel.Parent(gui2);
            const viewer : BasePanelViewer = new BasePanelViewer();
            const args : EventArgs = new EventArgs();
            assert.onGuiComplete(panel,
                () : void => {
                    (<any>BasePanel).scrollLeft(panel, 120);
                    assert.patternEqual(panel.Draw(),
                        "\r\n<div id=\"id44_PanelEnvelop\" class=\"Panel\" style=\"display: block;\">\r\n" +
                        "    <div class=\"IoOidisGuiPrimitives\">\r\n" +
                        "       <div id=\"id44_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                        "          <div id=\"id44\" class=\"BasePanel\" style=\"display: none;\"></div>\r\n" +
                        "       </div>\r\n" +
                        "    </div>\r\n</div>");
                }, $done, viewer);
        };
    }

    public __IgnoretestgetScrollLeft() : void {
        const basepanel : BasePanel = new BasePanel();
        assert.equal(basepanel.getScrollLeft(), -1);
    }

    public __IgnoretestsetArg() : void {
        const basepanel : BasePanel = new BasePanel("id8");
        const args : IGuiCommonsArg = <IGuiCommonsArg>{
            name : "Width",
            type : GuiCommonsArgType.NUMBER,
            value: 35
        };

        const basepanel2 : BasePanel = new BasePanel();
        args.name = "Height";
        args.type = GuiCommonsArgType.NUMBER;
        args.value = 35;
        basepanel2.setArg(args, true);

        const basepanel3 : BasePanel = new BasePanel();
        args.name = "Scrollable";
        args.type = GuiCommonsArgType.BOOLEAN;
        args.value = 35;
        basepanel3.setArg(args, true);
    }

    public __IgnoretestTurnOn() : void {
        const basepanel : BasePanel = new BasePanel();
        BasePanel.TurnOn(basepanel);
        assert.equal(basepanel.Visible(), true);
    }

    public __IgnoretestTurnOff() : void {
        const basepanel : BasePanel = new BasePanel();
        BasePanel.TurnOff(basepanel);
        assert.equal(basepanel.Visible(), true);
    }

    public __IgnoretestActive() : void {
        const basepanel : BasePanel = new BasePanel();
        BasePanel.TurnActive(basepanel);
        assert.equal(basepanel.Visible(), true);
        Reflection.getInstance();
    }

    public __IgnoretestgetEvents() : void {
        const basepanel : BasePanel = new BasePanel();
        const handler : any = () : void => {
            // test event handler
        };
        basepanel.getEvents().setEvent("test", handler);
        assert.equal(basepanel.getEvents().Exists("test"), true);
    }

    public __IgnoretestAddChild() : void {
        const viewer : BaseViewer = new BaseViewer();
        const basepanel : BasePanel = new BasePanel("id1");
        const gui1 : GuiCommons = new MockGuiCommons("id2");
        const gui2 : GuiCommons = new MockGuiCommons("id3");
        gui1.Parent(basepanel);
        gui2.Parent(basepanel);
        basepanel.AddChild(gui1, "test");
        basepanel.AddChild(gui2, "test2");
        assert.deepEqual(basepanel.getChildElements().getAll(), []);
    }

    public __IgnoretestGetScrollTop() : void {
        const basepanel : BasePanel = new BasePanel();
        assert.equal(basepanel.getScrollTop(), -1);
    }

    public __IgnoretestGetScrollLeft() : void {
        const basepanel : BasePanel = new BasePanel();
        assert.equal(basepanel.getScrollLeft(), -1);
    }

    public __IgnoretestDraw() : void {
        const basepanel : BasePanel = new BasePanel("basePanelId");
        assert.equal(ElementManager.Exists("basePanelId"), false);
        const gui : GuiOptionsManager = new GuiOptionsManager(new ArrayList<GuiOptionType>());
        basepanel.DisableAsynchronousDraw();
        Echo.Printf(JSON.stringify(basepanel.Draw()));
        assert.equal(basepanel.Draw(),
            "\r\n<div id=\"basePanelId_PanelEnvelop\" class=\"Panel\" style=\"display: block;\">" +
            "\r\n    <div class=\"IoOidisGuiPrimitives\">" +
            "\r\n       <div id=\"basePanelId_GuiWrapper\" guiType=\"GuiWrapper\">" +
            "\r\n          <div id=\"basePanelId\" class=\"BasePanel\" style=\"display: block;\">" +
            "\r\n             <div guiType=\"Panel\">" +
            "\r\n                <div id=\"basePanelId_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">" +
            "\r\n                   <div id=\"basePanelId_PanelContent\" class=\"Content\">" +
            "\r\n                      <div style=\"clear: both;\"></div>" +
            "\r\n                   </div>" +
            "\r\n                </div>" +
            "\r\n             </div>" +
            "\r\n          </div>" +
            "\r\n       </div>" +
            "\r\n    </div>" +
            "\r\n</div>");
    }

    public __IgnoretestpanelDraw() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id4");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    const args : IGuiCommonsArg = <IGuiCommonsArg>{
                        name : "Width",
                        type : GuiCommonsArgType.NUMBER,
                        value: 35
                    };
                    panel.setArg(args, false);
                },
                () : void => {
                    assert.equal(ElementManager.getElement("id4").outerHTML,
                        "<div id=\"id4\" class=\"BasePanel\" " +
                        "style=\"display: block; width: 35px; height: 100px; overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id4_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">\n" +
                        "                  <div id=\"id4_PanelContent\" class=\"Content\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  </div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestAddChildSecond() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id4");
            panel.Visible(true);
            const viewer : BaseViewer = new BaseViewer();
            const gui : GuiCommons = new MockGuiCommons("21");
            const gui2 : GuiCommons = new MockGuiCommons("28");
            let gui3 : GuiCommons;
            assert.onGuiComplete(panel,
                () : void => {
                    panel.AddChild(gui, "21");
                    panel.AddChild(gui2, "28");
                    panel.AddChild(gui3);
                },
                () : void => {
                    assert.equal(ElementManager.getElement("id4").outerHTML,
                        "<div id=\"id4\" class=\"BasePanel\" style=\"display: block; width: 0px; height: 100px;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id4_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">\n" +
                        "                  <div id=\"id4_PanelContent\" class=\"Content\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  </div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestpanelDrawNext() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new MockBasePanel("id4");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    const args : IGuiCommonsArg = <IGuiCommonsArg>{
                        name : "Width",
                        type : GuiCommonsArgType.BOOLEAN,
                        value: 35
                    };
                },
                () : void => {
                    assert.equal(ElementManager.getElement("id4").innerHTML,
                        "\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id4_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">\n" +
                        "                  <div id=\"id4_PanelContent\" class=\"Content\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  </div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       ");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestpanelDrawShow() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id4");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.ContentType(PanelContentType.ASYNC_LOADER);
                    new Size(panel.Id() + "_AsyncProgress");
                    ElementManager.setCssProperty(panel.Id() + "_PanelEnvelop", "float", "left");
                    ElementManager.setCssProperty(panel.Id() + "_AsyncProgress", "float", "top");
                    BasePanel.Show(panel);
                },
                () : void => {
                    assert.equal(ElementManager.getElement("id4").outerHTML,
                        "<div id=\"id4\" class=\"BasePanel\" style=\"display: block; width: 0px; height: 100px;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id4_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">\n" +
                        "                  <div id=\"id4_PanelContent\" class=\"Content\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  </div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestpanelDrawShowNext() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id4");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Enabled(true);
                    panel.Visible(true);
                    panel.ContentType(PanelContentType.ASYNC_LOADER);
                    ElementManager.setSize(panel.Id(), panel.Width(100), panel.Height(50));
                    new Size(panel.Id() + "_AsyncProgress");
                    ElementManager.setCssProperty(panel.Id() + "_PanelEnvelop", "float", "left");
                    ElementManager.setCssProperty(panel.Id() + "_AsyncProgress", "float", "top");
                    BasePanel.Show(panel);
                },
                () : void => {
                    assert.equal(ElementManager.getElement("id4").outerHTML,
                        "<div id=\"id4\" class=\"BasePanel\" style=\"display: block; width: 100px;" +
                        " height: 50px; overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id4_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">\n" +
                        "                  <div id=\"id4_PanelContent\" class=\"Content\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  </div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestContentFocusHandlerwithoutParent() : void {
        const panel : BasePanel = new BasePanel("id4");
        const viewer : BaseViewer = new BaseViewer();
        panel.Parent();
        const args : EventArgs = new EventArgs();
        args.Owner(panel);
        const manager : GuiObjectManager = new GuiObjectManager();
        const reflection : Reflection = new Reflection();
        Reflection.getInstance().getClass(panel.getClassName());
        reflection.IsMemberOf(<BasePanel>panel.Parent(), BasePanel);
        BasePanel.ContentFocusHandler(args, manager, reflection);

        assert.equal(ElementManager.getElement("id4").outerHTML,
            "<div id=\"id4\" class=\"BasePanel\" style=\"display: block; width: 100px; height: 50px;" +
            " overflow-x: hidden; overflow-y: hidden;\">\n" +
            "            <div guitype=\"Panel\">\n" +
            "               <div id=\"id4_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">\n" +
            "                  <div id=\"id4_PanelContent\" class=\"Content\">\n" +
            "                     <div style=\"clear: both;\"></div>\n" +
            "                  </div>\n" +
            "               </div>\n" +
            "            </div>\n" +
            "       </div>");
        this.initSendBox();
    }

    public __IgnoretestAddChildNext() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id4");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    const gui : GuiCommons = new MockGuiCommons();
                    panel.Parent(gui);
                    panel.AddChild(gui);
                    panel.InstanceOwner(viewer);
                },
                () : void => {
                    assert.equal(ElementManager.getElement("id4").outerHTML,
                        "<div id=\"id4\" class=\"BasePanel\" style=\"display: block; width: 0px; height: 100px;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id4_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">\n" +
                        "                  <div id=\"id4_PanelContent\" class=\"Content\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  </div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestAddChildEmptyChild() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id4");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    panel.AddChild(undefined);
                    panel.InstanceOwner(viewer);
                },
                () : void => {
                    assert.equal(ElementManager.getElement("id4").outerHTML,
                        "<div id=\"id4\" class=\"BasePanel\" style=\"display: block; width: 0px; height: 100px;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id4_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">\n" +
                        "                  <div id=\"id4_PanelContent\" class=\"Content\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  </div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestonHoverEventHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id7");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    panel.InstanceOwner(viewer);
                    const event : any = {altKey: false, button: 3};
                    const args : MouseEventArgs = new MouseEventArgs(event);
                    args.PreventDefault();
                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    manager.setHovered(panel, true);
                    manager.setActive(panel, true);
                    (<any>BasePanel).onHoverEventHandler(args, manager, Reflection.getInstance());
                },
                $done, viewer);
        };
    }

    public __IgnoretestonHoverEventHandlerNext() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new MockBasePanel("id148");
            const viewer : BasePanelViewer = new BasePanelViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    // panel.verticalScrollBar.Visible(true);
                    // panel.horizontalScrollBar.Visible(true);
                    // panel.verticalScrollBar.OrientationType(OrientationType.VERTICAL);
                    // panel.horizontalScrollBar.OrientationType(OrientationType.HORIZONTAL);
                    const event : any = {altKey: false, button: 3};
                    const args : MouseEventArgs = new MouseEventArgs(event);
                    args.PreventDefault();
                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    manager.setHovered(panel, true);
                    manager.setActive(panel, true);
                    (<any>BasePanel).onHoverEventHandler(args, manager, Reflection.getInstance());
                },
                $done, viewer);
        };
    }

    public __IgnoretestonKeyEventHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id77");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    panel.InstanceOwner(viewer);
                    ElementManager.getElement(panel.Id() + "_PanelContentEnvelop");
                    const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                    const keyboardEvent : any = {altKey: true, char: "T", ctrlKey: true, keyCode: KeyMap.UP_ARROW};
                    const args : KeyEventArgs = new KeyEventArgs(keyboardEvent);
                    args.PreventDefault();
                    args.getKeyCode();
                    eventArgs.OrientationType(OrientationType.VERTICAL);
                    eventArgs.Position(50);
                    // args.getKeyCode();

                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    manager.setHovered(panel, true);
                    manager.getHovered();
                    manager.setActive(panel, true);
                    args.Owner(panel);
                    (<any>BasePanel).onHoverEventHandler(args, manager, Reflection.getInstance());
                },
                $done, viewer);
        };
    }

    public __IgnoretestonKeyEventHandler2() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new MockBasePanel("id102");
            const viewer : BaseViewer = new BaseViewer();
            const gui : GuiCommons = new MockGuiCommons("id54");
            panel.AddChild(gui);
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    panel.verticalScrollBar.Visible();
                    panel.horizontalScrollBar.Visible();
                    panel.InstanceOwner(viewer);
                    ElementManager.getElement(panel.Id() + "_PanelContentEnvelop");
                    const eventArgs : ScrollEventArgs = new ScrollEventArgs();
                    eventArgs.Owner(panel);
                    const keyboardEvent : any = {altKey: true, char: "T", ctrlKey: true, keyCode: KeyMap.LEFT_ARROW};
                    const args : KeyEventArgs = new KeyEventArgs(keyboardEvent);
                    args.PreventDefault();
                    args.getKeyCode();
                    eventArgs.OrientationType(OrientationType.VERTICAL);
                    eventArgs.Position(120);
                    args.getKeyCode();
                    // args.getKeyCode();
                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    manager.setHovered(panel, true);
                    manager.getHovered();
                    manager.setActive(panel, true);
                    args.Owner(panel);
                    assert.equal(panel.Draw(), "");
                    (<any>BasePanel).onHoverEventHandler(args, manager, Reflection.getInstance());
                },
                $done, viewer);
        };
    }

    public __IgnoretestContentFocusHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id4");
            const gui : GuiCommons = new MockGuiCommons();
            const viewer : BaseViewer = new BaseViewer();
            panel.Parent(gui);
            assert.onGuiComplete(panel,
                () : void => {
                    const args : EventArgs = new EventArgs();
                    args.Owner(panel);
                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    const reflection : Reflection = new Reflection();
                    reflection.IsMemberOf(<BasePanel>panel.Parent(), BasePanel);
                    BasePanel.ContentFocusHandler(args, manager, Reflection.getInstance());
                    assert.equal(ElementManager.getElement("id4").outerHTML,
                        "<div id=\"id4\" class=\"BasePanel\" style=\"display: block; width: 0px; height: 100px;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id4_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">\n" +
                        "                  <div id=\"id4_PanelContent\" class=\"Content\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  </div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    BasePanel.ContentFocusHandler(args, manager, Reflection.getInstance());
                },
                $done, viewer);
        };
    }

    public __IgnoretestrecomputeNestedGuiElementSizes() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id25");
            const viewer : BasePanelViewer = new BasePanelViewer();
            const gui : GuiElement = new GuiElement();
            const child : GuiCommons = new MockGuiCommons("id95");
            panel.Parent(child);
            assert.onGuiComplete(panel,
                () : void => {

                    const size : Size = new Size();
                    const widthofColumn : PropagableNumber = new PropagableNumber({number: 60, unitType: UnitType.PX}, true);
                    const heightofColumn : PropagableNumber = new PropagableNumber({number: 150, unitType: UnitType.PX}, true);
                    (<any>BasePanel).recomputeNestedGuiElementSizes(gui, size, widthofColumn, heightofColumn);
                },
                $done, viewer);
        };
    }

    public __IgnoretestrecomputeNestedGuiElementSizesNext() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id26");
            const viewer : BasePanelViewer = new BasePanelViewer();
            const gui : GuiElement = new GuiElement();
            const child : GuiCommons = new MockGuiCommons("id96");
            panel.Parent(child);
            panel.Implements(IResponsiveElement);
            assert.onGuiComplete(panel,
                () : void => {

                    const size : Size = new Size();
                    const widthofColumn : PropagableNumber = new PropagableNumber({number: 60, unitType: UnitType.PCT}, true);
                    const heightofColumn : PropagableNumber = new PropagableNumber({number: 150, unitType: UnitType.PCT}, true);
                    (<any>BasePanel).recomputeNestedGuiElementSizes(gui, size, widthofColumn, heightofColumn);
                },
                $done, viewer);
        };
    }

    public __IgnoretestrecomputeInnerPaddings() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id26");
            const viewer : BasePanelViewer = new BasePanelViewer();
            const gui : GuiElement = new GuiElement();
            const child : GuiCommons = new MockGuiCommons("id96");
            panel.Parent(child);
            panel.Implements(IResponsiveElement);
            const alignment : Alignment = new Alignment();
            ElementManager.getElement(panel.Id() + "_" + GeneralCssNames.PADDING_BEFORE);
            ElementManager.getElement(panel.Id() + "_" + GeneralCssNames.PADDING_AFTER);
            ElementManager.setCssProperty("id26", "height", 200);
            assert.onGuiComplete(panel,
                () : void => {
                    assert.equal(panel.Draw().toString(),
                        "\r\n<div id=\"id26_PanelEnvelop\" class=\"Panel\" style=\"display: block;\">\r\n" +
                        "    <div class=\"IoOidisGuiPrimitives\">\r\n" +
                        "       <div id=\"id26_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
                        "          <div id=\"id26\" class=\"BasePanel\" style=\"display: none;\"></div>\r\n" +
                        "       </div>\r\n" +
                        "    </div>\r\n" +
                        "</div>");
                    (<any>BasePanel).recomputeInnerPaddings(gui, alignment);
                },
                $done, viewer);
        };
    }

    public __IgnoretestalignNestedChildElements() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id4");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    const gui : GuiElement = new GuiElement();
                    panel.Visible(true);
                    panel.Scrollable(true);
                    const args : ScrollEventArgs = new ScrollEventArgs();
                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    args.OrientationType(OrientationType.VERTICAL);
                    args.DirectionType(DirectionType.DOWN);
                    manager.setActive(panel, true);
                    manager.setHovered(panel, true);
                    args.Position(50);
                    (<any>BasePanel).alignNestedChildElements(gui, Alignment.CENTER_PROPAGATED);
                },
                $done, viewer);
        };
    }

    public __IgnoretestShowNext() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new MockBasePanel("id22");
            const viewer : BaseViewer = new BaseViewer();
            const gui : GuiElement = new GuiElement();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    panel.ContentType(PanelContentType.ASYNC_LOADER);
                    const args : ScrollEventArgs = new ScrollEventArgs();
                    // const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    Reflection.getInstance();
                    ElementManager.setSize("id4", panel.Width(500), panel.Height(500));
                    panel.Height(500);
                    panel.Width(500);
                    // new Size(ElementManager.getElement(panel.Id() + "_AsyncProgress"));
                    // manager.setActive(panel, true);
                    // manager.setHovered(panel, true);
                    args.Position(50);
                    BasePanel.Show(panel);
                },
                () : void => {
                    assert.equal(ElementManager.getElement("id22").outerHTML,
                        "<div id=\"id22\" class=\"BasePanel\" style=\"display: block; width: 500px; height: 500px;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id22_PanelContentEnvelop\" class=\"Envelop\" style=\"float: left;" +
                        " overflow-x: hidden;overflow-y: hidden;\">\n" +
                        "                  <div id=\"id22_PanelContent\" class=\"Content\" style=\"min-width: 0px;" +
                        " min-height: 0px;\">\n" +
                        "<div style=\"clear: both;\"></div>\n" +
                        "                  </div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    // ElementManager.getElement(panel.loaderIcon.Id()).offsetWidth;
                    // ElementManager.getElement(panel.loaderText.Id()).offsetWidth;
                    panel.loaderIcon.Visible(true);
                    $done();
                    this.initSendBox();
                    // ElementManager.getElement(panel.loaderIcon.Id()).offsetHeight;
                }, viewer);
        };
    }

    public __IgnoretestonBodyScrollEventHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id4");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    //  panel.verticalScrollBar.OrientationType(OrientationType.VERTICAL);
                    // panel.horizontalScrollBar.OrientationType(OrientationType.HORIZONTAL);
                    const args : ScrollEventArgs = new ScrollEventArgs();
                    const manager : GuiObjectManager = GuiObjectManager.getInstanceSingleton();
                    args.OrientationType(OrientationType.VERTICAL);
                    args.DirectionType(DirectionType.RIGHT);
                    args.OrientationType(OrientationType.HORIZONTAL);
                    args.DirectionType(DirectionType.DOWN);
                    args.PreventDefault();
                    manager.setActive(panel, true);
                    manager.setHovered(panel, true);
                    args.Position(50);
                    (<any>BasePanel).scrollTop(panel);
                    (<any>BasePanel).onBodyScrollEventHandler(args, manager, Reflection.getInstance());
                },
                () : void => {
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestAddChildVariableName() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id4");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    const gui : GuiCommons = new MockGuiCommons();
                    panel.Parent(gui);
                    panel.AddChild(gui, "BasePanel");
                    panel.InstanceOwner(viewer);
                },
                () : void => {
                    assert.equal(ElementManager.getElement("id4").outerHTML,
                        "<div id=\"id4\" class=\"BasePanel\" style=\"display: block; width: 0px; height: 100px;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id4_PanelContentEnvelop\" class=\"Envelop\" style=\"display: none;\">\n" +
                        "                  <div id=\"id4_PanelContent\" class=\"Content\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  </div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestGuiConntent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id6");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    panel.ContentType(PanelContentType.HIDDEN);
                    const gui : GuiCommons = new MockGuiCommons("id9");
                    panel.Parent(gui);
                    panel.AddChild(gui, "BasePanel");
                    panel.InstanceOwner(viewer);
                },
                () : void => {
                    assert.deepEqual((<any>panel).guiContent().toString(),
                        "\r\n<a href=\"/#/web/Io/Oidis/Gui/Primitives/BaseViewer\">\n" +
                        "<div class=\"Hidden\">id6</div></a>");
                    assert.equal(ElementManager.getElement("id6").outerHTML,
                        "<div id=\"id6\" class=\"BasePanel\" style=\"display: block; width: 0px; height: 100px;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id6_PanelContentEnvelop\" class=\"Envelop\" style=\"float: left;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "                  <div id=\"id6_PanelContent\" class=\"Content\" style=\"min-width: 0px;" +
                        " min-height: 0px;\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  <div class=\"IoOidisGuiPrimitives\">\n" +
                        "       <div id=\"id9_GuiWrapper\" guitype=\"GuiWrapper\">\n" +
                        "          <div id=\"id9\" class=\"GuiCommons\" style=\"display: none;\"></div>\n" +
                        "       </div>\n" +
                        "    </div></div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestGuiConntentAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new BasePanel("id88");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    panel.ContentType(PanelContentType.ASYNC_LOADER);
                    const gui : GuiCommons = new MockGuiCommons("id99");
                    panel.Parent(gui);
                    panel.AddChild(gui, "BasePanel");
                    panel.InstanceOwner(viewer);
                },
                () : void => {
                    assert.deepEqual((<any>panel).guiContent().toString(),
                        "\r\n<div id=\"id88_AsyncProgress\" class=\"Async\" style=\"left: 0px; top: 50px;\">\r\n" +
                        "   <div class=\"Label\">Loading ...</div>\r\n</div>");
                    assert.equal(ElementManager.getElement("id6").outerHTML,
                        "<div id=\"id6\" class=\"BasePanel\" style=\"display: block; width: 0px; height: 100px;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id6_PanelContentEnvelop\" class=\"Envelop\" style=\"float: left;" +
                        " overflow-x: hidden; overflow-y: hidden; display: block; width: 0px; height: 100px;\">\n" +
                        "                  <div id=\"id6_PanelContent\" class=\"Content\" style=\"min-height: 100px;\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  <div class=\"IoOidisGuiPrimitives\">\n" +
                        "       <div id=\"id9_GuiWrapper\" guitype=\"GuiWrapper\">\n" +
                        "          <div id=\"id9\" class=\"GuiCommons\" style=\"display: block;\">\n" +
                        "       </div>\n" +
                        "       </div>\n" +
                        "    </div></div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    $done();
                    this.initSendBox();
                }, viewer);
        };
    }

    public __IgnoretestscrollBarVisibilityHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new MockBasePanel("id145");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    ElementManager.setSize(panel.Id(), panel.Width(0), panel.Height(0));
                    //  panel.verticalScrollBar.OrientationType(OrientationType.VERTICAL);
                    //  panel.horizontalScrollBar.OrientationType(OrientationType.HORIZONTAL);
                    panel.ContentType(PanelContentType.HIDDEN);
                    const gui : GuiCommons = new MockGuiCommons("id9");
                    panel.Parent(gui);
                    panel.AddChild(gui, "BasePanel");
                    panel.InstanceOwner(viewer);
                    ElementManager.setSize(panel.Id() + "_PanelContent", 500, 500);
                    // ElementManager.getCssIntegerValue(panel.verticalScrollBar.Id(), "offsetHeig");
                    (<any>BasePanel).scrollBarVisibilityHandler(panel);
                },
                () : void => {
                    assert.equal(ElementManager.getElement("id145").outerHTML,
                        "<div id=\"id145\" class=\"BasePanel\" style=\"display: block; width: 0px; height: 0px;" +
                        " overflow-x: hidden; overflow-y: hidden;\">\n" +
                        "            <div guitype=\"Panel\">\n" +
                        "               <div id=\"id145_PanelContentEnvelop\" class=\"Envelop\" style=\"float: left;" +
                        " overflow-x: hidden; overflow-y: hidden; display: block; width: 0px; height: 0px;\">\n" +
                        "                  <div id=\"id145_PanelContent\" class=\"Content\" style=\"min-width: 0px; min-height: 0px;" +
                        " display: block; width: 500px; height: 500px;\">\n" +
                        "                     <div style=\"clear: both;\"></div>\n" +
                        "                  <div class=\"IoOidisGuiPrimitives\">\n" +
                        "       <div id=\"id9_GuiWrapper\" guitype=\"GuiWrapper\">\n" +
                        "          <div id=\"id9\" class=\"GuiCommons\" style=\"display: none;\"></div>\n" +
                        "       </div>\n" +
                        "    </div></div>\n" +
                        "               </div>\n" +
                        "            </div>\n" +
                        "       </div>");
                    $done();
                }, viewer);
        };
    }

    public __IgnoretestonScrollBarVisibilityChange() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new MockBasePanel("id166");
            const viewer : BaseViewer = new BaseViewer();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    panel.Implements(IResponsiveElement);
                    (<any>BasePanel).onScrollBarVisibilityChange(panel, true, true);
                },
                $done, viewer);
        };
    }

    public __IgnoretestfitToParentNestedChildElements() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const panel : BasePanel = new MockBasePanel("id177");
            const viewer : BasePanelViewer = new BasePanelViewer();
            panel.getChildPanelList().Add(viewer);
            const gui : GuiElement = new GuiElement();
            const gui2 : GuiElement = new GuiElement();
            const gui3 : GuiElement = new GuiElement();
            assert.onGuiComplete(panel,
                () : void => {
                    panel.Visible(true);
                    panel.Scrollable(true);
                    (<any>BasePanel).fitToParentNestedChildElements(gui, FitToParent.FULL);
                },
                $done, viewer);
            this.initSendBox();
        };
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
