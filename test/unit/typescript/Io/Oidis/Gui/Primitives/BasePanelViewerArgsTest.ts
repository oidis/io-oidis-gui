/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BasePanelViewerArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanelViewerArgs.js";

export class BasePanelViewerArgsTest extends UnitTestRunner {
    public testAsyncEnabled() : void {
        const panelviewer : BasePanelViewerArgs = new BasePanelViewerArgs();
        assert.equal(panelviewer.AsyncEnabled(), false);
        assert.equal(panelviewer.AsyncEnabled(true), true);
    }
}
