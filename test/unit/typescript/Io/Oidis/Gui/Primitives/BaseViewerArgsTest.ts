/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { BaseViewerArgs } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewerArgs.js";

export class BaseViewerArgsTest extends UnitTestRunner {
    public testVisible() : void {
        const view : BaseViewerArgs = new BaseViewerArgs();
        assert.equal(view.Visible(true), true);
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
