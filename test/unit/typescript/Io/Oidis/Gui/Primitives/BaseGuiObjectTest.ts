/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { GuiCommonsArgType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/GuiCommonsArgType.js";
import { GuiObjectManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/GuiObjectManager.js";
import { IToolTip } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Components/IToolTip.js";
import { IGuiCommonsArg } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommonsArg.js";
import { IGuiElement } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { BaseGuiObject } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseGuiObject.js";
import { BasePanel } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanel.js";
import { BaseViewer } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { GuiCommons } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ElementManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/ElementManager.js";
import { StaticPageContentManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/StaticPageContentManager.js";

class MockBaseGuiObject extends BaseGuiObject {
    protected getTitleClass() : any {
        return MockTitle;
    }

    protected innerCode() : IGuiElement {
        this.getEvents().setOnMouseOver(MockBaseGuiObject.onHoverEventHandler);
        this.getEvents().setOnMouseOut(MockBaseGuiObject.onUnhoverEventHandler);
        return super.innerCode();
    }
}

class MockBasePanel extends BasePanel {
}

class MockSetChanged extends BaseGuiObject {
    public testDataChange() : void {
        this.setChanged();
    }
}

class MockTitle extends GuiCommons implements IToolTip {
    private text : string;
    private guiType : any;

    public GuiType($toolTipType? : any) : any {
        return this.guiType = Property.String(this.guiType, $toolTipType);
    }

    public Text($value? : string) : string {
        return this.text = Property.String(this.text, $value);
    }

}

class MockBaseViewer extends BaseViewer {
}

export class BaseGuiObjectTest extends UnitTestRunner {
    public testTitle() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject("test");
        assert.patternEqual(basegui.Title().toString(), "Io.Oidis.Gui.Primitives.GuiCommons (GuiCommons*)");
    }

    public testChanged() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject("test");
        assert.equal(basegui.Changed(), false);
    }

    public testValue() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject("test");
        assert.equal(basegui.Value(100), null);
        assert.equal(basegui.Value(), null);
    }

    public testsetArgs() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject("id3");
        basegui.Id();
        basegui.StyleClassName("ToolTip");
        basegui.Enabled(true);
        basegui.Value("Value");
        assert.deepEqual(basegui.getArgs(), [
            {name: "Id", type: "Text", value: "id3"},
            {name: "StyleClassName", type: "Text", value: "ToolTip"},
            {name: "Enabled", type: "Bool", value: true},
            {name: "Visible", type: "Bool", value: true},
            {name: "Width", type: "Number", value: 0},
            {name: "Height", type: "Number", value: 0},
            {name: "Top", type: "Number", value: 0},
            {name: "Left", type: "Number", value: 0},
            {name: "Title", type: "Text", value: ""},
            {name: "Value", type: "Text", value: null}
        ]);

        const args : IGuiCommonsArg = <IGuiCommonsArg>{
            name : "Value",
            type : GuiCommonsArgType.TEXT,
            value: "test"
        };

        basegui.setArg(args, true);
        assert.patternEqual(JSON.stringify(basegui.getArgs()),
            "[{\"name\":\"Id\",\"type\":\"Text\",\"value\":\"id3\"}," +
            "{\"name\":\"StyleClassName\",\"type\":\"Text\",\"value\":\"ToolTip\"}," +
            "{\"name\":\"Enabled\",\"type\":\"Bool\",\"value\":true}," +
            "{\"name\":\"Visible\",\"type\":\"Bool\",\"value\":true}," +
            "{\"name\":\"Width\",\"type\":\"Number\",\"value\":0}," +
            "{\"name\":\"Height\",\"type\":\"Number\",\"value\":0}," +
            "{\"name\":\"Top\",\"type\":\"Number\",\"value\":0}," +
            "{\"name\":\"Left\",\"type\":\"Number\",\"value\":0}," +
            "{\"name\":\"Title\",\"type\":\"Text\",\"value\":\"\"}," +
            "{\"name\":\"Value\",\"type\":\"Text\",\"value\":null}]");
    }

    public testsetArgs20() : void {
        const basegui : BaseGuiObject = new MockBaseGuiObject("id");
        const basepanel : BasePanel = new MockBasePanel();
        basegui.Value("VALUE");
        basegui.Title().Text();
        basegui.Changed();
        basegui.Height(100);

        const args : IGuiCommonsArg = <IGuiCommonsArg>{
            name : "Title",
            text : "Title",
            type : GuiCommonsArgType.TEXT,
            value: "testValue"
        };

        basegui.DisableAsynchronousDraw();
        assert.patternEqual(basegui.Draw(),
            "\r\n" +
            "<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"id_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id\" class=\"BaseGuiObject\" style=\"display: block;\">\r\n" +
            "         <div class=\"IoOidisGuiPrimitives\">\r\n" +
            "            <div id=\"GuiCommons*_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "               <div id=\"GuiCommons*\" class=\"GuiCommons\" style=\"display: none;\"></div>\r\n" +
            "            </div>\r\n" +
            "         </div>\r\n" +
            "      </div>\r\n" +
            "   </div>\r\n" +
            "</div>"
        );
        basegui.setArg(args, true);
        assert.deepEqual(basegui.getArgs(), [
            {name: "Id", type: "Text", value: "id"},
            {name: "StyleClassName", type: "Text", value: ""},
            {name: "Enabled", type: "Bool", value: true},
            {name: "Visible", type: "Bool", value: true},
            {name: "Width", type: "Number", value: 0},
            {name: "Height", type: "Number", value: 0},
            {name: "Top", type: "Number", value: 0},
            {name: "Left", type: "Number", value: 0},
            {name: "Title", type: "Text", value: "testValue"},
            {name: "Value", type: "Text", value: null}
        ]);
        this.initSendBox();
    }

    public testChangedSecond4() : void {
        const basegui : MockSetChanged = new MockSetChanged("id343");
        basegui.testDataChange();
        basegui.DisableAsynchronousDraw();
        assert.patternEqual(basegui.Draw(),
            "\r\n" +
            "<div class=\"IoOidisGuiPrimitives\">\r\n" +
            "   <div id=\"id343_GuiWrapper\" guiType=\"GuiWrapper\">\r\n" +
            "      <div id=\"id343\" class=\"BaseGuiObject\" style=\"display: block;\"></div>\r\n" +
            "   </div>\r\n" +
            "</div>");
        this.initSendBox();
    }

    public testgetTitleClass() : void {
        const basegui : MockBaseGuiObject = new MockBaseGuiObject();
        assert.equal((<any>basegui).getTitleClass(), MockTitle);
    }

    public __IgnoretestonHoverEventHandler() : IUnitTestRunnerPromise {
        const basegui : MockBaseGuiObject = new MockBaseGuiObject();
        basegui.InstanceOwner(new MockBaseViewer());
        StaticPageContentManager.BodyAppend(basegui.Draw());
        StaticPageContentManager.Draw();
        return ($done : () => void) : void => {
            basegui.getEvents().setOnMouseOver(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                assert.equal($manager.IsHovered(basegui.Parent()), true);
                assert.equal($manager.IsHovered(basegui), false);
                $done();
            });
            basegui.getEvents().setOnComplete(() : void => {
                ElementManager.getElement(basegui).onmouseover(null);
            });
            basegui.Visible(true);
        };
    }

    public __IgnoretestonClickEvent() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const basegui : MockBaseGuiObject = new MockBaseGuiObject();
            basegui.InstanceOwner(new MockBaseViewer());
            basegui.getEvents().setOnClick(($eventArgs : EventArgs, $manager : GuiObjectManager) : void => {
                assert.equal($manager.IsHovered(basegui.Parent()), true);
                assert.equal($manager.IsHovered(basegui), false);
                $done();
            });
            basegui.getEvents().setOnComplete(() : void => {
                ElementManager.getElement(basegui).click();
            });
            StaticPageContentManager.BodyAppend(basegui.Draw());
            StaticPageContentManager.Draw();
            basegui.Visible(true);
        };
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
