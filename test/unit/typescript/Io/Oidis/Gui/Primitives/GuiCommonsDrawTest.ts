/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Events/EventType.js";
import { EventsManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Events/EventsManager.js";
import { IGuiElement } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { BaseViewer } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { GuiCommons } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { StaticPageContentManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/StaticPageContentManager.js";

export class CommonsDrawTestChildMock extends GuiCommons {
    protected innerHtml() : IGuiElement {
        return this.addElement("ChildContent");
    }
}

export class CommonsDrawTestParentMock extends GuiCommons {
    public childMock : CommonsDrawTestChildMock;

    constructor() {
        super();
        this.childMock = new CommonsDrawTestChildMock();
    }

    protected innerHtml() : IGuiElement {
        return this.addElement("ParentContent")
            .Add(this.childMock);
    }
}

export class GuiCommonsDrawTest extends UnitTestRunner {

    public testStateOnInitialVisibleDrawAsync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : CommonsDrawTestParentMock = new CommonsDrawTestParentMock();
            mock.InstanceOwner(new BaseViewer());

            this.assertNotFired(mock, [EventType.ON_START, EventType.ON_LOAD, EventType.ON_COMPLETE]);
            EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                $done();
            }, 100);

            mock.Visible(true);
            StaticPageContentManager.BodyAppend(mock.Draw());
            StaticPageContentManager.Draw();

            this.assertVisible(mock.Id(), false);
            this.assertExists("ParentContent", false);
            this.assertPrepared(mock, false);
            this.assertLoaded(mock, false);
            this.assertContentLoaded(mock, false);
            this.assertCompleted(mock, false);
        };
    }

    public testStateOnInitialVisibleDrawSync() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : CommonsDrawTestParentMock = new CommonsDrawTestParentMock();
            mock.DisableAsynchronousDraw();
            mock.InstanceOwner(new BaseViewer());

            this.assertNotFired(mock, [EventType.ON_START, EventType.ON_LOAD, EventType.ON_COMPLETE]);
            EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                $done();
            }, 100);

            mock.Visible(true);
            StaticPageContentManager.BodyAppend(mock.Draw());
            StaticPageContentManager.Draw();

            this.assertVisible(mock.Id(), true);
            this.assertVisible("ParentContent", true);
            this.assertPrepared(mock, true);
            this.assertContentLoaded(mock, true);
            this.assertLoaded(mock, false);
            this.assertCompleted(mock, false);
        };
    }

    public testStateOnVisibleCallAsyncParentAsyncChild() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : CommonsDrawTestParentMock = new CommonsDrawTestParentMock();
            mock.InstanceOwner(new BaseViewer());
            mock.Visible(true);
            StaticPageContentManager.BodyAppend(mock.Draw());
            StaticPageContentManager.Draw();

            this.assertFired(mock, [EventType.ON_START, EventType.ON_LOAD], () : void => {
                this.assertVisible(mock.Id(), true);
                this.assertVisible("ParentContent", true);
                this.assertExists(mock.childMock.Id(), true);
                this.assertVisible(mock.childMock.Id(), false);
                this.assertVisible("ChildContent", false);

                this.assertPrepared(mock, true);
                this.assertContentLoaded(mock, true);
                this.assertLoaded(mock, false);
                this.assertCompleted(mock, false);

                this.assertFired(mock, [EventType.ON_COMPLETE], () : void => {
                    this.assertVisible("ChildContent", true);
                    this.assertLoaded(mock, true);
                    this.assertCompleted(mock, true);
                    $done();
                });
            });
            mock.Visible(true);
        };
    }

    public testStateOnVisibleCallAsyncParentSyncChild() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : CommonsDrawTestParentMock = new CommonsDrawTestParentMock();
            mock.childMock.DisableAsynchronousDraw();
            mock.InstanceOwner(new BaseViewer());
            mock.Visible(true);
            StaticPageContentManager.BodyAppend(mock.Draw());
            StaticPageContentManager.Draw();

            this.assertFired(mock, [EventType.ON_START, EventType.ON_LOAD], () : void => {
                this.assertVisible(mock.Id(), true);
                this.assertVisible("ParentContent", true);
                this.assertExists(mock.childMock.Id(), true);
                this.assertVisible(mock.childMock.Id(), true);
                this.assertVisible("ChildContent", true);

                this.assertPrepared(mock, true);
                this.assertContentLoaded(mock, true);
                this.assertLoaded(mock, false);
                this.assertCompleted(mock, false);

                this.assertFired(mock, [EventType.ON_COMPLETE], () : void => {
                    this.assertLoaded(mock, true);
                    this.assertCompleted(mock, true);
                    $done();
                });
            });
            mock.Visible(true);
        };
    }

    public testStateOnVisibleCallSyncParentSyncChild() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : CommonsDrawTestParentMock = new CommonsDrawTestParentMock();
            mock.DisableAsynchronousDraw();
            mock.childMock.DisableAsynchronousDraw();
            mock.InstanceOwner(new BaseViewer());
            mock.Visible(true);
            StaticPageContentManager.BodyAppend(mock.Draw());
            StaticPageContentManager.Draw();

            this.assertNotFired(mock, [EventType.ON_START]);
            this.assertFired(mock, [EventType.ON_LOAD], () : void => {
                this.assertLoaded(mock, true);
                this.assertCompleted(mock, true);
            });
            this.assertFired(mock, [EventType.ON_COMPLETE], () : void => {
                $done();
            });

            mock.Visible(true);

            this.assertVisible(mock.Id(), true);
            this.assertVisible("ParentContent", true);
            this.assertExists(mock.childMock.Id(), true);
            this.assertVisible(mock.childMock.Id(), true);
            this.assertVisible("ChildContent", true);
            this.assertLoaded(mock, false);
            this.assertCompleted(mock, false);
        };
    }

    public after() : void {
        this.initSendBox();
    }

    protected assertPrepared($element : any, $value : boolean = true) {
        assert.equal($element.IsPrepared(), $value,
            "test if element " + $element.Id() + " is prepared");
    }

    protected assertContentLoaded($element : any, $value : boolean = true) {
        assert.equal($element.isContentLoaded(), $value,
            "test if element's " + $element.Id() + " content is loaded");
    }

    protected assertLoaded($element : any, $value : boolean = true) {
        assert.equal($element.IsLoaded(), $value,
            "test if element " + $element.Id() + " is loaded");
    }

    protected assertCompleted($element : any, $value : boolean = true) {
        assert.equal($element.IsCompleted(), $value,
            "test if element " + $element.Id() + " is completed");
    }
}
