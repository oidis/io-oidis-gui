/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { EventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/EventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Alignment } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Alignment.js";
import { FitToParent } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/FitToParent.js";
import { GeneralCssNames } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/GeneralCssNames.js";
import { OrientationType } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/OrientationType.js";
import { VisibilityStrategy } from "../../../../../../../source/typescript/Io/Oidis/Gui/Enums/VisibilityStrategy.js";
import { GuiObjectManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/GuiObjectManager.js";
import { IScrollBar } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Components/IScrollBar.js";
import { IScrollBarEvents } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Events/IScrollBarEvents.js";
import { IContainerSizeInfo } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IBasePanel.js";
import { IGuiCommons } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IGuiCommons.js";
import { IGuiElement } from "../../../../../../../source/typescript/Io/Oidis/Gui/Interfaces/Primitives/IGuiElement.js";
import { BasePanel } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanel.js";
import { BasePanelViewer } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BasePanelViewer.js";
import { GuiCommons } from "../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { PropagableNumber } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/PropagableNumber.js";
import { Size } from "../../../../../../../source/typescript/Io/Oidis/Gui/Structures/Size.js";
import { ElementManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/Utils/ElementManager.js";

export class ResizeTestMockScrollBar extends GuiCommons implements IScrollBar {
    private guiType : any;
    private size : number;

    constructor() {
        super();
        this.DisableAsynchronousDraw();
        (<any>this).loaded = true;
    }

    public GuiType($orientationType? : any) : any {
        return this.guiType = Property.String(this.guiType, $orientationType);
    }

    public OrientationType($orientationType? : any) : any {
        return this.GuiType($orientationType);
    }

    public Size($value? : number) : number {
        return this.size = Property.Integer(this.size, $value);
    }

    public getEvents() : IScrollBarEvents {
        return <IScrollBarEvents>super.getEvents();
    }

    protected innerCode() : IGuiElement {
        this.getEvents().setOnShow(() : void => {
            ElementManager.setCssProperty(this.Id(), "width", 10);
            ElementManager.setCssProperty(this.Id(), "height", 10);
            ElementManager.setCssProperty(this.Id() + "_ContentOffset", "width", 15);
            ElementManager.setCssProperty(this.Id() + "_ContentOffset", "height", 15);
        });

        return super.innerCode();
    }

    protected innerHtml() : IGuiElement {
        return this.addElement(this.Id() + "_Type").StyleClassName(this.GuiType()).GuiTypeTag(this.getGuiTypeTag())
            .Add(this.addElement(this.Id() + "_ContentOffset").StyleClassName("ContentOffset"));
    }
}

export class ResizeTestMockCommons extends GuiCommons {
    private width : number;
    private height : number;

    public Width($value? : number) : number {
        return this.width = ObjectValidator.IsSet($value) ? $value : this.width;
    }

    public Height($value? : number) : number {
        return this.height = ObjectValidator.IsSet($value) ? $value : this.height;
    }
}

export class ResizeTestMockPanel extends BasePanel {
    public isResponsiveEventsEnabled : boolean;

    public static resize($element : ResizeTestMockPanel) : void {
        if ($element.isResponsiveEventsEnabled) {
            BasePanel.resize($element);
        }
    }

    constructor() {
        super();
        (<any>ElementManager).domCache = new ArrayList<HTMLElement>();
    }

    public EnableResponsiveEvents() : void {
        this.isResponsiveEventsEnabled = true;
    }

    public addChild($id : string) : ResizeTestMockCommons {
        const mock : ResizeTestMockCommons = new ResizeTestMockCommons($id);
        mock.Visible(true);
        mock.DisableAsynchronousDraw();
        mock.Parent(this);
        return this[$id] = mock;
    }

    public getChild($id : string) : ResizeTestMockCommons {
        return this[$id];
    }

    public setInnerHtmlFunction($function : any) : void {
        this.innerHtml = $function;
    }

    protected getScrollBarClass() : any {
        return ResizeTestMockScrollBar;
    }

    protected innerCode() : IGuiElement {
        this.Scrollable(true);
        return super.innerCode();
    }
}

(<any>ResizeTestMockPanel).onCommonsShow = ($eventArgs : EventArgs, $manager? : GuiObjectManager, $reflection? : Reflection) : void => {
    const commons : IGuiCommons = $eventArgs.Owner();
    const basePanel : ResizeTestMockPanel = <ResizeTestMockPanel>commons.Parent();
    if (basePanel.isResponsiveEventsEnabled) {
        (<any>BasePanel).onCommonsShow($eventArgs, $manager, $reflection);
    }
};

(<any>ResizeTestMockPanel).onCommonsHide = ($eventArgs : EventArgs, $manager? : GuiObjectManager, $reflection? : Reflection) : void => {
    const commons : IGuiCommons = $eventArgs.Owner();
    const basePanel : ResizeTestMockPanel = <ResizeTestMockPanel>commons.Parent();
    if (basePanel.isResponsiveEventsEnabled) {
        (<any>BasePanel).onCommonsHide($eventArgs, $manager, $reflection);
    }
};

export class BasePanelResizeTest extends UnitTestRunner {
    public testIsVisibilityToggleHandled() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .Add(this.addColumn()
                        .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                        .Add(this.addChild("child1")))
                    .Add(this.addChild("child2"));
            });
            assert.onGuiComplete(mock, () : void => {
                assert.equal((<any>BasePanel).isVisibilityToggleHandled(mock.getChild("child1")), true);
                assert.equal((<any>BasePanel).isVisibilityToggleHandled(mock.getChild("child2")), false);

            }, $done, new BasePanelViewer());
        };
    }

    public testIsVisibilityToggleHandledPropagated() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                    .Add(this.addChild("child1"))
                    .Add(this.addChild("child2"));
            });
            assert.onGuiComplete(mock, () : void => {
                assert.equal((<any>BasePanel).isVisibilityToggleHandled(mock.getChild("child1")), true);
                assert.equal((<any>BasePanel).isVisibilityToggleHandled(mock.getChild("child2")), true);
            }, $done, new BasePanelViewer());
        };
    }

    public testHandleContainerVisibilitySingleHide() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .Add(this.addColumn()
                        .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                        .Add(this.addChild("child1")))
                    .Add(this.addChild("child2"));
            });
            assert.onGuiComplete(mock, () : void => {
                mock.getChild("child1").Visible(false);

                (<any>BasePanel).handleContainerVisibility(mock.getInnerHtmlMap(),
                    (<any>mock.getInnerHtmlMap()).getVisibilityStrategy());

                this.assertVisible(mock.getInnerHtmlMap().getId(), true);
                this.assertVisible(mock.getChild("child1").getWrappingContainer().getId(), false);
            }, $done, new BasePanelViewer());
        };
    }

    public testHandleContainerVisibilityHideAll() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .Add(this.addColumn()
                        .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                        .Add(this.addChild("child1")))
                    .Add(this.addChild("child2"));
            });
            assert.onGuiComplete(mock, () : void => {
                mock.getChild("child1").Visible(false);
                mock.getChild("child2").Visible(false);

                (<any>BasePanel).handleContainerVisibility(mock.getInnerHtmlMap(),
                    (<any>mock.getInnerHtmlMap()).getVisibilityStrategy());

                this.assertVisible(mock.getInnerHtmlMap().getId(), true);
                this.assertVisible(mock.getChild("child1").getWrappingContainer().getId(), false);
                this.assertVisible(mock.getChild("child2").getWrappingElement().getId(), true);
            }, $done, new BasePanelViewer());
        };
    }

    public testHandleContainerVisibilityPropagatedSingleHide() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                    .Add(this.addChild("child1"))
                    .Add(this.addChild("child2"));
            });
            assert.onGuiComplete(mock, () : void => {
                mock.getChild("child1").Visible(false);

                (<any>BasePanel).handleContainerVisibility(mock.getInnerHtmlMap(),
                    (<any>mock.getInnerHtmlMap()).getVisibilityStrategy());

                this.assertVisible(mock.getInnerHtmlMap().getId(), true);
                this.assertVisible(mock.getChild("child1").getWrappingElement().getId(), false);
            }, $done, new BasePanelViewer());
        };
    }

    public testHandleContainerVisibilityPropagatedHideAll() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                    .Add(this.addChild("child1"))
                    .Add(this.addChild("child2"));
            });
            assert.onGuiComplete(mock, () : void => {
                mock.getChild("child1").Visible(false);
                mock.getChild("child2").Visible(false);

                (<any>BasePanel).handleContainerVisibility(mock.getInnerHtmlMap(),
                    (<any>mock.getInnerHtmlMap()).getVisibilityStrategy());

                this.assertVisible(mock.getInnerHtmlMap().getId(), false);
                this.assertVisible(mock.getChild("child1").getWrappingElement().getId(), false);
                this.assertVisible(mock.getChild("child2").getWrappingElement().getId(), false);
            }, $done, new BasePanelViewer());
        };
    }

    public testHandleContainerVisibilityPropagatedVisibleEmptyColumn() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                    .Add(this.addChild("child1"))
                    .Add(this.addColumn("column"));
            });
            assert.onGuiComplete(mock, () : void => {
                mock.getChild("child1").Visible(false);

                (<any>BasePanel).handleContainerVisibility(mock.getInnerHtmlMap(),
                    (<any>mock.getInnerHtmlMap()).getVisibilityStrategy());

                this.assertVisible(mock.getInnerHtmlMap().getId(), false);
                this.assertVisible(mock.getChild("child1").getWrappingElement().getId(), false);
                this.assertVisible("column", true);
            }, $done, new BasePanelViewer());
        };
    }

    public testHandleContainerVisibilityPropagatedNested() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .Add(this.addColumn("col1")
                        .Add(this.addRow("col1_row1")
                            .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                            .Add(this.addChild("child1"))
                            .Add(this.addColumn("col1_row1_col1")
                                .Add(this.addChild("child2")))
                            .Add(this.addColumn("col1_row1_col2"))))
                    .Add(this.addColumn("col2")
                        .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                        .Add(this.addColumn()
                            .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                            .Add(this.addChild("child3")))
                        .Add(this.addColumn()
                            .Add(this.addChild("child4"))));
            });
            assert.onGuiComplete(mock, () : void => {
                mock.getChild("child2").Visible(false);
                mock.getChild("child3").Visible(false);
                mock.getChild("child4").Visible(false);

                (<any>BasePanel).handleContainerVisibility(mock.getInnerHtmlMap(),
                    (<any>mock.getInnerHtmlMap()).getVisibilityStrategy());

                this.assertVisible(mock.getInnerHtmlMap().getId(), true);
                this.assertVisible("col1", true);
                this.assertVisible("col1_row1", true);
                this.assertVisible(mock.getChild("child1").getWrappingElement().getId(), true);
                this.assertVisible("col1_row1_col1", false);
                this.assertVisible(mock.getChild("child2").getWrappingElement().getId(), false);
                this.assertVisible("col1_row1_col2", true);
                this.assertVisible("col2", false);
                this.assertVisible(mock.getChild("child3").getWrappingContainer().getId(), false);
                this.assertVisible(mock.getChild("child4").getWrappingContainer().getId(), true);
            }, $done, new BasePanelViewer());
        };
    }

    public testGetContainerSizeInfoEmptyColumnsHeightOfRowIgnored() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .Add(this.addColumn().HeightOfRow("99999px"))
                    .Add(this.addColumn().HeightOfRow("99999px"));
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                const sizeInfo : IContainerSizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size);

                assert.equal(sizeInfo.elementsWithoutWidth.Length(), 2);
                assert.equal(sizeInfo.elementsWithoutHeight.Length(), 2);
                assert.equal(sizeInfo.remainingWidth, size.Width());
                assert.equal(sizeInfo.remainingHeight, size.Height());
            }, $done, new BasePanelViewer());
        };
    }

    public testGetContainerSizeInfoRowWidthOfColumnIgnored() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn()
                    .Add(this.addRow().WidthOfColumn("99999px"))
                    .Add(this.addRow().WidthOfColumn("99999px"));
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                const sizeInfo : IContainerSizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size);

                assert.equal(sizeInfo.elementsWithoutWidth.Length(), 2);
                assert.equal(sizeInfo.elementsWithoutHeight.Length(), 2);
                assert.equal(sizeInfo.remainingWidth, size.Width());
                assert.equal(sizeInfo.remainingHeight, size.Height());
            }, $done, new BasePanelViewer());
        };
    }

    public testGetContainerSizeInfoColumnWidthSet() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .Add(this.addColumn().WidthOfColumn("50%"))
                    .Add(this.addColumn());
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                const sizeInfo : IContainerSizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size);

                assert.equal(sizeInfo.elementsWithoutWidth.Length(), 1);
                assert.equal(sizeInfo.elementsWithoutHeight.Length(), 2);
                assert.equal(sizeInfo.remainingWidth, size.Width() / 2);
                assert.equal(sizeInfo.remainingHeight, size.Height());
            }, $done, new BasePanelViewer());
        };
    }

    public testGetContainerSizeInfoRowHeightSet() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn()
                    .Add(this.addRow().HeightOfRow("50%"))
                    .Add(this.addRow());
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                const sizeInfo : IContainerSizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size);

                assert.equal(sizeInfo.elementsWithoutWidth.Length(), 2);
                assert.equal(sizeInfo.elementsWithoutHeight.Length(), 1);
                assert.equal(sizeInfo.remainingWidth, size.Width());
                assert.equal(sizeInfo.remainingHeight, size.Height() / 2);
            }, $done, new BasePanelViewer());
        };
    }

    public testGetContainerSizeInfoColumnInvisibleCommons() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                    .Add(this.addColumn().WidthOfColumn("50%")
                        .Add(this.addChild("child1")))
                    .Add(this.addColumn()
                        .Add(this.addChild("child2")));
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);
                mock.getChild("child1").Visible(false);

                (<any>BasePanel).handleContainerVisibility(mock.getInnerHtmlMap(),
                    (<any>mock.getInnerHtmlMap()).getVisibilityStrategy());
                const sizeInfo : IContainerSizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size);

                assert.equal(sizeInfo.elementsWithoutWidth.Length(), 1);
                assert.equal(sizeInfo.elementsWithoutHeight.Length(), 1);
                assert.equal(sizeInfo.remainingWidth, size.Width());
                assert.equal(sizeInfo.remainingHeight, size.Height());
            }, $done, new BasePanelViewer());
        };
    }

    public testGetContainerSizeInfoRowInvisibleCommons() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn()
                    .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY_PROPAGATED)
                    .Add(this.addRow().HeightOfRow("50%")
                        .Add(this.addChild("child1")))
                    .Add(this.addRow()
                        .Add(this.addChild("child2")));
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);
                mock.getChild("child1").Visible(false);

                (<any>BasePanel).handleContainerVisibility(mock.getInnerHtmlMap(),
                    (<any>mock.getInnerHtmlMap()).getVisibilityStrategy());
                const sizeInfo : IContainerSizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size);

                assert.equal(sizeInfo.elementsWithoutWidth.Length(), 1);
                assert.equal(sizeInfo.elementsWithoutHeight.Length(), 1);
                assert.equal(sizeInfo.remainingWidth, size.Width());
                assert.equal(sizeInfo.remainingHeight, size.Height());
            }, $done, new BasePanelViewer());
        };
    }

    public testHandleInnerPaddingsFlatRows() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn("col1")
                    .HeightOfRow("10%", true)
                    .Add(this.addRow("row1"))
                    .Add(this.addRow("row2"));
            });

            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                this.assertExists("col1_" + GeneralCssNames.PADDING_BEFORE, false);
                this.assertExists("col1_" + GeneralCssNames.PADDING_AFTER, false);

                let sizeInfo : IContainerSizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size,
                    undefined, (<any>mock.getInnerHtmlMap()).getHeightOfRow());
                (<any>BasePanel).handleInnerPaddings(mock.getInnerHtmlMap(), sizeInfo, size);

                this.assertSize("col1_" + GeneralCssNames.PADDING_BEFORE, null, 0);
                this.assertSize("col1_" + GeneralCssNames.PADDING_AFTER, null, 400);

                (<any>mock.getInnerHtmlMap()).HeightOfRow(undefined);

                sizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size,
                    undefined, (<any>mock.getInnerHtmlMap()).getHeightOfRow());
                (<any>BasePanel).handleInnerPaddings(mock.getInnerHtmlMap(), sizeInfo, size);

                this.assertSize("col1_" + GeneralCssNames.PADDING_BEFORE, null, 0);
                this.assertSize("col1_" + GeneralCssNames.PADDING_AFTER, null, 0);
            }, $done, new BasePanelViewer());
        };
    }

    public testHandleInnerPaddingsFlatColumns() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow("row1")
                    .WidthOfColumn("10%", true)
                    .Add(this.addColumn("col1"))
                    .Add(this.addColumn("col2"));
            });

            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                this.assertExists("row1_" + GeneralCssNames.PADDING_BEFORE, false);
                this.assertExists("row1_" + GeneralCssNames.PADDING_AFTER, false);

                let sizeInfo : IContainerSizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size,
                    (<any>mock.getInnerHtmlMap()).getWidthOfColumn(), undefined);
                (<any>BasePanel).handleInnerPaddings(mock.getInnerHtmlMap(), sizeInfo, size);

                this.assertSize("row1_" + GeneralCssNames.PADDING_BEFORE, 0, null);
                this.assertSize("row1_" + GeneralCssNames.PADDING_AFTER, 800, null);

                (<any>mock.getInnerHtmlMap()).WidthOfColumn(undefined);

                sizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size,
                    (<any>mock.getInnerHtmlMap()).getWidthOfColumn(), undefined);
                (<any>BasePanel).handleInnerPaddings(mock.getInnerHtmlMap(), sizeInfo, size);

                this.assertSize("row1_" + GeneralCssNames.PADDING_BEFORE, 0, null);
                this.assertSize("row1_" + GeneralCssNames.PADDING_AFTER, 0, null);
            }, $done, new BasePanelViewer());
        };
    }

    public testRecomputeNestedGuiElementSizesUnset() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn("col1")
                    .Add(this.addRow("row1")
                        .Add(this.addChild("child1"))
                        .Add(this.addChild("child2"))
                    )
                    .Add(this.addRow("row2"));
            });

            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                (<any>BasePanel).recomputeNestedGuiElementSizes(mock.getInnerHtmlMap(), size);

                this.assertSize(mock.getChild("child1").getWrappingElement().getId(), 500, 250);
                this.assertSize(mock.getChild("child2").getWrappingElement().getId(), 500, 250);

                this.assertExists("col1_" + GeneralCssNames.PADDING_BEFORE, false);
                this.assertExists("col1_" + GeneralCssNames.PADDING_AFTER, false);
                this.assertExists("row1_" + GeneralCssNames.PADDING_BEFORE, false);
                this.assertExists("row1_" + GeneralCssNames.PADDING_AFTER, false);
                this.assertExists("row2_" + GeneralCssNames.PADDING_BEFORE, false);
                this.assertExists("row2_" + GeneralCssNames.PADDING_AFTER, false);
            }, $done, new BasePanelViewer());
        };
    }

    public testRecomputeNestedGuiElementSizesPropagatedPx() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn("col1")
                    .WidthOfColumn("200px", true)
                    .HeightOfRow("100px", true)
                    .Add(this.addRow("col1_row1")
                        .Add(this.addColumn("col1_row1_col1")
                            .Add(this.addRow("col1_row1_col1_row1")
                                .Add(this.addChild("child1"))))
                        .Add(this.addChild("child2")));
            });

            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                (<any>BasePanel).recomputeNestedGuiElementSizes(mock.getInnerHtmlMap(), size,
                    (<any>mock.getInnerHtmlMap()).getWidthOfColumn(), (<any>mock.getInnerHtmlMap()).getHeightOfRow());

                // root wrapper size is not consistent with set width or height, perhaps it should be
                this.assertSize("col1", size.Width(), null);

                this.assertSize(mock.getChild("child1").getWrappingElement().getId(), 200, 100);
                this.assertSize(mock.getChild("child2").getWrappingElement().getId(), 200, 100);

                this.assertSize("col1_row1_" + GeneralCssNames.PADDING_BEFORE, 0, null);
                this.assertSize("col1_row1_" + GeneralCssNames.PADDING_AFTER, size.Width() - 200 * 2, null);
                this.assertSize("col1_row1_col1_" + GeneralCssNames.PADDING_BEFORE, null, 0);
                this.assertSize("col1_row1_col1_" + GeneralCssNames.PADDING_AFTER, null, 0);
                this.assertSize("col1_row1_col1_row1_" + GeneralCssNames.PADDING_BEFORE, 0, null);
                this.assertSize("col1_row1_col1_row1_" + GeneralCssNames.PADDING_AFTER, 0, null);
            }, $done, new BasePanelViewer());
        };
    }

    public testRecomputeNestedGuiElementSizesPropagatedPct() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn("col1")
                    .WidthOfColumn("50%", true)
                    .HeightOfRow("50%", true)
                    .Add(this.addRow("col1_row1")
                        .Add(this.addColumn("col1_row1_col1")
                            .Add(this.addRow("col1_row1_col1_row1")
                                .Add(this.addChild("child1"))))
                        .Add(this.addColumn("col1_row1_col2")
                            .Add(this.addChild("child2"))));
            });

            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                (<any>BasePanel).recomputeNestedGuiElementSizes(mock.getInnerHtmlMap(), size,
                    (<any>mock.getInnerHtmlMap()).getWidthOfColumn(), (<any>mock.getInnerHtmlMap()).getHeightOfRow());

                // root wrapper size is not consistent with set width or height due to current recursion implementation
                this.assertSize("col1", size.Width(), null);

                // user can look at this as automatic column creation
                this.assertSize(mock.getChild("child1").getWrappingElement().getId(), 1000 * .5 * .5, 500 * .5 * .5);
                // user can look at this as automatic row creation, even though column is created -> additional multiplication is
                // avoided
                this.assertSize(mock.getChild("child2").getWrappingElement().getId(), 1000 * .5, 500 * .5 * .5);

                this.assertSize("col1_row1_" + GeneralCssNames.PADDING_BEFORE, 0, null);
                this.assertSize("col1_row1_" + GeneralCssNames.PADDING_AFTER, 0, null);
                this.assertSize("col1_row1_col1_" + GeneralCssNames.PADDING_BEFORE, null, 0);
                this.assertSize("col1_row1_col1_" + GeneralCssNames.PADDING_AFTER, null, 500 * .5 * .5);
                this.assertSize("col1_row1_col1_row1_" + GeneralCssNames.PADDING_BEFORE, 0, null);
                this.assertSize("col1_row1_col1_row1_" + GeneralCssNames.PADDING_AFTER, 1000 * .5 * .5, null);
            }, $done, new BasePanelViewer());
        };
    }

    public testRecomputeNestedGuiElementSizesFillOutRest() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn()
                    .Add(this.addRow("row1")
                        .HeightOfRow("100px")
                        .Add(this.addColumn()
                            .WidthOfColumn("100px")
                            .Add(this.addChild("child1"))
                        )
                        .Add(this.addChild("child2"))
                    )
                    .Add(this.addRow("row2"));
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                (<any>BasePanel).recomputeNestedGuiElementSizes(mock.getInnerHtmlMap(), size);

                this.assertSize(mock.getChild("child1").getWrappingElement().getId(), 100, 100);
                this.assertSize(mock.getChild("child2").getWrappingElement().getId(), 900, 100);
                this.assertSize("row1", null, 100);
                this.assertSize("row2", null, 400);

                this.assertExists("col1_" + GeneralCssNames.PADDING_BEFORE, false);
                this.assertExists("col1_" + GeneralCssNames.PADDING_AFTER, false);
                this.assertExists("row1_" + GeneralCssNames.PADDING_BEFORE, false);
                this.assertExists("row1_" + GeneralCssNames.PADDING_AFTER, false);
                this.assertExists("row2_" + GeneralCssNames.PADDING_BEFORE, false);
                this.assertExists("row2_" + GeneralCssNames.PADDING_AFTER, false);
            }, $done, new BasePanelViewer());
        };
    }

    public testRecomputeNestedGuiElementSizesPctAndPx() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn("col1")
                    .Add(this.addRow("row1")
                        .HeightOfRow("100px")
                        .Add(this.addColumn()
                            .WidthOfColumn("100px")
                            .Add(this.addChild("child1"))
                        )
                        .Add(this.addColumn()
                            .WidthOfColumn("50%")
                            .Add(this.addChild("child2"))
                        )
                    );
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                (<any>BasePanel).recomputeNestedGuiElementSizes(mock.getInnerHtmlMap(), size);

                this.assertSize(mock.getChild("child1").getWrappingElement().getId(), 100, 100);
                this.assertSize(mock.getChild("child2").getWrappingElement().getId(), 500, 100);

                this.assertSize("col1_" + GeneralCssNames.PADDING_BEFORE, null, 0);
                this.assertSize("col1_" + GeneralCssNames.PADDING_AFTER, null, 400);
                this.assertSize("row1_" + GeneralCssNames.PADDING_BEFORE, 0, null);
                this.assertSize("row1_" + GeneralCssNames.PADDING_AFTER, 400, null);
            }, $done, new BasePanelViewer());
        };
    }

    public testRecomputeInnerPaddingsFlatRows() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn("col1")
                    .HeightOfRow("10%", true)
                    .Add(this.addRow("row1"))
                    .Add(this.addRow("row2"));
            });

            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(500);
                size.Height(500);

                Alignment.getProperties().forEach(($property : string) : void => {
                    const value : Alignment = Alignment[$property];
                    if (Alignment.Contains(value) && !Alignment.IsPropagated(value)) {

                        const sizeInfo : IContainerSizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size,
                            undefined, (<any>mock.getInnerHtmlMap()).getHeightOfRow());
                        (<any>BasePanel).handleInnerPaddings(mock.getInnerHtmlMap(), sizeInfo, size);
                        (<any>BasePanel).recomputeInnerPaddings(mock.getInnerHtmlMap(), value);

                        let beforeHeight : number = null;
                        let afterHeight : number = null;
                        switch (value) {
                        case Alignment.LEFT:
                        case Alignment.RIGHT:
                        case Alignment.CENTER:
                            beforeHeight = 200;
                            afterHeight = 200;
                            break;
                        case Alignment.TOP_LEFT:
                        case Alignment.TOP:
                        case Alignment.TOP_RIGHT:
                            beforeHeight = 0;
                            afterHeight = 0; // important for content that expands
                            break;
                        case Alignment.BOTTOM_LEFT:
                        case Alignment.BOTTOM:
                        case Alignment.BOTTOM_RIGHT:
                            beforeHeight = 400;
                            afterHeight = 0;
                            break;
                        default:
                            break;
                        }

                        this.assertSize("col1_" + GeneralCssNames.PADDING_BEFORE, null, beforeHeight,
                            "for alignment " + value.toString());
                        this.assertSize("col1_" + GeneralCssNames.PADDING_AFTER, null, afterHeight,
                            "for alignment " + value.toString());
                    }
                });
            }, $done, new BasePanelViewer());
        };
    }

    public testRecomputeInnerPaddingsFlatColumns() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow("row1")
                    .WidthOfColumn("10%", true)
                    .Add(this.addColumn("col1"))
                    .Add(this.addColumn("col2"));
            });

            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(500);
                size.Height(500);

                Alignment.getProperties().forEach(($property : string) : void => {
                    const value : Alignment = Alignment[$property];
                    if (Alignment.Contains(value) && !Alignment.IsPropagated(value)) {

                        const sizeInfo : IContainerSizeInfo = (<any>BasePanel).getContainerSizeInfo(mock.getInnerHtmlMap(), size,
                            (<any>mock.getInnerHtmlMap()).getWidthOfColumn(), undefined);
                        (<any>BasePanel).handleInnerPaddings(mock.getInnerHtmlMap(), sizeInfo, size);
                        (<any>BasePanel).recomputeInnerPaddings(mock.getInnerHtmlMap(), value);

                        let beforeWidth : number = null;
                        let afterWidth : number = null;
                        switch (value) {
                        case Alignment.TOP_LEFT:
                        case Alignment.LEFT:
                        case Alignment.BOTTOM_LEFT:
                            beforeWidth = 0;
                            afterWidth = 400;
                            break;
                        case Alignment.TOP:
                        case Alignment.CENTER:
                        case Alignment.BOTTOM:
                            beforeWidth = 200;
                            afterWidth = 200;
                            break;
                        case Alignment.TOP_RIGHT:
                        case Alignment.RIGHT:
                        case Alignment.BOTTOM_RIGHT:
                            beforeWidth = 400;
                            afterWidth = 0;
                            break;
                        default:
                            break;
                        }

                        this.assertSize("row1_" + GeneralCssNames.PADDING_BEFORE, beforeWidth, null,
                            "for alignment " + value.toString());
                        this.assertSize("row1_" + GeneralCssNames.PADDING_AFTER, afterWidth, null,
                            "for alignment " + value.toString());
                    }
                });
            }, $done, new BasePanelViewer());
        };
    }

    public testAlignNestedChildElementsNotPropagatedHandleImplicitColumn() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow("row1")
                    .Alignment(Alignment.CENTER)
                    .Add(this.addColumn("row1_col1")
                        .WidthOfColumn("250px")
                        .Add(this.addChild("child1"))
                    )
                    .Add(this.addColumn("row1_col2")
                        .WidthOfColumn("250px")
                        .Add(this.addRow("row1_col2_row1")
                            .Alignment(Alignment.RIGHT)
                            .WidthOfColumn("100px", true)
                            .Add(this.addChild("child2")))
                    );
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                (<any>BasePanel).recomputeNestedGuiElementSizes(mock.getInnerHtmlMap(), size);
                (<any>BasePanel).alignNestedChildElements(mock.getInnerHtmlMap(), (<any>mock.getInnerHtmlMap()).getAlignment());

                assert.equal(ElementManager.getClassName(mock.getChild("child1").getWrappingElement().getId()),
                    Alignment.StyleClassName(Alignment.getDefaultValue(), (<any>mock).getCssInterfaceName()));
                assert.equal(ElementManager.getClassName(mock.getChild("child2").getWrappingElement().getId()),
                    Alignment.StyleClassName(Alignment.RIGHT, (<any>mock).getCssInterfaceName())); // implicit column handled

                this.assertExists("row1_col1_" + GeneralCssNames.PADDING_BEFORE, false);
                this.assertExists("row1_col1_" + GeneralCssNames.PADDING_AFTER, false);

                this.assertSize("row1_col2_row1_" + GeneralCssNames.PADDING_BEFORE, 150, null);
                this.assertSize("row1_col2_row1_" + GeneralCssNames.PADDING_AFTER, 0, null);
            }, $done, new BasePanelViewer());
        };
    }

    public testAlignNestedChildElementsPropagated() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn("col1")
                    .Alignment(Alignment.CENTER_PROPAGATED)
                    .Add(this.addRow("col1_row1")
                        .HeightOfRow("200px")
                        .Add(this.addColumn("col1_row1_col1")
                            .Alignment(Alignment.BOTTOM_PROPAGATED)
                            .Add(this.addRow("col1_row1_col1_row1")
                                .HeightOfRow("100px")
                                .Add(this.addChild("child1")))))
                    .Add(this.addRow("col1_row2")
                        .HeightOfRow("100px")
                        .Alignment(Alignment.RIGHT_PROPAGATED)
                        .Add(this.addColumn("col1_row2_col1")
                            .WidthOfColumn("100px")
                            .Add(this.addChild("child2"))));
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);
                (<any>BasePanel).recomputeNestedGuiElementSizes(mock.getInnerHtmlMap(), size);
                (<any>BasePanel).alignNestedChildElements(mock.getInnerHtmlMap(), (<any>mock.getInnerHtmlMap()).getAlignment());

                assert.equal(ElementManager.getClassName(mock.getChild("child1").getWrappingElement().getId()),
                    Alignment.StyleClassName(Alignment.BOTTOM, (<any>mock).getCssInterfaceName()));
                assert.equal(ElementManager.getClassName(mock.getChild("child2").getWrappingElement().getId()),
                    Alignment.StyleClassName(Alignment.RIGHT, (<any>mock).getCssInterfaceName()));

                this.assertSize("col1_" + GeneralCssNames.PADDING_BEFORE, null, 100);
                this.assertSize("col1_" + GeneralCssNames.PADDING_AFTER, null, 100);
                this.assertSize("col1_row2_" + GeneralCssNames.PADDING_BEFORE, 900, null);
                this.assertSize("col1_row2_" + GeneralCssNames.PADDING_AFTER, 0, null);
                this.assertSize("col1_row1_col1_" + GeneralCssNames.PADDING_BEFORE, null, 100);
                this.assertSize("col1_row1_col1_" + GeneralCssNames.PADDING_AFTER, null, 0);
            }, $done, new BasePanelViewer());
        };
    }

    public testFitToParentNestedChildElements() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn()
                    .FitToParent(FitToParent.FULL)
                    .Add(this.addRow("row")
                        .HeightOfRow("100px")
                        .Add(this.addColumn()
                            .WidthOfColumn("250px")
                            .Add(this.addChild("child1"))
                        )
                        .Add(this.addColumn()
                            .FitToParent(FitToParent.HORIZONTAL)
                            .WidthOfColumn("250px")
                            .Add(this.addChild("child2"))
                        )
                        .Add(this.addColumn()
                            .FitToParent(FitToParent.VERTICAL)
                            .WidthOfColumn("250px")
                            .Add(this.addChild("child3"))
                        )
                        .Add(this.addColumn()
                            .FitToParent(FitToParent.NONE)
                            .WidthOfColumn("250px")
                            .Add(this.addChild("child4"))
                        )
                    );
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                (<any>BasePanel).recomputeNestedGuiElementSizes(mock.getInnerHtmlMap(), size);
                (<any>BasePanel).fitToParentNestedChildElements(mock.getInnerHtmlMap(), FitToParent.FULL);

                assert.equal(mock.getChild("child1").Width(), 250);
                assert.equal(mock.getChild("child1").Height(), 100);
                assert.equal(mock.getChild("child2").Width(), 250);
                assert.equal(mock.getChild("child2").Height(), undefined);
                assert.equal(mock.getChild("child3").Width(), undefined);
                assert.equal(mock.getChild("child3").Height(), 100);
                assert.equal(mock.getChild("child4").Width(), undefined);
                assert.equal(mock.getChild("child4").Height(), undefined);
            }, $done, new BasePanelViewer());
        };
    }

    public testContentResizeHandlerNonResponsiveRootDisabled() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addElement("col1")
                    .Add(this.addRow("col1_row1")
                        .FitToParent(FitToParent.FULL)
                        .VisibilityStrategy(Alignment.CENTER_PROPAGATED)
                        .HeightOfRow("100px")
                        .Add(this.addColumn("col1_row1_col1")
                            .WidthOfColumn("100px")
                            .Add(this.addChild("child1"))
                        )
                        .Add(this.addColumn("col1_row1_col2")
                            .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                            .WidthOfColumn("50%")
                            .Add(this.addChild("child2"))
                        )
                    );
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                mock.getChild("child2").Visible(false);

                (<any>BasePanel).contentResizeHandler(mock, size.Width(), size.Height());

                this.assertSize(mock.getChild("child1").getWrappingElement().getId(), null, null);
                assert.equal(mock.getChild("child1").Width(), undefined);
                assert.equal(mock.getChild("child1").Height(), undefined);
                this.assertExists("col1_" + GeneralCssNames.PADDING_BEFORE, false);
                this.assertExists("col1_" + GeneralCssNames.PADDING_AFTER, false);
                this.assertVisible("col1_row1_col2", true);
                assert.notEqual(ElementManager.getClassName(mock.getChild("child1").getWrappingElement().getId()),
                    Alignment.StyleClassName(Alignment.getDefaultValue(), (<any>mock).getCssInterfaceName()));
            }, $done, new BasePanelViewer());
        };
    }

    public testContentResizeHandler() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn("col1")
                    .Add(this.addRow("col1_row1")
                        .FitToParent(FitToParent.FULL)
                        .VisibilityStrategy(Alignment.CENTER_PROPAGATED)
                        .HeightOfRow("100px")
                        .Add(this.addColumn("col1_row1_col1")
                            .WidthOfColumn("100px")
                            .Add(this.addChild("child1"))
                        )
                        .Add(this.addColumn("col1_row1_col2")
                            .VisibilityStrategy(VisibilityStrategy.COLLAPSE_EMPTY)
                            .WidthOfColumn("50%")
                            .Add(this.addChild("child2"))
                        )
                    );
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                mock.getChild("child2").Visible(false);

                (<any>BasePanel).contentResizeHandler(mock, size.Width(), size.Height());

                this.assertSize(mock.getChild("child1").getWrappingElement().getId(), 100, 100);
                assert.equal(mock.getChild("child1").Width(), 100);
                assert.equal(mock.getChild("child1").Height(), 100);
                this.assertExists("col1_" + GeneralCssNames.PADDING_BEFORE, true);
                this.assertExists("col1_" + GeneralCssNames.PADDING_AFTER, true);
                this.assertVisible("col1_row1_col2", false);
                assert.equal(ElementManager.getClassName(mock.getChild("child1").getWrappingElement().getId()),
                    Alignment.StyleClassName(Alignment.getDefaultValue(), (<any>mock).getCssInterfaceName()));
            }, $done, new BasePanelViewer());
        };
    }

    public testContentResizeHandlerRootRowSet() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow("row1")
                    .HeightOfRow("99999px");
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                (<any>BasePanel).contentResizeHandler(mock, size.Width(), size.Height());
                this.assertSize("row1", undefined, 99999);
            }, $done, new BasePanelViewer());
        };
    }

    public testContentResizeHandlerRootColSet() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn("col1")
                    .WidthOfColumn("99999px");
            });
            assert.onGuiComplete(mock, () : void => {
                const size : Size = new Size();
                size.Width(1000);
                size.Height(500);

                (<any>BasePanel).contentResizeHandler(mock, size.Width(), size.Height());
                this.assertSize("col1", 99999, undefined);
            }, $done, new BasePanelViewer());
        };
    }

    public testScrollBarsNotVisible() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            assert.onGuiComplete(mock, () : void => {
                this.assertVisible(mock.verticalScrollBar.Id(), false);
                this.assertVisible(mock.horizontalScrollBar.Id(), false);
            }, $done, new BasePanelViewer());
        };
    }

    public testShowScrollBarVerticalChange() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            assert.onGuiComplete(mock, () : void => {
                let change : any = (<any>BasePanel).showScrollBar(mock, true, OrientationType.VERTICAL);
                assert.equal(change.vertical, true);
                assert.equal(change.horizontal, false);
                this.assertVisible(mock.verticalScrollBar.Id(), true);
                change = (<any>BasePanel).showScrollBar(mock, false, OrientationType.VERTICAL);
                assert.equal(change.vertical, true);
                assert.equal(change.horizontal, false);
                this.assertVisible(mock.verticalScrollBar.Id(), false);
            }, $done, new BasePanelViewer());
        };
    }

    public testShowScrollBarHorizontalChange() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            assert.onGuiComplete(mock, () : void => {
                let change : any = (<any>BasePanel).showScrollBar(mock, true, OrientationType.HORIZONTAL);
                assert.equal(change.vertical, false);
                assert.equal(change.horizontal, true);
                this.assertVisible(mock.horizontalScrollBar.Id(), true);
                change = (<any>BasePanel).showScrollBar(mock, false, OrientationType.HORIZONTAL);
                assert.equal(change.vertical, false);
                assert.equal(change.horizontal, true);
                this.assertVisible(mock.horizontalScrollBar.Id(), false);
            }, $done, new BasePanelViewer());
        };
    }

    public testShowScrollBarVerticalHorizontalChange() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            assert.onGuiComplete(mock, () : void => {
                let change : any = (<any>BasePanel).showScrollBar(mock, true);
                assert.equal(change.vertical, true);
                assert.equal(change.horizontal, true);
                this.assertVisible(mock.verticalScrollBar.Id(), true);
                this.assertVisible(mock.horizontalScrollBar.Id(), true);
                change = (<any>BasePanel).showScrollBar(mock, false);
                assert.equal(change.vertical, true);
                assert.equal(change.horizontal, true);
                this.assertVisible(mock.verticalScrollBar.Id(), false);
                this.assertVisible(mock.horizontalScrollBar.Id(), false);
            }, $done, new BasePanelViewer());
        };
    }

    public testGetIScrollBarSizesInvisible() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            assert.onGuiComplete(mock, () : void => {
                const sizes : any = (<any>BasePanel).getScrollBarSizes(mock);
                assert.equal(sizes.width, 0);
                assert.equal(sizes.height, 0);
                assert.equal(sizes.contentOffsetWidth, 0);
                assert.equal(sizes.contentOffsetHeight, 0);
            }, $done, new BasePanelViewer());
        };
    }

    public testGetScrollBarSizesVertical() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            assert.onGuiComplete(mock, () : void => {
                (<any>BasePanel).showScrollBar(mock, true, OrientationType.VERTICAL);
                const sizes : any = (<any>BasePanel).getScrollBarSizes(mock);
                assert.equal(sizes.width, 10);
                assert.equal(sizes.height, 0);
                assert.equal(sizes.contentOffsetWidth, 15);
                assert.equal(sizes.contentOffsetHeight, 0);
            }, $done, new BasePanelViewer());
        };
    }

    public testGetScrollBarSizesHorizontal() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            assert.onGuiComplete(mock, () : void => {
                (<any>BasePanel).showScrollBar(mock, true, OrientationType.HORIZONTAL);
                const sizes : any = (<any>BasePanel).getScrollBarSizes(mock);
                assert.equal(sizes.width, 0);
                assert.equal(sizes.height, 10);
                assert.equal(sizes.contentOffsetWidth, 0);
                assert.equal(sizes.contentOffsetHeight, 15);
            }, $done, new BasePanelViewer());
        };
    }

    public testGetScrollBarSizes() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            assert.onGuiComplete(mock, () : void => {
                (<any>BasePanel).showScrollBar(mock, true);
                const sizes : any = (<any>BasePanel).getScrollBarSizes(mock);
                assert.equal(sizes.width, 10);
                assert.equal(sizes.height, 10);
                assert.equal(sizes.contentOffsetWidth, 15);
                assert.equal(sizes.contentOffsetHeight, 15);
            }, $done, new BasePanelViewer());
        };
    }

    public testScrollBarVisibilityHandlerVerticalResponsiveVisible() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .HeightOfRow(() : PropagableNumber => {
                        return new PropagableNumber(["100%", "1px"]);
                    });
            });
            mock.Width(1000);
            mock.Height(500);
            assert.onGuiComplete(mock, () : void => {
                ElementManager.setCssProperty(mock, "padding", 12);
                ElementManager.setCssProperty(mock, "margin", 3);
                const offset : number = (12 + 3) * 2;

                (<any>BasePanel).contentResizeHandler(mock, mock.Width() - offset, mock.Height() - offset);
                (<any>BasePanel).scrollBarVisibilityHandler(mock);

                const scrollBarSize : any = (<any>BasePanel).getScrollBarSizes(mock);
                this.assertSize(mock.Id() + "_PanelContentEnvelop",
                    mock.Width() - offset - scrollBarSize.contentOffsetWidth,
                    mock.Height() - offset - scrollBarSize.contentOffsetHeight);

                this.assertVisible(mock.verticalScrollBar.Id(), true);
                this.assertVisible(mock.horizontalScrollBar.Id(), false);
            }, $done, new BasePanelViewer());
        };
    }

    public testScrollBarVisibilityHandlerVerticalResponsiveHidden() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .HeightOfRow(() : PropagableNumber => {
                        return new PropagableNumber(["100%"]);
                    });
            });
            mock.Width(1000);
            mock.Height(500);
            assert.onGuiComplete(mock, () : void => {
                ElementManager.setCssProperty(mock, "padding", 12);
                ElementManager.setCssProperty(mock, "margin", 3);
                const offset : number = (12 + 3) * 2;

                (<any>BasePanel).contentResizeHandler(mock, mock.Width() - offset, mock.Height() - offset);
                (<any>BasePanel).scrollBarVisibilityHandler(mock);

                this.assertVisible(mock.verticalScrollBar.Id(), false);
                this.assertVisible(mock.horizontalScrollBar.Id(), false);

                this.assertSize(mock.Id() + "_PanelContentEnvelop",
                    mock.Width() - offset,
                    mock.Height() - offset);
            }, $done, new BasePanelViewer());
        };
    }

    public testScrollBarVisibilityHandlerHorizontalResponsiveVisible() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn()
                    .WidthOfColumn(() : PropagableNumber => {
                        return new PropagableNumber(["100%", "1px"]);
                    });
            });
            mock.Width(1000);
            mock.Height(500);
            assert.onGuiComplete(mock, () : void => {
                ElementManager.setCssProperty(mock, "padding", 12);
                ElementManager.setCssProperty(mock, "margin", 3);
                const offset : number = (12 + 3) * 2;

                (<any>BasePanel).contentResizeHandler(mock, mock.Width() - offset, mock.Height() - offset);
                (<any>BasePanel).scrollBarVisibilityHandler(mock);

                const scrollBarSize : any = (<any>BasePanel).getScrollBarSizes(mock);
                this.assertSize(mock.Id() + "_PanelContentEnvelop",
                    mock.Width() - offset - scrollBarSize.contentOffsetWidth,
                    mock.Height() - offset - scrollBarSize.contentOffsetHeight);

                this.assertVisible(mock.verticalScrollBar.Id(), false);
                this.assertVisible(mock.horizontalScrollBar.Id(), true);
            }, $done, new BasePanelViewer());
        };
    }

    public testScrollBarVisibilityHandlerHorizontalResponsiveHidden() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addColumn()
                    .WidthOfColumn(() : PropagableNumber => {
                        return new PropagableNumber(["100%"]);
                    });
            });
            mock.Width(1000);
            mock.Height(500);
            assert.onGuiComplete(mock, () : void => {
                ElementManager.setCssProperty(mock, "padding", 12);
                ElementManager.setCssProperty(mock, "margin", 3);
                const offset : number = (12 + 3) * 2;
                (<any>BasePanel).contentResizeHandler(mock, mock.Width() - offset, mock.Height() - offset);
                (<any>BasePanel).scrollBarVisibilityHandler(mock);

                this.assertVisible(mock.verticalScrollBar.Id(), false);
                this.assertVisible(mock.horizontalScrollBar.Id(), false);

                this.assertSize(mock.Id() + "_PanelContentEnvelop",
                    mock.Width() - offset,
                    mock.Height() - offset);
            }, $done, new BasePanelViewer());
        };
    }

    public testScrollBarVisibilityHandlerVerticalNotScrollable() : IUnitTestRunnerPromise {
        // todo: make width overflow when its possible to parse offsetWidth
        return ($done : () => void) : void => {
            const mock : ResizeTestMockPanel = new ResizeTestMockPanel();
            mock.setInnerHtmlFunction(function () {
                return this.addRow()
                    .HeightOfRow(() : PropagableNumber => {
                        return new PropagableNumber(["100%", "1px"]);
                    });
            });
            mock.Width(1000);
            mock.Height(500);
            assert.onGuiComplete(mock, () : void => {
                ElementManager.setCssProperty(mock, "padding", 12);
                ElementManager.setCssProperty(mock, "margin", 3);
                const offset : number = (12 + 3) * 2;

                mock.Scrollable(false);

                (<any>BasePanel).contentResizeHandler(mock, mock.Width() - offset, mock.Height() - offset);
                (<any>BasePanel).scrollBarVisibilityHandler(mock);

                this.assertVisible(mock.verticalScrollBar.Id(), false);
                this.assertVisible(mock.horizontalScrollBar.Id(), false);
            }, $done, new BasePanelViewer());
        };
    }

    // todo: horizontal and vertical when offset sizes will calculate properly
    // todo: non responsive tests when offset sizes will calculate properly
    // todo: test events, and integration into resize method

    protected assertVisible($id : string, $value : boolean = true) {
        assert.equal(ElementManager.getCssValue($id, "display") !== "none", $value,
            "test if element " + $id + " is visible");
    }

    protected setUp() : void {
        (<any>ElementManager).getOffsetWidthDefault = ElementManager.getOffsetWidth;
        ElementManager.getOffsetWidth = function ($id : string | IGuiCommons, $force : boolean = false,
                                                  $ignoreHidden : boolean = true) : number {
            return this.getOffsetWidthDefault($id, false, $ignoreHidden);
        };
        (<any>ElementManager).getOffsetHeightDefault = ElementManager.getOffsetHeight;
        ElementManager.getOffsetHeight = function ($id : string | IGuiCommons, $force : boolean = false,
                                                   $ignoreHidden : boolean = true) : number {
            return this.getOffsetHeightDefault($id, false, $ignoreHidden);
        };
    }

    protected tearDown() : void {
        ElementManager.getOffsetWidth = (<any>ElementManager).getOffsetWidthDefault;
        (<any>ElementManager).getOffsetWidthDefault = undefined;
        ElementManager.getOffsetHeight = (<any>ElementManager).getOffsetHeightDefault;
        (<any>ElementManager).getOffsetHeightDefault = undefined;
    }
}
