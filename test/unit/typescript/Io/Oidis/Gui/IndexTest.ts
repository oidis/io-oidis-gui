/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestLoader, UnitTestRunner } from "./UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Index } from "../../../../../../source/typescript/Io/Oidis/Gui/Index.js";
import { StaticPageContentManager } from "../../../../../../source/typescript/Io/Oidis/Gui/Utils/StaticPageContentManager.js";

export class IndexTest extends UnitTestRunner {

    public testConstructor() : void {
        assert.doesNotThrow(() : void => {
            const instance : Index = new Index();
        });
    }

    public __IgnoretestProcess() : void {
        UnitTestLoader.Load(<any>{
            build: {time: new Date().toTimeString(), timestamp: 1234, type: "prod"}, name: "com-wui-framework-gui", version: "1.0.0"
        });
        StaticPageContentManager.Clear(true);
        assert.resolveEqual(Index, "" +
            "<head>\n" +
            "<title>WUI - GUI Index</title>\n\n" +
            "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
            "<link href=\"resource/graphics/icon.ico?v=1234\" rel=\"shortcut icon\" type=\"text/css\">\n" +
            "<link href=\"resource/css/com-wui-framework-gui-1-0-0.min.css?v=1234\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
            "</head>\n\n" +
            "<body onfocus=\"Io.Oidis.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
            "onblur=\"Io.Oidis.Gui.Events.EventsManager.bodyBlurEventHandler();\">\n" +
            "<div id=\"Browser\" class=\"FIREFOX\">\n" +
            "<div id=\"Language\" class=\"En\">\n" +
            "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n" +
            "<div class=\"GuiInterface\">" +
            "<h1>WUI Framework base GUI Library</h1>" +
            "<h3>Common and abstract classes or interfaces connected with creation and handling of GUI.</h3>" +
            "<div class=\"Index\">" +
            "<h3>Runtime tests</h3>" +
            "<h4>Page content</h4>" +
            "<a href=\"#/com-wui-framework-gui/RuntimeTest/Io/Oidis/Gui/RuntimeTests/Utils/WindowManagerTest\">" +
            "Window Manager</a><br>" +
            "*<br>" +
            "<h4>GUI elements</h4>" +
            "<a href=\"#/com-wui-framework-gui/RuntimeTest/Io/Oidis/Gui/RuntimeTests/GuiObjectManagerTest\">" +
            "GUI Object Manager</a><br>" +
            "*<br>" +
            "<h4>GUI utils</h4>" +
            "<a href=\"#/com-wui-framework-gui/RuntimeTest/Io/Oidis/Commons/RuntimeTests/HttpRequestParserTest\">" +
            "Http Request Parser Test</a><br>" +
            "*<br>" +
            "<h4>Io.Oidis.Gui.ImageProcessor</h4>" +
            "<a href=\"#/com-wui-framework-gui/RuntimeTest/Io/Oidis/Gui/RuntimeTests/ImageProcessor/" +
            "ImageTransformTest\">" +
            "ImageTransform test</a><br>*<br>" +
            "<h4>Io.Oidis.Gui.Primitives</h4>" +
            "<a href=\"#/com-wui-framework-gui/RuntimeTest/Io/Oidis/Gui/RuntimeTests/Primitives/BaseGuiObjectTest\">" +
            "BaseGuiObject test</a><br>" +
            "*<br>" +
            "</div></div><div class=\"Note\">version: 1.0.0, build: *</div>\n" +
            "<div class=\"Logo\">\n" +
            "   <div class=\"WUI\"></div>\n" +
            "</div>\n\n" +
            "</div>\n" +
            "</div>\n" +
            "</div>\n\n" +
            "</body>");
    }

    protected tearDown() : void {
        this.initSendBox();
    }
}
