/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { HttpRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/HttpRequestEventArgs.js";
import { HttpRequestParser } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpRequestParser.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Events/EventType.js";
import { HttpRequestConstants } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Enums/HttpRequestConstants.js";
import { ViewerManagerEventArgs } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/Args/ViewerManagerEventArgs.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/EventsManager.js";
import { DrawGuiObject } from "../../../../../../../../source/typescript/Io/Oidis/Gui/HttpProcessor/Resolvers/DrawGuiObject.js";
import { BaseViewer } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/BaseViewer.js";
import { FormsObject } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/FormsObject.js";
import { GuiCommons } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import { ViewerManager } from "../../../../../../../../source/typescript/Io/Oidis/Gui/ViewerManager.js";

class MockFormsObject extends FormsObject {
}

class MockGuiCommons extends GuiCommons {
}

class MockGuiObject extends DrawGuiObject {
    public testdeveloperCorner() : string {
        return this.developerCorner();
    }

    public testcontextMenu() : string {
        const object : DrawGuiObject = new MockGuiObject();
        return this.contextMenu();
    }

    public testwuiBuilder() : string {
        const object : DrawGuiObject = new MockGuiObject();
        return this.builderContent();
    }

    public testincludeChildren() : boolean {
        const object : DrawGuiObject = new MockGuiObject();
        return this.includeChildren();
    }

    public testrefresh() : boolean {
        const object : DrawGuiObject = new MockGuiObject();
        return this.refresh();
    }

    public testresolver() : void {
        return this.resolver();
    }
}

export class DrawGuiObjectTest extends UnitTestRunner {

    public testConstructor() : void {
        const drawguiobject : DrawGuiObject = new DrawGuiObject();
    }

    public testObjectClassName() : void {
        const drawguiobject : DrawGuiObject = new DrawGuiObject();
        assert.equal(drawguiobject.ObjectClassName(null), "tTestLoader");
        assert.equal(drawguiobject.ObjectClassName("BaseGuiObject"), "BaseGuiObject");
        assert.equal(drawguiobject.ObjectClassName(), "BaseGuiObject");
        assert.equal(drawguiobject.ObjectClassName("TestMode"), HttpRequestConstants.TEST_MODE);
    }

    public __IgnoretestObjectTest() : void {
        const drawguiobject : DrawGuiObject = new DrawGuiObject();
        assert.patternEqual(this.stripInstrumentation(DrawGuiObject.toLocaleString()),
            "function DrawGuiObject() {\n" +
            "                                var _this = ((_super.call(this)) || /* istanbul ignore next /this);\n" +
            "                                _this.forceRefreshLinkEvents = new ElementEventsManager(\"ForceRefreshLink\");\n" +
            "                                _this.viewCodeLinkEvents = new ElementEventsManager(\"ViewCodeLink\");\n" +
            "_this.echoOutputLinkEvents = new ElementEventsManager(\"EchoOutputLink\");\n" +
            "                                _this.cacheLinkEvents = new ElementEventsManager(\"CacheLink\");\n" +
            "                                _this.cacheGeneratorEvents = new ElementEventsManager(\"CacheGeneratorLink\");\n" +
            "                                _this.backToIndexLinkEvents = new ElementEventsManager(\"BackToIndexLink\");\n" +
            "                                _this.echoOutputId = \"DeveloperCorner_EchoOutput\";\n" +
            "                                return _this;\n" +
            "                            }");
    }

    public testProcess() : void {
        const data : ArrayList<string> = new ArrayList<string>();
        data.Add("value2", "key2");
        data.Add("value3", "key3");
        data.Add("value4", HttpRequestConstants.TEST_MODE);

        const args : HttpRequestEventArgs = new HttpRequestEventArgs(
            "/com-wui-framework-gui/web/Io/Oidis/Gui/RuntimeTests/BasePanelViewerTest/TestMode", data);
        assert.equal(args.POST().getItem(HttpRequestConstants.TEST_MODE), "value4");
    }

    public testCorner() : void {
        const drawGui : MockGuiObject = new MockGuiObject();
        assert.patternEqual(drawGui.testdeveloperCorner().toString(),
            "<div guiType=\"DeveloperCorner\" class=\"DeveloperCorner\">\r\n" +
            "   <div id=\"DeveloperCorner_Distance\" class=\"Distance\" style=\"display: none\"></div>\r\n" +
            "   <div id=\"DeveloperCorner\" class=\"Envelop\">\r\n" +
            "       <div id=\"DeveloperCorner_Content\" class=\"Content\" style=\"display: none\">\r\n" +
            "           <div class=\"Links\">\r\n" +
            "               <a href=\"#UnitTestLoader/TestMode\">Test mode</a>\r\n" +
            "                | <a id=\"ViewCodeLink\" href=\"#UnitTestLoader\">View HTML Code</a>\r\n" +
            "                | <a id=\"EchoOutputLink\" href=\"#UnitTestLoader\">Echo output</a>\r\n" +
            "                | <a id=\"CacheLink\" href=\"#UnitTestLoader\">Cache</a>\r\n" +
            "                | <a id=\"ForceRefreshLink\" href=\"#UnitTestLoader\">Force refresh</a>\r\n" +
            "                | <a id=\"BackToIndexLink\" href=\"#UnitTestLoader\">Back to index</a>\r\n" +
            "           </div>\r\n" +
            "           <div class=\"Info\">\r\n" +
            "               <div class=\"Viewer\">tTestLoader</div>\r\n" +
            "               <div class=\"Process\">" +
            "Page was generated in <span id=\"DeveloperCorner_ProcessTime\">*</span> seconds.</div>\r\n" +
            "           <div style=\"clear: both;\"></div>\r\n" +
            "           </div>\r\n" +
            "           <div id=\"DeveloperCorner_Body\" class=\"Debug\">\r\n" +
            "           <div id=\"DeveloperCorner_ViewCode\" class=\"ViewCode\"></div>\r\n" +
            "           <div id=\"DeveloperCorner_EchoOutput_Envelop\">\r\n" +
            "               <div id=\"DeveloperCorner_EchoOutput_Clear\" class=\"Link\">Clear output</div>\r\n" +
            "               <div id=\"DeveloperCorner_EchoOutput\" class=\"Echo\">Nothing has been printed by Echo yet.</div>\r\n" +
            "           </div>\r\n" +
            "           </div>\r\n" +
            "       </div>\r\n" +
            "   </div>\r\n" +
            "</div>");
        this.initSendBox();
    }

    public testMenu() : void {
        const drawGui : MockGuiObject = new MockGuiObject();
        assert.deepEqual(drawGui.testcontextMenu(),
            "<div guiType=\"DeveloperCorner\" class=\"DeveloperCorner\">\r\n" +
            "   <div id=\"DeveloperCorner_ContextMenu\" class=\"ContextMenu\">\r\n" +
            "       <div id=\"DeveloperCorner_ContextMenu_ViewCode\" class=\"Link\">View HTML Code</div>\r\n" +
            "       <div id=\"DeveloperCorner_ContextMenu_GenerateCache\" class=\"Link\">Generate Cache</div>\r\n" +
            "       <div id=\"DeveloperCorner_ContextMenu_BackToIndex\" class=\"Link\">Back to index</div>\r\n" +
            "   </div>\r\n</div>");
        this.initSendBox();
    }

    public testwuiBuilderContent() : void {
        const drawGui : MockGuiObject = new MockGuiObject();
        this.setUserAgent(window.navigator.userAgent + " com-wui-framework-plugin");
        const request = new HttpRequestParser();
        assert.equal(request.getUrlArgs().KeyExists(HttpRequestConstants.DESIGNER), false);
        assert.deepEqual(drawGui.testwuiBuilder(), "");
        this.initSendBox();
    }

    public testChildren() : void {
        const drawGui : MockGuiObject = new MockGuiObject();
        assert.equal(drawGui.testincludeChildren(), true);
    }

    public testrefresh() : void {
        const drawGui : MockGuiObject = new MockGuiObject();
        assert.equal(drawGui.testrefresh(), false);
    }

    public __Ignoretestresolver2() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const form : FormsObject = new MockFormsObject();
            const form2 : FormsObject = new MockFormsObject();
            const gui : GuiCommons = new MockGuiCommons();
            form.Parent(gui);
            form2.Parent(gui);
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            data.Add(form2, HttpRequestConstants.HIDE_CHILDREN);
            data.Add(form, HttpRequestConstants.ELEMENT_INSTANCE);

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form2);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);
            assert.equal(args.POST().getItem(HttpRequestConstants.ELEMENT_INSTANCE), form);

            const viewer : BaseViewer = <BaseViewer>args.Result();
            const args2 : ViewerManagerEventArgs = new ViewerManagerEventArgs();
            args2.Result(viewer);

            EventsManager.getInstanceSingleton().setEvent(ViewerManager.ClassName(), EventType.ON_COMPLETE,
                ($eventArgs : ViewerManagerEventArgs) : void => {
                    assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form2);
                    assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);
                    assert.equal(args.POST().getItem(HttpRequestConstants.ELEMENT_INSTANCE), form);
                    this.initSendBox();
                    $done();
                });
            const guiobjectdraw : DrawGuiObject = new DrawGuiObject();
            assert.equal(guiobjectdraw.ObjectClassName("Io.Oidis.Gui.Primitives.FormsObject"),
                "Io.Oidis.Gui.Primitives.FormsObject");
            guiobjectdraw.Process();
        };
    }
}
