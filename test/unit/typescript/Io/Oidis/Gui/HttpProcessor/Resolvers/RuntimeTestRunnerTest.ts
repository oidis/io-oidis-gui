/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { EventType } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Events/EventType.js";
import { GeneralEventOwner } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Enums/Events/GeneralEventOwner.js";
import { HttpRequestConstants } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Enums/HttpRequestConstants.js";
import { EventsManager } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Events/EventsManager.js";
import { AsyncDrawGuiObject } from "../../../../../../../../source/typescript/Io/Oidis/Gui/HttpProcessor/Resolvers/AsyncDrawGuiObject.js";
import { RuntimeTestRunner } from "../../../../../../../../source/typescript/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner.js";
import { FormsObject } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/FormsObject.js";
import { GuiCommons } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";
import {
    ImageTransformTest
} from "../../../../../../../../source/typescript/Io/Oidis/Gui/RuntimeTests/ImageProcessor/ImageTransformTest.js";
import { StaticPageContentManager } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Utils/StaticPageContentManager.js";

class MockFormsObject extends FormsObject {
}

class MockGuiCommons extends GuiCommons {
}

class MockRuntimeTestRunner extends RuntimeTestRunner {
}

class MockRuntimeTestRunnerResolve extends RuntimeTestRunner {

    public testData() : void {
        this.assertEquals("hello", "hello");
    }

    protected setUp() : void {
    }

    protected tearDown() : void {
    }
}

class MockRuntimeTestRunnerEquals extends RuntimeTestRunner {
    public testCreateLinkRun() : void {
        assert.equal(RuntimeTestRunner.CreateLink(ImageTransformTest),
            "#/com-wui-framework-builder/RuntimeTest/Io/Oidis/Gui/RuntimeTests/ImageProcessor/ImageTransformTest");
    }
}

class MockRequest extends RuntimeTestRunner {
    public testincludeChildren() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const form : FormsObject = new MockFormsObject();
            const gui : GuiCommons = new MockGuiCommons();
            form.Parent(gui);

            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            data.Add(form, HttpRequestConstants.HIDE_CHILDREN);

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);

            const guiobjectdraw : AsyncDrawGuiObject = new AsyncDrawGuiObject();
            guiobjectdraw.RequestArgs(args);
            assert.equal(guiobjectdraw.ObjectClassName("Io.Oidis.Gui.Primitives.GuiElement"),
                "Io.Oidis.Gui.Primitives.GuiElement");
            guiobjectdraw.Process();
            $done();
        };
    }

    public testCreateLinkThird() : void {
        this.assertEquals(this.createLink("/" + HttpRequestConstants.RUNTIME_TEST + "/testName"),
            "/com-wui-framework-commons/testName",
            "absolute link '/testName'");
    }
}

export class RuntimeTestRunnerTest extends UnitTestRunner {
    public __IgnoretestCreateLink() : void {
        assert.equal(RuntimeTestRunner.CreateLink(ImageTransformTest),
            "#/com-wui-framework-builder/RuntimeTest/Io/Oidis/Gui/RuntimeTests/ImageProcessor/ImageTransformTest");
    }

    public __IgnoretestCallbackLink() : void {
        assert.equal(RuntimeTestRunner.CallbackLink(),
            "#/com-wui-framework-builder/RuntimeTest/Io/Oidis/Gui/HttpProcessor/Resolvers/RuntimeTestRunner");
    }

    public __Ignoretestresolve() : void {
        assert.resolveEqual(MockRuntimeTestRunnerResolve, "" +
            "<head>\n" +
            "<title>WUI - Unit Test</title>\n\n" +
            "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
            "<link href=\"resource/graphics/icon.ico?v=*\" rel=\"shortcut icon\" type=\"text/css\">\n" +
            "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css?v=*\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
            "</head>\n\n" +
            "<body onfocus=\"Io.Oidis.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
            "onblur=\"Io.Oidis.Gui.Events.EventsManager.bodyBlurEventHandler();\" class=\"RuntimeTest\">\n" +
            "<div id=\"Browser\" class=\"FIREFOX\">\n" +
            "<div id=\"Language\" class=\"En\">\n" +
            "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n\n" +
            "<span guitype=\"HtmlAppender\"><h2>Runtime UNIT Test</h2><h2></h2></span><span guitype=\"HtmlAppender\">" +
            "<h3>Io.Oidis.Gui.HttpProcessor.Resolvers.RuntimeTestRunner - Data</h3></span>" +
            "<span guitype=\"HtmlAppender\">" +
            "<h4 id=\"RuntimeTestRunner_Assert_1\">" +
            "<i id=\"RuntimeTestRunner_Assert_Pass_1\" style=\"color: green;\">assert #1 has passed</i></h4>" +
            "</span><span guitype=\"HtmlAppender\"><hr></span><span guitype=\"HtmlAppender\"><br>" +
            "<span class=\"Result\">SUCCESS<br></span>Tests: 1, Assertions: 1, Failures: 0.</span>" +
            "<span guitype=\"HtmlAppender\"><br><br>Page was generated in * seconds.</span>" +
            "</div>\n" +
            "</div>\n" +
            "</div>\n\n" +
            "</body>");
    }

    public __Ignoretestresolve2() : void {
        assert.resolveEqual(MockRuntimeTestRunnerEquals, "" +
            "<head>\n" +
            "<title>WUI - Unit Test</title>\n\n" +
            "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
            "<link href=\"resource/graphics/icon.ico?v=*\" rel=\"shortcut icon\" type=\"text/css\">\n" +
            "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css?v=*\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
            "</head>\n\n" +
            "<body onfocus=\"Io.Oidis.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
            "onblur=\"Io.Oidis.Gui.Events.EventsManager.bodyBlurEventHandler();\" class=\"RuntimeTest\">\n" +
            "<div id=\"Browser\" class=\"FIREFOX\">\n" +
            "<div id=\"Language\" class=\"En\">\n" +
            "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n\n" +
            "<span guitype=\"HtmlAppender\">" +
            "<h2>Runtime UNIT Test</h2>" +
            "<h2></h2></span>" +
            "<span guitype=\"HtmlAppender\">" +
            "<h3>Io.Oidis.Gui.HttpProcessor.Resolvers.RuntimeTestRunner - CreateLinkRun</h3></span>" +
            "<span guitype=\"HtmlAppender\"><hr></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<span class=\"Result\">SUCCESS<br></span>" +
            "Tests: 1, Assertions: 0, Failures: 0.</span>" +
            "<span guitype=\"HtmlAppender\"><br><br>Page was generated in * seconds.</span></div>\n" +
            "</div>\n" +
            "</div>\n\n" +
            "</body>");
    }

    public __Ignoretestresolve3() : IUnitTestRunnerPromise {
        assert.resolveEqual(MockRequest, "" +
            "<head>\n" +
            "<title>WUI - Unit Test</title>\n\n" +
            "<meta content=\"text/html; charset=UTF-8\" http-equiv=\"Content-Type\">\n\n" +
            "<link href=\"resource/graphics/icon.ico?v=*\" rel=\"shortcut icon\" type=\"text/css\">\n" +
            "<link href=\"resource/css/com-wui-framework-builder-2-0-0.min.css?v=*\" rel=\"stylesheet\" type=\"text/css\">\n\n" +
            "</head>\n\n" +
            "<body " +
            "onfocus=\"Io.Oidis.Gui.Events.EventsManager.bodyFocusEventHandler();\" " +
            "onblur=\"Io.Oidis.Gui.Events.EventsManager.bodyBlurEventHandler();\" class=\"RuntimeTest\">\n" +
            "<div id=\"Browser\" class=\"FIREFOX\">\n" +
            "<div id=\"Language\" class=\"En\">\n" +
            "<div id=\"Content\" class=\"Content\" guitype=\"PageContent\">\n\n" +
            "<span guitype=\"HtmlAppender\"><h2>Runtime UNIT Test</h2><h2></h2></span>" +
            "<span guitype=\"HtmlAppender\">" +
            "<h3>Io.Oidis.Gui.HttpProcessor.Resolvers.RuntimeTestRunner - includeChildren</h3></span>" +
            "<span guitype=\"HtmlAppender\"><hr></span><span guitype=\"HtmlAppender\">" +
            "<h3>Io.Oidis.Gui.HttpProcessor.Resolvers.RuntimeTestRunner - CreateLinkThird</h3></span>" +
            "<span guitype=\"HtmlAppender\"><h4 id=\"RuntimeTestRunner_Assert_1\"> - absolute link '/testName':<br>" +
            "<i id=\"RuntimeTestRunner_Assert_Fail_1\" style=\"color: red;\">assert #1 has failed</i></h4></span>" +
            "<span guitype=\"HtmlAppender\"><br>" +
            "<u>actual:</u><br>/com-wui-framework-builder/RuntimeTest/testName</span><span guitype=\"HtmlAppender\"><br>" +
            "<u>expected:</u><br>/com-wui-framework-commons/testName</span><span guitype=\"HtmlAppender\"><hr>" +
            "</span><span guitype=\"HtmlAppender\"><br>" +
            "<span class=\"Result\">FAILURES!<br></span>Tests: 2, Assertions: 1, Failures: 1.</span>" +
            "<span guitype=\"HtmlAppender\"><br><br>" +
            "Page was generated in * seconds.</span>" +
            "<span guitype=\"HtmlAppender\">" +
            "<div id=\"RuntimeTestRunner_GoToNextFailing\" class=\"GoToNextFailing\">Go to next failing assert</div></span></div>\n" +
            "</div>\n" +
            "</div>\n" +
            "</body>"
        );
        this.registerElement("RuntimeTestRunner_GoToNextFailing");
        return ($done : any) : void => {
            setTimeout(() : void => {
                this.initSendBox();
                $done();
            }, 200);
        };
    }

    public __IgnoretestincludeChildren() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const form : FormsObject = new MockFormsObject();
            const gui : GuiCommons = new MockGuiCommons();
            form.Parent(gui);

            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            data.Add(form, HttpRequestConstants.HIDE_CHILDREN);
            data.Add("#/#/RuntimeTest/Io/Oidis/Gui/RuntimeTests/ImageProcessor/ImageTransformTest",
                HttpRequestConstants.RUNTIME_TEST);
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.RUNTIME_TEST), true);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);

            const runner : RuntimeTestRunner = new MockRuntimeTestRunner();
            runner.Process();
            EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.BODY, EventType.ON_START, false);
            $done();
        };
    }

    protected before() : void {
        StaticPageContentManager.Clear(true);
    }

    protected tearDown() : void {
        this.initSendBox();
        document.documentElement.innerHTML = "";
        this.registerElement("Content");
    }
}
