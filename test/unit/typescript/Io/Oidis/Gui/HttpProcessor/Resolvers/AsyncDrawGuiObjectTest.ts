/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { AsyncRequestEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/AsyncRequestEventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { HttpRequestConstants } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Enums/HttpRequestConstants.js";
import { AsyncDrawGuiObject } from "../../../../../../../../source/typescript/Io/Oidis/Gui/HttpProcessor/Resolvers/AsyncDrawGuiObject.js";
import { FormsObject } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/FormsObject.js";
import { GuiCommons } from "../../../../../../../../source/typescript/Io/Oidis/Gui/Primitives/GuiCommons.js";

class MockFormsObject extends FormsObject {
}

class MockGuiCommons extends GuiCommons {
}

class Mockresolver extends AsyncDrawGuiObject {
    protected resolver() : void {
        Echo.Print("MockResolver");
        this.registerResolver("test", "resolver");
    }
}

export class AsyncDrawGuiObjectTest extends UnitTestRunner {

    public testObjectClassName() : void {
        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs("/test/url/for/asyncdraw");
        const asyncguiobject : AsyncDrawGuiObject = new AsyncDrawGuiObject();
        asyncguiobject.RequestArgs(args);
        delete (<any>AsyncDrawGuiObject).objectName;
        assert.equal(asyncguiobject.ObjectClassName(null), ".url.for.asyncdraw");
        delete (<any>AsyncDrawGuiObject).objectName;
        assert.equal(asyncguiobject.ObjectClassName("BaseGuiObject"), "BaseGuiObject");
        (<any>AsyncDrawGuiObject).objectName = "..localhost:8888.UnitTestEnvironment.js.";
        assert.equal(asyncguiobject.ObjectClassName(), "BaseGuiObject");

        const asyncguiobject2 : AsyncDrawGuiObject = new AsyncDrawGuiObject();
        asyncguiobject2.RequestArgs(new AsyncRequestEventArgs(
            "async/Io/Oidis/Gui/HttpProcessor/Resolvers/AsyncDrawGuiObject/TestMode/ForceRefresh"));
        assert.equal(asyncguiobject2.ObjectClassName(""), AsyncDrawGuiObject.ClassName());
    }

    public testObjectClassName20() : void {
        const asyncguiobject2 : AsyncDrawGuiObject = new AsyncDrawGuiObject();
        const data : ArrayList<any> = new ArrayList<any>();
        data.Add(true, HttpRequestConstants.TEST_MODE);

        const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
        assert.equal(args.POST().KeyExists(HttpRequestConstants.TEST_MODE), true);
        assert.equal(asyncguiobject2.ObjectClassName(HttpRequestConstants.TEST_MODE), "TestMode");
        this.initSendBox();
    }

    public testObjectClassName300() : void {
        const asyncguiobject2 : AsyncDrawGuiObject = new AsyncDrawGuiObject();
        assert.equal(asyncguiobject2.ObjectClassName(HttpRequestConstants.TEST_MODE), "TestMode");
    }

    public testresolver() : void {
        assert.resolveEqual(Mockresolver,
            "<head></head><body><div id=\"Content\"><span guitype=\"HtmlAppender\">MockResolver</span></div></body>");
        this.initSendBox();
    }

    public testgetChildElement() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const form : FormsObject = new MockFormsObject();
            const gui : GuiCommons = new MockGuiCommons();
            form.Parent(gui);
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            data.Add(form, HttpRequestConstants.ELEMENT_INSTANCE);

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.POST().getItem(HttpRequestConstants.ELEMENT_INSTANCE), form);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.ELEMENT_INSTANCE), true);

            const guiobjectdraw : AsyncDrawGuiObject = new AsyncDrawGuiObject();
            guiobjectdraw.RequestArgs(args);
            assert.equal(guiobjectdraw.ObjectClassName("Io.Oidis.Gui.Primitives.GuiCommons"),
                "Io.Oidis.Gui.Primitives.GuiCommons");
            guiobjectdraw.Process();
            this.initSendBox();
            $done();
        };
    }

    public testincludeChildren() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const form : FormsObject = new MockFormsObject();
            const gui : GuiCommons = new MockGuiCommons();
            form.Parent(gui);

            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            data.Add(form, HttpRequestConstants.HIDE_CHILDREN);

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);

            const guiobjectdraw : AsyncDrawGuiObject = new AsyncDrawGuiObject();
            guiobjectdraw.RequestArgs(args);
            assert.equal(guiobjectdraw.ObjectClassName("Io.Oidis.Gui.Primitives.GuiElement"),
                "Io.Oidis.Gui.Primitives.GuiElement");
            guiobjectdraw.Process();
            this.initSendBox();
            $done();
        };
    }

    public testincludeChildrenElse() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const form : FormsObject = new MockFormsObject();
            const gui : GuiCommons = new MockGuiCommons();
            form.Parent(gui);
            (<any>AsyncDrawGuiObject).includeChildrenEnabled = true;
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            data.Add(form, HttpRequestConstants.HIDE_CHILDREN);

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);

            const guiobjectdraw : AsyncDrawGuiObject = new AsyncDrawGuiObject();
            guiobjectdraw.RequestArgs(args);
            assert.equal(guiobjectdraw.ObjectClassName("Io.Oidis.Gui.Primitives.GuiElement"),
                "Io.Oidis.Gui.Primitives.GuiElement");
            guiobjectdraw.Process();
            this.initSendBox();
            $done();
        };
    }

    public testresolver2() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const form : FormsObject = new MockFormsObject();
            const form2 : FormsObject = new MockFormsObject();
            const gui : GuiCommons = new MockGuiCommons();
            form.Parent(gui);
            form2.Parent(gui);
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            data.Add(form2, HttpRequestConstants.HIDE_CHILDREN);
            data.Add(form, HttpRequestConstants.ELEMENT_INSTANCE);
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            assert.equal(args.POST().getItem(HttpRequestConstants.HIDE_CHILDREN), form2);
            assert.equal(args.POST().KeyExists(HttpRequestConstants.HIDE_CHILDREN), true);
            assert.equal(args.POST().getItem(HttpRequestConstants.ELEMENT_INSTANCE), form);
            this.initSendBox();
            $done();
        };
    }
}
