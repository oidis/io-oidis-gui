/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpResolver } from "../../../../../../../source/typescript/Io/Oidis/Gui/HttpProcessor/HttpResolver.js";
import { UnitTestRunner } from "../UnitTestRunner.js";

export class HttpResolverTest extends UnitTestRunner {
    public testConstructor() : void {
        const resolver1 : HttpResolver = new HttpResolver();
        const resolver2 : HttpResolver = new HttpResolver("http://localhost:8080/myapp");
        this.initSendBox();
    }
}
