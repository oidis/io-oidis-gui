/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { HttpRequestParser } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpRequestParser.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { HttpManager } from "../../../../../../../source/typescript/Io/Oidis/Gui/HttpProcessor/HttpManager.js";

export class HttpManagerTest extends UnitTestRunner {
    public testConstructor() : void {
        const parser : HttpRequestParser = new HttpRequestParser("example.com");
        const manager : HttpManager = new HttpManager(parser);
        assert.equal(manager.HttpPatternExists("/PersistenceManager"), true);
        assert.equal(manager.HttpPatternExists("/test/not/exists/Pattern"), false);
    }
}
