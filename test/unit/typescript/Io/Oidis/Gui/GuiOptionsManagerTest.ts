/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2022 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "./UnitTestRunner.js";

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { GuiOptionType } from "../../../../../../source/typescript/Io/Oidis/Gui/Enums/GuiOptionType.js";
import { GuiOptionsManager } from "../../../../../../source/typescript/Io/Oidis/Gui/GuiOptionsManager.js";

export class GuiOptionsManagerTest extends UnitTestRunner {

    public testConstructor() : void {
        assert.doesNotThrow(() : void => {
            const manager : GuiOptionsManager = new GuiOptionsManager(new ArrayList<GuiOptionType>());
        });
        assert.doesNotThrow(() : void => {
            const availableOptionsList : ArrayList<GuiOptionType> = new ArrayList<GuiOptionType>();
            availableOptionsList.Add(GuiOptionType.ACTIVED);
            const manager : GuiOptionsManager = new GuiOptionsManager(availableOptionsList);
        });
    }

    public testAdd() : void {
        const availableOptionsList : ArrayList<GuiOptionType> = new ArrayList<GuiOptionType>();
        let manager : GuiOptionsManager = new GuiOptionsManager(availableOptionsList);
        assert.throws(() : void => {
            manager.Add(GuiOptionType.ACTIVED);
            manager.Add(GuiOptionType.SCROLLABLE);
        }, /No available gui option has been set to current GUI object./gm);

        availableOptionsList.Add(GuiOptionType.ACTIVED);
        manager = new GuiOptionsManager(availableOptionsList);
        assert.doesNotThrow(() : void => {
            manager.Add(GuiOptionType.ACTIVED);
        });
        assert.throws(() : void => {
            manager.Add(GuiOptionType.SCROLLABLE);
        }, /Option 'SCROLLABLE' is not allowed on this object./);

        manager.Add(GuiOptionType.ALL);
    }

    public testContains() : void {
        const availableOptionsList : ArrayList<GuiOptionType> = new ArrayList<GuiOptionType>();
        availableOptionsList.Add(GuiOptionType.ACTIVED);
        const manager : GuiOptionsManager = new GuiOptionsManager(availableOptionsList);
        manager.Add(GuiOptionType.ACTIVED);
        assert.equal(manager.Contains(GuiOptionType.ACTIVED), true);
        assert.equal(manager.Contains(GuiOptionType.SCROLLABLE), false);
    }

    public testAddSecond() : void {
        const availableOptionsList2 : ArrayList<GuiOptionType> = new ArrayList<GuiOptionType>();
        const manager2 : GuiOptionsManager = new GuiOptionsManager(availableOptionsList2);
        assert.throws(() : void => {
            manager2.Add(null);
            manager2.Add(null);
            throw new Error("GuiOptionType is null");
        }, /GuiOptionType is null/);
    }

    public testAddThird() : void {
        const availableOptionsList3 : ArrayList<GuiOptionType> = new ArrayList<GuiOptionType>();
        assert.doesNotThrow(() : void => {
            availableOptionsList3.Add(GuiOptionType.ACTIVED);
            const manager3 : GuiOptionsManager = new GuiOptionsManager(availableOptionsList3);
            manager3.Add(GuiOptionType.ACTIVED);
            assert.equal(manager3.Contains(GuiOptionType.ACTIVED), true);
        });
    }
}
