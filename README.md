# io-oidis-gui

> Oidis Framework base GUI library

## Requirements

This library does not have any special requirements, but it depends on the [Oidis Builder](https://gitlab.com/oidis/io-oidis-builder). 
See the Builder requirements, before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically-generated documentation in the [TypeDoc](http://typedoc.org/) from TypeScript source by running the 
`oidis docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2022.2.0
Updated dependency version.
### v2022.1.1
Updated dependency version.
### v2022.1.0
Several minor fixes and enhancements.
### v2022.0.0
Deep integration of Bootstrap.
### v2021.2.0
Integration of novel logger. Updated support for IntelliJ indexing.
### v2021.0.0
Release to baseline changes in dependencies.
### v2020.3.0
Added support for audio media type. 
### v2020.1.0
Correct static HTML output is statically provided.
### v2020.0.0
Identity update. Change of configuration files format.
### v2019.3.0
Refactoring of namespaces. Migration to gitlab. Initial update of identity for project fork. 
Creation and usage of animation engine. Rendering engine performance enhancements. Added support for media streams.
### v2019.1.0
Responsive design enhancements. Fixed performance issue with large count of text inputs. Added static initial CSS for gui selector.
### v2019.0.2
Responsive design bug fixes and performance update. Usage of new app loader.
### v2019.0.1
Fixed cross-dependency issues. Removed Alpha and Beta checks. Project settings and tests update.
### v2019.0.0
Added CursorType enum. Fixed execution of tests. Updated get offset API.
### v2018.3.0
Bug fixes required by integration into new WUI Builder. Enhancements in GUI performance. Tests clean up.
### v2018.2.0
Added more helper methods for ViewerRuntimeTests and simple events emulator. 
Performance update of rendering engine based on better DOM cache mainly focused on responsive design needs. Convert of docs to TypeDoc.
### v2018.1.3
Added new test runner suitable for functional tests of GUI.
### v2018.1.2
Update of PropagableNumber API. Tests clean up.
### v2018.1.1
Fixed tests connected with updated core. API clean up. Added better handling of scroll propagation.
### v2018.1.0
Update of WUI Core for ability to support multithreading model. API clean up. Fixed check for UI Id duplicity. Fixed panel scroll handling.
### v2018.0.4
Fixed bugs connected with calculation of ScrollBar offset and top most position of GUI group.
### v2018.0.3
Updated CSS manipulation broken by new secure police in Chromium. Integrated new scrollbar animation to BasePanel. Fixed events propagation.
### v2018.0.2
Added ability to handle persistent static page headers. Bug fixes for scrolling and on complete events. 
Added utility for user text selection.
### v2018.0.1
Dependencies update. Fixed linting issues.
### v2018.0.0
RuntimeTestRunner enhancements based on consumption of status events. More robust StaticPageContentManager. Increase of code coverage. 
Change version format.
### v2.2.1
Simplified logging for service layer. Mouse distance is acquired from screen position now. 
Refactoring of color management methods in SASS. Code coverage increase.
### v2.2.0
Bug fixes for responsive design. Increase of code coverage. Update of SCR and change of history ordering.
### v2.1.1
Bug fix for override of ElementEventsManager class at GuiCommons scope. Added ability to disable usage of HTML5 Canvas.
### v2.1.0
Usage of new UnitTestRunner with better sandboxing. Update of Commons library.
### v2.0.3
Usage of UnitTestRunner PromiseAPI. Tests bug fixes. Added fixed version for dependencies.
### v2.0.2
Update of unit tests required for compatibility with jsdom.
### v2.0.1
Increase of code coverage. Update of TypeScript syntax. Enhancements in test environment.
### v2.0.0
Namespaces refactoring.
### v1.1.0
Changed the licence from proprietary to BSD-3-Clause.
### v1.0.7
Usage of new version compiler features—mainly protected, lambda expression, and multi-type arguments.
### v1.0.6
Added static cache.
### v1.0.5
Usage of a stand-alone builder.
### v1.0.4
Package renamed from com-freescale-mvc-gui to com-freescale-wui-gui and minor bug fixes.
### v1.0.3
Enhancements to support the extended GUI libraries.
### v1.0.2
Implemented asynchronous processing of all GUI objects and business logic.
### v1.0.1
Implemented all GUI utils and base classes needed for the extension to the UserControls and Interfaces libraries.
### v1.0.0
Initial release.

## License

This software is owned or controlled by Oidis. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Copyright 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright 2017-2019 [NXP](http://nxp.com/),
Copyright 2019-2025 [Oidis](https://www.oidis.io/)
